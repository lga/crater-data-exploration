# CRATER-DATA-EXPLORATION

Ce projet a pour but de faire de l'exploration de données dans le cadre du projet CRATer

<<<<<<< HEAD
## Mise en place de l'environnement de developpement

### Pré-requis (Linux)

1/ configurer pyenv et python 3.8.8

Il est recommandé d'utiliser la version python 3.8 qui est celle qui tourne sur les pipeline gitlab. Pour cela, installer [pyenv](https://github.com/pyenv) qui permet de gérer facilement plusieurs versions
python.

pyenv permet d'installer python 3.8 et de pointer par défaut sur cette version depuis le projet :

```
pyenv install 3.8.8
pyenv local 3.8.8
```

Cela a pour effet de générer le .python-version (Rq : ce fichier est déjà présent dans le repo git) 

2/ Installer pipenv

A la racine du projet

```
# pour vérifier que la version de python utilisée est bien la 3.8.8 installée par pyenv
python --version 
# Installer pipenv
pip install pipenv
```

### Installation des dépendances Python

A la racine du projet :
-`pipenv install` pour installer les dépendances dans l'environnement virtuel que pipenv a créé pour ce projet

- `pipenv lock` pour (re)construire le fichier Pipfile.lock


## Utiliser l'environnement virtuel

Se mettre à la racine du projet :

```
# To activate this project's virtualenv : 
pipenv shell
# Alternatively, run a command inside the virtualenv : 
pipenv run
```
=======
Le code est en R ou python (par ex notebooks)
>>>>>>> Update Readme.md
