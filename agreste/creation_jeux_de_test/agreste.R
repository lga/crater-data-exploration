library(data.table)

setwd("~/crater")

#creation des jeux de test

for (numero_fichier in c('2003', '2004', '2005')) {
  fichiers = c()
  for (annee in c('2010', '1988')) {
    fichier = paste0("FDS_G_",numero_fichier,"_",annee,".txt")
    jeu_test = fread(unzip(paste0("crater-data-sources/agreste/1970-2010/FDS_G_",numero_fichier,".zip"), files = fichier), sep = ";", stringsAsFactors = F)
    
    jeu_test = jeu_test[(FRDOM == 'METRO' & REGION == '............') | COM %in% c('32145','40001')]
    
    fwrite(jeu_test, fichier, sep = ";")
    
    fichiers = c(fichiers, fichier)
  }
  zip(paste0("FDS_G_",numero_fichier,".zip"), fichiers)
}