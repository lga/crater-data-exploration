library(data.table)

setwd("~/crater/crater-data-exploration/")

resultats_crater = fread("../crater-data-resultats/data/crater/population_agricole/population_agricole.csv")

agreste2005 = fread(unzip("../crater-data-sources/agreste/1970-2010/FDS_G_2005.zip", files = "FDS_G_2005_2010.txt"), sep = ";", stringsAsFactors = F)

setnames(agreste2005,'G_2005_LIB_DIM1', 'taille_exploitation')
setnames(agreste2005,'G_2005_LIB_DIM2', 'lien_avec_exploitation')
setnames(agreste2005,'G_2005_LIB_DIM3', 'indicateur')
         
pop_agreste = agreste2005[
  taille_exploitation  == 'Ensemble des exploitations (hors pacages collectifs)' &
  lien_avec_exploitation  == 'Ensemble' &
  indicateur == 'Nombre de personnes'
  ]

commune = '29153'
pop_agreste[COM == commune]$VALEUR
resultats_crater[id_territoire == paste0("C-", commune)]$actifs_agricoles_permanents_2010

departement = '29'
pop_agreste[DEP == departement & COM == '............']$VALEUR
pop_agreste[DEP == departement & COM != '............', sum(VALEUR, na.rm=T)]
resultats_crater[id_territoire == paste0("D-", departement)]$actifs_agricoles_permanents_2010

region = '53'
pop_agreste[REGION == paste0('NR',region) & DEP == '............']$VALEUR
pop_agreste[REGION == paste0('NR',region) & COM != '............', sum(VALEUR, na.rm=T)]
resultats_crater[id_territoire == paste0("R-", region)]$actifs_agricoles_permanents_2010

#differences somme communes agreste vs resultats crater du 29
resultats_crater[substr(id_territoire, 3, 4)  == departement & is.na(actifs_agricoles_permanents_2010)]
pop_agreste[COM == "29003"] #AUDIERNE
pop_agreste[COM == "29052"] #AUDIERNE

