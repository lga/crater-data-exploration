message("\n => Eau")

# Usage agriculture
usagesIrrigation = c("Irrigation gravitaire","IRRIGATION","Lutte antigel de cultures pérennes")
usagesAgriHorsIrrigation= c("AGRICULTURE-ELEVAGE (hors irrigation)","Abreuvage")

#Chargement des données
eau = fread(fichier_eau, sep = ",", stringsAsFactors = F)
setnames(eau,"Département","DEP")
setnames(eau,"Code INSEE","CODE_INSEE")

#retrait des lignes problématiques
eau = eau[`Code de l'ouvrage` != "Code de l'ouvrage"]

#conversion volumes
eau[,vol_m3 := as.numeric(`Volume (m3)`)]


#regroupement des usages liés à l'agriculture
eau[,UsageAgreg := Usage]
eau[UsageAgreg %in% usagesIrrigation, UsageAgreg := "Irrigation"]
eau[UsageAgreg %in% usagesAgriHorsIrrigation, UsageAgreg := "Agri Hors Irrigation" ]

#agregation par commune
eau_communes = eau[UsageAgreg %in% c("Irrigation","Agri Hors Irrigation"), lapply(.SD, sum, na.rm=TRUE), .SDcols = c("vol_m3"), by=c("CODE_INSEE")]

#verif calculs
eau[, lapply(.SD, sum, na.rm=TRUE), .SDcols = c("vol_m3"), by=c("Type d'eau")]
eau[, lapply(.SD, sum, na.rm=TRUE), .SDcols = c("vol_m3"), by=c("UsageAgreg")]

if(sum(eau[UsageAgreg == "Irrigation"]$vol_m3) != 2960635039)
  stop("Probleme volume total irrigation !!")
if(sum(eau[UsageAgreg == "Agri Hors Irrigation"]$vol_m3) != 633449)
  stop("Probleme volume total agri hors irrigation !!")
if(sum(eau_communes$vol_m3) != 2960635039 + 633449)
  stop("Probleme volume total agri !!")

#jointure
communes[eau_communes, on = .(CODGEO = CODE_INSEE), ':='(eau_m3 = i.vol_m3)]

#vérif
sum(eau_communes$vol_m3, na.rm=T)
sum(communes$eau_m3, na.rm=T)

eau_communes[,present := F]
eau_communes[communes, on = .(CODE_INSEE = CODGEO), ':='(present = T)]
eau_communes[present == F & CODE_INSEE < 97000]

eau[CODE_INSEE == "75056"]
eau[CODE_INSEE == "75110"]
