message("\n => Géographie")

# Chargement des communes ------------------------------------------------------------------
communes = read.xlsx(xlsxFile = fichier_communes,
                             sheet = 1,
                             startRow = 6)
setDT(communes)

arrondissements = read.xlsx(xlsxFile = fichier_arrondissements,
                     sheet = 2,
                     startRow = 6)
setDT(arrondissements)

message("Nombre de communes:")
message("NB : communes
 01/01/2020 : 34 968
 01/01/2019 : 34 970
 01/01/2018 : 35 357
 01/01/2017 : 35 416
 01/01/2016 : 35 885
 01/01/2015 : 36 658
 01/01/2014 : 36 681
 01/01/2013 : 36 681
 01/01/2012 : 36 700
 01/01/2011 : 36 680
 01/01/2010 : 36 682
 ")
print(nrow(communes))

# on ajoute les noms de département et de région
departements = fread(fichier_departements, sep = ",", stringsAsFactors = F)
regions = fread(fichier_regions, sep = ",", stringsAsFactors = F)
setnames(departements,c("dep","reg"),c("DEP","REG"))
departements$DEP = as.character(departements$DEP)
departements$REG = as.character(departements$REG)
setnames(regions,c("reg"),c("REG"))
regions$REG = as.character(regions$REG)

departements[,REG := ifelse(REG<10,paste0("0",REG),REG)]
regions[,REG := ifelse(REG<10,paste0("0",REG),REG)]

communes[departements, on=.(DEP), LIBDEP := i.libelle]
communes[regions, on=.(REG), LIBREG := i.libelle]

#on ne garde que les communes, départements et régions métropolitains
communes = communes[REG > 10]
departements = departements[REG > 10]
regions = regions[REG > 10]

# on garde les colonnes utiles
communes = communes[,c("CODGEO","LIBGEO","DEP","LIBDEP","REG","LIBREG","EPCI","NATURE_EPCI")]
departements = departements[,-c("tncc","nccenr")]
regions = regions[,-c("tncc","nccenr")]

#on ajoute les infos de mouvements de communes
communes[,mvt := F]
fusions_communes = read.xlsx(xlsxFile = fichier_mvts_communes,
                             sheet = 2,
                             startRow = 6)
scissions_communes = read.xlsx(xlsxFile = fichier_mvts_communes,
                             sheet = 3,
                             startRow = 6)

setDT(fusions_communes)
setDT(scissions_communes)
fusions_communes[,annee_modif := as.integer(annee_modif)]
scissions_communes[,annee_modif := as.integer(annee_modif)]
fusions_communes[,':='(com=COM_INI,com_lib=LIB_COM_INI,type="fusion")]
scissions_communes[,':='(com=COM_FIN,com_lib=LIB_COM_FIN,type="scission")]

mvts_communes = rbind(fusions_communes, scissions_communes)
mvts_communes = mvts_communes[order(com,annee_modif)]
mvts_communes[,N:=.N,.(com)]
mvts_communes[ , id := .GRP, by = .(com)]

communes[scissions_communes, on = c("CODGEO" = "COM_INI"), ':='(mvt = T)]
communes[scissions_communes, on = c("CODGEO" = "COM_FIN"), ':='(mvt = T)]
communes[fusions_communes, on = c("CODGEO" = "COM_INI"), ':='(mvt = T)]
communes[fusions_communes, on = c("CODGEO" = "COM_FIN"), ':='(mvt = T)]

message("Nombre de communes ayant bougé:")
print(communes[, .(N=.N,r=.N/nrow(communes)*100), by = .(mvt)])

#vérifs
communes[is.na(LIBDEP) | is.na(LIBREG)]

#tests
if(nrow(communes) != 34968 - 129)  #commmunes - DOM au 1e janvier 2020
  warning("Nombre de communes incorrect !!")


#Chargement des EPCI ------------------------------------------------------------------
epcis = read.xlsx(xlsxFile = fichier_epcis,
                 sheet = 1,
                 startRow = 6)
setDT(epcis)

#jointure
communes[epcis, on = "EPCI", ':='(LIBEPCI = i.LIBEPCI)]

#vérifs
communes[is.na(LIBEPCI)]

nrow(epcis)
nrow(unique(communes[,.(EPCI)]))

epcis[, present := FALSE]
epcis[communes, on = "EPCI", ':='(present = TRUE)]
epcis[present == F]

#tests
if(nrow(epcis) != 1256)  #1255 EPCI au 1e janvier 2020 + 1 EPCI ZZ des communes qui n'ont n'en pas
  warning("Nombre d'EPCI incorrect !!")
if(nrow(communes[NATURE_EPCI=="ZZ"]) != 4)  #4 communes sans EPCI au 1e janvier 2020
  warning("Nombre de communes sans EPCI incorrect !!")

#on ne garde que les communes, départements et régions métropolitains
epcis = epcis[present == T]

#Chargement des codes postaux ------------------------------------------------------------------
codes_postaux = fread(fichier_codes_postaux, sep = ";", stringsAsFactors = F)

#correction communes arrondissements
codes_postaux[,CODGEO := as.character(Code_commune_INSEE)]
codes_postaux[arrondissements, on = .(CODGEO), ':='(CODGEO = i.COM)]

#concatenation par code commune
codes_postaux = unique(codes_postaux[,.(CODGEO,Code_postal)])[order(CODGEO,Code_postal)]
codes_postaux[, Code_postal := ifelse(Code_postal < 10000, paste0("0", Code_postal), Code_postal)]
codes_postaux = codes_postaux[, lapply(.SD, paste0, collapse="|"), by = CODGEO]

#jointure
communes[codes_postaux, on = .(CODGEO), ':='(codes_postaux = i.Code_postal)]

#vérifs
communes[is.na(codes_postaux)]




