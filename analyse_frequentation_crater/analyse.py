import pandas as pd
import plotly.express as px

from crater.reglages import (
    CHEMIN_OUTPUT_DATA,
    DOSSIER_TERRITOIRES
)

def _log_vers_df(nom_fichier):
    dates = []
    communes = []
    with open(nom_fichier) as f:
        for ligne in f:
            liste = ligne.split(' ')
            if "crater/api/communes/" in liste[6]:
                dates.append(liste[3].split(':')[0].replace("[", ""))
                communes.append(liste[6].split('/')[-1])
    df = pd.DataFrame({'date': dates, 'id_commune': communes})
    df['date'] = pd.to_datetime(df["date"], infer_datetime_format=True)
    return df


if __name__ == '__main__':

    communes = pd.read_csv("../" / CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES / "communes.csv", sep=";")
    departements = pd.read_csv("../" / CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES / "departements.csv", sep=";").rename(
        columns={'code_insee': 'code_insee_departement', 'nom': 'nom_departement'})
    regions = pd.read_csv("../" / CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES / "regions.csv", sep=";").rename(
        columns={'code_insee': 'code_insee_region', 'nom': 'nom_region'})
    communes = pd.merge(communes, departements[['code_insee_departement', 'nom_departement']],
                        on='code_insee_departement', how='left')
    communes = pd.merge(communes, regions[['code_insee_region', 'nom_region']],
                        on='code_insee_region', how='left')

    df = _log_vers_df("logs/crater.resiliencealimentaire.org-ssl_log-Dec-2020")
    occurencesCommunes = df.groupby("id_commune").count().reset_index()
    occurencesCommunes = pd.merge(occurencesCommunes, communes[['code_insee', 'nom_departement', 'nom_region']], left_on='id_commune', right_on='code_insee', how='left')
    occurencesCommunes = occurencesCommunes[occurencesCommunes['code_insee'].notnull()].sort_values(by=['nom_region', 'nom_departement'])
    print(occurencesCommunes)

    occurencesDepartements = occurencesCommunes.groupby(["nom_departement", "nom_region"]).sum().reset_index().sort_values(by=['nom_region', 'nom_departement'])
    print(occurencesDepartements)

    occurencesRegions = occurencesCommunes.groupby("nom_region").sum().reset_index()
    print(occurencesRegions)

    fig = px.bar(occurencesDepartements, x="nom_departement", y="date", color='nom_region')
    fig.show('chrome')

