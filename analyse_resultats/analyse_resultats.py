import pandas as pd
import plotly.express as px

from crater.reglages import (
    CHEMIN_OUTPUT_DATA,
    DOSSIER_TERRITOIRES,
    DOSSIER_POPULATION_AGRICOLE, 
    DOSSIER_POLITIQUE_FONCIERE, 
    DOSSIER_PRATIQUES_AGRICOLES
)

if __name__ == '__main__':

    df = pd.read_csv(CHEMIN_OUTPUT_DATA/DOSSIER_POPULATION_AGRICOLE/"synthese.csv", sep=";")
    fig = px.histogram(df.query('categorie_territoire == "DEPARTEMENT"'), x="synthese_note")
    fig.show('chrome')

