import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np

from crater.reglages import (
    CHEMIN_OUTPUT_DATA,
    DOSSIER_TERRITOIRES,
    DOSSIER_POPULATION_AGRICOLE, DOSSIER_POLITIQUE_FONCIERE, DOSSIER_PRATIQUES_AGRICOLES
)

df = pd.read_csv(CHEMIN_OUTPUT_DATA/DOSSIER_POLITIQUE_FONCIERE/"synthese.csv", sep=";")
echelle_options = ['COMMUNE', 'EPCI', 'DEPARTEMENT', 'REGION', 'PAYS']
variable_options = df.columns[3:]

app = dash.Dash()

app.layout = html.Div([
    html.H2("Analyse de la distribution"),
    html.Div(
        [
            dcc.Dropdown(
                id="Variable",
                options=[{
                    'label': i,
                    'value': i
                } for i in variable_options],
                value=variable_options[0]),
            dcc.Dropdown(
                id="Echelle",
                options=[{
                    'label': i,
                    'value': i
                } for i in echelle_options],
                value=echelle_options[0])
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='Histogramme'),
    dcc.Textarea(
        id='Texte',
        value='',
        style={'width': '100%', 'height': 100},
    )
])


@app.callback(
    [dash.dependencies.Output('Histogramme', 'figure'),
     dash.dependencies.Output('Texte', 'value')],
    [dash.dependencies.Input('Variable', 'value'),
     dash.dependencies.Input('Echelle', 'value')]
)
def update_graph(variable, echelle):
    df2 = df.query('categorie_territoire == @echelle')
    print(df2[['id_territoire', variable]].groupby(variable).count().reset_index())
    return [px.histogram(df2, x=variable), "Q1 quantile : " + str(np.nanquantile(df2[variable], .20)) + "\n" + \
    "Q2 quantile : " + str(np.nanquantile(df2[variable], .25)) + "\n" + \
    "Q3 quantile : " + str(np.nanquantile(df2[variable], .50)) + "\n" + \
    "Q4 quantile : " + str(np.nanquantile(df2[variable], .75)) + "\n" + \
    "Q5 quantile : " + str(np.nanquantile(df2[variable], 1)) + "\n"]

if __name__ == '__main__':
    app.run_server(debug=True)

