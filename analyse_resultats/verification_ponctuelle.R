## verif sau ha RPG - Métropole de Nice

library(data.table)
library(sf)

rm(list = ls())
setwd("/Users/Benjamin/Desktop")

rpg = fread("./crater/crater-data-sources/rpg/2017/sau_rpg_communes_2017.csv")
communes = fread("./crater/crater-data-resultats/territoires/communes.csv")
setnames(communes, "code_insee","id_commune")
setnames(communes, "code_insee_epci","id_epci")
communes$code_insee = substr(communes$id_commune, 3,7)
epcis = fread("./crater/crater-data-resultats/territoires/epcis.csv")
setnames(epcis, "code_insee","id_epci")

rpg[communes, on = .(id_commune), id_epci := i.id_epci]
rpg[epcis, on = .(id_epci), nom_epci := i.nom]

rpg[grep("Nice", nom_epci)][,sum(sau_ha), by=.(id_commune)]
rpg[grep("Nice", nom_epci)][,sum(sau_ha), by=.(code_culture_rpg )]
nrow(communes[id_epci == "E-200030195"])
fwrite(rpg[grep("Nice", nom_epci)][,sum(sau_ha), by=.(code_culture_rpg)], "rpg.csv")

#chargement donnees AGRESTE 1013 (Cultures par commune)
zz <- unzip("/Users/Benjamin/Downloads/FDS_G_1013.zip", files = "FDS_G_1013_2010.txt")
f_1013 = fread(zz, sep = ";", stringsAsFactors = F)
f_1013 = f_1013[COM != "............" &
  G_1013_LIB_DIM1  == "Ensemble des exploitations (hors pacages collectifs)" &
  G_1013_LIB_DIM4 == "Superficie correspondante (hectares)"]
f_1013[communes, on = .(COM = code_insee), id_epci := i.id_epci]
sum(f_1013[
      G_1013_LIB_DIM2  == "Superficie agricole utilisée (1)" &
        id_epci == "E-200030195"
    ]$VALEUR, na.rm=T)
fwrite(f_1013[id_epci == "E-200030195", sum(VALEUR, na.rm=T), by=.(G_1013_LIB_DIM2)], "f1013.csv")

#chargement donnees AGRESTE 2003 (Superficie agricole utilisée (SAU) des exploitations)
zz <- unzip("/Users/Benjamin/Downloads/FDS_G_2003.zip", files = "FDS_G_2003_2010.txt")
f_2003 = fread(zz, sep = ";", stringsAsFactors = F)
f_2003 = f_2003[COM != "............" &
  G_2003_LIB_DIM1  == "Ensemble des exploitations (hors pacages collectifs)" &
  G_2003_LIB_DIM2  == "Ensemble" &
  G_2003_LIB_DIM3 == "Superficie agricole utilisée (ha)"]
f_2003[communes, on = .(COM = code_insee), id_epci := i.id_epci]
write(f_2003[id_epci == "E-200030195", sum(VALEUR, na.rm=T)],"f2003.csv")

#chargement donnees AGRESTE 2012 (Cultures selon la superficie par commune)
zz <- unzip("/Users/Benjamin/Downloads/FDS_G_2012.zip", files = "FDS_G_2012_2010.txt")
f_2012 = fread(zz, sep = ";", stringsAsFactors = F)
f_2012 = f_2012[COM != "............" &
  G_2012_LIB_DIM1  == "Ensemble des exploitations (hors pacages collectifs)" &
  G_2012_LIB_DIM3 == "Superficie correspondante (hectares)"]
f_2012[communes, on = .(COM = code_insee), id_epci := i.id_epci]
sum(f_2012[
      G_2012_LIB_DIM2  == "Superficie agricole utilisée (1)" &
        id_epci == "E-200030195"
    ]$VALEUR, na.rm=T)
fwrite(f_2012[id_epci == "E-200030195", sum(VALEUR, na.rm=T), by=.(G_2012_LIB_DIM2)], "f1012.csv")

#recalcul sau depuis RPG
epci_shp = st_read("./crater/crater-data-sources/BANATIC/EPCI_SIG/2020/Epci2020_region.shp") %>% st_transform(crs = 2154)
rpg_shp = st_read("./RPG_2-0__SHP_LAMB93_R93-2019_2019-01-15/RPG_2-0_SHP_LAMB93_R93-2019/RPG/1_DONNEES_LIVRAISON_2019/RPG_2-0_SHP_LAMB93_R93-2019/PARCELLES_GRAPHIQUES.shp") %>% st_transform(crs = 2154)
cod_cult_princ = fread("./crater/crater-data-sources/IGN/2019/Codification_cultures_principales.csv",stringsAsFactors = F)
setnames(cod_cult_princ,c("CODE_CULTU","NOM_CULTU","CODE_GROUP","NOM_GROUP"))

intersect = st_intersection(rpg_shp,epci_shp)
intersect$superficie_ha = as.integer(st_area(intersect))/10000
setDT(intersect)
final = intersect[SIREN == "200030195",lapply(.SD,sum), keyby = .(SIREN,RAISON_SOC,CODE_CULTU,CODE_GROUP), .SDcols = c("superficie_ha")]
final = final[cod_cult_princ, on=.(CODE_CULTU), ':='(NOM_CULTU, NOM_GROUP)]
fwrite(final, "rpg_2019.csv")

#comparaison sau total par departement
departements = fread("./crater/crater-data-resultats/territoires/donnees.csv")[categorie_territoire == "DEPARTEMENT"]
departements$DEP = substr(departements$id_territoire, 3, 5)
zz <- unzip("/Users/Benjamin/Downloads/FDS_G_2003.zip", files = "FDS_G_2003_2010.txt")
f = fread(zz, sep = ";", stringsAsFactors = F)
f = f[DEP != "............" &
  COM == "............" &
  G_2003_LIB_DIM1  == "Ensemble des exploitations (hors pacages collectifs)" &
  G_2003_LIB_DIM2  == "Ensemble" &
  G_2003_LIB_DIM3 == "Superficie agricole utilisée (ha)"]
departements[f, on = .(DEP), sau_ha_AGRESTE := i.VALEUR]
departements[, ratio := sau_ha_AGRESTE / sau_ha * 100]

