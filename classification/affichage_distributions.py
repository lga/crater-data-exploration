#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 15 10:08:37 2021

@author: vincent
"""
import outils_analyse as outils
#import seaborn as sns
import matplotlib.pyplot as plt
import os
import numpy as np

#sns.set_theme(style="darkgrid")

chemin_fichier_resultats_API = r'../../resultats/2021_05_15_Resultats_API_crater.csv'
chemin_sauvegarde = r'../../crater-data-exploration/classification/resultats/2_distributions'

if chemin_sauvegarde != '':
    if not os.path.exists(chemin_sauvegarde):
        os.makedirs(chemin_sauvegarde)

# Chargement
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)

notes = [x for x in df.columns if x[:4]=='note']

colonnes_a_exclure = ['id_territoire','nom_territoire','categorie_territoire']
colonnes_a_exclure.extend(notes)

colonnes = [x for x in df.columns if x not in colonnes_a_exclure]

df = df[df['categorie_territoire']!='PAYS']
df2 = df.copy()

for col in colonnes:
    print(col)
    df.loc[df[col]<=0.01,col] = 1e-2
    df2.loc[df2['categorie_territoire']=='COMMUNE',col] /= np.max(df2.loc[df2['categorie_territoire']=='COMMUNE',col])
    print(df2.loc[df2['categorie_territoire']=='COMMUNE',col])
    print(df2.loc[df2['categorie_territoire']=='COMMUNE',col] /= np.max(df2.loc[df2['categorie_territoire']=='COMMUNE',col]))
    df2.loc[df2['categorie_territoire']=='EPCI',col] /= np.max(df2.loc[df2['categorie_territoire']=='EPCI',col])
    df2.loc[df2['categorie_territoire']=='DEPARTEMENT',col] /= np.max(df2.loc[df2['categorie_territoire']=='DEPARTEMENT',col])
    df2.loc[df2['categorie_territoire']=='REGION',col] /= np.max(df2.loc[df2['categorie_territoire']=='REGION',col])
    
    plt.figure(figsize=(16,8), dpi=150)
    
    ax=plt.subplot(2,3,1)
    g = sns.histplot(data=df, x=col, hue='categorie_territoire', bins=200, element="step", stat='probability', common_norm=False, legend=True)
    plt.xlabel('')
    plt.title('Linéaire / Histogramme')
    
    ax=plt.subplot(2,3,2)
    g = sns.histplot(data=df2, x=col, hue='categorie_territoire', bins=200, element="step", stat='probability', common_norm=False, legend=True)
    plt.xlabel('')
    plt.title('Linéaire normalisé / Histogramme')
    
    ax=plt.subplot(2,3,3)
    sns.histplot(data=df, x=col, hue='categorie_territoire', bins=200, element="step", stat='probability', common_norm=False, log_scale=True, legend=False)
    plt.xlabel('')
    plt.ylabel('')
    plt.title('Log / Histogramme')
    
    ax=plt.subplot(2,3,4)
    sns.histplot(data=df, x=col, hue='categorie_territoire', bins=200, element="step", stat='density', common_norm=False, log_scale=False, cumulative=True, legend=False)
    plt.xlabel('')
    plt.title('Linéaire / Cumulé')
    
    ax=plt.subplot(2,3,5)
    sns.histplot(data=df2, x=col, hue='categorie_territoire', bins=200, element="step", stat='density', common_norm=False, log_scale=False, cumulative=True, legend=False)
    plt.xlabel('')
    plt.title('Linéaire normalisé / Cumulé')
    
    ax=plt.subplot(2,3,6)
    sns.histplot(data=df, x=col, hue='categorie_territoire', bins=200, element="step", stat='density', common_norm=False, log_scale=True, cumulative=True, legend=False)
    plt.xlabel('')
    plt.ylabel('')
    #plt.yticks([])
    plt.title('Log / Cumulé')
    
    plt.suptitle(col)
    plt.tight_layout()
    if chemin_sauvegarde != '':
        plt.savefig(os.path.join(chemin_sauvegarde,col+'.png'))


plt.figure(figsize=(16,8), dpi=150)
for i in range(len(notes)):
    plt.subplot(2,2,i+1)
    for cat in df['categorie_territoire'].unique():
        df2 = df[df['categorie_territoire']==cat].round()
        df3 = df2[notes[i]]
        N = len(df2)
        x = np.arange(0,11)
        y = [len(df3[df3==n])/N for n in x]
        plt.plot(x, y, 'o-', mfc='white',label=cat)
    plt.legend()
    plt.xlabel('Note')
    plt.ylabel('Probability')
    plt.title(notes[i].replace('_',' '))
plt.suptitle('Notes CRATer')
plt.tight_layout()
if chemin_sauvegarde != '':
    plt.savefig(os.path.join(chemin_sauvegarde,'notes_crater.png'))