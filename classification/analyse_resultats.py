#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 22:14:51 2021

@author: vincent
"""

import outils_analyse as outils
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
import os

#%%
chemin_fichier_resultats_API = r'../../resultats/2021_04_22_resultats_crater.csv'
chemin_sauvegarde_figures = '../../resultats/2021_04_22_correlations' # Vide pour ne pas enregistrer les figures
type_territoire = '' # Vide pour tout traiter
tag = '' # Tag a ajouter aux noms de fichier. Laisser vide si pas utile


# On ne garde que certaines colonnes pour une 1ere analyse
# En particulier, on enleve les sous categories pour les cultures et l'indice hvn

colonnes_utiles = {'population':'population',
                   'superficie_ha':'superficie',
                   'densite_population_hab_km2':'dens.pop.',
                   'sau_ha':'sau',
                   'sau_bio_ha':'sau bio',
                   'sau_par_habitant_m2':'sau/hab',
                   'rythme_artificialisation_sau_pourcent':'rythme artif.',
                   'population_agricole_2010':'pop. agr.',
                   'part_population_agricole_2010':'part pop. agr.',
                   'hvn_indice_total':'indice hvn',
                   'part_sau_bio_pourcent':'part sau bio',
                   'besoin_assiette_actuelle_ha':'besoin assiette',
                   'taux_couverture_besoin_total':'tx. couv. besoin'}

'''
colonnes_utiles = {'population':'population',
                   'superficie_ha':'superficie',
                   'sau_ha':'sau',
                   'sau_bio_ha':'sau bio',
                   'rythme_artificialisation_sau_pourcent':'rythme artif.',
                   'population_agricole_2010':'pop. agr.',
                   'hvn_indice_total':'indice hvn',
                   'besoin_assiette_actuelle_ha':'besoin assiette'}
'''

if chemin_sauvegarde_figures != '':
    if not os.path.exists(chemin_sauvegarde_figures):
        os.makedirs(chemin_sauvegarde_figures)

#%%
# Chargement
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)

# Normalisation
df_norm = outils.normaliser_donnees(df,colonnes_utiles)
#tri en ordre inverse, evite que les communes ecrasent les autres territoires sur les plots
df_norm = df_norm.sort_index(ascending=False)

# Type de territoire a traiter
if type_territoire != '':
    df_norm = df_norm[df_norm['categorie_territoire']==type_territoire]

# Matrice de correlations
corr = df[colonnes_utiles.keys()].corr()
corr_norm = df_norm[colonnes_utiles.keys()].corr()

# Correlations en ne tenant compte que des donnees non nulles
corr_norm_pos = corr_norm.copy()
for x in corr_norm_pos.index:
    for y in corr_norm_pos.columns:
        x1 = df_norm[(df_norm[x]>-10) & (df_norm[y]>-10)][str(x)]
        y1 = df_norm[(df_norm[x]>-10) & (df_norm[y]>-10)][str(y)]
        corr_norm_pos.loc[x,y] = np.corrcoef(x1,y1)[1,0]

#%%
sns.set_theme(style="dark")

#Heatmaps avec les correlations
lim = 1
cmap = sns.diverging_palette(230, 20, as_cmap=True)
f, ax = plt.subplots(figsize=(20, 8))
plt.subplot(131)
sns.heatmap(corr, cmap=cmap, vmax=lim, vmin=-1*lim, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.title('brut')
plt.subplot(132)
sns.heatmap(corr_norm, cmap=cmap, vmax=lim, vmin=-1*lim, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.title('norm')
plt.subplot(133)
sns.heatmap(corr_norm_pos, cmap=cmap, vmax=lim, vmin=-1*lim, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.title('norm >0')
if type_territoire == '':
    plt.suptitle('TOUS LES TERRITOIRES')
else:
    plt.suptitle(type_territoire)
plt.tight_layout()
if chemin_sauvegarde_figures != '':
    plt.savefig(os.path.join(chemin_sauvegarde_figures,'heatmaps'+tag+'.png'))



# Histogrammes sur les donnees brutes et normalisees
cmap = cm.tab20(range(len(df.columns)))
ax = plt.figure(figsize=[16,8])
ax1 = plt.subplot(121)
ax2 = plt.subplot(122)
i=0
for col in colonnes_utiles:
    ax1.hist(df[col],bins=100,histtype='step',density=True,label=col,color=cmap[i])
    ax2.hist(df_norm[col],bins=100,histtype='step',density=True,label=col,color=cmap[i])
    i+=1
ax1.legend()
ax1.set_title('brut')
ax1.set_ylabel('densité de proba')
ax1.set_ylim([0,1.5e-6])
ax2.set_title('norm')
ax2.legend()
ax2.set_ylim([0,3])
ax2.set_ylabel('densité de proba')
if type_territoire == '':
    plt.suptitle('TOUS LES TERRITOIRES')
else:
    plt.suptitle(type_territoire)
if chemin_sauvegarde_figures != '':
    plt.savefig(os.path.join(chemin_sauvegarde_figures,'histogrammes'+tag+'.png'))

# Ensemble des correlations
# TODOQ : Afficher seulement un echantillon representatif pour gagner du temps et de la memoirte
outils.affichage_correlations(df_norm,colonnes_utiles,avec_seuil=False,chemin_sauvegarde_figures=chemin_sauvegarde_figures, tag=tag)
outils.affichage_correlations(df_norm,colonnes_utiles,avec_seuil=True,seuil=-10,chemin_sauvegarde_figures=chemin_sauvegarde_figures, tag=tag)

for terr in ['','COMMUNE','EPCI','DEPARTEMENT','REGION']:
    outils.affichage_correlations_avec_hist(df_norm,colonnes_utiles, type_territoire=terr,size=12, avec_seuil=False,chemin_sauvegarde_figures=chemin_sauvegarde_figures, tag=tag)
    outils.affichage_correlations_avec_hist(df_norm,colonnes_utiles, type_territoire=terr,size=12, avec_seuil=True, seuil=-10,chemin_sauvegarde_figures=chemin_sauvegarde_figures, tag=tag)








