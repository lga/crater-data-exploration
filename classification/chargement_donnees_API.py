#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 21:40:32 2021

@author: vincent
"""

import outils_API


# Creation dataframe contenant les indicateurs CRATer des territoires selectionnes
# Utilise l'API CRATer

fichier_territoires_crater = r'../../crater-data-resultats/territoires/territoires.csv'
chemin_sauvegarde = r''


# Recuperation d'un petit nombre de resultats

id_territoire = ['C-31069','C-31044']
df = outils_API.chargement_donnees(id_territoire,chemin_sauvegarde)


# Recuperation de tous les territoires
'''
id_territoire = outils_API.creer_liste_id_territoires(fichier_territoires_crater)
df = outils_API.chargement_donnees(id_territoire,chemin_sauvegarde)
'''
