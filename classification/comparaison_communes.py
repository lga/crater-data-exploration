#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 15 19:56:56 2021

@author: vincent
"""

import outils_analyse as outils
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os


chemin_fichier_resultats_API = r'../../resultats/2021_05_15_Resultats_API_crater.csv'
id_commune = 'C-64223'#31044, 31481, 71025

chemin_sauvegarde = r'../../crater-data-exploration/classification/resultats/4_comparaison_communes'

if chemin_sauvegarde != '':
    if not os.path.exists(chemin_sauvegarde):
        os.makedirs(chemin_sauvegarde)

# Chargement
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)
df = df[df['categorie_territoire']=='COMMUNE']

# Indicateurs pour lesquels on souhaite effectuer l'analyse
indicateurs = ['population','densite_population_hab_km2']
N = len(indicateurs)

# Colonnes a garder
colonnes_a_exclure = ['id_territoire','nom_territoire','categorie_territoire']
colonnes = [x for x in df.columns if x not in colonnes_a_exclure]



plt.figure(figsize=(8,N*8),dpi=150)
for i in range(N):
    ind = indicateurs[i]
    ind_reference = df[df['id_territoire']==id_commune][ind].iloc[0]
    # On ne garde que les communes ayant l'indicateur en question dans un facteur 2
    df2 = df[(df[ind]<=ind_reference*np.sqrt(2)) & (df[ind]>=ind_reference/np.sqrt(2))].copy()
    df3=df2[df2['id_territoire']==id_commune].copy()
    for col in colonnes:
        df2.loc[df2[col]<=0.01,col] = 1e-2
        df3.loc[df3[col]<=0.01,col] = 1e-2
        m = np.nanmedian(df2[col])
        df2[col] = df2[col] / m
        df3[col] = df3[col] / m
    aux = df3.mean().sort_values(ascending=False).index
    df3 = df3.reindex(aux, axis=1)
    df2 = df2.reindex(aux, axis=1)
    ax = plt.subplot(N,1,i+1)
    sns.boxplot(data=df2, palette="light:r", whis=[0, 100], width=.6, orient="h")
    sns.pointplot(data=df3, orient="h", color='red', join=False)
    ax.set_xscale("log")
    ax.xaxis.grid(True)
    ax.set(ylabel="")
    sns.despine(trim=True, left=True)
    tit = '{} similaire (N={})'.format(ind,len(df2))
    plt.title(tit, size=10)
nom_commune = df[df['id_territoire']==id_commune]['nom_territoire'].iloc[0]
population = df[df['id_territoire']==id_commune]['population'].iloc[0]
densite = df[df['id_territoire']==id_commune]['densite_population_hab_km2'].iloc[0]
tit = '{} ({}hab, {:.1f}hab/km2)'.format(nom_commune,int(population),densite)
plt.suptitle(tit)
plt.tight_layout()

if chemin_sauvegarde != '':
    nom = nom_commune + '.png'
    plt.savefig(os.path.join(chemin_sauvegarde,nom))

