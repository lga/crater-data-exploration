#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 23 13:24:57 2021

@author: vincent
"""
import outils_analyse as outils
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns
from math import pi


id_territoire = 'R-76'#31044, 31481, 71025

chemin_fichier_resultats_API = r'../../resultats/2021_05_15_Resultats_API_crater.csv'
chemin_fichier_territoires = r'../../crater-data-resultats/territoires/territoires.csv'
chemin_sauvegarde = r'../../crater-data-exploration/classification/resultats/5_comparaison_territoire_parent'

if chemin_sauvegarde != '':
    if not os.path.exists(chemin_sauvegarde):
        os.makedirs(chemin_sauvegarde)

# Chargement
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)
territoires = pd.read_csv(chemin_fichier_territoires, sep=';')

# Colonnes a garder
colonnes_a_exclure = ['id_territoire','nom_territoire','categorie_territoire']
colonnes = [x for x in df.columns if x not in colonnes_a_exclure]

def trouver_territoire_parent(id_territoire):
    if id_territoire[0]=='C':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_epci'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='E':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_departement'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='D':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_region'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='R':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_pays'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    else:
        print('Erreur code territoire')
    return id_parent, nom_parent

def trouver_territoires_enfants(id_territoire):
    if id_territoire[0] == 'P':
        id_enfants = territoires[(territoires['id_pays']==id_territoire) & (territoires['categorie_territoire']=='REGION')]['id_territoire']
    elif id_territoire[0] == 'R':
        id_enfants = territoires[(territoires['id_region']==id_territoire) & (territoires['categorie_territoire']=='DEPARTEMENT')]['id_territoire']
    elif id_territoire[0] == 'D':
        id_enfants = territoires[(territoires['id_departement']==id_territoire) & (territoires['categorie_territoire']=='EPCI')]['id_territoire']
    elif id_territoire[0] == 'E':
        id_enfants = territoires[(territoires['id_epci']==id_territoire) & (territoires['categorie_territoire']=='COMMUNE')]['id_territoire']
    else:
        print('Erreur code territoire')
    return list(id_enfants)


id_parent, nom_parent = trouver_territoire_parent(id_territoire)
id_enfants = trouver_territoires_enfants(id_parent)

df2 = df.loc[df['id_territoire'].isin(id_enfants)].copy()
df3 = df.loc[df['id_territoire'] == id_territoire].copy()

valeurs_max = df2.max()
valeurs_med = df2.median()


# On met la mediane à 1
for col in colonnes:
    df2.loc[df2[col]<=0.01,col] = 1e-2
    df3.loc[df3[col]<=0.01,col] = 1e-2
    m = np.nanmedian(df2[col])
    df2[col] = df2[col] / m
    df3[col] = df3[col] / m
aux = df3.mean().sort_values(ascending=False).index
df3 = df3.reindex(aux, axis=1)
df2 = df2.reindex(aux, axis=1)


categories=['note_politique_fonciere','note_population_agricole','note_pratiques_agricoles','note_production_besoins']
categories_2=['politique fonciere', 'population agricole', 'pratiques agricoles', 'production besoins']
N = len(categories)
angles = [n / float(N) * 2 * pi for n in range(N)]
angles += angles[:1]


plt.figure(figsize=(8,12),dpi=150)

ax = plt.subplot2grid((3, 6), (0, 0), rowspan=2, colspan=6)
#ax = plt.subplot(3,1,[1::2])
sns.boxplot(data=df2, palette="light:r", whis=[0, 100], width=.6, orient="h")
sns.pointplot(data=df3, orient="h", color='red', join=False)
ax.set_xscale("log")
ax.xaxis.grid(True)
ax.set(ylabel="")

ax2 = plt.subplot2grid((3, 6), (2, 0), polar=True, colspan=4)
plt.xticks(angles[:-1], categories_2, color='grey',size=10)
ax2.set_rlabel_position(0)
plt.yticks([2,4,6,8,10], ['2','4','6','8','10'], color="grey")
plt.ylim(0,10)
values=valeurs_max[categories].tolist()
values += values[:1]
ax2.plot(angles, values, linestyle='solid', label='Notes max')
ax2.fill(angles, values, alpha=0.1)
values=valeurs_med[categories].tolist()
values += values[:1]
ax2.plot(angles, values, linestyle='solid', label='Notes medianes')
ax2.fill(angles, values, alpha=0.1)
values=df.loc[df['id_territoire'] == id_territoire][categories].iloc[0].tolist()
values += values[:1]
ax2.plot(angles, values,linewidth=3, linestyle='solid', label='Notes Territoire')
ax2.fill(angles, values, alpha=0.1)
plt.legend()
handles, labels = ax2.get_legend_handles_labels()
ax2.get_legend().remove()
ax3 = plt.subplot2grid((3, 6), (2, 4), colspan=2)
ax3.set_axis_off()
plt.legend(handles, labels, loc='lower center')

nom_territoire = df[df['id_territoire']==id_territoire]['nom_territoire'].iloc[0]
type_territoire = df[df['id_territoire']==id_territoire]['categorie_territoire'].iloc[0].lower()
tit = nom_territoire + ' (' + id_territoire + ') vs '+ type_territoire + 's de ' + nom_parent + ' (' + id_parent + ')'
plt.suptitle(tit)
plt.tight_layout()
if chemin_sauvegarde != '':
    nom = nom_territoire + '.png'
    nom = nom.replace(' ','_')
    plt.savefig(os.path.join(chemin_sauvegarde,nom))














