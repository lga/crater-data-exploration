#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 30 10:46:02 2021

@author: vincent
"""

import os
import pandas as pd
import outils_analyse as outils
import outils_visu_demo_day as visu


chemin_fichier_resultats_API = r'../../resultats/2021_05_15_Resultats_API_crater.csv'
chemin_fichier_territoires = r'../../crater-data-resultats/territoires/territoires.csv'
chemin_sauvegarde = r'../../crater-data-exploration/classification/resultats/6_affichages_demo_day'

if chemin_sauvegarde != '':
    if not os.path.exists(chemin_sauvegarde):
        os.makedirs(chemin_sauvegarde)
    if not os.path.exists(os.path.join(chemin_sauvegarde,'csv')):
        os.makedirs(os.path.join(chemin_sauvegarde,'csv'))

# Chargement resultats CRATer + liste des territoires
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)
territoires = pd.read_csv(chemin_fichier_territoires, sep=';')
df['evolution_part_population_agricole_1988_2010'] = df['part_population_agricole_2010'] - df['part_population_agricole_1988']
df['evolution_population_agricole_1988_2010'] = df['population_agricole_2010'] - df['population_agricole_1988']

# indicateurs CRATer a garder pour l'analyse (toutes les valeurs chiffrées)
indicateurs_a_exclure = ['id_territoire','nom_territoire','categorie_territoire']
indicateurs = [x for x in df.columns if x not in indicateurs_a_exclure]

# On ne conserve que les donnees au niveau departement
liste_id_departements = territoires[territoires['categorie_territoire']=='DEPARTEMENT']['id_territoire'].tolist()
df_departements = df.loc[df['id_territoire'].isin(liste_id_departements)].copy()
# Enregistrement fichier départements
df_departements.to_csv(os.path.join(chemin_sauvegarde,'csv','0_resultats_CRATer_departements.csv'), index=False, sep=';')

# On fait les statistiques pour toutes les régions (+ france entiere)
liste_id_regions = territoires[territoires['categorie_territoire']=='REGION']['id_territoire'].tolist()

# On construit un dataframe avec les stats de toutes les regions (+ France) et tous les indicateurs
iterables = [liste_id_regions, indicateurs]
index = pd.MultiIndex.from_product(iterables, names=["id_territoire", "indicateur_CRAter"])
liste_colonnes = ['id_territoire','nom_territoire','categorie_territoire','nb_departements','moyenne','ecart-type','mediane','min','max','Q1','Q3']
statistiques = pd.DataFrame(columns=liste_colonnes)

# France
stats = {'id_territoire':'P-FR',
             'nom_territoire':df[df['id_territoire']=='P-FR']['nom_territoire'].iloc[0],
             'categorie_territoire':df[df['id_territoire']=='P-FR']['categorie_territoire'].iloc[0],
             'nb_departements':df_departements.count(),
             'moyenne':df_departements.mean(),
             'ecart-type':df_departements.std(),
             'mediane':df_departements.median(),
             'min':df_departements.min(),
             'max':df_departements.max(),
             'Q1':df_departements.quantile(0.25, interpolation='nearest'),
             'Q3':df_departements.quantile(0.75, interpolation='nearest')}
aux = pd.DataFrame(index=indicateurs, columns=liste_colonnes, data=stats)
statistiques = statistiques.append(aux)

# Regions
for id_terr in liste_id_regions:
    depts = visu.trouver_territoires_enfants(id_terr,territoires)
    df_depts = df.loc[df['id_territoire'].isin(depts)].copy()
    stats = {'id_territoire':id_terr,
             'nom_territoire':df[df['id_territoire']==id_terr]['nom_territoire'].iloc[0],
             'categorie_territoire':df[df['id_territoire']==id_terr]['categorie_territoire'].iloc[0],
             'nb_departements':df_depts.count(),
             'moyenne':df_depts.mean(),
             'ecart_type':df_depts.std(),
             'mediane':df_depts.median(),
             'minimum':df_depts.min(),
             'maximum':df_depts.max(),
             'Q1':df_depts.quantile(0.25, interpolation='nearest'),
             'Q3':df_depts.quantile(0.75, interpolation='nearest')}
    aux = pd.DataFrame(index=indicateurs, columns=liste_colonnes, data=stats)
    statistiques = statistiques.append(aux)


# On genere un fichier csv par indicateur CRATer avec les statistiques
for indic in indicateurs:
    stats_indicateur = statistiques.loc[indic]
    #stats_indicateur.to_csv(os.path.join(chemin_sauvegarde,'csv',indic+'.csv'), index=False, sep=';')


# Essai d'affichage sur quelques departements
for departement in ['D-48', 'D-33', 'D-26']:
    chemin_sauvegarde=''
    visu.calculs_et_affichage_radar(departement,territoires,df,statistiques,chemin_sauvegarde)
    visu.affichage_caracteristiques_territoire(departement,territoires,df,statistiques,chemin_sauvegarde)
    visu.affichage_resultats_pratiques_agricoles(departement,territoires,df,statistiques,chemin_sauvegarde)
    visu.affichage_resultats_population_agricole(departement,territoires,df,statistiques,chemin_sauvegarde)
    visu.affichage_resultats_politique_fonciere(departement,territoires,df,statistiques,chemin_sauvegarde)
    visu.affichage_resultats_production_besoins(departement,territoires,df,statistiques,chemin_sauvegarde)
    
     

    





