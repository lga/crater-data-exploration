#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 11:05:43 2021

@author: vincent
"""

import requests
import pandas as pd

API_URL = 'http://localhost:3000/crater/api/diagnostics/'


def creer_liste_id_territoires(fichier_territoires_crater):
    '''
    fichier_territoires_crater : crater-data-resultats/territoires/territoires.csv
    '''
    territoires = pd.read_csv(fichier_territoires_crater,sep=';')
    liste_id = territoires['id_territoire']
    return list(liste_id)

def lire_json(id_territoire):
    url = API_URL + id_territoire
    r = requests.get(url)
    return r.json()

def selection_donnees_json(json_territoire):
    d = {'id_territoire': json_territoire['idTerritoire'],
         'nom_territoire': json_territoire['nomTerritoire'],
         'categorie_territoire': json_territoire['categorieTerritoire'],
         'population': json_territoire['population'],
         'superficie_ha': json_territoire['superficieHa'],
         'sau_ha': json_territoire['surfaceAgricoleUtile']['sauTotaleHa'],
         'sau_bio_ha': json_territoire['surfaceAgricoleUtile']['sauBioHa'],
         'sau_CER_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][0]['sauHa'],
         'sau_DVC_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][1]['sauHa'],
         'sau_FLC_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][2]['sauHa'],
         'sau_FOU_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][3]['sauHa'],
         'sau_OLP_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][4]['sauHa'],
         'sau_SNC_ha': json_territoire['surfaceAgricoleUtile']['sauParGroupeCulture'][5]['sauHa'],
         'note_politique_fonciere': json_territoire['politiqueFonciere']['indicateurGlobal']['note'],
         'sau_par_habitant_m2': json_territoire['politiqueFonciere']['sauParHabitantM2'],
         'rythme_artificialisation_sau_pourcent': json_territoire['politiqueFonciere']['rythmeArtificialisationSauPourcent'],
         'note_population_agricole': json_territoire['populationAgricole']['indicateurGlobal']['note'],
         'population_agricole_1988': json_territoire['populationAgricole']['populationAgricole1988'],
         'population_agricole_2010': json_territoire['populationAgricole']['populationAgricole2010'],
         'part_population_agricole_1988': json_territoire['populationAgricole']['partPopulationAgricole1988'],
         'part_population_agricole_2010': json_territoire['populationAgricole']['partPopulationAgricole2010'],
         'note_pratiques_agricoles': json_territoire['pratiquesAgricoles']['indicateurGlobal']['note'],
         'hvn_indice_1': json_territoire['pratiquesAgricoles']['indicateurHvn']['indice1'],
         'hvn_indice_2': json_territoire['pratiquesAgricoles']['indicateurHvn']['indice2'],
         'hvn_indice_3': json_territoire['pratiquesAgricoles']['indicateurHvn']['indice3'],
         'hvn_indice_total': json_territoire['pratiquesAgricoles']['indicateurHvn']['indiceTotal'],
         'part_sau_bio_pourcent': json_territoire['pratiquesAgricoles']['pourcentageSauBio'],
         'note_production_besoins': json_territoire['productionBesoins']['indicateurGlobal']['note'],
         'besoin_assiette_actuelle_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsHa'],
         'besoin_CER_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][0]['besoinsHa'],
         'taux_couverture_besoin_CER_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][0]['tauxCouvertureBesoins'],
         'besoin_DVC_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][1]['besoinsHa'],
         'taux_couverture_besoin_DVC_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][1]['tauxCouvertureBesoins'],
         'besoin_FLC_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][2]['besoinsHa'],
         'taux_couverture_besoin_FLC_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][2]['tauxCouvertureBesoins'],
         'besoin_FOU_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][3]['besoinsHa'],
         'taux_couverture_besoin_FOU_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][3]['tauxCouvertureBesoins'],
         'besoin_OLP_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][4]['besoinsHa'],
         'taux_couverture_besoin_OLP_ha':json_territoire['productionBesoins']['assietteActuelle']['besoinsParGroupeCulture'][4]['tauxCouvertureBesoins']
         }
    return d


def chargement_donnees(liste_id_territoires, chemin_sauvegarde=''):
    #
    d = []
    for id_territoire in liste_id_territoires:
        territoire_json = lire_json(id_territoire)
        territoire_dict = selection_donnees_json(territoire_json)
        d.append(territoire_dict)
    df = pd.DataFrame(d)
    
    # Remplacement des Nan par 0
    dic = {}
    for col in df.columns:
        if df[col].dtypes=='float64':
            dic[col] = 0.
    df = df.fillna(dic)
    
    if chemin_sauvegarde != '':
        df.to_csv(chemin_sauvegarde)
    return df


