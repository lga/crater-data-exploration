#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 22:05:40 2021

@author: vincent
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os


def charger_resultats_crater(chemin_fichier_resultats_API):
    '''
    chemin_fichier_resultats_API : csv contenant les donnees issues de l'API'
    '''
    df = pd.read_csv(chemin_fichier_resultats_API, index_col=0)
    
    # Ajout de quelques indicateurs complémentaires
    df['densite_population_hab_km2'] = df['population'] / (df['superficie_ha'] /100)
    df['taux_couverture_besoin_total'] = (df['sau_CER_ha']+df['sau_DVC_ha']+df['sau_FLC_ha']+df['sau_FOU_ha']+df['sau_OLP_ha']+df['sau_SNC_ha']) / df['besoin_assiette_actuelle_ha']
    df.replace([np.inf, -np.inf], np.nan, inplace=True)
    df.dropna(how='any')
    
    return df


def normaliser_donnees(df,colonnes_utiles):
    '''
    colonnes_utiles: liste des colonnes de df sur lesquelles appliquer le traitement
    '''
    df_norm = df.copy()
    # Normalisation des donnees en log, pour avoir des distributions plus homogenes
    # On attribue arbitrairement une valeur de -10 aux valeurs qui etaient a zero
    for col in colonnes_utiles.keys():
        df_norm[col] /= np.max(df[col])
        df_norm.loc[df[col]>0,col] = np.log10(df_norm[col].loc[df_norm[col]>0])
        df_norm.loc[df[col]<=0,col] = -10
    return df_norm




def affichage_correlations(df,colonnes_utiles,size=12,avec_seuil=True,seuil=-10,chemin_sauvegarde_figures='', tag=''):
    colonnes = list(colonnes_utiles.keys())
    colonnes_court = list(colonnes_utiles.values())
    plt.figure(figsize=[20,20],dpi=50)
    N = len(colonnes)
    for i in range(N):
        for j in range(N):
            x = colonnes[j]
            y = colonnes[i]
            if avec_seuil:
                data = df[(df[x]>seuil) & (df[y]>seuil)]
            else:
                data = df
            if i==j:
                ax=plt.subplot(N,N,N*(i)+i+1)
                sns.histplot(x=x,kde=True,element="step", linewidth=0, data=data)
                if j!=N-1:
                    plt.ylabel('')
                    plt.yticks([])
                    #plt.xlabel('')
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    ax.xaxis.set_label_position('top') 
                    plt.xticks([])
                else:
                    plt.ylabel('')
                    plt.yticks([])
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    plt.xticks([])
            elif i>j:
                ax = plt.subplot(N,N,N*(i)+j+1)
                sns.scatterplot(x=x, y=y, hue='categorie_territoire', palette='muted', data=data)
                handles, labels = ax.get_legend_handles_labels()
                ax.get_legend().remove()
                if j!=0:
                    plt.ylabel('')
                    plt.yticks([])
                else:
                    plt.ylabel(colonnes_court[i],fontsize=size)
                    plt.yticks([])
                if i!=N-1:
                    plt.xlabel('')
                    plt.xticks([])
                else:
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    plt.xticks([])
    ax = plt.subplot(N,N,2)
    ax.set_axis_off()
    plt.legend(handles, labels, loc='center')
    plt.tight_layout()
    if chemin_sauvegarde_figures !='':
        if avec_seuil:
            figname = 'correlations_seuil_'+tag+'.png'
        else:
            figname = 'correlations'+tag+'.png'
        plt.savefig(os.path.join(chemin_sauvegarde_figures,figname))




def affichage_correlations_avec_hist(df,colonnes_utiles, type_territoire='',size=12,avec_seuil=True,seuil=-10,chemin_sauvegarde_figures='', tag=''):
    colonnes = list(colonnes_utiles.keys())
    colonnes_court = list(colonnes_utiles.values())
    
    if type_territoire != '':
        df = df[df['categorie_territoire']==type_territoire]
        titre = type_territoire
    else:
        titre = 'TOUS LES TERRITOIRES'
    if avec_seuil:
        titre = titre + ' - seuil'
    
    if len(df)>1000:
        markersize=5
    elif len(df)>50:
        markersize=10
    else:
        markersize=20
    plt.figure(figsize=[20,20],dpi=50)
    N = len(colonnes)
    for i in range(N):
        for j in range(N):
            x = colonnes[j]
            y = colonnes[i]
            if avec_seuil:
                data = df[(df[x]>seuil) & (df[y]>seuil)]
            else:
                data = df
            if i==j:
                ax=plt.subplot(N,N,N*(i)+i+1)
                sns.histplot(x=x,kde=True,element="step", linewidth=0, data=data)
                if j!=N-1:
                    plt.ylabel('')
                    plt.yticks([])
                    plt.xlabel('')
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    ax.xaxis.set_label_position('top') 
                    plt.xticks([])
                else:
                    plt.ylabel('')
                    plt.yticks([])
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    plt.xticks([])
            elif i>j:
                plt.subplot(N,N,N*(i)+j+1)
                sns.scatterplot(x=x,y=y, s=markersize, color=".15",data=data)
                sns.histplot(x=x, y=y, bins=100, pthresh=.1, cmap="mako",data=data)
                if j!=0:
                    plt.ylabel('')
                    plt.yticks([])
                else:
                    plt.ylabel(colonnes_court[i],fontsize=size)
                    plt.yticks([])
                if i!=N-1:
                    plt.xlabel('')
                    plt.xticks([])
                else:
                    plt.xlabel(colonnes_court[j],fontsize=size)
                    plt.xticks([])
    plt.suptitle(titre,fontsize=size)
    plt.tight_layout()
    if chemin_sauvegarde_figures !='':
        figname = titre.replace(' ','_')
        figname = figname + tag+'.png'
        plt.savefig(os.path.join(chemin_sauvegarde_figures,figname))





















