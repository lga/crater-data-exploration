from math import pi
import os
from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from matplotlib.colors import to_rgba

def arrondir(x):
    if abs(x)>=10:
        return round(x)
    elif abs(x)>=1:
        return round(x,1)
    else:
        return round(x,2)

def trouver_territoires_enfants(id_territoire, territoires):
    if id_territoire[0] == 'P':
        id_enfants = territoires[(territoires['id_pays']==id_territoire) & (territoires['categorie_territoire']=='REGION')]['id_territoire']
    elif id_territoire[0] == 'R':
        id_enfants = territoires[(territoires['id_region']==id_territoire) & (territoires['categorie_territoire']=='DEPARTEMENT')]['id_territoire']
    elif id_territoire[0] == 'D':
        id_enfants = territoires[(territoires['id_departement']==id_territoire) & (territoires['categorie_territoire']=='EPCI')]['id_territoire']
    elif id_territoire[0] == 'E':
        id_enfants = territoires[(territoires['id_epci']==id_territoire) & (territoires['categorie_territoire']=='COMMUNE')]['id_territoire']
    else:
        print('Erreur code territoire')
    return list(id_enfants)

def trouver_territoire_parent(id_territoire, territoires):
    if id_territoire[0]=='C':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_epci'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='E':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_departement'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='D':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_region'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    elif id_territoire[0]=='R':
        id_parent = territoires[territoires['id_territoire']==id_territoire]['id_pays'].iloc[0]
        nom_parent = territoires[territoires['id_territoire']==id_parent]['nom_territoire'].iloc[0]
    else:
        print('Erreur code territoire')
    return id_parent, nom_parent


def calculs_et_affichage_radar(departement,territoires,resultats_crater,statistiques,chemin_sauvegarde=''):
    notes_crater = ['note_pratiques_agricoles','note_production_besoins','note_politique_fonciere','note_population_agricole']
    noms_axes = ['Pratiques\nagricoles', 'Production /\nbesoins', 'Politique\nfoncière', 'Population\nagricole']
    df = resultats_crater.copy()
    
    # On recupere les notes du departement
    notes_dep = []
    for nom_note in notes_crater:
        notes_dep.append(df[df['id_territoire']==departement][nom_note].iloc[0])
    nom_departement = df[df['id_territoire']==departement]['nom_territoire'].iloc[0]
    
    # Moyenne region et France
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    notes_region = []
    notes_fr = []
    for nom_note in notes_crater:
        stats = statistiques.loc[nom_note]
        notes_region.append(stats[stats['id_territoire']==id_parent]['moyenne'].iloc[0])
        notes_fr.append(stats[stats['id_territoire']=='P-FR']['moyenne'].iloc[0])
    
    # Affichage radar
    donnees = [notes_fr,notes_region,notes_dep]
    legendes = ['France',nom_parent,nom_departement]
    couleurs = ['skyblue','thistle','mediumaquamarine']
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement + '_radar.png')
    else:
        nom_fichier = ''
    afficher_radar(noms_axes, donnees, legendes, couleurs, nom_fichier=nom_fichier)



def afficher_radar(noms_axes, donnees, legendes, couleurs, nom_fichier=''):
    N = len(noms_axes)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]
    
    plt.figure(figsize=(5,6))
    ax = plt.subplot2grid((7, 1), (0, 0), rowspan=5, polar=True)
    #ax = plt.subplot(111, polar=True)
    
    ax.grid(False)
    ax.spines['polar'].set_visible(False)
    plt.xticks(angles[:-1], noms_axes, color='black',size=10)
    ax.set_rlabel_position(0)
    plt.yticks([2,4,6,8,10], ['2','4','6','8','10'], color="grey")
    plt.ylim(0,12.5)
    
    for i in [2,4,6,8,10]:
        ax.plot(angles, [i]*(N+1), 'gray', lw=0.5)
    ax.plot([0,pi],[10,10],'gray', lw=0.5)
    ax.plot([pi/2,3*pi/2],[10,10],'gray', lw=0.5)
    
    elements_legende = []
    
    for i in range(len(donnees)):
        d = donnees[i].copy()
        d += d[:1]
        ax.plot(angles, d, linestyle='solid', color=couleurs[i])
        elements_legende.append(Patch(facecolor=to_rgba(couleurs[i],0.5), edgecolor=couleurs[i],label=legendes[i]))
        ax.fill(angles, d, alpha=0.5, color=couleurs[i])
    #plt.legend(handles=elements_legende,loc='lower right')
    #ax.get_legend().remove()
    #handles, labels = ax.get_legend_handles_labels()
    
    ax2 = plt.subplot2grid((7, 1), (6, 0), rowspan=2, polar=True)
    ax2.set_axis_off()
    #plt.legend(handles, labels, loc='center')
    plt.legend(handles=elements_legende,loc='center')
    
    if nom_fichier != '':
        plt.savefig(nom_fichier)

def afficher_barres(ax,df, indicateur, statistiques, departement):
    df = df.sort_values(by=indicateur)
    df['rang'] = df[indicateur].rank(method='first')
    rang_reg = int(df[df['id_territoire']==departement]['rang'].iloc[0])-1
    valeur_max = df[indicateur].max()
    barlist = plt.barh(df['nom_territoire'], df[indicateur], color='skyblue')
    barlist[rang_reg].set_color('mediumaquamarine')
    ax.set_xticks([])
    ax.set_xlabel(indicateur)
    ax.xaxis.set_label_position('top')
    plt.box(False)
    labs = df[indicateur].tolist()
    for i in range(len(barlist)):
        rect = barlist[i]
        width = int(rect.get_width())
        if width < valeur_max/4:
            xloc = 5
            align = 'left'
        else:
            xloc = -5
            align = 'right'
        yloc = rect.get_y() + rect.get_height() / 2
        
        lab = arrondir(labs[i])
        ax.annotate(lab, xy=(width, yloc), xytext=(xloc, 0), textcoords="offset points", horizontalalignment=align, verticalalignment='center', weight='medium', clip_on=True)
    # Ajouts stats France
    stats = statistiques.loc[indicateur]
    med_fr = stats[stats['id_territoire']=='P-FR']['mediane'].iloc[0]
    ax.axvline(med_fr, color='grey', alpha=0.25)
    yloc = barlist[0].get_y() - rect.get_height() / 2
    ax.annotate('Mediane', xy=(med_fr, yloc), xytext=(0, 0), textcoords="offset points", horizontalalignment='center', verticalalignment='center', clip_on=True, size='small')
    Q1_fr = stats[stats['id_territoire']=='P-FR']['Q1'].iloc[0]
    ax.axvline(Q1_fr, color='grey', alpha=0.25)
    ax.annotate('Q1', xy=(Q1_fr, yloc), xytext=(0, 0), textcoords="offset points", horizontalalignment='center', verticalalignment='center', clip_on=True, size='small')
    Q3_fr = stats[stats['id_territoire']=='P-FR']['Q3'].iloc[0]
    ax.axvline(Q3_fr, color='grey', alpha=0.25)
    ax.annotate('Q3', xy=(Q3_fr, yloc), xytext=(0, 0), textcoords="offset points", horizontalalignment='center', verticalalignment='center', clip_on=True, size='small')
    ax.annotate('France', xy=(0, yloc), xytext=(0, 0), textcoords="offset points", horizontalalignment='left', verticalalignment='center', clip_on=True, size='small')


def affichage_caracteristiques_territoire(departement, territoires, resultats_crater, statistiques, chemin_sauvegarde=''):
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    id_enfants = trouver_territoires_enfants(id_parent,territoires)
    df = resultats_crater.loc[resultats_crater['id_territoire'].isin(id_enfants)].copy()
    
    plt.figure(figsize=[16,8],dpi=150)
    ax1=plt.subplot(221)
    afficher_barres(ax1,df, 'population', statistiques, departement)
    ax2=plt.subplot(222)
    afficher_barres(ax2,df, 'superficie_ha', statistiques, departement)
    ax3=plt.subplot(223)
    afficher_barres(ax3,df, 'sau_ha', statistiques, departement)
    ax4=plt.subplot(224)
    afficher_barres(ax4,df, 'densite_population_hab_km2', statistiques, departement)
    
    plt.suptitle('Caractéristiques département')
    plt.tight_layout()
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement+'_caracteristiques.png')
        plt.savefig(nom_fichier)

def affichage_resultats_pratiques_agricoles(departement, territoires, resultats_crater, statistiques, chemin_sauvegarde=''):
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    id_enfants = trouver_territoires_enfants(id_parent,territoires)
    df = resultats_crater.loc[resultats_crater['id_territoire'].isin(id_enfants)].copy()
    
    plt.figure(figsize=[16,8],dpi=150)
    ax1=plt.subplot(231)
    afficher_barres(ax1,df, 'hvn_indice_1', statistiques, departement)
    ax2=plt.subplot(232)
    afficher_barres(ax2,df, 'hvn_indice_2', statistiques, departement)
    ax3=plt.subplot(233)
    afficher_barres(ax3,df, 'hvn_indice_3', statistiques, departement)
    ax4=plt.subplot(234)
    afficher_barres(ax4,df, 'hvn_indice_total', statistiques, departement)
    ax5=plt.subplot(235)
    afficher_barres(ax5,df, 'sau_bio_ha', statistiques, departement)
    ax6=plt.subplot(236)
    afficher_barres(ax6,df, 'part_sau_bio_pourcent', statistiques, departement)
    
    plt.suptitle('Pratiques agricoles')
    plt.tight_layout()
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement+'_pratiques_agricoles.png')
        plt.savefig(nom_fichier)


def affichage_resultats_population_agricole(departement, territoires, resultats_crater, statistiques, chemin_sauvegarde=''):
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    id_enfants = trouver_territoires_enfants(id_parent,territoires)
    df = resultats_crater.loc[resultats_crater['id_territoire'].isin(id_enfants)].copy()
    
    plt.figure(figsize=[16,8],dpi=150)
    ax1=plt.subplot(231)
    afficher_barres(ax1,df, 'population_agricole_1988', statistiques, departement)
    ax2=plt.subplot(232)
    afficher_barres(ax2,df, 'population_agricole_2010', statistiques, departement)
    ax3=plt.subplot(233)
    afficher_barres(ax3,df, 'evolution_population_agricole_1988_2010', statistiques, departement)
    ax4=plt.subplot(234)
    afficher_barres(ax4,df, 'part_population_agricole_1988', statistiques, departement)
    ax5=plt.subplot(235)
    afficher_barres(ax5,df, 'part_population_agricole_2010', statistiques, departement)
    ax6=plt.subplot(236)
    afficher_barres(ax6,df, 'evolution_part_population_agricole_1988_2010', statistiques, departement)
    
    plt.suptitle('Population agricole')
    plt.tight_layout()
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement+'_population_agricole.png')
        plt.savefig(nom_fichier)

def affichage_resultats_politique_fonciere(departement, territoires, resultats_crater, statistiques, chemin_sauvegarde=''):
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    id_enfants = trouver_territoires_enfants(id_parent,territoires)
    df = resultats_crater.loc[resultats_crater['id_territoire'].isin(id_enfants)].copy()
    
    plt.figure(figsize=[16,8],dpi=150)
    ax1=plt.subplot(121)
    afficher_barres(ax1,df, 'sau_par_habitant_m2', statistiques, departement)
    ax2=plt.subplot(122)
    afficher_barres(ax2,df, 'rythme_artificialisation_sau_pourcent', statistiques, departement)
    
    
    plt.suptitle('Politique foncière')
    plt.tight_layout()
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement+'_politique_fonciere.png')
        plt.savefig(nom_fichier)

def affichage_resultats_production_besoins(departement, territoires, resultats_crater, statistiques, chemin_sauvegarde=''):
    id_parent, nom_parent = trouver_territoire_parent(departement,territoires)
    id_enfants = trouver_territoires_enfants(id_parent,territoires)
    df = resultats_crater.loc[resultats_crater['id_territoire'].isin(id_enfants)].copy()
    
    plt.figure(figsize=[16,8],dpi=150)
    ax1=plt.subplot(231)
    afficher_barres(ax1,df, 'taux_couverture_besoin_CER_ha', statistiques, departement)
    ax2=plt.subplot(232)
    afficher_barres(ax2,df, 'taux_couverture_besoin_DVC_ha', statistiques, departement)
    ax3=plt.subplot(233)
    afficher_barres(ax3,df, 'taux_couverture_besoin_FLC_ha', statistiques, departement)
    ax4=plt.subplot(234)
    afficher_barres(ax4,df, 'taux_couverture_besoin_FOU_ha', statistiques, departement)
    ax5=plt.subplot(235)
    afficher_barres(ax5,df, 'taux_couverture_besoin_OLP_ha', statistiques, departement)
    ax6=plt.subplot(236)
    afficher_barres(ax6,df, 'taux_couverture_besoin_total', statistiques, departement)
    
    plt.suptitle('Adequation production - besoins')
    plt.tight_layout()
    if chemin_sauvegarde!='':
        nom_fichier = os.path.join(chemin_sauvegarde,departement+'_production_besoins.png')
        plt.savefig(nom_fichier)








