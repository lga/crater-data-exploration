#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 15 14:05:42 2021

@author: vincent
"""

import outils_analyse as outils
import pandas as pd
import matplotlib.pyplot as plt
from math import pi
import os

id_EPCI = 'E-200069169'

chemin_fichier_resultats_API = r'../../resultats/2021_05_15_Resultats_API_crater.csv'
chemin_fichier_EPCI = r'../../crater-data-resultats/territoires/epcis.csv'

chemin_sauvegarde = r'../../crater-data-exploration/classification/resultats/3_radviz_epci'

if chemin_sauvegarde != '':
    if not os.path.exists(chemin_sauvegarde):
        os.makedirs(chemin_sauvegarde)


# Chargement
df = outils.charger_resultats_crater(chemin_fichier_resultats_API)
df = df[df['categorie_territoire']=='EPCI']
colonnes = ['id_territoire', 'nom_territoire','note_politique_fonciere','note_population_agricole','note_pratiques_agricoles','note_production_besoins']
df = df[colonnes]

donnees_epcis = pd.read_csv(chemin_fichier_EPCI, sep=';')
donnees_epcis = donnees_epcis.rename(columns={'code_insee':'id_territoire','nom':'nom_territoire'})

epcis = pd.merge(left=df, right=donnees_epcis, on=['id_territoire','nom_territoire'])

epcis_mean = epcis.groupby(by='categorie').mean()
epcis_cnt = epcis.groupby(by='categorie').count()

EPCI = epcis[epcis['id_territoire']==id_EPCI]


categories=['note_politique_fonciere','note_population_agricole','note_pratiques_agricoles','note_production_besoins']
categories_2=['politique fonciere', 'population agricole', 'pratiques agricoles', 'production besoins']
N = len(categories)
angles = [n / float(N) * 2 * pi for n in range(N)]
angles += angles[:1]
 

 
plt.figure(figsize=(16,8),dpi=150)
ax = plt.subplot(121,polar=True)
plt.xticks(angles[:-1], categories_2, color='grey',size=15)
ax.set_rlabel_position(0)
plt.yticks([2,4,6,8,10], ['2','4','6','8','10'], color="grey")
plt.ylim(0,10)
for type_epci in ['METROPOLE','COMMUNAUTE_COMMUNES','COMMUNAUTE_AGGLOMERATION','COMMUNAUTE_URBAINE']:
    lw=1
    if EPCI['categorie'].iloc[0] == type_epci:
        lw=3
    values=epcis_mean.loc[type_epci].tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=lw, linestyle='solid', label=type_epci)
    ax.fill(angles, values, alpha=0.1)
values=EPCI[categories].iloc[0].tolist()
values += values[:1]
ax.plot(angles, values, 'r', linewidth=4, linestyle='solid', label=EPCI['nom_territoire'].iloc[0])
ax.fill(angles, values, alpha=0.1)

plt.legend(loc='upper left')
handles, labels = ax.get_legend_handles_labels()
ax.get_legend().remove()

ax=plt.subplot(122)
ax.set_axis_off()
plt.legend(handles, labels, loc='center',fontsize=15)

plt.suptitle(EPCI['nom_territoire'].iloc[0], size=20)
plt.tight_layout()

if chemin_sauvegarde != '':
    nom = EPCI['nom_territoire'].iloc[0].replace(' ','_') + '.png'
    plt.savefig(os.path.join(chemin_sauvegarde,nom))










