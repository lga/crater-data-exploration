# Classification des territoires à partir des résultats CRATer

## Analyse des données

### Normalisation

A partir des résutats CRATer, je ne garde dans un 1er temps que les indicateurs de plus haut niveau, en faisant par exemple l'impasse sur les types de cultures, ou le détail des indicateurs hvn, pour essayer de ne pas introduire trop de corrélations artificielles.

J'ai aussi ajouté un calcul de la densité de population et du taux global de couverture des besoins (plutôt que de détailler par type de culture).

Ensuite je normalise les données, pour avoir des indicateurs qui soient représentatives de la distribution des valeurs, et qui varient sur des échelles comparables:

- division par la valeur max pour tout mettre entre 0 et 1
- je prends le log pour les données >0 et j'attribue arbitrairement la valeur -10 aux données qui valaient 0

(Rq : on peut surement faire mieux sur certains indicateurs, comme le score hvn qui n'a a priori pas de raison d'être renormalisé).

Ca permet d'obtenir des histogrammes à peu près lisibles. On voit que les distributions sont assez homogènes.

![Repartition des donnees CRATer avant et après normalisation.](histogrammes.png)

### Corrélations

Calcul des coefficients de corrélations sur les données brutes, renormalisées et renormalisées en ne gardant que les données positives (sur certains indicateurs il y a énormément de valeurs à zéro, ça impact les corrélations de manière significative).

![Corrélations](heatmaps.png)


Pour avoir une idée un peu plus quantitative, on affiche les scatter plots correspondants, en prenant ou non en compte les données nulles :

![Corrélations entre les indicateurs CRATer](correlations.png)

![Corrélations entre les indicateurs CRATer (données positives)](correlations_seuil_.png)

Conclusions préliminaires :

- Pas de cluster qui ressorte de manière franche de cette première analyse
- De manière générale, bonne continuité entre les types de territoires (commune - epci - dep. - region), même si la méthode de calcul de certains indicateurs biaise un peu la répartition.

Pour la suite de l'analyse, on se place au niveau des communes uniquement. On superpose aux scatter plots un histogramme, pour avoir une idée de la répartition :

![Corrélations entre les indicateurs CRATer pour les communes](COMMUNE.png)

Sur certains indicateurs, les données nulles occupent une place importante (par exemple, environ 80% des communes ont une SAU bio qui est nulle...)

Si on regarde uniquement les données non nulles pour chaque paire d'indicateurs :

![Corrélations entre les indicateurs CRATer pour les communes (donnees positives)](COMMUNE_-_seuil.png)

Sur certaines paires de paramètres, on voit une corrélation nette, par exemple 'besoin assiette' vs 'densité de population'. Cette corrélation est en fait introduite par le mode de calcul :

- le besoin est proportionnel à la population d'un territoire
- la densité de population est proportionnelle à la population (avec cette normalisation la superficie des communes est très homogène)
- On corrèle donc la population avec elle-même...

J'ai essayé de conserver uniquement les indicateurs 'indépendants', pour éviter de biaiser les corrélations :

![Corrélations entre les indicateurs CRATer pour les communes (donnees positives, indicateurs non corrélés)](COMMUNE_-_seuil_subset.png)

### Recherche de clusters

On essaye de voir si on peut faire des choses plus poussées pour faire apparaître des clusters dans les données. Première analyse rapide en utilisant [Orange](https://orangedatamining.com/), sur les données normalisées des communes.

Je n'ai gardé que les indicateurs qui n'ont pas trop de valeurs nulles (sinon on voit apparaître des groupes 'triviaux' qui correspondent aux communes pour lesquelles une forte proportion a un indicateur nul --> ça pourrait en fait constituer un 1er niveau de classification, même s'il n'apporte pas grand chose). Cela revient surtout à supprimer la SAU bio, la population agricole et le rythme d'articialisation des sols.

- Je commence avec une analyse en composantes principales (PCA), sur 2 composantes. On voit que la répartition des points se corrèle avec la population, qui est en fait le paramètre qui introduit le plus de corrélation dans les données

![Analyse PCA sur les données CRATer](PCA.png)

- On regarde ensuite avec la méthode [t-SNE](https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html#sklearn.manifold.TSNE), qui est aussi une méthode de réduction du nombre de dimensions. Là encore, pas de structure claire, et on retrouve une corrélation de la sortie avec la population

![Analyse tSNE sur les données CRATer](tSNE.png)

- On essaye de chercher des clusters avec la méthode [k-Means](https://scikit-learn.org/stable/modules/clustering.html#k-means). Ici j'ai forcé le nombre de clusters à 8 pour essayer de dégager des choses. On obtient bien des clusters, mais qui ne sont pas statistiquement résolus. Si on laisse le paramètre libre, la méthode converge vers 1 seul groupe.

![Analyse k-means sur les données CRATer](kmeans.png)

## Conclusions préliminaires

- pas de structure qui émerge naturellement des données à ce stade : les données semblent très homogènes
- on peut surement améliorer la manière de normaliser, ou ajouter d'autres indicateurs, mais il est peu probable que ça modifie le comportement de manière significative
- sur certains indicateurs, un facteur de tri pourrait être 'nul' ou 'non nul' (exemple : sau bio, articialisation, population agricole)
- on peut par contre regarder comment un territoire donné se compare aux territoires de taille similaire / populatin similaire, etc (donc regarder où on se situe dans les distributions plutôt que dans une classification)


## Ajout du 17/05/2021

### Corrélations

Corrélations pour les départements et EPCI. Pour les départements, la statistique est un peu faible pour ce type de représentation.

![Corrélations entre les indicateurs CRATer pour les EPCI](EPCI.png)

![Corrélations entre les indicateurs CRATer pour les EPCI](EPCI_-_seuil.png)

![Corrélations entre les indicateurs CRATer pour les départements](DEPARTEMENT.png)

![Corrélations entre les indicateurs CRATer pour les départements](DEPARTEMENT_-_seuil.png)

### PCA

Même graphe, mais en colorant selon la densité de population. On voit que le groupe qui se dégage à droite correspond aux grandes villes.

Remarque : toute cette analyse a été faite sur un petit échantillon de communes (environ 2000) tirées au hasard pour réduire le temps de calcul.

![Analyse PCA sur les données CRATer](PCA_2.png)

