# Affichage des résultats CRATer

## Notes CRATer

![Notes CRATer](./notes_crater.png)

## Territoires

![population](./population.png)

![superficie_ha](./superficie_ha.png)

![densite_population_hab_km2](./densite_population_hab_km2.png)


## SAU

![sau_ha](./sau_ha.png)

![sau_bio_ha](./sau_bio_ha.png)

![sau_CER_ha](./sau_CER_ha.png)

![sau_DVC_ha](./sau_DVC_ha.png)

![sau_FLC_ha](./sau_FLC_ha.png)

![sau_FOU_ha](./sau_FOU_ha.png)

![sau_OLP_ha](./sau_OLP_ha.png)

![sau_SNC_ha](./sau_SNC_ha.png)


## Politique foncière

![sau_par_habitant_m2](./sau_par_habitant_m2.png)

![rythme_artificialisation_sau_pourcent](./rythme_artificialisation_sau_pourcent.png)


## Population agricole

![population_agricole_1988](./population_agricole_1988.png)

![population_agricole_2010](./population_agricole_2010.png)

![part_population_agricole_1988](./part_population_agricole_1988.png)

![part_population_agricole_2010](./part_population_agricole_2010.png)


## Pratiques agricoles

![hvn_indice_1](./hvn_indice_1.png)

![hvn_indice_2](./hvn_indice_2.png)

![hvn_indice_3](./hvn_indice_3.png)

![hvn_indice_total](./hvn_indice_total.png)

![part_sau_bio_pourcent](./part_sau_bio_pourcent.png)


## Adéquation production - besoins

![besoin_assiette_actuelle_ha](./besoin_assiette_actuelle_ha.png)

![taux_couverture_besoin_total](./taux_couverture_besoin_total.png)

![besoin_CER_ha](./besoin_CER_ha.png)

![taux_couverture_besoin_CER_ha](./taux_couverture_besoin_CER_ha.png)

![besoin_DVC_ha](./besoin_DVC_ha.png)

![taux_couverture_besoin_DVC_ha](./taux_couverture_besoin_DVC_ha.png)

![besoin_FLC_ha](./besoin_FLC_ha.png)

![taux_couverture_besoin_FLC_ha](./taux_couverture_besoin_FLC_ha.png)

![besoin_FOU_ha](./besoin_FOU_ha.png)

![taux_couverture_besoin_FOU_ha](./taux_couverture_besoin_FOU_ha.png)

![besoin_OLP_ha](./besoin_OLP_ha.png)

![taux_couverture_besoin_OLP_ha](./taux_couverture_besoin_OLP_ha.png)

