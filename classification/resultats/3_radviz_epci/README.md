# Visualisation des notes CRATer pour les EPCI

Le script radviz.py permet de tracer le radar des notes CRATer pour un EPCI donné, et de comparer à la moyenne des EPCI de chaque catégorie (Métropole, communauté d'agglo, communauté de communes, communauté urbaine).


## Exemple pour des EPCI autour de Clermont-Ferrand

![Clermont_Auvergne_Métropole](Clermont_Auvergne_Métropole.png)

![CA_Riom_Limagne_et_Volcans](CA_Riom_Limagne_et_Volcans.png)

![CC_Billom_Communauté](CC_Billom_Communauté.png)

![CC_Dômes_Sancy_Artense](CC_Dômes_Sancy_Artense.png)

![CC_Mond'Arverne_Communauté](CC_Mond'Arverne_Communauté.png)