# Comparaison d'un commune aux communes similaires

## Méthode

Pour une commune donnée, on cherche à savoir comment elle se compare aux communes similaires, pour l'ensemble des indicateurs CRATer.

Pour l'instant, l'analyse est effectuée sur la population et la densité de population, mais on peut généraliser.

On commence par sélectionner les communes dont la population est comprise entre P/sqrt(2) et P*sqrt(2), où P est la population de la commune en question. De cette manière on garde un échantillon de communes ayant toutes des populations proches (au pire un facteur 2).

Ensuite, on regarde comment la commune choisie se compare à cet échantillon. Pour faciliter l'affichage, les boîtes à moustache sont en échelle log, et on a divisé par la valeur médiane.

On effectue la même analyse sur la densité de population.

## Exemples de résultats
Quelques exemples, en partant d'une petite commune pour aller vers une grande ville.

![Etsaut](./Etsaut.png)
![Beaubery](./Beaubery.png)
![Sainte-Foy-de-Peyrolières](./Sainte-Foy-de-Peyrolières.png)
![Balma](./Balma.png)
![Bordeaux](./Bordeaux.png)