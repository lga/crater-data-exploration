# Comparaison d'un territoire aux territoires de même rang

Le script comparaison_territoire_parent.py permet de comparer une commune aux autres communes de l'EPCI, ou un EPCI aux autres EPCI du département, ou un département aux départements de la région, etc...

La comparaison se fait sur le même principe que la comparaison d'une commune aux communes similaires ([Voir ici](../4_comparaison_communes/README.md))

On ajoute aussi un radar avec les notes CRATer du territoire, comparées aux notes maximales et médianes.

![Balma](Balma.png)

![Toulouse-Métropole](Toulouse_Métropole.png)

![Haute-Garonne](Haute-Garonne.png)

![Occitanie](Occitanie.png)
