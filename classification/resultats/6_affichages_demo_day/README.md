# Affichage classifications pour les demo days

code utilisé : [demo_day.py](../../demo_day.py) et [outils_visu_demo_day.py](../../outils_visu_demo_day.py)

## Idée générale

L'objectif est de compléter les résultats de CRATer, en ajoutant des statistiques et des comparaisons aux départements de la même région.

Pour cela, on a généré des fichiers csv qui contiennent différentes valeurs statistiques pour tous les indicateurs CRATer, à l'échelle de la région et de la France. Les statisitiques sont faites sur les départements de la région, ou sur l'ensemble des départements français.


Ci-dessous 3 exemples de ce qu'on pourrait afficher pour la démo D4G. Toutes les valeurs sont accessibles soit via l'API CRATer, soit en utilisant les fichiers csv pour les statistiques.
- On complète le radar en ajoutant la moyenne sur les départements de la région et la moyenne sur les départements français
- Un ensemble de graphes sur les caractéristiques du département
- Un ensemble de graphes par catégorie CRATer

## Lozère

### Diagnostic du système alimentaire

Notes CRATer du département, comparées à la moyenne des départements de la région et à la moyenne des départements français.

![Lozère_radar](D-48_radar.png)

### Caractéristiques du territoire

Comparaison du département aux autres départements de la région, portant sur les caractéristiques géographiques et démographiques.

![Lozère_caracteristiques](D-48_caracteristiques.png)

### Adéquation production / besoins

Comparaison du taux de couverture des besoins des départements de la région, pour chacun des principaux groupes de cultures (CER : céréales, FLC : fruits et légumes, OLP : oléoprotéagineux, FOU : fourrages, DVC : autres cultures).

![Lozère_production_besoins](D-48_production_besoins.png)

### Pratiques agricoles

Comparaison des pratiques agricoles entre les départements de la région. Les indicateurs hvn (haute valeur naturelle) sont calculés par Solagro.

![Lozère_pratiques_agricoles](D-48_pratiques_agricoles.png)

### Population agricole

Comparaison des populations agricoles, et de leur évolution entre 1998 et 2010.

![Lozère_population_agricole](D-48_population_agricole.png)

### Politique foncière

Comparaison des indicateurs fonciers (SAU par habitant et artificialisation des sols).

![Lozère_politique_fonciere](D-48_politique_fonciere.png)


## Gironde

![Gironde_radar](D-33_radar.png)

![Gironde_caracteristiques](D-33_caracteristiques.png)

![Gironde_production_besoins](D-33_production_besoins.png)

![Gironde_pratiques_agricoles](D-33_pratiques_agricoles.png)

![Gironde_population_agricole](D-33_population_agricole.png)

![Gironde_politique_fonciere](D-33_politique_fonciere.png)


## Drôme

![Drôme_radar](D-26_radar.png)

![Drôme_caracteristiques](D-26_caracteristiques.png)

![Drôme_production_besoins](D-26_production_besoins.png)

![Drôme_pratiques_agricoles](D-26_pratiques_agricoles.png)

![Drôme_population_agricole](D-26_population_agricole.png)

![Drôme_politique_fonciere](D-26_politique_fonciere.png)

