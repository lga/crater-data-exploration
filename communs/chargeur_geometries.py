import geopandas
import pandas

from communs.config import CHEMIN_CRATER_DATA_SOURCES
from communs.crater_logger import log
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater, \
    traduire_code_insee_vers_id_departement_crater, traduire_code_insee_vers_id_region_crater, \
    traduire_code_insee_vers_id_epci_crater

CHEMIN_FICHIER_GEOMETRIES_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_communes/2021/communes-20210101/communes-20210101.shp'
CHEMIN_FICHIER_GEOMETRIES_EPCIS = CHEMIN_CRATER_DATA_SOURCES / 'banatic/geometries_epcis/2021/Métropole/EPCI 2021_region.shp'
CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_departements/2018/departements-20180101-shp/departements-20180101.shp'
CHEMIN_FICHIER_GEOMETRIES_REGIONS = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_regions/2018/regions-20180101.shp'
CHEMIN_FICHIER_CARROYAGES_INSEE = CHEMIN_CRATER_DATA_SOURCES / 'insee/carroyage/2015_carreaux_200m/Filosofi2015_carreaux_200m_metropole.gpkg'


def charger_geometries_communes(chemin_fichier_geometries_communes):
    gdf = geopandas.read_file(chemin_fichier_geometries_communes)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)
    gdf['id_commune'] = traduire_code_insee_vers_id_commune_crater(gdf['insee'])
    gdf = gdf.rename(columns={'id_commune': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_departements(chemin_fichier_geometries_departement):
    gdf = geopandas.read_file(chemin_fichier_geometries_departement)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)

    # traitement du département du rhone : fusion de 2 lignes (code 69D et 69M en une seule)
    mask_departements_rhone = gdf['code_insee'].str.startswith('69')
    gdf_rhone = geopandas.GeoDataFrame(pandas.DataFrame(data={'code_insee': ['69']}), geometry=[gdf.loc[mask_departements_rhone, :].geometry.unary_union],
    crs="EPSG:2154")
    gdf = gdf.loc[~mask_departements_rhone, :].append(gdf_rhone)


    gdf['id_departement'] = traduire_code_insee_vers_id_departement_crater(gdf['code_insee'])
    gdf = gdf.rename(columns={'id_departement': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_regions(chemin_fichier_geometries_region):
    gdf = geopandas.read_file(chemin_fichier_geometries_region)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)
    gdf['id_region'] = traduire_code_insee_vers_id_region_crater(gdf['code_insee'])
    gdf = gdf.rename(columns={'id_region': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_epcis(chemin_fichier_geometries):
    gdf = geopandas.read_file(chemin_fichier_geometries)
    gdf['id_territoire'] = traduire_code_insee_vers_id_epci_crater(gdf['SIREN'].astype('Int64').astype('str'))
    return gdf.loc[:, ['id_territoire', 'geometry']]


def _charger_geometries(chemin_fichier_geometries_regions, chemin_fichier_geometries_departements,
                        chemin_fichier_geometries_epcis, chemin_fichier_geometries_communes=None):
    gdf_contours = charger_geometries_regions(chemin_fichier_geometries_regions)
    gdf_contours = gdf_contours.append(charger_geometries_departements(chemin_fichier_geometries_departements))
    gdf_contours = gdf_contours.append(charger_geometries_epcis(chemin_fichier_geometries_epcis))
    if (chemin_fichier_geometries_communes is not None):
        gdf_contours = gdf_contours.append(charger_geometries_communes(chemin_fichier_geometries_communes))
    return gdf_contours


def _charger_geometries_territoire_cible(id_territoire_perimetre_carte, categorie_territoire,
                                         chemin_fichier_geometries_communes, chemin_fichier_geometries_departements,
                                         chemin_fichier_geometries_epcis, chemin_fichier_geometries_regions,
                                         chemin_fichier_referentiel_territoires):
    gdf = _charger_geometries(chemin_fichier_geometries_regions, chemin_fichier_geometries_departements,
                              chemin_fichier_geometries_epcis, chemin_fichier_geometries_communes)
    log.info(f"  - Chargement referentiel territoires")
    df_territoires = pandas.read_csv(chemin_fichier_referentiel_territoires, sep=";")
    gdf = gdf.merge(df_territoires)
    gdf_partiel = gdf.loc[(gdf['categorie_territoire'] == categorie_territoire)
                          & ((gdf['id_pays'] == id_territoire_perimetre_carte)
                             | (gdf['id_region'] == id_territoire_perimetre_carte)
                             | (gdf['id_departement'] == id_territoire_perimetre_carte)
                             | (gdf['id_epci'] == id_territoire_perimetre_carte)
                             ), :]
    return gdf_partiel