import pandas

from communs.config import CHEMIN_CRATER_DATA_SOURCES
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater

CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / 'insee/grille_communale_densite/2021/grille_densite_2021_agrege.xlsx'


def charger_grille_densite_communes(chemin_fichier_grille_densite = CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNES):
    df_grille_densite = pandas.read_excel(chemin_fichier_grille_densite).rename(columns={
        'Degré de \nDensité de la commune\n': 'indice_densite'
    })
    df_grille_densite['categorie_densite'] = df_grille_densite['indice_densite'].map(
        {
            1: 'DENSEMENT_PEUPLEE',
            2: 'DENSITE_INTERMEDIAIRE',
            3: 'PEU_DENSE',
            4: 'TRES_PEU_DENSE'
        }
    )
    df_grille_densite['id_commune'] = traduire_code_insee_vers_id_commune_crater(
        df_grille_densite['\nCode \nCommune\n'])
    return df_grille_densite.loc[:, ['id_commune', 'categorie_densite', 'indice_densite']]

def ajouter_colonnes_indice_densite_commune(df):
    df_grille_densite = charger_grille_densite_communes(CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNES)

    return df.merge(df_grille_densite, on='id_commune')
