from pathlib import Path

CHEMIN_CRATER_DATA_SOURCES = Path('../../../crater-data-sources')
CHEMIN_CRATER_DATA_RESULTATS = Path('../../../crater-data-resultats')
CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES = CHEMIN_CRATER_DATA_RESULTATS / 'data/crater/territoires/referentiel_territoires.csv'

