import io
import shutil
from pathlib import Path

from pandas import DataFrame


def reinitialiser_dossier(nom_dossier: Path):
    if nom_dossier.exists():
        shutil.rmtree(nom_dossier)
    nom_dossier.mkdir(parents=True)


def exporter_csv(df: DataFrame, fichier_csv: Path, avec_index=False):
    df.to_csv(
        fichier_csv,
        sep=";",
        encoding="utf-8",
        index=avec_index,
        float_format='%.2f'
    )


def exporter_description_dataframe(df: DataFrame, fichier_sortie: Path):
    buffer = io.StringIO()
    df.info(verbose=True, buf=buffer)
    s = buffer.getvalue()
    with open(fichier_sortie, "w", encoding="utf-8") as f:
        f.write(s)


def exporter_csv_et_description(df: DataFrame, dossier_sortie: Path, nom_fichier_sans_extension, avec_index=False):
    exporter_csv(df, dossier_sortie / (nom_fichier_sans_extension + '.csv'), avec_index)
    exporter_description_dataframe(df, dossier_sortie / (nom_fichier_sans_extension + '.txt'))
