from pathlib import Path

import numpy as np
from geopandas import GeoDataFrame
from matplotlib import pyplot
from pandas import DataFrame

from communs.crater_logger import log


def exporter_carte(gdf: GeoDataFrame, df: DataFrame, nom_colonne_indicateur: str, fichier_resultat: Path, bins):
    log.info(f"Exporter carte {fichier_resultat}")
    pyplot.close('all')

    df = df.loc[:, ['id_territoire', nom_colonne_indicateur]].replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    gdf_indicateur = gdf.merge(df, on='id_territoire', how='left')
    if (bins is None):
        gdf_indicateur.plot(column=nom_colonne_indicateur,
                        legend=True,
                        figsize=(15, 15),
                        cmap='summer'
                        )
    else:
        gdf_indicateur.plot(column=nom_colonne_indicateur,
                        legend=True,
                        figsize=(15, 15),
                        scheme="User_Defined",
                        classification_kwds=dict(bins=bins),
                        cmap='summer'
                        )

    pyplot.savefig(fichier_resultat)
