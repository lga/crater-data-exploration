import logging

import requests
import pandas as pd
import time

logger = logging.getLogger('')


class AbstractApiReader:
    def read_to_data_frame(self):
        raise NotImplementedError("Method load from AbstractApiReader can't be used")


class SireneApiReader:

    BASE_PATH = 'https://entreprise.data.gouv.fr/api/sirene/v3'

    def read_to_data_frame(self, main_activity_code: str, page_size: int, base_path: str,
                           max_pages: int = None, rate_limit_in_seconds=1):
        response = self._get_page(1, page_size, base_path, main_activity_code)
        page_count = response.json()['meta']['total_pages']
        list_etablissements = response.json()['etablissements']

        pages_to_read = self._calculate_pages_to_read(max_pages, page_count)

        print(f"Requete a l'API SIRENE : {pages_to_read} pages à récupérer de taille {page_size} pour le code activité {main_activity_code}")

        for page in range(2, pages_to_read + 1):
            print(f"Récupération de la page {page} sur {pages_to_read}")
            time.sleep(rate_limit_in_seconds)
            response = self._get_page(page, page_size, base_path, main_activity_code)
            list_etablissements += response.json()['etablissements']

        frame = pd.DataFrame(list_etablissements)
        return frame

    def _calculate_pages_to_read(self, max_pages: int, page_count: int) -> int:
        if max_pages is None:
            pages_to_read = page_count
        else:
            pages_to_read = min(page_count, max_pages)
        return pages_to_read

    def _get_page(self, page: int, page_size: int, base_path: str = BASE_PATH, activite_principale='49.41A') -> requests.Response:
        params = {'activite_principale': activite_principale, 'etat_administratif': 'A', 'per_page': page_size, 'page': page}
        URL = f"{base_path}/etablissements"
        logger.info("Getting page %s from %s with params %s", page, URL, params)
        try:
            response = requests.get(
                URL, params=params)
            if response.status_code == 200:
                return response
            else:
                raise ApiReadError(f'Unexpected response status code : {response.status_code}')
        except Exception as ex:
            logger.warning(f"Exception while reading from {URL} : {ex}")
            raise ApiReadError('error while reading from api')


class ApiReadError(Exception):
    pass
