from pathlib import Path

import geopandas
import pandas
from distance_moyenne_aux_commerces.collecte_donnees_sirene.api_reader import SireneApiReader

input = Path('input')
output = Path('output')

SIRENE_API_BASE_PATH = 'https://entreprise.data.gouv.fr/api/sirene/v3'

def collecter_donnees_api_sirene(fichier_codes_activite: Path, fichier_resultat: Path, max_pages=None):
    apiReader = SireneApiReader()
    if fichier_codes_activite:
        codes_activite = pandas.read_csv(fichier_codes_activite, sep=';', dtype=str)['code']
        df_etablissements_sirene = None
        for code_activite in codes_activite:
            print(f"Récupération des établissement SIRENE pour le code activité {code_activite}")
            df_etablissements_pour_code_activite = apiReader.read_to_data_frame(main_activity_code=code_activite,
                                                                                page_size=100,
                                                                                base_path=SIRENE_API_BASE_PATH,
                                                                                max_pages=max_pages,
                                                                                rate_limit_in_seconds=1 / 5)
            if df_etablissements_sirene is not None:
                df_etablissements_sirene = df_etablissements_sirene.append(df_etablissements_pour_code_activite,
                                                                           ignore_index=True)
            else:
                df_etablissements_sirene = df_etablissements_pour_code_activite
    #  TODO : voir comment filtrer et quand filtrer
    # df_etablissements_sirene.loc[:, [
    #     'siren',
    #     'siret',
    #     'code_commune',
    #     'libelle_commune',
    #     'enseigne_1',
    #     'denomination_usuelle',
    #     'activite_principale',
    #     'longitude',
    #     'latitude',
    #     'geo_score']]

    df_etablissements_sirene.to_csv(fichier_resultat, sep=';', index=False)


def extract_geodata_for_logistic_companies(sirene_cas_filepath: Path, result_filepath: Path) -> None:
    donnees_sirene = pandas.read_csv(sirene_cas_filepath,
                                     sep=';')
    donnees_sirene['code_pays_etranger'] = donnees_sirene['code_pays_etranger'].fillna(0)
    extract_points_with_geoloc = donnees_sirene.loc[
        (donnees_sirene.longitude != 0)
        & (donnees_sirene.latitude != 0)
        & (donnees_sirene.code_pays_etranger == 0)
        & (donnees_sirene.code_postal < 97000),
        # pourquoi le code postal?
        ['id', 'code_postal', 'activite_principale', 'latitude', 'longitude']
    ]
    # load latitude/longitude into GeoDataFrame, we take crs="EPSG:4326" as it is the latitude-longitude projection (also called  WGS84 )
    gdf = geopandas.GeoDataFrame(
        extract_points_with_geoloc,
        geometry=geopandas.points_from_xy(extract_points_with_geoloc.longitude, extract_points_with_geoloc.latitude,
                                          crs="EPSG:4326"))
    # translate to the same csr as the one used by iris shp file (ie lambert projection)
    gdf = gdf.to_crs("EPSG:2154")
    gdf.to_file(result_filepath, driver="GPKG")


def extract_relevant_columns_from_sirene_data(sirene_result_file_path: Path, logistic_premises_file_path: Path):
    sirene_data = pandas.read_csv(sirene_result_file_path, sep=";", dtype=str)
    logistic_premises_data = sirene_data.loc[:,
                             ['id', 'siret', 'numero_voie', 'type_voie', 'libelle_voie', 'code_postal',
                              'libelle_commune',
                              'code_pays_etranger', 'activite_principale', 'longitude', 'latitude', 'geo_adresse']]

    logistic_premises_data['code_pays_etranger'] = logistic_premises_data['code_pays_etranger'].fillna(0)
    logistic_premises_data['latitude'] = logistic_premises_data['latitude'].fillna(0)
    logistic_premises_data['longitude'] = logistic_premises_data['longitude'].fillna(0)
    logistic_premises_data.to_csv(logistic_premises_file_path, index=False, sep=";")


if __name__ == '__main__':
    collecter_donnees_api_sirene(input / 'codes_activites_sirene.csv',
                                 output / 'resultat_api_sirene.csv')
    # sirene.extract_relevant_columns_from_sirene_data(
    #     output / 'resultat_api_sirene.csv',
    #     output / 'commerces_alimentaires_sirene.csv')



