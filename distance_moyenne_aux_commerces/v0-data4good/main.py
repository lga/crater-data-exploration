import pandas
import pandas as pd
import numpy as np
from shapely.geometry import Point
import geopandas as gpd
import matplotlib.pyplot as plt


def charger_donnees_bpe():
    print("----Chargement données BPE----")
    filename = "data/bpe19_ensemble_xy_csv/bpe19_ensemble_xy.csv"
    data_bpe = pd.read_csv(filename, sep=";")
    print(data_bpe.shape)
    return data_bpe


def construction_bpe_concat():
    print("----Construction BPE concat----")
    ## Types de commerces
    # DCIRIS : Département, commune et IRIS d’implantation de l’équipement
    # TYPEQU : Prendre tous lles commerces alimentaires
    # B101 - Hypermarché, B102 - Supermarché, B201 - Supérette, B202 - Épicerie,
    # B203 - Boulangerie, B204 - Boucherie charcuterie,
    # B205 - Produits surgelés, B206 - Poissonnerie
    list_alimentation = ['B101', 'B102', 'B201', 'B202', 'B203', 'B204', 'B205', 'B206']
    data_bpe_alimentaire = data_bpe[data_bpe.TYPEQU.isin(list_alimentation)]
    data_bpe_alimentaire = data_bpe_alimentaire[data_bpe_alimentaire.QUALITE_XY != 'Non géolocalisé'].reset_index(
        drop=True)
    data_bpe_alimentaire[
        'isCommerceAlimentaire'] = 1  # création d'une variable binaire pour vérifier qu'il s'agit bien d'un commerce alimentaire
    print("Nombre de communes avec au moins un commerce alimentaire : {}".format(len(set(data_bpe_alimentaire.DEPCOM))))
    data_bpe_vide = data_bpe[~data_bpe.DEPCOM.isin(data_bpe_alimentaire.DEPCOM)]
    data_bpe_vide = data_bpe_vide[data_bpe_vide.QUALITE_XY.isin(['Bonne', 'Acceptable', 'Mauvaise'])].reset_index(
        drop=True)
    print("Nombre de communes sans commerce alimentaire : {}".format(len(set(data_bpe_vide.DEPCOM))))
    ## Sélectionner une seule ligne par territoire avec un commerce nonalimentaire
    data_bpe_vide = data_bpe_vide.sample(frac=1).groupby(['REG', 'DEP', 'DEPCOM', 'DCIRIS'], sort=False).head(1)
    data_bpe_vide['isCommerceAlimentaire'] = 0
    ## Renommage des variables
    data_bpe_alimentaire = data_bpe_alimentaire.rename(columns={'REG': 'id_region', 'DEP': 'id_departement',
                                                                'DEPCOM': 'id_commune', 'DCIRIS': 'id_iris',
                                                                'AN': 'annee', 'TYPEQU': 'code_equipement_bpe',
                                                                'LAMBERT_Y': 'coordonnee_y',
                                                                'LAMBERT_X': 'coordonnee_x',
                                                                'QUALITE_XY': 'qualite_geolocalisation'})
    data_bpe_vide = data_bpe_vide.rename(columns={'REG': 'id_region', 'DEP': 'id_departement',
                                                  'DEPCOM': 'id_commune', 'DCIRIS': 'id_iris',
                                                  'AN': 'annee', 'TYPEQU': 'code_equipement_bpe',
                                                  'LAMBERT_Y': 'coordonnee_y', 'LAMBERT_X': 'coordonnee_x',
                                                  'QUALITE_XY': 'qualite_geolocalisation'})
    ## Création d'une variable typologie de commerce
    data_bpe_alimentaire['taille_commerce'] = 'petite'
    # B101 - Hypermarché, B102 - Supermarché
    data_bpe_alimentaire.loc[
        data_bpe_alimentaire['code_equipement_bpe'].isin(['B101', 'B102']), 'taille_commerce'] = 'grande'
    # B201 - Supérette, B202 - Épicerie, B205 - Produits surgelés
    data_bpe_alimentaire.loc[
        data_bpe_alimentaire['code_equipement_bpe'].isin(['B201', 'B202', 'B205']), 'taille_commerce'] = 'moyenne'
    data_bpe_alimentaire.taille_commerce.value_counts().sort_values().plot(kind='barh',
                                                                           title='Typologie de commerce en France métropolitaine \n (en milliers)')
    plt.legend(loc='best')
    plt.show()
    ## Concatenation des tables d'abord (territoires avec commerces alimentaires vs reste)
    # print(data_bpe_alimentaire.shape, data_bpe_vide.shape)
    data_bpe_concat = pd.concat([data_bpe_alimentaire, data_bpe_vide], axis=0)
    # print(data_bpe_concat.shape)
    geometry = [Point(xy) for xy in zip(data_bpe_concat['coordonnee_x'], data_bpe_concat['coordonnee_y'])]
    crs = {'init': 'epsg:2154'}  # Coordinate reference system : Lambert (norme française)
    # Creating a Geographic data frame
    data_bpe_concat = gpd.GeoDataFrame(data_bpe_concat, crs=crs, geometry=geometry)
    # print(data_bpe_concat.shape)
    # data_bpe_concat.head(1)
    return data_bpe_concat


def chargement_donnees_insee():
    print("----chargement données insee----")
    filename = "../data/Filosofi2015_carreaux_1000m_shp/Filosofi2015_carreaux_1000m_metropole_shp/Filosofi2015_carreaux_1000m_metropole.shp"
    data_insee = gpd.read_file(filename)
    data_insee = data_insee[['Id_carr1km', 'geometry', 'Ind']]
    return data_insee


if __name__ == '__main__':
    crs = {'init': 'epsg:2154'}
    data_bpe = charger_donnees_bpe()
    data_bpe.head(1)
    data_bpe_concat = construction_bpe_concat()
    data_insee = chargement_donnees_insee()


    print("Ajout des centroids sur carreaux insee")
    # on calcule en premier le centroid de chaque carreau
    data_insee['polygonCentroid'] = data_insee['geometry'].centroid

    print("Etendre les carreaux insee à x km de chaque coté")
    # on agrandi chaque carroyage à x km de chaque coté
    gs_carroyage_insee_agrandi = data_insee['geometry'].envelope
    gs_carroyage_insee_agrandi = gs_carroyage_insee_agrandi.buffer(3000, cap_style = 3).envelope

    gdf_insee_agrandi = data_insee
    gdf_insee_agrandi['geometry'] = gs_carroyage_insee_agrandi


    print("jointure spatiale entre bpe et insee")
    pointsInPolygon = gpd.sjoin(data_bpe_concat, gdf_insee_agrandi, how='inner', op='intersects')
    pointsInPolygon = pointsInPolygon.reset_index(drop=True)
    print(f'Résultat jointure spatiale {pointsInPolygon.shape}')

    gdf_commerces_carreaux = pointsInPolygon.loc[pointsInPolygon.isCommerceAlimentaire == 1]
    gdf_commerces_carreaux['centroid_carreau_x'] = gdf_commerces_carreaux.polygonCentroid.x
    gdf_commerces_carreaux['centroid_carreau_y'] = gdf_commerces_carreaux.polygonCentroid.y

    df_commerces_carreaux = pandas.DataFrame(gdf_commerces_carreaux.loc[:, ['id_departement', 'id_commune', 'code_equipement_bpe', 'coordonnee_x', 'coordonnee_y', 'taille_commerce', 'Id_carr1km', 'Ind', 'centroid_carreau_x', 'centroid_carreau_y']])
    df_commerces_carreaux.to_csv('data/jointure_commerces_carreaux.csv', sep=";")

    ## On choisit un département
    # dep = '85' # Vendée
    # pointsInPolygon = pointsInPolygon[pointsInPolygon.id_departement==dep]
    # reg = pointsInPolygon.id_region.unique()[0]
    #
    #
    # # 2- On récupère tous les centroides uniques des carreaux de la région du département (Pays de le Loire)
    # gdf_carreaux_pris_en_compte = pointsInPolygon.groupby('Id_carr1km').nunique().loc[:, ['Id_carr1km', 'Ind', 'polygonCentroid']]
    #
    # gdf_commerces_alimentaires_par_carreau = pointsInPolygon[pointsInPolygon.isCommerceAlimentaire==1]
    #
    # for i in range(len(gdf_carreaux_pris_en_compte)):
    #     id_carreau_courant = gdf_carreaux_pris_en_compte.loc[i, "Id_carr1km"]
    #     gdf_commerces_alimentaires_par_carreau = pointsInPolygon.loc[(pointsInPolygon.isCommerceAlimentaire == 1) & pointsInPolygon.Id_carr1km == id_carreau_courant]
    #
    #     print(gdf_carreaux_pris_en_compte.loc[i, "Name"], gdf_carreaux_pris_en_compte.loc[i, "Age"])
    #
    #
    # # 1- geoloc des commerces alimentaires dans ce département
    # geoloc_commerces = pointsInPolygon[pointsInPolygon.isCommerceAlimentaire==1]['geometry']
    #
    # # 2- On récupère tous les centroides uniques des carreaux de la région du département (Pays de le Loire)
    # geoloc_centroids = gpd.GeoSeries(pointsInPolygon[pointsInPolygon.id_region==reg]['polygonCentroid'].unique(), crs=crs)

    # # 3- On calcule de toutes les distances entre les centroides des carreaux de la région
    # # les commerces du département et on récupère la distance minimale par centroide
    # print('{} centroides x {} commerces à calculer :'.format(len(geoloc_centroids),len(geoloc_commerces)))
    # all_dist = geoloc_centroids.apply(lambda g: geoloc_commerces.distance(g))
    # all_dist = pd.DataFrame(all_dist)
    # print(all_dist.shape)
    #
    # #%%
    #
    # all_dist['polygonCentroid'] = geoloc_centroids # pour la jointure finale d'après
    # all_dist['min_distance'] = all_dist.min(axis=1)
    # all_dist = all_dist[['polygonCentroid','min_distance']]
    # display(all_dist.head())
    #
    # #%%
    #
    # # 4- Distance moyenne aux commerces dans la région
    # jointure_finale = pd.merge(temp, all_dist, how='left', on='polygonCentroid')
    # print('Distance moyenne aux commerces dans le département {} = {} mètres'.format(dep,np.round(np.mean(jointure_finale['min_distance']),2)))
    #
    # #%%
    #
    # jointure_finale['min_distance'].describe()