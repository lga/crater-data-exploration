from pathlib import Path

from distance_moyenne_aux_commerces.v1_analyse_cosci_nov2021.src.creation_base_commerces_consolidee import \
    creer_base_commerces_osm_bpe_avec_dedoublonnage
from distance_moyenne_aux_commerces.v1_analyse_cosci_nov2021.src.generation_cartes_et_stats_commerces import \
    main_generer_carte

if __name__ == '__main__':

    # Générer la carte a partir du fichier commerce_crater.csv => le fichier utilisé pour faire la mesure de qualité dispo ici https://nuage.resiliencealimentaire.org/apps/onlyoffice/54624?filePath=%2F2.%20Les%20Greniers%20d%27Abondance%2FProjets%20internes%2FCRATer%2FEtudes%20et%20specs%2Fdistribution-transfo%2Fqualite_bases_donnees_commerces.xlsx
    # main_generer_carte("commerces_crater.csv")

    # Pour regénérer la base consolidée des commerces et les cartes (mais petits deltas possibles avec le fichiers utilisé pour le sondage
    # creer_base_commerces_osm_bpe_sirene_sans_dedoublonnage()
    creer_base_commerces_osm_bpe_avec_dedoublonnage()
    main_generer_carte("commerces_crater_osm_bpe.csv")

