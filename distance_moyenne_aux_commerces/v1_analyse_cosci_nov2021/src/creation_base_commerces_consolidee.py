import geopandas as gpd
import pandas as pd

#  Script permettant de fusionner et normaliser plusieurs bases de geoloc de commerces en un seul fichier
# Suite a croisement avec OSM, on essaie en ne projetant que sur 2 categ de supermarché : moins de 400m2 (B201 et B202) et plus de 400m2 (B101, B102)
types_commerces_alimentaires_bpe = {
    'B101': {'code': 'HYPE', 'libelle': "Hypermarché"},
    # 'B101': {'code': 'SUPM', 'libelle': "Hypermarché"},
    'B102': {'code': 'SUPM', 'libelle': "Supermarché"},
    'B201': {'code': 'SUPE', 'libelle': "Supérette"},
    'B202': {'code': 'EPIC', 'libelle': "Épicerie"},
    # 'B202': {'code': 'SUPE', 'libelle': "Épicerie"},
    'B203': {'code': 'BOUL', 'libelle': "Boulangerie"},
    'B204': {'code': 'BOUC', 'libelle': "Boucherie charcuterie"},
    'B205': {'code': 'SURG', 'libelle': "Produits surgelés"},
    'B206': {'code': 'POIS', 'libelle': "Poissonnerie"}
}

#  Pas évident de faire ce mapping => les types ne sont pas normalisés dans OSM, plus de 1500 différents
# mais un value_counts() sur le dataframe, montre quand meme que les principaux types ressortent le plus souvent
# par contre pas de disctinction entre les tailles de supermarchés
# Voir  https://wiki.openstreetmap.org/wiki/FR:Key:shop
types_commerces_alimentaires_osm = {
    'supermarket': {'code': 'SUPM', 'libelle': "Supermarché"},
    'department_store': {'code': 'SUPM', 'libelle': "Department store"},
    'convenience': {'code': 'SUPE', 'libelle': "Supérette"},
    'deli': {'code': 'SUPE', 'libelle': "Épicerie fine"},
    'bakery': {'code': 'BOUL', 'libelle': "Boulangerie"},
    'pastry': {'code': 'BOUL', 'libelle': "Patisserie"},
    'butcher': {'code': 'BOUC', 'libelle': "Boucherie charcuterie"},
    'seafood': {'code': 'POIS', 'libelle': "Poissonnerie"},
    'frozen_food': {'code': 'SURG', 'libelle': "Produits surgelés"},
    'cheese': {'code': 'AUTRE', 'libelle': "Fromagerie"},
    'dairy': {'code': 'AUTRE', 'libelle': "Crèmerie"},
    'farm': {'code': 'AUTRE', 'libelle': "Vente à la ferme"},
    'greengrocer': {'code': 'AUTRE', 'libelle': "Primeur"},
    'pasta': {'code': 'AUTRE', 'libelle': "Pâtes"},
    'wholesale': {'code': 'AUTRE', 'libelle': "Vente en entrepôt"},
    "caterer": {'code': 'AUTRE', 'libelle': "Traiteur"},
    "cannery": {'code': 'AUTRE', 'libelle': "Conserverie"}
}

types_commerces_alimentaires_sirene = {
    '47.11F': {'code': 'HYPE', 'libelle': "Hypermarché"},
    # '47.11F': {'code': 'SUPM', 'libelle': "Hypermarché"},
    '47.11D': {'code': 'SUPM', 'libelle': "Supermarché"},
    #     Les codes 47.11E, 47.19A et 47.19B sont classés en SUPM, car même si surface de vente plus grande qu'un supermarché, la part réservé à l'alimentaire est moindre
    '47.11E': {'code': 'SUPM', 'libelle': "Supermarché-Magasin multi-commerces"},
    '47.19A': {'code': 'SUPM', 'libelle': "Supermarché-Grands magasins"},
    '47.19B': {'code': 'SUPM', 'libelle': "Supermarché-Autres commerces de détail en magasin non spécialisé"},
    '47.11C': {'code': 'SUPE', 'libelle': "Supérette"},
    # '47.11B': {'code': 'SUPE', 'libelle': "Épicerie-Commerce d'alimentation générale"},
    '47.11B': {'code': 'EPIC', 'libelle': "Épicerie-Commerce d'alimentation générale"},
    '47.24Z': {'code': 'BOUL',
               'libelle': "Boulangerie-Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé"},
    '47.22Z': {'code': 'BOUC',
               'libelle': "Boucherie-Commerce de détail de viandes et de produits à base de viande en magasin spécialisé"},
    '47.11A': {'code': 'SURG', 'libelle': "Produits surgelés"},
    '47.23Z': {'code': 'POIS',
               'libelle': "Poissonnerie-Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé"},
    '47.21Z': {'code': 'AUTRE', 'libelle': "Commerce de détail de fruits et légumes en magasin spécialisé"},
    '47.29Z': {'code': 'AUTRE', 'libelle': "Autres commerces de détail alimentaires en magasin spécialisé"},
    '10.71C': {'code': 'BOUL',
               'libelle': "Boulangerie et boulangerie-pâtisserie"},
    '10.71D': {'code': 'BOUL', 'libelle': 'Patisserie'}
}


#  Les codes ci-dessous ne sont pas retenus, car pas significatifs (pour le 47.81Z il s'agit de restauration rapide)
# 47.25Z;Commerce de détail de boissons en magasin spécialisé
# 47.26Z;Commerce de détail de produits à base de tabac en magasin spécialisé
# 47.81Z;Commerce de détail alimentaire sur éventaires et marchés

def charger_donnees_bpe(nrows=None):
    print("----CHARGER DONNEES BPE----")
    filename = "data/bpe20_ensemble_xy_csv/bpe20_ensemble_xy.csv"
    data_bpe = pd.read_csv(filename, sep=";",
                           dtype={
                               'AAV2020': str,
                               'BV2012': str,
                               'UU2020': str,
                               'REG': str,
                               'DEP': str,
                               'DEPCOM': str},
                           nrows=nrows
                           )
    return data_bpe

def normaliser_donnees_bpe(df_bpe):
    print("----NORMALISER DONNEES BPE----")
    # On filtre les données non geolocalisées et les commerces non alimentaires
    df_bpe = (df_bpe
                  .loc[df_bpe.TYPEQU.isin(types_commerces_alimentaires_bpe.keys()), :]
                  .loc[df_bpe.QUALITE_XY != 'Non géolocalisé', :]
                  )
    # Calcul long/lat
    gdf_bpe = gpd.GeoDataFrame(df_bpe, geometry=gpd.points_from_xy(df_bpe.LAMBERT_X, df_bpe.LAMBERT_Y, crs="EPSG:2154"))
    gdf_bpe.to_crs("EPSG:4326", inplace=True)

    gdf_bpe['longitude'] = gdf_bpe.geometry.x
    gdf_bpe['latitude'] = gdf_bpe.geometry.y

    #  Normalisation des noms
    df_bpe = df_bpe.rename(columns=
                           {'REG': 'id_region',
                            'DEP': 'id_departement',
                            'DEPCOM': 'id_commune',
                            'AN': 'annee',
                            'LAMBERT_Y': 'lambert_y',
                            'LAMBERT_X': 'lambert_x',
                            'QUALITE_XY': 'qualite_geolocalisation'
                            })
    # traduction type de commerces
    dict_correspondance_codes_commerces = {code: valeur_normalisee['code'] for (code, valeur_normalisee) in
                                           types_commerces_alimentaires_bpe.items()}
    dict_correspondance_libelle_commerces = {code: valeur_normalisee['libelle'] for (code, valeur_normalisee) in
                                             types_commerces_alimentaires_bpe.items()}
    df_bpe['code_type_commerce'] = df_bpe.TYPEQU.map(dict_correspondance_codes_commerces)
    df_bpe['libelle_type_commerce'] = df_bpe.TYPEQU.map(dict_correspondance_libelle_commerces)
    # La BPE n'a pas le nom exact des commerces, on met un nom générique
    df_bpe['nom_commerce'] = df_bpe['libelle_type_commerce'] + ' BPE'

    df_bpe = df_bpe.loc[:, ['id_region', 'id_departement', 'id_commune',
                            # 'annee',
                            'nom_commerce',
                            'code_type_commerce', 'libelle_type_commerce',
                            # 'lambert_x','lambert_y',
                            'qualite_geolocalisation',
                            'longitude', 'latitude']]

    #  Il y a des doublons dans la BPE => le meme commmerce declaré sur plusieurs lignes
    df_bpe = df_bpe.drop_duplicates(ignore_index=True)
    return df_bpe


def charger_donnees_osm():
    print("----Chargement données OSM----")
    filename = "data/osm/osm-shop-fr.csv"
    df_osm = pd.read_csv(filename, sep=";",
                         dtype={
                             "code_region": str,
                             "code_departement": str,
                             "code_commune": str
                         })
    return df_osm


def extraire_commerces_alimentaires_osm(df_osm):
    df_osm = df_osm.loc[df_osm.type.isin(types_commerces_alimentaires_osm.keys())]
    df_osm.to_csv('resultats/commerces_alimentaires_osm.csv', sep=";")
    return df_osm




def normaliser_donnees_osm(df_osm):
    print("----Normaliser données OSM----")

    df_osm = extraire_commerces_alimentaires_osm(df_osm)

    # Extraction longitude/latitude
    df_osm_ll = df_osm['Geo Point'].str.split(",", expand=True)
    df_osm['longitude'] = df_osm_ll.iloc[:, 1]
    df_osm['latitude'] = df_osm_ll.iloc[:, 0]

    #  Normalisation des noms
    df_osm = df_osm.rename(columns=
                           {'code_region': 'id_region',
                            'code_departement': 'id_departement',
                            'code_commune': 'id_commune',
                            'name': 'nom_commerce'
                            })
    # traduction type de commerces
    dict_correspondance_codes_commerces = {code_bpe: type_pivot['code'] for (code_bpe, type_pivot) in
                                           types_commerces_alimentaires_osm.items()}
    dict_correspondance_libelle_commerces = {code_bpe: type_pivot['libelle'] for (code_bpe, type_pivot) in
                                             types_commerces_alimentaires_osm.items()}
    df_osm['code_type_commerce'] = df_osm.type.map(dict_correspondance_codes_commerces)
    df_osm['libelle_type_commerce'] = df_osm.type.map(dict_correspondance_libelle_commerces)

    df_osm = df_osm.loc[:, ['id_region', 'id_departement', 'id_commune',
                            'nom_commerce',
                            'code_type_commerce', 'libelle_type_commerce',
                            'longitude', 'latitude']]
    #  Suppression des doublons pour certains cas de commerces déclarés dans 2 communes
    # Voir par exemple la boucherie F.Gueydon qui est en 2 exemplaires
    # 1 à Talence (com_id=33522, mais com_insee=33522)
    # 1 à Talence selon com_id mais a bordeaux selon com_insee (com_id=33522, mais com_insee=33063)
    df_osm = df_osm.drop_duplicates(ignore_index=True)
    return df_osm


def construction_base_consolidee_osm_bpe(df_osm, df_bpe, df_sirene=None):
    print("----Construction base consolidee----")
    df_osm['source'] = 'OSM'
    df_bpe['source'] = 'BPE'
    return pd.concat([df_bpe, df_osm])

def construction_base_consolidee_osm_bpe_sirene(df_osm, df_bpe, df_sirene):
    print("----Construction base consolidee----")
    df_osm['source'] = 'OSM'
    df_bpe['source'] = 'BPE'
    df_sirene['source'] = 'SIRENE'
    return pd.concat([df_bpe, df_osm, df_sirene])


def charger_donnees_sirene():
    print("----Chargement données SIRENE----")
    filename = "../../crater-data-sources/sirene/import_resultats_api_sirene_20211102.csv"
    df_sirene = pd.read_csv(filename, sep=";",
                            dtype={
                                "code_commune": str
                            }
                            )
    return df_sirene


def normaliser_donnees_sirene(df_sirene_complet):
    print("----Normaliser données SIRENE----")

    # Extraction longitude/latitude
    df_sirene = df_sirene_complet.loc[
        df_sirene_complet.longitude.notna()
        & df_sirene_complet.latitude.notna()
        & df_sirene_complet.code_pays_etranger.isna()
        ]

    df_sirene['id_departement'] = df_sirene['code_commune'].str[:2]
    df_sirene['id_region'] = ''
    df_sirene = df_sirene.rename(columns={'code_commune': 'id_commune'})
    df_sirene = df_sirene.rename(columns={'geo_score': 'qualite_geolocalisation'})
    df_sirene.loc[:, 'nom_commerce'] = 'SIRENE' + df_sirene['enseigne_1'] + ' - ' + df_sirene['denomination_usuelle']

    # traduction type de commerces
    df_sirene['code_type_commerce'] = "AUTRE"
    df_sirene['libelle_type_commerce'] = "Type de commerce inconnu"
    # traduction type de commerces
    dict_correspondance_codes_commerces = {code: valeur_normalisee['code'] for (code, valeur_normalisee) in
                                           types_commerces_alimentaires_sirene.items()}
    dict_correspondance_libelle_commerces = {code: valeur_normalisee['libelle'] for (code, valeur_normalisee) in
                                             types_commerces_alimentaires_sirene.items()}
    df_sirene['code_type_commerce'] = df_sirene.activite_principale.map(dict_correspondance_codes_commerces)
    df_sirene['libelle_type_commerce'] = df_sirene.activite_principale.map(dict_correspondance_libelle_commerces)

    df_sirene = df_sirene.loc[:, ['id_region', 'id_departement', 'id_commune',
                                  'nom_commerce',
                                  'code_type_commerce', 'libelle_type_commerce',
                                  'longitude', 'latitude', 'qualite_geolocalisation']]
    df_sirene = df_sirene.drop_duplicates(ignore_index=True)
    return df_sirene


def ajouter_colonne_id_commerce(df_commerces):
    # projection des long & lat sur des entiers, en baissant la précision
    # tronquer une latitude a la 4eme décimale après la virgule (resp 3eme) revient à un précision de 10m (resp 100m)
    # voir dernier tableau de cet article https://www.tuto-carto.fr/longitude-latitude-precision/
    #  le calcul ci dessous ramène à une grille de 100m en gros
    df_commerces = df_commerces.astype({'longitude': float, 'latitude': float})
    # df_commerces['hash_longitude'] = df_commerces['longitude'] * 100000
    # df_commerces['hash_latitude'] = df_commerces['latitude'] * 100000
    # df_commerces = df_commerces.astype({'hash_longitude': int, 'hash_latitude': int})
    df_commerces['id_commerce'] = df_commerces['source'] + '_' + df_commerces['code_type_commerce'] + '_' + \
                                  df_commerces['id_commune'] + '_' + df_commerces.index.values.astype('str')
    # df_commerces['id_commerce'] = df_commerces['source'] + '_' + df_commerces['code_type_commerce'] + '_' + df_commerces['hash_longitude'].map(
    #     str) + '_' + df_commerces['hash_latitude'].map(str)
    # df_commerces = df_commerces.drop(columns=['hash_latitude', 'hash_longitude'])
    return df_commerces


def identifier_doublons_osm_bpe(df_commerces):
    print('Identification des doublons')
    gdf = gpd.GeoDataFrame(df_commerces,
                           geometry=gpd.points_from_xy(df_commerces['longitude'], df_commerces['latitude']),
                           crs='EPSG:4326')
    gdf_osm = gdf.loc[gdf.source == 'OSM']
    gdf_bpe_sirene = gdf.loc[gdf.source != 'OSM']

    perimetre_recherche_voisins_osm = gdf_osm.copy().loc[:, ['id_commerce', 'code_type_commerce', 'geometry']] \
        .rename(columns={'id_commerce': 'id_commerce_osm', 'code_type_commerce': 'code_type_commerce_osm'})
    # 0.001 ~ 100metres
    perimetre_recherche_voisins_osm['geometry'] = perimetre_recherche_voisins_osm.buffer(0.001)

    voisins_osm = perimetre_recherche_voisins_osm.sjoin(gdf_bpe_sirene)
    id_doublons = voisins_osm.loc[
        voisins_osm['code_type_commerce_osm'] == voisins_osm['code_type_commerce'], ['id_commerce_osm', 'id_commerce']]
    id_doublons['doublon_a_supprimer'] = True

    df_commerces = df_commerces.merge(id_doublons, how='left', left_on='id_commerce', right_on='id_commerce')
    df_commerces = df_commerces.drop(columns=['geometry'])

    return df_commerces


def identifier_doublons_internes(df_commerces, source):
    print(f'Identification des doublons internes à la source {source}')

    gdf = gpd.GeoDataFrame(df_commerces,
                           geometry=gpd.points_from_xy(df_commerces['longitude'], df_commerces['latitude']),
                           crs='EPSG:4326')
    gdf = gdf.loc[gdf.source == source]

    perimetre_recherche_voisins = (gdf.copy()
                                   .loc[:, ['id_commerce', 'code_type_commerce', 'geometry']]
                                   .rename(columns={'id_commerce': 'id_commerce_principal',
                                                    'code_type_commerce': 'code_type_commerce_principal', }))
    # 0.001 ~ 100metres
    perimetre_recherche_voisins['geometry'] = perimetre_recherche_voisins.buffer(0.002)

    gdf_voisins = gdf.rename(columns=
                             {'id_commerce': 'id_commerce_voisin',
                              'code_type_commerce': 'code_type_commerce_voisin'
                              })
    gdf_voisins = perimetre_recherche_voisins.sjoin(gdf_voisins)

    df_voisins = pd.DataFrame(gdf_voisins.loc
                              [
                                  (gdf_voisins['code_type_commerce_principal'] == gdf_voisins[
                                      'code_type_commerce_voisin']) &
                                  (gdf_voisins['id_commerce_principal'] != gdf_voisins['id_commerce_voisin']),
                                  ['id_commerce_principal', 'id_commerce_voisin']
                              ])

    nb_voisins = (df_voisins
                  .groupby('id_commerce_principal')
                  .count().reset_index()
                  .rename(columns={'id_commerce_voisin': 'nb_voisins'})
                  )
    liste_voisins = (df_voisins
                     .groupby('id_commerce_principal')['id_commerce_voisin']
                     .apply(list).reset_index()
                     .rename(columns={'id_commerce_voisin': 'liste_commerces_voisins'})
                     )

    df_commerces_a_conserver = (df_voisins
                                .drop(columns='id_commerce_voisin')
                                .drop_duplicates()
                                .merge(liste_voisins, how='left', on='id_commerce_principal')
                                .merge(nb_voisins, how='left', on='id_commerce_principal')
                                )
    df_commerces_a_conserver = df_commerces_a_conserver.sort_values(by=['nb_voisins'], ascending=False)

    i = 0
    while i < df_commerces_a_conserver.shape[0]:
        commerces_a_supprimer = df_commerces_a_conserver.iloc[i]['liste_commerces_voisins']
        df_commerces_a_conserver = df_commerces_a_conserver.loc[
            ~df_commerces_a_conserver['id_commerce_principal'].isin(commerces_a_supprimer)]
        i += 1
        if (i % 500 == 0):
            print(
                f"Identification commerces à conserver dans {source}, avancement {i} sur {df_commerces_a_conserver.shape[0]}")

    df_commerces_a_conserver['commerce_a_conserver'] = 'OUI'
    df_commerces_a_conserver = df_commerces_a_conserver.rename(
        columns={'id_commerce_principal': 'id_commerce_a_conserver'})

    df_parent = df_voisins.merge(df_commerces_a_conserver, how='left', left_on='id_commerce_voisin',
                                 right_on='id_commerce_a_conserver')
    df_parent = (df_parent
                 .loc[df_parent['id_commerce_a_conserver'].isna(), ['id_commerce_voisin', 'id_commerce_principal']]
                 .rename(columns={'id_commerce_voisin': 'id_commerce', 'id_commerce_principal': 'id_commerce_parent'}))

    df_commerces = df_commerces.merge(df_parent, how='inner', left_on='id_commerce', right_on='id_commerce')

    return df_commerces


# def supprimer_doublons_internes(df_commerces, ligne):
#     ligne_df_commerce = df_commerces[df_commerces['id_commerce'] == ligne['id_commerce']]
#     if (ligne_df_commerce['id_parent'].values[0] == ''):
#         ligne_df_commerce['id_parent'] = ligne['id_commerce_principal']


def charger_base_commerces_brute():
    print("----Chargement commerces----")
    filename = "resultats/commerces_crater_brute.csv"
    commerces = pd.read_csv(filename, sep=";", dtype={'id_region': str,
                                                      'id_departement': str,
                                                      'id_commune': str})
    return commerces


def creer_base_commerces_osm_bpe_sirene_sans_dedoublonnage():
    df_bpe = charger_donnees_bpe()
    df_bpe = normaliser_donnees_bpe(df_bpe)
    df_osm = charger_donnees_osm()
    df_osm = normaliser_donnees_osm(df_osm)
    df_sirene = charger_donnees_sirene()
    df_sirene = normaliser_donnees_sirene(df_sirene)
    # df_consolide = construction_base_consolidee_osm_bpe(df_osm, df_bpe)
    df_consolide = construction_base_consolidee_osm_bpe_sirene(df_osm, df_bpe, df_sirene)
    df_consolide = ajouter_colonne_id_commerce(df_consolide)
    # df_consolide.to_csv("resultats/commerces_crater_brute.csv",
    #         sep=";",
    #         index=False)
    #
    # df_consolide = charger_base_commerces_brute()
    # df_consolide = identifier_doublons_internes(df_consolide, 'BPE')
    # df_consolide = identifier_doublons(df_consolide)
    df_consolide.to_csv("resultats/commerces_crater_osm_bpe_sirene.csv",
                        sep=";",
                        index=False)

def creer_base_commerces_osm_bpe_avec_dedoublonnage():
    df_bpe = charger_donnees_bpe()
    df_bpe = normaliser_donnees_bpe(df_bpe)
    df_osm = charger_donnees_osm()
    df_osm = normaliser_donnees_osm(df_osm)
    df_consolide = construction_base_consolidee_osm_bpe(df_osm, df_bpe)
    df_consolide = ajouter_colonne_id_commerce(df_consolide)
    # df_consolide.to_csv("resultats/commerces_crater_brute.csv",
    #         sep=";",
    #         index=False)
    #
    # df_consolide = charger_base_commerces_brute()
    # df_consolide = identifier_doublons_internes(df_consolide, 'BPE')
    df_consolide = identifier_doublons_osm_bpe(df_consolide)
    df_consolide.to_csv("resultats/commerces_crater_osm_bpe.csv",
                        sep=";",
                        index=False)

if __name__ == '__main__':
    creer_base_commerces_osm_bpe_sirene_sans_dedoublonnage()
