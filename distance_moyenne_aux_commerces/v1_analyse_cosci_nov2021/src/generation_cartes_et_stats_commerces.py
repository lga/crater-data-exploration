from functools import partial
from multiprocessing import Pool
from pathlib import Path

import folium
import matplotlib
import pandas as pd
# Couleurs valides pour folium
from folium.plugins import MarkerCluster

couleurs_folium = ['white', 'lightblue', 'lightred', 'black', 'darkpurple', 'beige', 'cadetblue', 'red', 'green',
                   'purple', 'lightgreen', 'blue', 'darkgreen', 'gray', 'darkred', 'lightgray', 'orange', 'pink',
                   'darkblue']

# couleur_marqueur_par_source = {
#     'BPE': 'red',
#     'OSM': 'blue'
# }
from matplotlib_venn import venn2, venn3

icone_marqueur_par_source = {
    'BPE': 'square',
    'OSM': 'times',
    'SIRENE': 'circle'
}

couleur_marqueur_par_type_de_commerce = {
    "HYPE": 'black',
    "SUPM": 'black',
    "SUPE": 'green',
    "EPIC": 'green',
    "BOUL": 'orange',
    "BOUC": 'red',
    "POIS": 'blue',
    "SURG": 'lightgray',
    "AUTRE": 'lightgray'
}
couleur_marqueur_par_source = {
    'BPE': 'red',
    'OSM': 'blue',
    'SIRENE': 'green'
}

commerces = ''


def charger_commerces(nom_fichier_commerces):
    print("----Chargement commerces----")
    filename = "resultats/" + nom_fichier_commerces
    data_bpe = pd.read_csv(filename, sep=";",
                           dtype={'id_region': str,
                                  'id_departement': str,
                                  'id_commune': str})
    return data_bpe


def generer_carte_fichier(df_commerces, id_departement, codes_type_commerce):
    nom_carte_html = f"resultats/carte_commerces_{'_'.join(codes_type_commerce)}_{id_departement}.html"
    print(f"----Generation carte dans {nom_carte_html}----")

    extract_commerces = df_commerces.loc[(df_commerces['id_departement'] == id_departement) & (
        df_commerces['code_type_commerce'].isin(codes_type_commerce))]

    m = generer_carte_folium(extract_commerces)
    m.save(nom_carte_html)


def generer_carte_folium(extract_commerces):
    latitude_moyenne = (extract_commerces['latitude'].min() + extract_commerces['latitude'].max()) / 2
    longitude_moyenne = (extract_commerces['longitude'].min() + extract_commerces['longitude'].max()) / 2

    m = folium.Map(location=[latitude_moyenne, longitude_moyenne], zoom_start=9, max_zoom=19, control_scale=True)
    marker_cluster = MarkerCluster(options={
        # 'disableClusteringAtZoom': 13,
        # 'spiderfyOnMaxZoom': True
    })  # create marker clusters

    # Ajouter les marqueurs
    for i in range(0, len(extract_commerces)):
        folium.Marker(
            location=[extract_commerces.iloc[i]['latitude'], extract_commerces.iloc[i]['longitude']],
            # popup=extract_commerces.iloc[i]['source'] + '<br>Qualité geoloc :' + str(extract_commerces.iloc[i]['qualite_geolocalisation']),
            popup='<b><u>' + extract_commerces.iloc[i]['libelle_type_commerce'] + '</u></b>' +
                    '<br>ID commerce:' + extract_commerces.iloc[i]['id_commerce'] +
                    '<br>Nom:' + str(extract_commerces.iloc[i]['nom_commerce']) +
                    '<br>Type:' + extract_commerces.iloc[i]['libelle_type_commerce'] +
                    '<br>ID commune:' + str(extract_commerces.iloc[i]['id_commune']) +
                    '<br>Base de données source:' + str(extract_commerces.iloc[i]['source']) +
                    # '<br>Doublon de (OSM):' + str(extract_commerces.iloc[i]['id_commerce_osm']) +
                    # '<br>Doublon interne de :' + str(extract_commerces.iloc[i]['id_commerce_parent']) +
                    '<br>Qualite geoloc:' + str(extract_commerces.iloc[i]['qualite_geolocalisation']),
            icon=icone(extract_commerces.iloc[i], False)
        ).add_to(marker_cluster)
    marker_cluster.add_to(m)
    return m


def icone(i, activer_couleurs_marqueur_par_source):
    if activer_couleurs_marqueur_par_source:
        return folium.Icon(color=couleur_marqueur_par_source.get(i['source']),
                           icon=icone_marqueur_par_source.get(i['source']), prefix="fa")
            #
            #
            # folium.Icon(color=couleurs_folium[
            # int(''.join(map(str, map(ord, i['id_commerce'])))) % len(couleur_marqueur_par_type_de_commerce)],
            #                icon='circle', prefix='fa')
    else:
        return folium.Icon(color=couleur_marqueur_par_type_de_commerce.get(i['code_type_commerce']),
                           icon=icone_marqueur_par_source.get(i['source']), prefix="fa")


def generer_venn2(nom_source_A, serie_A, nom_source_B, serie_B, nom_territoire):
    plt = venn2([set(serie_A), set(serie_B)], set_labels=(nom_source_A, nom_source_B))
    matplotlib.pyplot.savefig(
        'resultats/recouvrement_' + nom_territoire + '_' + nom_source_A + '_' + nom_source_B + '.png')
    matplotlib.pyplot.clf()


def generer_venn2_departement(id_departement, nom_departement):
    generer_venn2('BPE',
                  commerces_bpe.loc[commerces_bpe['id_departement'] == id_departement]['id_commerce_regroupement'],
                  'OSM',
                  commerces_osm.loc[commerces_osm['id_departement'] == id_departement]['id_commerce_regroupement'],
                  nom_departement)
    generer_venn2('OSM',
                  commerces_osm.loc[commerces_osm['id_departement'] == id_departement]['id_commerce_regroupement'],
                  'SIRENE', commerces_sirene.loc[commerces_sirene['id_departement'] == id_departement][
                      'id_commerce_regroupement'],
                  nom_departement)
    generer_venn2('BPE',
                  commerces_bpe.loc[commerces_bpe['id_departement'] == id_departement]['id_commerce_regroupement'],
                  'SIRENE', commerces_sirene.loc[commerces_sirene['id_departement'] == id_departement][
                      'id_commerce_regroupement'],
                  nom_departement)


def generer_venn3_france(codes_types_commerces):
    global plt
    plt = venn3(
        [set(
            commerces_bpe.loc[commerces_bpe['code_type_commerce'].isin(codes_types_commerces)][
                'id_commerce_regroupement']
        ), set(
            commerces_osm.loc[commerces_osm['code_type_commerce'].isin(codes_types_commerces)][
                'id_commerce_regroupement']
        ), set(
            commerces_sirene.loc[commerces_sirene['code_type_commerce'].isin(codes_types_commerces)][
                'id_commerce_regroupement'])
        ], set_labels=('BPE', 'OSM', 'SIRENE'))
    matplotlib.pyplot.savefig(
        'resultats/recouvrement_France_' + '-'.join(codes_types_commerces) + '_BPE_OSM_SIRENE.png')
    matplotlib.pyplot.clf()


def generer_carte_departement(commerces, id_departement):
    print(f"Génération carte html pour le département {id_departement}")
    generer_carte_fichier(commerces, id_departement,
                      ["BOUL", "BOUC", "POIS", "HYPE", "SUPM", "SUPE", "EPIC", "SURG", "AUTRE"])
    print(f"Fin génération carte html pour le département {id_departement}")


def generer_index_html(ids_departements):
    print(f"Génération index.html")

    html = open("index.html", "w")
    html.write('<!DOCTYPE html><html lang="fr"><head> <meta charset="utf-8"></head><body>\n')
    html.write('<h1>Etudes distances moyennes aux commerces</h1>\n')
    html.write('<p>Cartes des commerces par département :</p><ul>\n')
    for id_departement in ids_departements:
        html.write(
            f'<li><a href="distance_moyenne_aux_commerces/resultats/carte_commerces_BOUL_BOUC_POIS_HYPE_SUPM_SUPE_EPIC_SURG_AUTRE_{id_departement}.html">Cartes département {id_departement}</a> </li>\n')
    html.write("</ul></body>")
    html.close()


def generer_cartes_html(commerces):
    ids_departements = [f"{d:02d}" for d in range(1, 95) if d != 20]
    ids_departements.append('2A')
    ids_departements.append('2B')


    ids_departements = ["31"]


    # ids_departements.append('971')
    # ids_departements.append('972')
    # ids_departements.append('973')
    # ids_departements.append('974')
    # ids_departements.append('976')

    partial_generer_carte_departement = partial(
        generer_carte_departement,
        commerces
    )

    with Pool(processes=4) as pool:
        pool.map(partial_generer_carte_departement, ids_departements)

    generer_index_html(ids_departements)


def generer_diag_ven():
    global commerces_bpe, commerces_osm, commerces_sirene, plt
    commerces_bpe = commerces.loc[commerces['source'] == 'BPE']
    commerces_osm = commerces.loc[commerces['source'] == 'OSM']
    commerces_sirene = commerces.loc[commerces['source'] == 'SIRENE']
    commerces_bpe['id_commerce_regroupement'] = commerces_bpe['id_commerce_osm'].fillna(commerces_bpe['id_commerce'])
    commerces_sirene['id_commerce_regroupement'] = commerces_sirene['id_commerce_osm'].fillna(
        commerces_sirene['id_commerce'])
    commerces_osm['id_commerce_regroupement'] = commerces_osm['id_commerce']
    generer_venn2('BPE', commerces_bpe['id_commerce_regroupement'], 'OSM', commerces_osm['id_commerce_regroupement'],
                  'France')
    generer_venn2('OSM', commerces_osm['id_commerce_regroupement'], 'SIRENE',
                  commerces_sirene['id_commerce_regroupement'], 'France')
    generer_venn2('OSM', commerces_osm['id_commerce_regroupement'], 'SIRENE',
                  commerces_sirene['id_commerce_regroupement'], 'France')
    generer_venn2_departement('48', 'Lozere')
    generer_venn2_departement('31', 'Haute-Garonne')
    generer_venn2_departement('34', 'Herault')
    generer_venn2_departement('29', 'Finistere')
    plt = venn3([set(commerces_bpe['id_commerce_regroupement']), set(commerces_osm['id_commerce_regroupement']),
                 set(commerces_sirene['id_commerce_regroupement'])], set_labels=('BPE', 'OSM', 'SIRENE'))
    matplotlib.pyplot.savefig('resultats/recouvrement_France_BPE_OSM_SIRENE.png')
    matplotlib.pyplot.clf()
    generer_venn3_france(['BOUC'])
    generer_venn3_france(['BOUL'])
    generer_venn3_france(['POIS'])
    generer_venn3_france(["HYPE", "SUPM", "SUPE", "EPIC"])


def main_generer_carte(nom_fichier_commerces):
    commerces = charger_commerces(nom_fichier_commerces)
    # commerces = commerces.loc[commerces['source'] == 'OSM']
    #  On exclut les commerces BPE qui ont été rapproché d'un commerce OSM
    commerces  = commerces.loc[commerces['id_commerce_osm'].isna()]

    for f in Path('resultats').glob('carte_*.html'):
        f.unlink()
    for f in Path('resultats').glob('*.png'):
        f.unlink()

    generer_cartes_html(commerces)

    # generer_diag_ven()
