# Cartes distribution

J'ai affiché les cartes avec les surfaces plutôt que les centres des communes. Je trouve ça plus facile à lire, mais à discuter. Ca donne plus de poids aux grandes communes peu peuplées, mais finalement il me semble que c'est une partie de l'indicateur.

J'ai choisi des indicateurs continus plutôt qu'avec un nombre fini de couleurs, aussi pour des raisons de lisibilité. La encore, à discuter. Peut-être qu'à des échelles plus petites (département), le rendu avec un nombre fini de couleurs serait plus lisible.

Le code permet facilement de modifier les seuils pour l'affichage.

# FRANCE

## Distance au commerce le plus proche, par type de commerce

![Distance au commerce le plus proche](France_-_Distance_au_plus_proche_commerce.png)

## Avec un seuil à 15 minutes, par moyen de transport

![Commerces accessibles à pieds](France_-_Distance_au_plus_proche_commerce_-_seuil_1_km.png)

![Commerces accessibles en vélo](France_-_Distance_au_plus_proche_commerce_-_seuil_3_km.png)

![Commerces accessibles en voiture](France_-_Distance_au_plus_proche_commerce_-_seuil_10_km.png)

## Distance au commerce le plus proche (tous types confondus)

![Distance au commerce le plus proche](France_-_Distance_au_plus_proche_commerce_-_tous_types_confondus.png)

## Distance pour accéder à tous les types de points de distribution

![Distance pour accéder à tous les types de points de distribution](France_-_Distance_pour_acceder_a_tous_les_types_de_points_de_distribution.png)

## Part de la population à une distance donnée des commerces

![Part des habitants à moins de 1 km](France_-_Part_des_habitants_a_moins_de_1_km.png)

![Part des habitants à moins de 3 km](France_-_Part_des_habitants_a_moins_de_3_km.png)

![Part des habitants à moins de 10 km](France_-_Part_des_habitants_a_moins_de_10_km.png)


# HAUTE-GARONNE

![Distance au commerce le plus proche](Haute-Garonne_-_Distance_au_plus_proche_commerce.png)

![Commerces accessibles à pieds](Haute-Garonne_-_Distance_au_plus_proche_commerce_-_seuil_1_km.png)

![Commerces accessibles en vélo](Haute-Garonne_-_Distance_au_plus_proche_commerce_-_seuil_3_km.png)

![Commerces accessibles en voiture](Haute-Garonne_-_Distance_au_plus_proche_commerce_-_seuil_10_km.png)

![Distance au commerce le plus proche](Haute-Garonne_-_Distance_au_plus_proche_commerce_-_tous_types_confondus.png)

![Distance pour accéder à tous les types de points de distribution](Haute-Garonne_-_Distance_pour_acceder_a_tous_les_types_de_points_de_distribution.png)

![Part des habitants à moins de 1 km](Haute-Garonne_-_Part_des_habitants_a_moins_de_1_km.png)

![Part des habitants à moins de 3 km](Haute-Garonne_-_Part_des_habitants_a_moins_de_3_km.png)

![Part des habitants à moins de 10 km](Haute-Garonne_-_Part_des_habitants_a_moins_de_10_km.png)

# CHER

![Distance au commerce le plus proche](Cher_-_Distance_au_plus_proche_commerce.png)

![Commerces accessibles à pieds](Cher_-_Distance_au_plus_proche_commerce_-_seuil_1_km.png)

![Commerces accessibles en vélo](Cher_-_Distance_au_plus_proche_commerce_-_seuil_3_km.png)

![Commerces accessibles en voiture](Cher_-_Distance_au_plus_proche_commerce_-_seuil_10_km.png)

![Distance au commerce le plus proche](Cher_-_Distance_au_plus_proche_commerce_-_tous_types_confondus.png)

![Distance pour accéder à tous les types de points de distribution](Cher_-_Distance_pour_acceder_a_tous_les_types_de_points_de_distribution.png)

![Part des habitants à moins de 1 km](Cher_-_Part_des_habitants_a_moins_de_1_km.png)

![Part des habitants à moins de 3 km](Cher_-_Part_des_habitants_a_moins_de_3_km.png)

![Part des habitants à moins de 10 km](Cher_-_Part_des_habitants_a_moins_de_10_km.png)
