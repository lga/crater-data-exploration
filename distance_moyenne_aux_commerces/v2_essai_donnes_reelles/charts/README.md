# Charts distribution


## France

### Indices INSEE

![Densité de population INSEE]Communes_France_densite_INSEE.png)

![Part de ménages ayant une voiture](Communes_France_part_menages_voiture.png)

### Distance aux commerces

![Distance aux commerces](Communes_France_distance_commerces.png)


![Distance aux commerces (population)](Population_totale_distance_aux_commerces.png)

## Haute-Garonne

### Indices INSEE

![Densité de population INSEE](Communes_Haute-Garonne_densite_INSEE.png)

![Part de ménages ayant une voiture](Communes_Haute-Garonne_part_menages_voiture.png)

### Distance aux commerces

![Distance aux commerces](Communes_Haute-Garonne_distance_commerces.png)


## Cher

### Indices INSEE

![Densité de population INSEE](Communes_Cher_densite_INSEE.png)

![Part de ménages ayant une voiture](Communes_Cher_part_menages_voiture.png)

### Distance aux commerces

![Distance aux commerces](Communes_Cher_distance_commerces.png)