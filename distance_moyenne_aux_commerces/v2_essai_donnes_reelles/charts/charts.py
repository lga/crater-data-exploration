#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 14:36:22 2021

@author: vincent
"""

# D'apres Notebooks Lionel : https://framagit.org/lga/crater-data-exploration/-/blob/master/distance_moyenne_aux_commerces/v2_essai_donnes_reelles/distance_au_plus_proche_commerce_charts.ipynb
# https://framagit.org/lga/crater-data-exploration/-/blob/charts_distrib_Vincent/distance_moyenne_aux_commerces/v2_essai_donnes_reelles/distance_au_plus_proche_commerce_cartes.ipynb?expanded=true&viewer=rich

#%%
import pandas as pd
import geopandas as gpd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.gridspec import GridSpec

from communs.outils import traduire_code_insee_vers_id_commune_crater

from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.indicateurs_accessibilite.calculer_accessibilite_points_distribution import charger_taux_motorisation_menages, charger_grille_densite_communes

CHEMIN_FICHIER_INDICATEURS_PROXIMITE = Path('../resultats/indicateurs_proximite/indicateurs_proximite.csv')
CHEMIN_CRATER_DATA_SOURCES = Path('../../../../crater-data-sources')

TERRITOIRES = {'Cher':'C-18',
               'Haute-Garonne':'C-31',
               'France':''}
'''
TERRITOIRES = {'Cher':'C-18',
               'Haute-Garonne':'C-31'}
'''

#%%
def charger_et_fusionner_donnees():
    # ATTENTION : les indicateurs de proximité contiennent tous les types de territoire mais pas les donnees INSEE
    #TODO: élargir aux autres types de territoires
    df = pd.read_csv(CHEMIN_FICHIER_INDICATEURS_PROXIMITE, sep=';')
    df_taux_motorisation = charger_taux_motorisation_menages(CHEMIN_CRATER_DATA_SOURCES / 'insee/logements/2018/base-ccc-logement-2018.zip', 'base-cc-logement-2018.CSV')
    df_grille_densite = charger_grille_densite_communes(CHEMIN_CRATER_DATA_SOURCES / 'insee/grille_communale_densite/2021/grille_densite_2021_agrege.xlsx')
    df = df.merge(df_grille_densite, left_on='id_territoire', right_on='id_commune').merge(df_taux_motorisation, left_on='id_territoire', right_on='id_commune')
    
    gdf_communes = gpd.read_file(CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_communes/2021/communes-20210101/communes-20210101.shp')
    gdf_communes.set_crs("EPSG:4326")
    gdf_communes.to_crs("EPSG:2154", inplace=True)
    gdf_communes['id_commune'] = traduire_code_insee_vers_id_commune_crater(gdf_communes['insee'])
    gdf_indicateurs = gdf_communes.merge(df, left_on='id_commune', right_on='id_territoire')
    
    return gdf_indicateurs, gdf_communes


#%%
def pivoter_dataframe(gdf_indicateurs, gdf_communes, colonne):
    df_pivotee = gdf_indicateurs.pivot(['id_commune', 'population', 'surf_ha', 'categorie_densite', 'indice_densite', 'part_menages_au_moins_une_voiture'], 'code_type_point_distribution', colonne).reset_index()
    
    gdf_pivotee = gdf_communes.loc[:, ['id_commune', 'geometry']].merge(df_pivotee, on='id_commune', how='inner')
    
    return gdf_pivotee

#%%
def creer_variable_categorie(df, colonne, nom_colonne_resultat='categorie', bins=4, seuil=[0,1]):
    if type(bins)==int:
        if seuil[1]>10:
            bins2 = np.round(np.arange(seuil[0],seuil[1]+2*seuil[1]/(bins),np.round(seuil[1]/(bins))))
        else:
            bins2 = np.arange(seuil[0],seuil[1]+2*seuil[1]/(bins),seuil[1]/(bins))
    elif type(bins)==list:
        bins2 = bins
    labels=[]
    for i in range(len(bins2)-1):
        if i==len(bins2)-2:
            lab = '>= {}'.format(bins2[-2])
        else:
            lab = '{} - {}'.format(bins2[i],bins2[i+1])
        labels.append(lab)
    
    df2 = df.copy()
    df2[colonne] = df2[colonne].clip(upper=bins2[-1], lower=seuil[0])
    df2[nom_colonne_resultat] = pd.cut(df2[colonne], bins=bins2, labels=labels,include_lowest=True).values
    return df2

#%%
def afficher_histogrammes_subplot(df):

    df['population_millions'] = df.population*1e-6
    population_totale = df.population_millions.sum()
    ymax = 1.1*population_totale
    fs=8

    plt.plot([1000,1000],[0,ymax],'k',lw=2)
    plt.plot([3000,3000],[0,ymax],'k',lw=2,)
    plt.plot([10000,10000],[0,ymax],'k',lw=2)
    plt.text(1100,0.02*ymax,'15 min marche', rotation='vertical', fontsize=fs)
    plt.text(3100,0.02*ymax,'15 min vélo', rotation='vertical', fontsize=fs)
    plt.text(10100,0.02*ymax,'15 min voiture', rotation='vertical', fontsize=fs)

    plt.plot([0,15000],[0.5*population_totale,0.5*population_totale],'k:',lw=1)
    plt.plot([0,15000],[0.25*population_totale,0.25*population_totale],'k:',lw=1)
    plt.plot([0,15000],[0.75*population_totale,0.75*population_totale],'k:',lw=1)
    plt.plot([0,15000],[1*population_totale,1*population_totale],'k:',lw=1)
    plt.text(14000,0.26*population_totale,'Q1', fontsize=fs)
    plt.text(14000,0.51*population_totale,'med.', fontsize=fs)
    plt.text(14000,0.76*population_totale,'Q3', fontsize=fs)
    plt.text(14000,1.01*population_totale,'total', fontsize=fs)

    n, bins, patches = plt.hist(df.BOULANGERIE_PATISSERIE, bins=1000, cumulative=True, density=False, histtype='step', label='Boulangerie',weights=df.population_millions)
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.COMMERCE_SPECIALISE, bins=1000, cumulative=True, density=False, histtype='step', label='Commerces specialisés',weights=df.population_millions)
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.PETITE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=False, histtype='step', label='Petite surface',weights=df.population_millions)
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.GRANDE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=False, histtype='step', label='Grande surface',weights=df.population_millions)
    patches[0].set_xy(patches[0].get_xy()[:-1])

    n, bins, patches = plt.hist(df.TOUS_PLUS_LOIN, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Plus lointain commerce',color='firebrick',lw=2, ls='--')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.TOUS_PLUS_PROCHE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Plus proche commerce',color='maroon',lw=2, ls='--')
    patches[0].set_xy(patches[0].get_xy()[:-1])

    plt.xlabel('Distance (m)')
    plt.ylabel('Distribution cumulée')
    plt.grid(b=True)
    plt.xlim([0,15000])
    plt.ylim([0,ymax])



#%%
def afficher_histogrammes(df,title):
    
    # Histogramme indice densité INSEE
    df_densite_insee = df.groupby(by='indice_densite').count()
    plt.figure(figsize=[8,6])
    plt.bar(df_densite_insee.index,df_densite_insee.population / df_densite_insee.population.sum())
    plt.xlabel('Indice densité INSEE')
    plt.ylabel('Densité de proba.')
    plt.grid()
    plt.title(title)
    plt.tight_layout()
    plt.savefig('{}_densite_INSEE.png'.format(title.replace(' ','_')))
    
    # Histogramme part des ménages ayant une voiture
    hist, bins = np.histogram(df.part_menages_au_moins_une_voiture, bins=100)
    norm_hist = hist.astype(np.float32) / hist.sum()
    plt.figure(figsize=[8,6])
    plt.bar(bins[:-1], norm_hist, width=(bins[1]-bins[0]))
    plt.xlabel('Part de menages ayant au moins une voiture')
    plt.ylabel('Densité de proba.')
    plt.grid()
    plt.title(title)
    plt.tight_layout()
    plt.savefig('{}_part_menages_voiture.png'.format(title.replace(' ','_')))
    
    # Histogrammes par catégorie de densité
    plt.figure(figsize=[10,8])
    for i in range(1,5):
        ax = plt.subplot(3,2,i)
        df2 = df[df['indice_densite']==i].copy()
        afficher_histogrammes_subplot(df2)
        plt.title('Indice densité INSEE = {}'.format(i))
    
    ax = plt.subplot(3,2,5)
    afficher_histogrammes_subplot(df.copy())
    plt.legend(loc='lower right')
    handles, labels = ax.get_legend_handles_labels()
    ax.get_legend().remove()
    plt.title('Toutes les communes')
    
    ax = plt.subplot(3,2,6)
    ax.set_axis_off()
    plt.legend(handles, labels, loc='center')

    plt.suptitle(title + ' - Distance au plus proche point de distrib\n(distrib. cumulées)')
    plt.tight_layout()
    plt.savefig('{}_distance_commerces.png'.format(title.replace(' ','_')))
        
#%%
def afficher_cartes(gdf, title, figname, cmap='viridis', seuil=(0,1), legende=''):
    N=1
    fig = plt.figure(constrained_layout=True, figsize=[15,15])
    gs = GridSpec(2*N, 2*N, figure=fig)
    
    normalize = colors.Normalize(vmin=seuil[0], vmax=seuil[1])

    ax1 = fig.add_subplot(gs[0:N, 0:N])
    ax1.set_title('Boulangerie Patisserie')
    gdf.plot(column='BOULANGERIE_PATISSERIE', legend=False, cmap=plt.get_cmap(cmap), norm=normalize, ax=ax1)
    ax1.set_axis_off()
    ax1.set_aspect('equal')
    
    ax2 = fig.add_subplot(gs[N:2*N, 0:N])
    ax2.set_title('Petite surface')
    gdf.plot(column='PETITE_SURFACE_GENERALISTE', legend=False, cmap=plt.get_cmap(cmap), norm=normalize, ax=ax2)
    ax2.set_axis_off()
    ax2.set_aspect('equal')
    
    ax3 = fig.add_subplot(gs[0:N, N:2*N])
    ax3.set_title('Commerce spécialisé')
    gdf.plot(column='COMMERCE_SPECIALISE', legend=False, cmap=plt.get_cmap(cmap), norm=normalize, ax=ax3)
    ax3.set_axis_off()
    ax3.set_aspect('equal')
    
    ax4 = fig.add_subplot(gs[N:2*N, N:2*N])
    ax4.set_title('Grande surface')
    gdf.plot(column='GRANDE_SURFACE_GENERALISTE', legend=False, cmap=plt.get_cmap(cmap), norm=normalize, ax=ax4)
    ax4.set_axis_off()
    ax4.set_aspect('equal')
    
    ax5 = fig.add_subplot(gs[:,-1])
    ax5.set_axis_off()
    plt.colorbar(plt.cm.ScalarMappable(norm=normalize, cmap=cmap), orientation='vertical', label=legende, ax=ax5, shrink=0.5)
    
    fig.suptitle(title)
    
    if len(figname)>0:
        plt.savefig(figname)
#%%
def afficher_cartes_categories(gdf, title, figname, cmap='viridis', seuil=(0,1), legende='', bins=4):
    N=1
    fig = plt.figure(constrained_layout=True, figsize=[15,15])
    gs = GridSpec(2*N, 2*N, figure=fig)

    ax1 = fig.add_subplot(gs[0:N, 0:N])
    ax1.set_title('Boulangerie Patisserie')
    gdf2 = creer_variable_categorie(gdf, 'BOULANGERIE_PATISSERIE', bins=bins, seuil=seuil)
    gdf2.plot(column='categorie', legend=False, cmap=plt.get_cmap(cmap), categorical=True, ax=ax1)
    ax1.set_axis_off()
    ax1.set_aspect('equal')
    
    ax2 = fig.add_subplot(gs[N:2*N, 0:N])
    ax2.set_title('Petite surface')
    gdf2 = creer_variable_categorie(gdf, 'PETITE_SURFACE_GENERALISTE', bins=bins, seuil=seuil)
    gdf2.plot(column='categorie', legend=False, cmap=plt.get_cmap(cmap), categorical=True, ax=ax2)
    ax2.set_axis_off()
    ax2.set_aspect('equal')
    
    ax3 = fig.add_subplot(gs[0:N, N:2*N])
    ax3.set_title('Commerce spécialisé')
    gdf2 = creer_variable_categorie(gdf, 'COMMERCE_SPECIALISE', bins=bins, seuil=seuil)
    gdf2.plot(column='categorie', legend=True, cmap=plt.get_cmap(cmap), categorical=True, ax=ax3, legend_kwds=dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende))
    ax3.set_axis_off()
    ax3.set_aspect('equal')
    
    ax4 = fig.add_subplot(gs[N:2*N, N:2*N])
    ax4.set_title('Grande surface')
    gdf2 = creer_variable_categorie(gdf, 'GRANDE_SURFACE_GENERALISTE', bins=bins, seuil=seuil)
    gdf2.plot(column='categorie', legend=False, cmap=plt.get_cmap(cmap), categorical=True, ax=ax4)
    ax4.set_axis_off()
    ax4.set_aspect('equal')
    fig.suptitle(title)
    
    if len(figname)>0:
        plt.savefig(figname)


#%%
def afficher_une_carte(gdf, colonne, title, figname, cmap='viridis', seuil=(0,1), legende=''):
    normalize = colors.Normalize(vmin=seuil[0], vmax=seuil[1])
    ax=gdf.plot(column=colonne, cmap=plt.get_cmap(cmap), norm=normalize, figsize=[12,12])
    ax.set_axis_off()
    plt.colorbar(plt.cm.ScalarMappable(norm=normalize, cmap=cmap), orientation='vertical', label=legende, ax=ax, shrink=0.5)
    plt.title(title)
    plt.tight_layout()
    if len(figname)>0:
        plt.savefig(figname)

#%%
def afficher_une_carte_avec_categories(gdf, colonne, title, figname, cmap='viridis', legende=''):
    
    ax=gdf.plot(column=colonne, cmap=plt.get_cmap(cmap), categorical=True, figsize=[12,12], legend=True, legend_kwds=dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende))
    ax.set_axis_off()
    plt.title(title)
    plt.tight_layout()
    if len(figname)>0:
        plt.savefig(figname)

#%%

if __name__ == '__main__':
    gdf_indicateurs, gdf_communes = charger_et_fusionner_donnees()
    
    #%%
    gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'distance_plus_proche_point_distribution_m')
    
    plt.close('all')
    
    print('HISTOGRAMMES')
    for territoire in TERRITOIRES.keys():
        print(territoire)
        indice_commune = TERRITOIRES[territoire]
        afficher_histogrammes(gdf.loc[gdf.id_commune.str.startswith(indice_commune),:],'Communes {}'.format(territoire))
    
    '''
    #%%
    # Remarque : pas de colonne distance moyenne dans la version actuelle du csv, a completer plus tard
    gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'distance_plus_proche_point_distribution_m')
    
    print('CARTES DISTANCE AUX COMMERCES')
    for territoire in TERRITOIRES.keys():
        print(territoire)
        indice_commune = TERRITOIRES[territoire]
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        bins=4
        plt.close('all')
        
        tit = '{} - Distance au plus proche commerce'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        #afficher_cartes(gdf2, tit, figname, seuil=(0,10000), legende='Distance (m)')
        afficher_cartes_categories(gdf2, tit, figname, seuil=(0,10000), legende='Distance (m)', bins=bins)
        
        bins=[0,5000,10000,15000,20000]
        tit = '{} - Distance au plus proche commerce\nSeuil 10km (Rouge : non accessible en voiture)'.format(territoire)
        figname = './Cartes/' + '{}_-_Distance_au_plus_proche_commerce_-_seuil_10_km.png'.format(territoire)
        #afficher_cartes(gdf2, tit, figname, 'RdBu_r', (0,20000), legende='Distance (m)')
        afficher_cartes_categories(gdf2, tit, figname, cmap='RdBu_r',seuil=(0,bins[-2]), legende='Distance (m)', bins=bins)
        
        bins=[0,1500,3000,4500,6000]
        tit = '{} - Distance au plus proche commerce\nSeuil 3km (Rouge : non accessible en vélo)'.format(territoire)
        figname = './Cartes/' + '{}_-_Distance_au_plus_proche_commerce_-_seuil_3_km.png'.format(territoire)
        #afficher_cartes(gdf2, tit, figname, 'RdBu_r', (0,6000), legende='Distance (m)')
        afficher_cartes_categories(gdf2, tit, figname, cmap='RdBu_r', seuil=(0,bins[-2]), legende='Distance (m)', bins=bins)
        
        bins=[0,500,1000,1500,2000]
        tit = '{} - Distance au plus proche commerce\nSeuil 1km (Rouge : non accessible à pied)'.format(territoire)
        figname = './Cartes/' + '{}_-_Distance_au_plus_proche_commerce_-_seuil_1_km.png'.format(territoire)
        #afficher_cartes(gdf2, tit, figname, 'RdBu_r', (0,2000), legende='Distance (m)')
        afficher_cartes_categories(gdf2, tit, figname, cmap='RdBu_r', seuil=(0,bins[-2]), legende='Distance (m)', bins=bins)
        
    
    #%%
    
    seuil=(0,1.)
    print('CARTES PART DES HABITANTS A UNE DISTANCE DONNEE')
    for territoire in TERRITOIRES.keys():
        print(territoire)
        indice_commune = TERRITOIRES[territoire]
        bins=4
        
        plt.close('all')
    
        gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'part_population_a_moins_de_1km')
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        tit = '{} - Part des habitants a moins de 1 km'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        #afficher_cartes(gdf2, tit, figname, seuil=seuil)
        afficher_cartes_categories(gdf2, tit, figname, seuil=seuil, bins=bins)
        
        gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'part_population_a_moins_de_3km')
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        tit = '{} - Part des habitants a moins de 3 km'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        #afficher_cartes(gdf2, tit, figname, seuil=seuil)
        afficher_cartes_categories(gdf2, tit, figname, seuil=seuil, bins=bins)
        
        gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'part_population_a_moins_de_10km')
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        tit = '{} - Part des habitants a moins de 10 km'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        #afficher_cartes(gdf2, tit, figname, seuil=seuil)
        afficher_cartes_categories(gdf2, tit, figname, seuil=seuil, bins=bins)

    
    #%%
    
    print('CARTES DISTANCES MIN MAX')
    for territoire in TERRITOIRES.keys():
        print(territoire)
        indice_commune = TERRITOIRES[territoire]
        
        plt.close('all')
        
        gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'distance_plus_proche_point_distribution_m')
        gdf['plus_proche_commerce'] = gdf[['BOULANGERIE_PATISSERIE','COMMERCE_SPECIALISE','PETITE_SURFACE_GENERALISTE','GRANDE_SURFACE_GENERALISTE']].min(axis=1)
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        tit = '{} - Distance au plus proche commerce - tous types confondus'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        
        afficher_une_carte_avec_categories(creer_variable_categorie(gdf2, 'plus_proche_commerce', bins=4, seuil=(0,8000)), 'categorie', tit, figname, legende='')
        
        gdf = pivoter_dataframe(gdf_indicateurs, gdf_communes, 'distance_plus_proche_point_distribution_m')
        gdf['distance_tous_types_commerce'] = gdf[['BOULANGERIE_PATISSERIE','COMMERCE_SPECIALISE','PETITE_SURFACE_GENERALISTE','GRANDE_SURFACE_GENERALISTE']].max(axis=1)
        gdf2 = gdf.loc[gdf.id_commune.str.startswith(indice_commune),:]
        tit = '{} - Distance pour acceder a tous les types de points de distribution'.format(territoire)
        figname = './Cartes/' + tit.replace(' ','_') + '.png'
        #afficher_une_carte(gdf2, 'distance_tous_types_commerce', tit, figname, seuil=(0,8000), legende='Distance (m)')
        afficher_une_carte_avec_categories(creer_variable_categorie(gdf2, 'distance_tous_types_commerce', bins=4, seuil=(0,8000)), 'categorie', tit, figname, legende='Distance (m)')
    '''
    
    
    
    
  
    
  
    
  
    
  
    
  
    
  
    
  
    
  
    
  
    
    
    
    