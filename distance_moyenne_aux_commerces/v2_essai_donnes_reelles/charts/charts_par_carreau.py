#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  9 12:54:05 2022

@author: vincent
"""
import pandas as pd
import matplotlib.pyplot as plt


chemin_donnees_par_carreau = r'../../../../local/indicateurs_proximite_par_carreau.csv'


#%%
def afficher_histogrammes_subplot(df):
    plt.plot([1000,1000],[0,1],lw=2)
    plt.plot([3000,3000],[0,1],lw=2,)
    plt.plot([10000,10000],[0,1],lw=2)
    plt.plot([0,15000],[0.5,0.5],'k:',lw=1)
    plt.plot([0,15000],[0.25,0.25],'k:',lw=1)
    plt.plot([0,15000],[0.75,0.75],'k:',lw=1)
    plt.text(14000,0.26,'Q1', fontsize=8)
    plt.text(14000,0.51,'med.', fontsize=8)
    plt.text(14000,0.76,'Q3', fontsize=8)
    plt.text(1100,0.02,'15 min marche', rotation='vertical', fontsize=8)
    plt.text(3100,0.02,'15 min vélo', rotation='vertical', fontsize=8)
    plt.text(10100,0.02,'15 min voiture', rotation='vertical', fontsize=8)
    n, bins, patches = plt.hist(df.BOULANGERIE_PATISSERIE, bins=1000, cumulative=True, density=True, histtype='step', label='Boulangerie')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.COMMERCE_SPECIALISE, bins=1000, cumulative=True, density=True, histtype='step', label='Commerces specialisés')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.PETITE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=True, histtype='step', label='Petite surface')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.GRANDE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=True, histtype='step', label='Grande surface')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    #n, bins, patches = plt.hist(df.TOUS, bins=1000, cumulative=True, density=True, histtype='step', label='Tous')
    #patches[0].set_xy(patches[0].get_xy()[:-1])
    plt.xlabel('Distance (m)')
    plt.ylabel('Histogramme cumulé')
    plt.grid(b=True)
    plt.xlim([0,15000])
    plt.ylim([0,1])



#%%
def afficher_distribution_par_type_commerce(df):
    df['population_millions'] = df.population*1e-6
    population_totale = df.population_millions.sum()
    ymax = 1.1*population_totale
    fs=10
    
    plt.figure(figsize=[10,8])
    plt.plot([1000,1000],[0,ymax],'k',lw=2)
    plt.plot([3000,3000],[0,ymax],'k',lw=2,)
    plt.plot([10000,10000],[0,ymax],'k',lw=2)
    plt.text(1100,0.02*ymax,'15 min marche', rotation='vertical', fontsize=fs)
    plt.text(3100,0.02*ymax,'15 min vélo', rotation='vertical', fontsize=fs)
    plt.text(10100,0.02*ymax,'15 min voiture', rotation='vertical', fontsize=fs)
    
    plt.plot([0,15000],[0.5*population_totale,0.5*population_totale],'k:',lw=1)
    plt.plot([0,15000],[0.25*population_totale,0.25*population_totale],'k:',lw=1)
    plt.plot([0,15000],[0.75*population_totale,0.75*population_totale],'k:',lw=1)
    plt.plot([0,15000],[1*population_totale,1*population_totale],'k:',lw=1)
    plt.text(14000,0.26*population_totale,'Q1', fontsize=fs)
    plt.text(14000,0.51*population_totale,'med.', fontsize=fs)
    plt.text(14000,0.76*population_totale,'Q3', fontsize=fs)
    plt.text(14000,1.01*population_totale,'total', fontsize=fs)
    
    n, bins, patches = plt.hist(df.BOULANGERIE_PATISSERIE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Boulangerie patisserie')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.COMMERCE_SPECIALISE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Commerce spécialisé')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.PETITE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Petite surface')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.GRANDE_SURFACE_GENERALISTE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Grande surface')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    
    n, bins, patches = plt.hist(df.TOUS_PLUS_LOIN, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Plus lointain commerce',color='firebrick',lw=2, ls='--')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    n, bins, patches = plt.hist(df.TOUS_PLUS_PROCHE, bins=1000, cumulative=True, density=False, weights=df.population_millions, histtype='step', label='Plus proche commerce',color='maroon',lw=2, ls='--')
    patches[0].set_xy(patches[0].get_xy()[:-1])
    
    plt.xlabel('Distance (m)')
    plt.ylabel('Millions d\'habitants (distribution cumulée)')
    #plt.grid(b=True)
    plt.legend(loc='lower right')
    plt.xlim([0,15000])
    plt.ylim([0,ymax])

    plt.title('Distance aux commerces')
    plt.tight_layout()
    plt.savefig('Population_totale_distance_aux_commerces.png')



#%%
if __name__ == '__main__':
    df = pd.read_csv(chemin_donnees_par_carreau, sep=';')
    #%%
    df_pivotee = df.pivot(['id_carreau','population'],'code_type_point_distribution','distance_m').reset_index()
    #%%
    plt.close('all')
    afficher_distribution_par_type_commerce(df_pivotee)
    