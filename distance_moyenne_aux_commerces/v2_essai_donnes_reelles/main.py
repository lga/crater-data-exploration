from pathlib import Path

import pandas

from communs.crater_logger import log
from communs.export_fichier import reinitialiser_dossier
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.charts.generer_cartes_accessibilite_physique import generer_cartes

from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.indicateurs_accessibilite.calculer_accessibilite_points_distribution import \
    generer_carroyage_simplifie_format_feather, generer_carroyage_simplifie_format_gpkg, calculer_indicateurs_proximite
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.calculateur_referentiel_points_distribution import \
    calculer_referentiel_points_distribution

ID_TERRITOIRE = 'P-FR'
TYPE_POINT_DISTRIBUTION = 'BOULANGERIE_PATISSERIE'

CHEMIN_CRATER_DATA_SOURCES = Path('../../../crater-data-sources')
CHEMIN_CRATER_DATA_RESULTATS = Path('../../../crater-data-resultats')
CHEMIN_FICHIER_GEOMETRIES_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_communes/2021/communes-20210101/communes-20210101.shp'
CHEMIN_FICHIER_GEOMETRIES_EPCIS = CHEMIN_CRATER_DATA_SOURCES / 'banatic/geometries_epcis/2021/Métropole/EPCI 2021_region.shp'
CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_departements/2018/departements-20180101-shp/departements-20180101.shp'
CHEMIN_FICHIER_GEOMETRIES_REGIONS = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_regions/2018/regions-20180101.shp'
CHEMIN_FICHIER_CARROYAGES_INSEE = CHEMIN_CRATER_DATA_SOURCES / 'insee/carroyage/2015_carreaux_200m/Filosofi2015_carreaux_200m_metropole.gpkg'
CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES = CHEMIN_CRATER_DATA_RESULTATS / 'data/crater/territoires/referentiel_territoires.csv'

CHEMIN_DOSSIER_RESULTATS_REFERENTIEL_POINTS_DISTRIBUTION = Path('./resultats/referentiel_points_distribution')
CHEMIN_FICHIER_REFERENTIEL_POINTS_DISTRIBUTION = CHEMIN_DOSSIER_RESULTATS_REFERENTIEL_POINTS_DISTRIBUTION / 'referentiel_points_distribution.csv'

CHEMIN_DOSSIER_RESULTATS_CARROYAGE_SIMPLIFIE = Path('./resultats/carroyage_simplifie')
CHEMIN_FICHIER_CARROYAGES_SIMPLIFIE_GPKG = CHEMIN_DOSSIER_RESULTATS_CARROYAGE_SIMPLIFIE / 'carroyage_simplifie.gpkg'
CHEMIN_FICHIER_CARROYAGES_SIMPLIFIE_FEATHER = CHEMIN_DOSSIER_RESULTATS_CARROYAGE_SIMPLIFIE / 'carroyage_simplifie.feather'

CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE = Path('./resultats/indicateurs_proximite')
CHEMIN_DOSSIER_RESULTATS_CARTES = Path('./resultats/cartes')
CHEMIN_DOSSIER_RESULTATS_CHARTS = Path('./resultats/charts')
CHEMIN_DOSSIER_RESULTATS_TABLEAUX = Path('./resultats/tableaux')
CHEMIN_FICHIER_RESULTATS_INDICATEURS_PROXIMITE = CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE / 'indicateurs_proximite.csv'
CHEMIN_FICHIER_RESULTATS_INDICATEURS_PROXIMITE_PAR_CARREAU = CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE / 'indicateurs_proximite_par_carreau.csv'

CHEMIN_FICHIER_SOURCE_COMMERCES_OSM = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/commerces/2021' / 'osm-shop-fr-20211214.csv'
CHEMIN_FICHIER_SOURCE_COMMERCES_BPE = CHEMIN_CRATER_DATA_SOURCES / 'insee/bpe/2020' / 'bpe20_ensemble_xy_csv.zip'
NOM_FICHIER_BASE_BPE_DANS_ZIP = 'bpe20_ensemble_xy.csv'


def generer_carroyages_simplifie():
    reinitialiser_dossier(CHEMIN_DOSSIER_RESULTATS_CARROYAGE_SIMPLIFIE)
    generer_carroyage_simplifie_format_feather(CHEMIN_FICHIER_CARROYAGES_INSEE,
                                               CHEMIN_FICHIER_CARROYAGES_SIMPLIFIE_FEATHER)
    generer_carroyage_simplifie_format_gpkg(CHEMIN_FICHIER_CARROYAGES_INSEE,
                                            CHEMIN_FICHIER_CARROYAGES_SIMPLIFIE_GPKG)


def generer_referentiel_points_distribution():
    reinitialiser_dossier(CHEMIN_DOSSIER_RESULTATS_REFERENTIEL_POINTS_DISTRIBUTION)
    calculer_referentiel_points_distribution(
        CHEMIN_FICHIER_SOURCE_COMMERCES_OSM,
        CHEMIN_FICHIER_SOURCE_COMMERCES_BPE,
        NOM_FICHIER_BASE_BPE_DANS_ZIP,
        CHEMIN_DOSSIER_RESULTATS_REFERENTIEL_POINTS_DISTRIBUTION)


def generer_indicateurs_proximite(id_territoire: str):
    reinitialiser_dossier(CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE)
    calculer_indicateurs_proximite(CHEMIN_FICHIER_REFERENTIEL_POINTS_DISTRIBUTION,
                                   CHEMIN_FICHIER_CARROYAGES_SIMPLIFIE_FEATHER,
                                   id_territoire,
                                   CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS,
                                   CHEMIN_FICHIER_GEOMETRIES_REGIONS,
                                   CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES,
                                   CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE)


def _generer_cartes(id_territoire, categorie_territoire):
    generer_cartes(
        CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE / 'indicateurs_proximite.csv',
        CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES,
        CHEMIN_FICHIER_GEOMETRIES_REGIONS,
        CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS,
        CHEMIN_FICHIER_GEOMETRIES_EPCIS,
        # None,
        CHEMIN_FICHIER_GEOMETRIES_COMMUNES,
        id_territoire,
        categorie_territoire,
        [
            {
                'nom_colonne': 'distance_plus_proche_point_distribution_m',
                'legende': 'Distance (m)  aux points de distribution',
                'bins': [0, 1000, 3000, 6000, 15000],
                # 'bins': None,
                'seuils': [0, 15000],
                'cmap': 'copper'
            },
            {
                'nom_colonne': 'part_population_a_moins_de_1km',
                'legende': 'Part de la population à moins de 1km',
                'bins': 4,
                'seuils': [0, 1],
                'cmap': 'summer_r'
            },
            {
                'nom_colonne': 'part_population_a_moins_de_3km',
                'legende': 'Part de la population à moins de 3km',
                'bins': 4,
                'seuils': [0, 1],
                'cmap': 'summer_r'
            },
            {
                'nom_colonne': 'part_population_a_moins_de_10km',
                'legende': 'Part de la population à moins de 10km',
                'bins': 4,
                'seuils': [0, 1],
                'cmap': 'summer_r'
            },
            {
                'nom_colonne': '50_pourcent_population_a_moins_de',
                'legende': 'Distance pour au moins 50% des habitants',
                'bins': None,
                'seuils': [0, 15000],
                'cmap': 'summer'
            },
            {
                'nom_colonne': '75_pourcent_population_a_moins_de',
                'legende': 'Distance pour au moins 75% des habitants',
                'bins': None,
                'seuils': [0, 15000],
                'cmap': 'summer'
            },
            {
                'nom_colonne': '80_pourcent_population_a_moins_de',
                'legende': 'Distance pour au moins 80% des habitants',
                'bins': None,
                'seuils': [0, 15000],
                'cmap': 'summer'
            }
        ],
        CHEMIN_DOSSIER_RESULTATS_CARTES
    )


def generer_tableau_indicateurs_pour_un_territoire(id_territoires, chemin_dossier_resultat):
    fichier_resultat = chemin_dossier_resultat / "tableaux.html"
    log.info(f"### Generation des tableaux dans {fichier_resultat}")
    reinitialiser_dossier(chemin_dossier_resultat)

    df = pandas.read_csv(CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE / 'indicateurs_proximite.csv', sep=";")

    with open(fichier_resultat, 'w') as f:
        f.write('<!DOCTYPE html><html lang="fr"><head> <meta charset="utf-8"></head><body>')
        _creer_tableaux_part_population(df, f, id_territoires)
        _creer_tableaux_population_menages(df, f, id_territoires)
        f.write('</body></html>')


def _creer_tableaux_part_population(df, f, id_territoires):
    f.write(f"<h1>Indicateurs part de la population</h1>")
    df['habitants_a_plus_de_3km'] = df['population'] - df['habitants_a_moins_de_3km']
    df['part_population_a_plus_de_3km'] = 100 * df['habitants_a_plus_de_3km'] / df['population']
    df['part_population_a_moins_de_1km'] = 100 * df['part_population_a_moins_de_1km']
    df['part_population_a_moins_de_3km'] = 100 * df['part_population_a_moins_de_3km']
    for id_territoire in id_territoires:
        f.write(construire_html_part_population(df.loc[df.id_territoire == id_territoire, :]))


def construire_html_part_population(dft):
    html_territoire = f"<h2>Territoire {dft['nom_territoire'].iat[0]}</h2>\n"
    html_territoire += f"<p>Population : {dft['population'].iat[0]}</p>\n"
    dft = dft.loc[~dft['code_type_point_distribution'].str.startswith("TOUS_"),
                  ['code_type_point_distribution',
                   'part_population_a_moins_de_1km',
                   'part_population_a_moins_de_3km',
                   'part_population_a_plus_de_3km'
                   ]].rename(columns={
        'code_type_point_distribution': 'Part de la population ayant accès à un type de point de distribution donné..._',
        'part_population_a_moins_de_1km': 'à pied (< 1km)',
        'part_population_a_moins_de_3km': 'à vélo (< 3km)',
        'part_population_a_plus_de_3km': 'en voiture (> 3km)',

    })
    html_territoire += dft.to_html(index=False, float_format=lambda x: f"{x:.0f} %")
    return html_territoire

def _creer_tableaux_population_menages(df, f, id_territoires):
    f.write(f"<h1>Indicateurs distance pour la popualtion et les ménages</h1>")
    for id_territoire in id_territoires:
        f.write(construire_html_population_menages(df.loc[df.id_territoire == id_territoire, :]))


def construire_html_population_menages(dft):
    html_territoire = f"<h2>Territoire {dft['nom_territoire'].iat[0]}</h2>\n"
    html_territoire += f"<p>Population : {dft['population'].iat[0]}</p>\n"
    html_territoire += f"<p>Ménages : {dft['menages'].iat[0]}</p>\n"
    dft = dft.loc[:,
                  ['code_type_point_distribution',
                   'distance_plus_proche_point_distribution_m',
                   'distance_plus_proche_point_distribution_par_menage_m'
                   ]].rename(columns={
        'code_type_point_distribution': '_',
        'distance_plus_proche_point_distribution_m': 'Distance au plus proche aux habitants',
        'distance_plus_proche_point_distribution_par_menage_m': 'Distance au plus proche aux ménages'
    })
    html_territoire += dft.to_html(index=False, float_format=lambda x: f"{x:.0f} m")
    return html_territoire


if __name__ == '__main__':
    # Etape preliminaire, permet de générer les fichiers feather
    # generer_carroyages_simplifie()
    # Etape 1
    # generer_referentiel_points_distribution()
    # Etape 2
    # generer_indicateurs_proximite('P-FR')
    # Etape 3
    _generer_cartes('P-FR', 'COMMUNE')
    # _generer_cartes('P-FR', 'DEPARTEMENT')
    # _generer_cartes('P-FR', 'EPCI')
    # _generer_cartes('D-32', 'COMMUNE')
    # _generer_cartes('D-32', 'EPCI')
    # _generer_cartes('D-31', 'COMMUNE')
    # _generer_cartes('D-31', 'EPCI')
    # _generer_cartes('R-76', 'COMMUNE')
    # _generer_cartes('R-76', 'EPCI')
    # Etape 4
    generer_tableau_indicateurs_pour_un_territoire(['P-FR', 'D-32', 'E-200066926', 'E-200035756', 'E-200072320'], CHEMIN_DOSSIER_RESULTATS_TABLEAUX)
    # Et
    # generer_charts_accessibilite_physique(
    #     CHEMIN_CRATER_DATA_SOURCES,
    #     CHEMIN_DOSSIER_RESULTATS_INDICATEURS_PROXIMITE,
    #     CHEMIN_DOSSIER_RESULTATS_CHARTS)
    log.info("FIN MAIN")
