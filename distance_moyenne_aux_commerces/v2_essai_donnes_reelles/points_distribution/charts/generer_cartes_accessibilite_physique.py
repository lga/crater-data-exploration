from pathlib import Path

import geopandas
import pandas
import numpy
from shapely.geometry import Polygon
from geopandas import GeoDataFrame
from matplotlib import pyplot
from matplotlib.colors import Normalize
from matplotlib.gridspec import GridSpec

from communs.crater_logger import log
from communs.export_fichier import reinitialiser_dossier
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater, \
    traduire_code_insee_vers_id_departement_crater, traduire_code_insee_vers_id_region_crater, \
    traduire_code_insee_vers_id_epci_crater


def charger_geometries_communes(chemin_fichier_geometries_communes):
    gdf = geopandas.read_file(chemin_fichier_geometries_communes)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)
    gdf['id_commune'] = traduire_code_insee_vers_id_commune_crater(gdf['insee'])
    gdf = gdf.rename(columns={'id_commune': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_departements(chemin_fichier_geometries_departement):
    gdf = geopandas.read_file(chemin_fichier_geometries_departement)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)

    # traitement du département du rhone : fusion de 2 lignes (code 69D et 69M en une seule)
    mask_departements_rhone = gdf['code_insee'].str.startswith('69')
    gdf_rhone = geopandas.GeoDataFrame(pandas.DataFrame(data={'code_insee': ['69']}), geometry=[gdf.loc[mask_departements_rhone, :].geometry.unary_union],
    crs="EPSG:2154")
    gdf = gdf.loc[~mask_departements_rhone, :].append(gdf_rhone)


    gdf['id_departement'] = traduire_code_insee_vers_id_departement_crater(gdf['code_insee'])
    gdf = gdf.rename(columns={'id_departement': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_regions(chemin_fichier_geometries_region):
    gdf = geopandas.read_file(chemin_fichier_geometries_region)
    gdf.set_crs("EPSG:4326")
    gdf.to_crs("EPSG:2154", inplace=True)
    gdf['id_region'] = traduire_code_insee_vers_id_region_crater(gdf['code_insee'])
    gdf = gdf.rename(columns={'id_region': 'id_territoire'})
    return gdf.loc[:, ['id_territoire', 'geometry']]

def charger_geometries_epcis(chemin_fichier_geometries):
    gdf = geopandas.read_file(chemin_fichier_geometries)
    gdf['id_territoire'] = traduire_code_insee_vers_id_epci_crater(gdf['SIREN'].astype('Int64').astype('str'))
    return gdf.loc[:, ['id_territoire', 'geometry']]



def generer_cartes(chemin_fichier_indicateurs_proximite: Path,
                   chemin_fichier_referentiel_territoires: Path,
                   chemin_fichier_geometries_regions: Path,
                   chemin_fichier_geometries_departements: Path,
                   chemin_fichier_geometries_epcis: Path,
                   chemin_fichier_geometries_communes: Path,
                   id_territoire_perimetre_carte: str,
                   categorie_territoire: str,
                   liste_indicateurs,
                   chemin_dossier_resultat: Path):

    dossier_resultat = chemin_dossier_resultat / id_territoire_perimetre_carte / categorie_territoire

    log.info(f" ## Generation cartes dans {dossier_resultat} ##")

    reinitialiser_dossier(dossier_resultat)
    pyplot.close('all')

    log.info(f"  - Chargement geometries")
    gdf_partiel = _charger_geometries_territoire_cible(id_territoire_perimetre_carte, categorie_territoire,
                                                       chemin_fichier_geometries_communes,
                                                       chemin_fichier_geometries_departements,
                                                       chemin_fichier_geometries_epcis,
                                                       chemin_fichier_geometries_regions,
                                                       chemin_fichier_referentiel_territoires)

    log.info(f"  - Chargement donnees indicateurs")
    df = pandas.read_csv(chemin_fichier_indicateurs_proximite, sep=";")
    df.replace(to_replace='1km', value='0 à 1 km', inplace=True)
    df.replace(to_replace='3km', value='1 à 3 km', inplace=True)
    df.replace(to_replace='10km', value='3 à 10 km ou plus', inplace=True)


    for indicateur in liste_indicateurs:
        log.info(f"  - Generation cartes pour l'indicateur {indicateur['nom_colonne']}, sur le territoire {id_territoire_perimetre_carte}, et le niveau {categorie_territoire}")
        df_un_indicateur = df.pivot(['id_territoire'], 'code_type_point_distribution',
                                              indicateur['nom_colonne']).reset_index()
        gdf_un_indicateur = gdf_partiel.merge(df_un_indicateur, on='id_territoire', how='left')
        creer_cartes_4_categories_points_distribution(
            gdf_un_indicateur,
            f"{indicateur['legende']} pour les {categorie_territoire}s du territoire {id_territoire_perimetre_carte}",
            f"{dossier_resultat}/{id_territoire_perimetre_carte}_{categorie_territoire}_{indicateur['nom_colonne']}_par_categorie",
            legende=indicateur['legende'],
            bins=indicateur['bins'],
            seuil=indicateur['seuils'],
            cmap=indicateur['cmap']
        )
        creer_cartes_categories_TOUS_points_distribution(
            gdf_un_indicateur,
            f"{indicateur['legende']} pour les {categorie_territoire}s du territoire {id_territoire_perimetre_carte}",
            f"{dossier_resultat}/{id_territoire_perimetre_carte}_{categorie_territoire}_{indicateur['nom_colonne']}_tous",
            legende=indicateur['legende'],
            bins=indicateur['bins'],
            seuil=indicateur['seuils'],
            cmap=indicateur['cmap']
        )

    # pyplot.show()
    return gdf_un_indicateur


def _charger_geometries_territoire_cible(id_territoire_perimetre_carte, categorie_territoire,
                                         chemin_fichier_geometries_communes, chemin_fichier_geometries_departements,
                                         chemin_fichier_geometries_epcis, chemin_fichier_geometries_regions,
                                         chemin_fichier_referentiel_territoires):
    gdf = _charger_geometries(chemin_fichier_geometries_regions, chemin_fichier_geometries_departements,
                              chemin_fichier_geometries_epcis, chemin_fichier_geometries_communes)
    log.info(f"  - Chargement referentiel territoires")
    df_territoires = pandas.read_csv(chemin_fichier_referentiel_territoires, sep=";")
    gdf = gdf.merge(df_territoires)
    gdf_partiel = gdf.loc[(gdf['categorie_territoire'] == categorie_territoire)
                          & ((gdf['id_pays'] == id_territoire_perimetre_carte)
                             | (gdf['id_region'] == id_territoire_perimetre_carte)
                             | (gdf['id_departement'] == id_territoire_perimetre_carte)
                             | (gdf['id_epci'] == id_territoire_perimetre_carte)
                             ), :]
    return gdf_partiel


def _charger_geometries(chemin_fichier_geometries_regions, chemin_fichier_geometries_departements,
                        chemin_fichier_geometries_epcis, chemin_fichier_geometries_communes=None):
    gdf_contours = charger_geometries_regions(chemin_fichier_geometries_regions)
    gdf_contours = gdf_contours.append(charger_geometries_departements(chemin_fichier_geometries_departements))
    gdf_contours = gdf_contours.append(charger_geometries_epcis(chemin_fichier_geometries_epcis))
    if (chemin_fichier_geometries_communes is not None):
        gdf_contours = gdf_contours.append(charger_geometries_communes(chemin_fichier_geometries_communes))
    return gdf_contours



def creer_cartes_4_categories_points_distribution(gdf: GeoDataFrame, title, figname, cmap='viridis', seuil=None, legende='Légende', bins=None):
    N = 1
    fig = pyplot.figure(constrained_layout=True, figsize=[15, 13])
    gs = GridSpec(2 * N, 2 * N, figure=fig)

    if ((gdf['BOULANGERIE_PATISSERIE'].dtype == 'object') | (gdf['BOULANGERIE_PATISSERIE'].dtype == 'str')):
        modeCategorie = True
        legend_kwd = dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende)
        normalize = None
    elif (bins is not None):
        modeCategorie = True
        legend_kwd = dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende)
        normalize = None
        gdf = creer_variable_categorie(gdf, 'BOULANGERIE_PATISSERIE', 'BOULANGERIE_PATISSERIE', bins, seuil)
        gdf = creer_variable_categorie(gdf, 'PETITE_SURFACE_GENERALISTE', 'PETITE_SURFACE_GENERALISTE', bins, seuil)
        gdf = creer_variable_categorie(gdf, 'GRANDE_SURFACE_GENERALISTE', 'GRANDE_SURFACE_GENERALISTE', bins, seuil)
        gdf = creer_variable_categorie(gdf, 'COMMERCE_SPECIALISE', 'COMMERCE_SPECIALISE', bins, seuil)
    else:
        modeCategorie = False
        legend_kwd = dict(label=legende)
        normalize = Normalize(vmin=seuil[0], vmax=seuil[1])

    ax1 = fig.add_subplot(gs[0:N, 0:N])
    ax1.set_title('Boulangerie Patisserie')
    gdf.plot(column='BOULANGERIE_PATISSERIE', legend=False, categorical=modeCategorie, cmap=pyplot.get_cmap(cmap), ax=ax1, norm=normalize)
    ax1.set_axis_off()
    ax1.set_aspect('equal')

    ax2 = fig.add_subplot(gs[N:2 * N, 0:N])
    ax2.set_title('Petite surface alimentaire (<400m²)')
    gdf.plot(column='PETITE_SURFACE_GENERALISTE', legend=False, categorical=modeCategorie,cmap=pyplot.get_cmap(cmap), ax=ax2, norm=normalize)
    ax2.set_axis_off()
    ax2.set_aspect('equal')

    ax3 = fig.add_subplot(gs[0:N, N:2 * N])
    ax3.set_title('Commerce spécialisé')
    gdf.plot(column='COMMERCE_SPECIALISE', legend=True, legend_kwds=legend_kwd, cmap=pyplot.get_cmap(cmap), categorical=modeCategorie, ax=ax3, norm=normalize)
    ax3.set_axis_off()
    ax3.set_aspect('equal')

    ax4 = fig.add_subplot(gs[N:2 * N, N:2 * N])
    ax4.set_title('Grande surface alimentaire (>400m²)')
    gdf.plot(column='GRANDE_SURFACE_GENERALISTE', legend=False, categorical=modeCategorie,cmap=pyplot.get_cmap(cmap), ax=ax4, norm=normalize)
    ax4.set_axis_off()
    ax4.set_aspect('equal')

    fig.suptitle(title)

    if len(figname) > 0:
        pyplot.savefig(figname)


def creer_cartes_categories_TOUS_points_distribution(gdf: GeoDataFrame, title, figname, cmap='viridis', seuil=None, legende='Légende', bins=None):
    fig = pyplot.figure(constrained_layout=True, figsize=[16, 7])
    gs = GridSpec(1, 2, figure=fig)

    if ((gdf['TOUS_PLUS_PROCHE'].dtype == 'object') | (gdf['TOUS_PLUS_PROCHE'].dtype == 'str')):
        modeCategorie = True
        legend_kwd = dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende)
        normalize = None
    elif (bins is not None):
        modeCategorie = True
        legend_kwd = dict(loc='upper left', bbox_to_anchor=(1, 1), title=legende)
        normalize = None
        gdf = creer_variable_categorie(gdf, 'TOUS_PLUS_PROCHE', 'TOUS_PLUS_PROCHE', bins, seuil)
        gdf = creer_variable_categorie(gdf, 'TOUS_PLUS_LOIN', 'TOUS_PLUS_LOIN', bins, seuil)
    else:
        modeCategorie = False
        legend_kwd = dict(label=legende)
        normalize = Normalize(vmin=seuil[0], vmax=seuil[1])

    ax1 = fig.add_subplot(gs[0, 0])
    ax1.set_title('Un point de distribution quelquesoit sa catégorie')
    gdf.plot(column='TOUS_PLUS_PROCHE', legend=False, categorical=modeCategorie, cmap=pyplot.get_cmap(cmap), ax=ax1, norm=normalize)
    ax1.set_axis_off()
    ax1.set_aspect('equal')

    ax2 = fig.add_subplot(gs[0, 1])
    ax2.set_title('Un point de distribution de chaque catégorie')
    gdf.plot(column='TOUS_PLUS_LOIN', legend=True, legend_kwds=legend_kwd, cmap=pyplot.get_cmap(cmap), categorical=modeCategorie, ax=ax2, norm=normalize)
    ax2.set_axis_off()
    ax2.set_aspect('equal')

    fig.suptitle(title)

    if len(figname) > 0:
        pyplot.savefig(figname)



def creer_variable_categorie(df, colonne, nom_colonne_resultat='categorie', bins=4, seuil=[0, 1]):
    if type(bins) == int:
        bins2 = numpy.arange(seuil[0], seuil[1] + (seuil[1] - seuil[0]) / (bins), (seuil[1] - seuil[0]) / (bins))
    elif type(bins) == list:
        bins2 = bins
    labels = []

    df2 = df.copy()
    polygon_pixel = df2.iloc[0]['geometry'].centroid.buffer(0)

    for i in range(len(bins2) - 1):
        lab = '{} - {}'.format(bins2[i], bins2[i + 1])
        labels.append(lab)
        # tweak pour geopandas : créer des entrées dans le geodf pour chaque ensemble du bin, sinon la couleur de la légende ne correspond pas a celle utilisée pour les polygon
        df2 = df2.append(
            [{'id_territoire': f"ZZZ-{nom_colonne_resultat}-{lab}", nom_colonne_resultat: bins2[i+1], 'geometry': polygon_pixel}], ignore_index=True)

    df2[colonne] = df2[colonne].clip(upper=bins2[-1], lower=seuil[0])
    df2[nom_colonne_resultat] = pandas.cut(df2[colonne], bins=bins2, labels=labels, include_lowest=True)

    return df2


def afficher_une_carte(gdf, colonne, title, figname, cmap='viridis', seuil=(0, 1), legende=''):
    normalize = Normalize(vmin=seuil[0], vmax=seuil[1])
    ax = gdf.plot(column=colonne, cmap=pyplot.get_cmap(cmap), norm=normalize, figsize=[12, 12])
    ax.set_axis_off()
    pyplot.colorbar(pyplot.cm.ScalarMappable(norm=normalize, cmap=cmap), orientation='vertical', label=legende, ax=ax,
                 shrink=0.5)
    pyplot.title(title)
    pyplot.tight_layout()
    if len(figname) > 0:
        pyplot.savefig(figname)