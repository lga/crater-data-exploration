from pathlib import Path

import pandas
from matplotlib import pyplot

from communs.crater_logger import log
from communs.export_fichier import reinitialiser_dossier

from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.indicateurs_accessibilite.calculer_accessibilite_points_distribution import \
    charger_taux_motorisation_menages, charger_grille_densite_communes


def generer_charts_accessibilite_physique(chemin_crater_data_sources: Path, chemin_dossier_indicateurs_accessibilite: Path, chemin_dossier_resultat: Path):

    dossier_resultat = chemin_dossier_resultat
    log.info(f" ## Generation charts dans {dossier_resultat} ##")
    reinitialiser_dossier(dossier_resultat)

    df = pandas.read_csv(chemin_dossier_indicateurs_accessibilite / 'indicateurs_proximite.csv', sep=";")
    df = df.loc[df['categorie_territoire'] == 'COMMUNE', : ].rename(columns={'id_territoire': 'id_commune'})

    df_taux_motorisation = charger_taux_motorisation_menages(
        chemin_crater_data_sources / 'insee/logements/2018/base-ccc-logement-2018.zip', 'base-cc-logement-2018.CSV')
    df_grille_densite = charger_grille_densite_communes(
        chemin_crater_data_sources / 'insee/grille_communale_densite/2021/grille_densite_2021_agrege.xlsx')


    df = df.merge(df_grille_densite, on='id_commune').merge(df_taux_motorisation, on='id_commune')

    df.hist()

    df.loc[:,
    ['id_commune', 'population', 'categorie_densite', ]].groupby(
        'categorie_densite'
        ).agg(
        Nombre_de_communes=("id_commune", "count"),
        Population=("population", "sum"))

    df_distance = df.pivot(['id_commune', 'population', 'categorie_densite', 'indice_densite',
                                         'part_menages_au_moins_une_voiture'], 'code_type_point_distribution',
                                        'distance_plus_proche_point_distribution_m').reset_index()

    # df_distance_indicateurs = df_distance.loc[:, ['id_commune', 'indice_densite', 'part_menages_au_moins_une_voiture',
    #                                               'BOULANGERIE_PATISSERIE', 'GRANDE_SURFACE_GENERALISTE',
    #                                               'PETITE_SURFACE_GENERALISTE', 'COMMERCE_SPECIALISE']]

    df_distance.hist(figsize=(15, 15), bins=100)
    pyplot.savefig(chemin_dossier_resultat / 'chart')
