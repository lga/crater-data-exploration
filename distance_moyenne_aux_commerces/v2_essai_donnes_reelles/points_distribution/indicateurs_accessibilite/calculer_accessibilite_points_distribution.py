import zipfile
from pathlib import Path

import pandas
import geopandas
from geopandas import GeoDataFrame
from numpy import ndarray
from pandas import DataFrame
from sklearn.neighbors import BallTree
import numpy as np

from communs.crater_logger import chronometre, log
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.config_referentiels_points_distribution import \
    CODE_GRANDE_SURFACE_GENERALISTE, CODE_PETITE_SURFACE_GENERALISTE, CODE_BOULANGERIE_PATISSERIE, \
    CODE_COMMERCE_SPECIALISE

DISTANCE_BUFFER_TERRITOIRE = 20000


def charger_taux_motorisation_menages(chemin_fichier_zip_motorisation_menages: Path, nom_fichier: str):
    df = pandas.read_csv(
            zipfile.ZipFile(chemin_fichier_zip_motorisation_menages, 'r').open(nom_fichier),
            usecols=['CODGEO', 'P18_MEN', 'P18_RP_VOIT1P'],
            sep=';',
            dtype={'CODGEO': str}
    )
    df['part_menages_au_moins_une_voiture'] = df['P18_RP_VOIT1P'] / df['P18_MEN']

    df['id_commune'] = traduire_code_insee_vers_id_commune_crater(df['CODGEO'])
    return df.loc[:, ['id_commune', 'part_menages_au_moins_une_voiture' ]]


def charger_grille_densite_communes(chemin_fichier_grille_densite: Path):
    df_grille_densite = pandas.read_excel(chemin_fichier_grille_densite).rename(columns={
        'Degré de \nDensité de la commune\n': 'indice_densite'
    })
    df_grille_densite['categorie_densite'] = df_grille_densite['indice_densite'].map(
        {
            1: 'DENSEMENT_PEUPLEE',
            2: 'DENSITE_INTERMEDIAIRE',
            3: 'PEU_DENSE',
            4: 'TRES_PEU_DENSE'
        }
    )
    df_grille_densite['id_commune'] = traduire_code_insee_vers_id_commune_crater(
        df_grille_densite['\nCode \nCommune\n'])
    return df_grille_densite.loc[:, ['id_commune', 'categorie_densite', 'indice_densite']]


@chronometre
def charger_sources(chemin_fichier_points_distribution: Path,
                    chemin_fichier_carroyage_population: Path,
                    id_territoire: str,
                    chemin_fichier_geometries_departements: Path,
                    chemin_fichier_geometries_regions: Path,
                    ) :

    gdf_territoire = _charger_contours_territoire(id_territoire, chemin_fichier_geometries_departements, chemin_fichier_geometries_regions)

    gdf_points_distribution_territoire = _charger_referentiel_points_distribution(chemin_fichier_points_distribution,
                                                                                  gdf_territoire)

    # gdf_carreaux_population = _charger_carroyage_population_simplifie_gpkg(chemin_fichier_carroyage_population, gdf_territoire)
    gdf_carreaux_population = _charger_carroyage_population_simplifie_feather(chemin_fichier_carroyage_population, gdf_territoire)

    return {'gdf_territoire': gdf_territoire,
            'gdf_points_distribution': gdf_points_distribution_territoire,
            'gdf_carreaux_population': gdf_carreaux_population
            }

@chronometre
def _charger_referentiel_points_distribution(chemin_fichier_points_distribution, gdf_territoire):

    log.info(f"    - Chargement fichiers des points de distribution {chemin_fichier_points_distribution}")
    df_points_distribution = pandas.read_csv(chemin_fichier_points_distribution, sep=";",
                                   dtype={'id_region': str,
                                          'id_departement': str,
                                          'id_commune': str,
                                          'qualite_geolocalisation': str
                                          #  Voir en quoi caster le boolean (marche pas bien avec les NA). sinon nettorer la base en amont
                                          # 'doublon_a_supprimer': 'boolean'
                                          })
    gdf_points_distribution = geopandas.GeoDataFrame(
        df_points_distribution,
        geometry=geopandas.points_from_xy(df_points_distribution.longitude, df_points_distribution.latitude, crs="EPSG:4326"))
    gdf_points_distribution.to_crs("EPSG:2154", inplace=True)
    gdf_points_distribution_territoire = gdf_points_distribution

    if (gdf_territoire is not None) :
        log.info(f"Buffer et sjoin pour capter les points de distribution au dela des limites strictes du territoire")
        mask_territoire_etendu = gdf_territoire.buffer(DISTANCE_BUFFER_TERRITOIRE)
        gdf_points_distribution_territoire = gdf_points_distribution.sjoin(geopandas.GeoDataFrame(geometry=mask_territoire_etendu))

    return gdf_points_distribution_territoire


@chronometre
def _charger_contours_territoire(id_territoire:str, chemin_geometries_departements:Path, chemin_geometries_regions:Path) -> GeoDataFrame:

    log.info(f"   - Chargement fichiers des contours du territoire {id_territoire}")
    if (id_territoire == 'P-FR') :
        # Pour besoin du contour pour la France, car on ne limite pas le carroyage ou les points de distrib a une zone précise
        return None

    chemin_geometries_territoires = ''
    if id_territoire.startswith('D-'):
        chemin_geometries_territoires = chemin_geometries_departements
    elif id_territoire.startswith('R-'):
        chemin_geometries_territoires = chemin_geometries_regions

    region_ou_departement = geopandas.read_file(chemin_geometries_territoires)
    region_ou_departement.set_crs("EPSG:4326")
    region_ou_departement.to_crs("EPSG:2154", inplace=True)
    return region_ou_departement.loc[region_ou_departement.code_insee == id_territoire.split('-')[1], 'geometry']


@chronometre
def _charger_carroyage_population(chemin_fichier_carroyage_population: Path, mask_territoire: GeoDataFrame=None) -> GeoDataFrame:
    log.info(f"##### Chargement carroyages population à partir du fichier {chemin_fichier_carroyage_population} #####")
    #  TODO : lire dans fichier zip pour éviter de stocker le gpkg unzippe
    return geopandas.read_file(
                chemin_fichier_carroyage_population,
                mask=mask_territoire
            ).loc[:, ['IdINSPIRE', 'Depcom', 'Ind', 'geometry']
           ].rename(columns={
                    'Depcom': 'code_insee_commune',
                    'Ind': 'population'})

@chronometre
def _charger_carroyage_population_simplifie_gpkg(chemin_fichier_carroyage_population: Path, mask_territoire: GeoDataFrame=None) -> GeoDataFrame:
    log.info(f"##### Chargement carroyages population à partir du fichier gpkg {chemin_fichier_carroyage_population} #####")
    return geopandas.read_file(
                chemin_fichier_carroyage_population,
                mask=mask_territoire
        )

@chronometre
def _charger_carroyage_population_simplifie_feather(chemin_fichier_carroyage_population: Path, gdf_territoire: GeoDataFrame=None) -> GeoDataFrame:
    log.info(f"    - Chargement carroyages population à partir du fichier feather {chemin_fichier_carroyage_population}")
    feather = geopandas.read_feather(chemin_fichier_carroyage_population)
    if (gdf_territoire is not None):
        feather = feather.sjoin(geopandas.GeoDataFrame(geometry=gdf_territoire))
    return feather

@chronometre
def generer_carroyage_simplifie_format_feather(chemin_fichier_carroyage_population: Path, chemin_fichier_sortie: Path):
    log.info(f"##### Generation d'un fichier de carroyages simplifié de {chemin_fichier_carroyage_population} vers {chemin_fichier_sortie} #####")
    gdf = _charger_carroyage(chemin_fichier_carroyage_population)
    gdf.to_feather(chemin_fichier_sortie)

@chronometre
def generer_carroyage_simplifie_format_gpkg(chemin_fichier_carroyage_population: Path, chemin_fichier_sortie: Path):
    log.info(f"##### Generation d'un fichier de carroyages simplifié de {chemin_fichier_carroyage_population} vers {chemin_fichier_sortie} #####")
    gdf = _charger_carroyage(chemin_fichier_carroyage_population)
    gdf.to_file(chemin_fichier_sortie, driver='GPKG')


def _charger_carroyage(chemin_fichier_carroyage_population):
    gdf = geopandas.read_file(
        chemin_fichier_carroyage_population
    ).rename(columns={'Ind': 'population',
                      'Men': 'menages',
                      'IdINSPIRE': 'id_carreau'})
    gdf.geometry = gdf.geometry.centroid
    gdf['id_commune'] = 'C-' + gdf['Depcom']
    gdf = gdf.loc[:, ['id_carreau', 'id_commune', 'population', 'menages', 'geometry']]
    return gdf



def rechercher_index_et_distance_plus_proches_voisins(points: ndarray, voisins_potentiels: ndarray):
    tree = BallTree(voisins_potentiels, leaf_size=15, metric='euclidean')

    # k=1 car on ne recherche que le plus proche (avec k>1 on pourrait trouver les k plus proches pour chaque point)
    distances, indices = tree.query(points, k=1)

    distances = distances.transpose()
    indices = indices.transpose()

    index_plus_proche = indices[0]
    distance_m = distances[0]

    return (index_plus_proche, distance_m)




@chronometre
def rechercher_plus_proche_voisin_selon_distance_euclidienne(points: GeoDataFrame, voisins_potentiels: GeoDataFrame) -> GeoDataFrame:

    log.info(f"          - Recherche du plus proche voisin")

    # Positionner des index numériques : nécessaire sur les voisins pour l'algo de recherche, et nécessaire sur les points pour la jointure finale
    points_copie = points.copy().reset_index(drop=True)
    voisins_potentiels_copie = voisins_potentiels.copy().reset_index(drop=True).rename(columns={'geometry': 'geometry_voisin'})

    # # Parse coordinates from points and insert them into a numpy array as RADIANS
    np_points = np.array([points_copie['geometry'].x, points_copie['geometry'].y]).transpose()
    np_voisins = np.array([voisins_potentiels_copie['geometry_voisin'].x, voisins_potentiels_copie['geometry_voisin'].y]).transpose()

    index_plus_proche_voisin, distance_m = rechercher_index_et_distance_plus_proches_voisins(points=np_points, voisins_potentiels=np_voisins)

    # Return points from right GeoDataFrame that are closest to points in left GeoDataFrame
    voisins_trouves = voisins_potentiels_copie.loc[index_plus_proche_voisin]

    # Ensure that the index corresponds the one in left_gdf
    voisins_trouves = voisins_trouves.reset_index(drop=True)

    voisins_trouves['distance_m'] = distance_m
    voisins_trouves = voisins_trouves.loc[:, [
                                                 'code_type_point_distribution', 'nom_point_distribution',
                                                    'distance_m'
                                             ]]

    return points_copie.join(voisins_trouves)


@chronometre
def calculer_distance_plus_proche_point_distribution(gdf_carreaux_population: GeoDataFrame,
                                                     gdf_points_distribution: GeoDataFrame,
                                                     type_commerce: str) -> DataFrame:

    log.info(f"        - Calcul distance au plus proche point de distribution de type {type_commerce} pour tous les carreaux")
    gdf_points_distribution = gdf_points_distribution.loc[(gdf_points_distribution['code_type_point_distribution'] == type_commerce), :]

    gdf_carreaux_proche_voisin_et_distance = rechercher_plus_proche_voisin_selon_distance_euclidienne(gdf_carreaux_population,
                                                                           gdf_points_distribution)
    df_carreaux = pandas.DataFrame(gdf_carreaux_proche_voisin_et_distance.loc[:,
                   ['id_carreau', 'id_commune', 'code_type_point_distribution', 'population', 'menages', 'distance_m']])
    return df_carreaux


def ajouter_colonne_habitants_a_moins_de(df_carreaux: DataFrame) -> DataFrame:
    classes_distances_km = [1, 3, 10]
    for d in classes_distances_km:
        nom_colonne = f'habitants_a_moins_de_{d}km'
        df_carreaux[nom_colonne] = df_carreaux['population']
        df_carreaux.loc[(df_carreaux['distance_m'] > d*1000), [nom_colonne]] = 0
    return df_carreaux


@chronometre
def calculer_indicateurs_proximite_par_commune(df_carreaux: DataFrame) -> DataFrame:
    log.info(f"    - Calcul des indicateurs par commune")

    df_carreaux['distance_ponderee_par_menages'] = (df_carreaux['distance_m'] * df_carreaux['menages'])
    df_carreaux['distance_ponderee_par_population'] = (df_carreaux['distance_m'] * df_carreaux['population'])
    df_carreaux = df_carreaux.drop(columns=['distance_m'])

    # traitement des arrondissements
    df_carreaux.loc[df_carreaux.id_commune.str.startswith('C-751'), 'id_commune'] = 'C-75056'
    df_carreaux.loc[df_carreaux.id_commune.str.startswith('C-6938'), 'id_commune'] = 'C-69123'
    df_carreaux.loc[df_carreaux.id_commune.str.startswith('C-1320'), 'id_commune'] = 'C-13055'
    df_carreaux.loc[df_carreaux.id_commune.str.startswith('C-1321'), 'id_commune'] = 'C-13055'

    df_commune = df_carreaux.groupby(['id_commune', 'code_type_point_distribution']).sum().reset_index()

    return df_commune

def filtrer_points_distribution(gdf_points_distribution, type_commerce):
    gdf_points_distribution = gdf_points_distribution.loc[(gdf_points_distribution['code_type_point_distribution'] == type_commerce), :]
    return gdf_points_distribution



@chronometre
def calculer_indicateurs_proximite_par_niveau_supracommunal(chemin_fichier_referentiel_territoires: Path, df_communes: DataFrame):
    log.info(f"    - Calcul des indicateurs par niveu supracommunal")

    territoires = pandas.read_csv(chemin_fichier_referentiel_territoires, sep=";")

    df_communes= df_communes.merge(territoires, left_on='id_commune', right_on='id_territoire', how='left')

    df_communes = df_communes.loc[:, ['id_commune', 'id_pays', 'id_region', 'id_departement', 'id_epci',
                    'code_type_point_distribution', 'population', 'menages',
                    'habitants_a_moins_de_1km', 'habitants_a_moins_de_3km', 'habitants_a_moins_de_10km', 'distance_ponderee_par_population', 'distance_ponderee_par_menages'
                    ]]

    df_indicateurs = (df_communes.groupby(['id_pays', 'code_type_point_distribution']).sum().reset_index().rename(columns={'id_pays': 'id_territoire'})
                      .append(df_communes.groupby(['id_region', 'code_type_point_distribution']).sum().reset_index().rename(columns={'id_region': 'id_territoire'}), ignore_index=True)
                      .append(df_communes.groupby(['id_departement', 'code_type_point_distribution']).sum().reset_index().rename(columns={'id_departement': 'id_territoire'}), ignore_index=True)
                      .append(df_communes.groupby(['id_epci', 'code_type_point_distribution']).sum().reset_index().rename(columns={'id_epci': 'id_territoire'}), ignore_index=True)
                      .append(df_communes.rename(columns={'id_commune': 'id_territoire'}), ignore_index=True)
                      )
    df_indicateurs['distance_plus_proche_point_distribution_m'] = df_indicateurs['distance_ponderee_par_population'] / df_indicateurs['population']
    df_indicateurs['distance_plus_proche_point_distribution_par_menage_m'] = df_indicateurs['distance_ponderee_par_menages'] / df_indicateurs['menages']
    df_indicateurs = df_indicateurs.loc[:, ['id_territoire',
                                   'code_type_point_distribution', 'population', 'menages',
                                   'distance_plus_proche_point_distribution_m', 'distance_plus_proche_point_distribution_par_menage_m',
                                   'habitants_a_moins_de_1km', 'habitants_a_moins_de_3km', 'habitants_a_moins_de_10km',
                                   'distance_ponderee_par_population', 'distance_ponderee_par_menages'
                                   ]]
    return territoires.loc[:, ['id_territoire', 'nom_territoire', 'categorie_territoire']].merge(df_indicateurs, how="inner")



@chronometre
def calculer_indicateurs_proximite(
                    chemin_fichier_points_distribution: Path,
                    chemin_fichier_carroyage_population: Path,
                    id_territoire: str,
                    chemin_fichier_geometries_departements: Path,
                    chemin_fichier_geometries_regions: Path,
                    chemin_fichier_referentiel_territoires: Path,
                    chemin_dossier_resultat: Path):

    log.info(f"##### Calcul des indicateurs de proximite aux points de distribution pour le territoire {id_territoire} #####")

    sources = charger_sources(chemin_fichier_points_distribution,
                              chemin_fichier_carroyage_population,
                              id_territoire,
                              chemin_fichier_geometries_departements,
                              chemin_fichier_geometries_regions
                              )
    gdf_points_distribution = sources['gdf_points_distribution']
    gdf_carreaux_population = sources['gdf_carreaux_population']
    #
    df_carreaux = calculer_indicateurs_proximite_par_carreau(gdf_carreaux_population, gdf_points_distribution)
    df_carreaux.loc[:, ['id_carreau', 'code_type_point_distribution', 'population', 'menages', 'distance_m']].to_csv(chemin_dossier_resultat / 'indicateurs_proximite_par_carreau.csv', sep=";", index=False,
              float_format='%.2f')
    df_communes = calculer_indicateurs_proximite_par_commune(df_carreaux)
    df_indicateurs = calculer_indicateurs_proximite_par_niveau_supracommunal(chemin_fichier_referentiel_territoires, df_communes)

    df_indicateurs = ajouter_colonnes_part_pop(df_indicateurs)

    log.info(f"    - Export du fichier resultat")
    df_indicateurs.to_csv(chemin_dossier_resultat / 'indicateurs_proximite.csv', sep=";", index=False,
              float_format='%.2f')


def ajouter_colonnes_part_pop(df_indicateurs):
    df_indicateurs['part_population_a_moins_de_1km'] = df_indicateurs['habitants_a_moins_de_1km'] / df_indicateurs[
        'population']
    df_indicateurs['part_population_a_moins_de_3km'] = df_indicateurs['habitants_a_moins_de_3km'] / df_indicateurs[
        'population']
    df_indicateurs['part_population_a_moins_de_10km'] = df_indicateurs['habitants_a_moins_de_10km'] / df_indicateurs[
        'population']
    df_indicateurs['50_pourcent_population_a_moins_de'] = '10km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_3km'] >= 0.50, '50_pourcent_population_a_moins_de'] = '3km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_1km'] >= 0.50, '50_pourcent_population_a_moins_de'] = '1km'
    df_indicateurs['75_pourcent_population_a_moins_de'] = '10km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_3km'] >= 0.75, '75_pourcent_population_a_moins_de'] = '3km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_1km'] >= 0.75, '75_pourcent_population_a_moins_de'] = '1km'
    df_indicateurs['80_pourcent_population_a_moins_de'] = '10km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_3km'] >= 0.80, '80_pourcent_population_a_moins_de'] = '3km'
    df_indicateurs.loc[
        df_indicateurs['part_population_a_moins_de_1km'] >= 0.80, '80_pourcent_population_a_moins_de'] = '1km'
    return df_indicateurs


def calculer_indicateurs_proximite_par_carreau(gdf_carreaux_population, gdf_points_distribution):
    log.info(f"    - Calcul des indicateurs par carreau")

    df_carreaux = ajouter_indicateurs_pour_type_point_distribution(gdf_carreaux_population, gdf_points_distribution,
                                                                   CODE_GRANDE_SURFACE_GENERALISTE)
    df_carreaux = ajouter_indicateurs_pour_type_point_distribution(gdf_carreaux_population, gdf_points_distribution,
                                                                   CODE_PETITE_SURFACE_GENERALISTE, df_carreaux)
    df_carreaux = ajouter_indicateurs_pour_type_point_distribution(gdf_carreaux_population, gdf_points_distribution,
                                                                   CODE_BOULANGERIE_PATISSERIE, df_carreaux)
    df_carreaux = ajouter_indicateurs_pour_type_point_distribution(gdf_carreaux_population, gdf_points_distribution,
                                                                   CODE_COMMERCE_SPECIALISE, df_carreaux)
    df_carreaux = ajouter_indicateurs_pour_type_point_distribution_TOUS(df_carreaux)
    return df_carreaux


@chronometre
def ajouter_indicateurs_pour_type_point_distribution_TOUS(df):
    log.info(f"      - Calcul pour les types TOUS_xxx")

    colonnes_groupby = ['id_carreau', 'id_commune', 'population', 'menages']
    df_communes_valeurs_moyennes = (df.groupby(colonnes_groupby)
                                    .mean().reset_index())
    df_communes_valeurs_moyennes['code_type_point_distribution'] = 'TOUS_MOYENNE'

    # idxmin est prend plusieurs minutes, on fait a la place un min ou max ciblé sur chaque colonne
    # dans notre cas, le max (resp min) des "habitants à moins de" correspond toujours au min (resp max) de la distance
    # df_communes_valeurs_plus_proche = df.loc[df.reset_index().groupby(colonnes_groupby)['distance_m'].idxmin()]
    df_communes_valeurs_plus_proche = (df.groupby(colonnes_groupby)
     .agg(
        habitants_a_moins_de_1km=("habitants_a_moins_de_1km", "max"),
        habitants_a_moins_de_3km=("habitants_a_moins_de_3km", "max"),
        habitants_a_moins_de_10km=("habitants_a_moins_de_10km", "max"),
        distance_m=("distance_m", "min"))
     .reset_index())
    df_communes_valeurs_plus_proche['code_type_point_distribution'] = 'TOUS_PLUS_PROCHE'

    # idxmax est prend plusieurs minutes, on fait a la place un min ou max ciblé sur chaque colonne
    # dans notre cas, le max (resp min) des "habitants à moins de" correspond toujours au min (resp max) de la distance
    # df_communes_valeurs_plus_loin = df.loc[df.reset_index().groupby(colonnes_groupby)['distance_m'].idxmax()]
    df_communes_valeurs_plus_loin = (df.groupby(colonnes_groupby)
     .agg(
        habitants_a_moins_de_1km=("habitants_a_moins_de_1km", "min"),
        habitants_a_moins_de_3km=("habitants_a_moins_de_3km", "min"),
        habitants_a_moins_de_10km=("habitants_a_moins_de_10km", "min"),
        distance_m=("distance_m", "max"))
     .reset_index())
    df_communes_valeurs_plus_loin['code_type_point_distribution'] = 'TOUS_PLUS_LOIN'

    df = df.append(df_communes_valeurs_moyennes)
    df = df.append(df_communes_valeurs_plus_proche)
    df = df.append(df_communes_valeurs_plus_loin)
    return df


def ajouter_indicateurs_pour_type_point_distribution(gdf_carreaux_population, gdf_points_distribution, type_point_distribution, df_resultat: DataFrame=None):
    log.info(f"      - Calcul pour le type de point de distribution {type_point_distribution}")

    df_carreaux = calculer_distance_plus_proche_point_distribution(gdf_carreaux_population,
                                                                                        gdf_points_distribution,
                                                                                        type_point_distribution)
    df_carreaux = ajouter_colonne_habitants_a_moins_de(df_carreaux)
    if (df_resultat is None):
        return df_carreaux
    else:
        return df_resultat.append(df_carreaux, ignore_index=True)

