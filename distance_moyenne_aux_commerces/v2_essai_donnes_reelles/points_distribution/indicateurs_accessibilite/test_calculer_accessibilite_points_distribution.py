import unittest



# augmenter_nombre_colonnes_affichees_pandas()
import geopandas
import pandas

from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.indicateurs_accessibilite.calculer_accessibilite_points_distribution import \
    rechercher_plus_proche_voisin_selon_distance_euclidienne


class TestCalculerAccessibilitePointsDistribution(unittest.TestCase):

    def test_calculer_voisins(self):
        points = pandas.DataFrame(columns=['nom_point', 'x', 'y'],
                               data=[
                                   ['A', 0, 0],
                                   ['B', 10, 10],
                                   ['C', 11, 11]],
                                  index=['a', 'z', 'C'])
        voisins_potentiels = pandas.DataFrame(columns=['nom_voisin', 'x', 'y'],
                               data=[
                                   ['voisinA', -4, -3],
                                   ['voisinBetC', 11, 10],
                                   ['pasVoisin1', 20, 10],
                                   ['pasVoisin2', 21, 10]])

        gdf_points = geopandas.GeoDataFrame(points.loc[:, 'nom_point'], geometry=geopandas.points_from_xy(points.x, points.y))
        gdf_voisins_potentiels = geopandas.GeoDataFrame(voisins_potentiels.loc[:, 'nom_voisin'], geometry=geopandas.points_from_xy(voisins_potentiels.x, voisins_potentiels.y))
        gdf_points_et_voisins = rechercher_plus_proche_voisin_selon_distance_euclidienne(gdf_points, gdf_voisins_potentiels)

        self.assertEqual(gdf_points_et_voisins.nom_point.tolist(), ['A', 'B', 'C'])
        self.assertEqual(gdf_points_et_voisins.nom_voisin.tolist(), ['voisinA', 'voisinBetC', 'voisinBetC'])
        self.assertEqual(gdf_points_et_voisins.distance_m.tolist(), [5, 1, 1])



if __name__ == '__main__':
    unittest.main()
