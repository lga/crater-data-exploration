from pathlib import Path

import geopandas as gpd
import pandas as pd

from communs.crater_logger import log
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.chargeur_points_distribution_bpe import \
    charger_donnees_bpe
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.chargeur_points_distribution_osm import \
    charger_donnees_osm
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.config_referentiels_points_distribution import \
    CODE_GRANDE_SURFACE_GENERALISTE, CODE_PETITE_SURFACE_GENERALISTE

CODE_SURFACE_GENERALISTE = 'SURFACE_GENERALISTE'

def _construction_base_consolidee_osm_bpe(df_osm, df_bpe, df_sirene=None):
    log.info(f"Construction base consolidee OSM + BPE")
    df_osm['source'] = 'OSM'
    df_bpe['source'] = 'BPE'
    return pd.concat([df_bpe, df_osm])



def _ajouter_colonne_id_point_distribution(df_points_distribution):
    log.info(f"Ajout colonne id_point_distribution")
    df_points_distribution = df_points_distribution.astype({'longitude': float, 'latitude': float})
    df_points_distribution['hash_longitude'] = df_points_distribution['longitude'] * 100000
    df_points_distribution['hash_latitude'] = df_points_distribution['latitude'] * 100000
    df_points_distribution = df_points_distribution.astype({'hash_longitude': int, 'hash_latitude': int})
    df_points_distribution['id_point_distribution'] = df_points_distribution['source'] + '_' + df_points_distribution['code_type_point_distribution'] + '_' + \
                                  df_points_distribution['id_commune'] + '_' + df_points_distribution['hash_longitude'].map(str) + '_' + df_points_distribution['hash_latitude'].map(str)
    return df_points_distribution


def _identifier_doublons_osm_bpe(df_points_distribution):
    log.info(f"Identifier doublons OSM et BPE")
    gdf = gpd.GeoDataFrame(df_points_distribution,
                           geometry=gpd.points_from_xy(df_points_distribution['longitude'], df_points_distribution['latitude']),
                           crs='EPSG:4326')
    # Projection Lambert pour fiabiliser l'évaluation de la distance lors du buffer (1 unité = 1mètre)
    gdf = gdf.to_crs('EPSG:2154')

    gdf['code_type_point_distribution_simplifie'] = gdf['code_type_point_distribution']
    gdf.loc[gdf['code_type_point_distribution_simplifie'].isin([CODE_GRANDE_SURFACE_GENERALISTE, CODE_PETITE_SURFACE_GENERALISTE]), 'code_type_point_distribution_simplifie'] = CODE_SURFACE_GENERALISTE

    gdf_osm = gdf.loc[gdf.source == 'OSM']
    gdf_bpe = gdf.loc[gdf.source != 'OSM']

    perimetre_recherche_voisins_osm = gdf_osm.copy().loc[:, ['id_point_distribution', 'code_type_point_distribution', 'code_type_point_distribution_simplifie', 'geometry']] \
        .rename(columns={'id_point_distribution': 'id_point_distribution_osm',
                         'code_type_point_distribution': 'code_type_point_distribution_osm',
                         'code_type_point_distribution_simplifie': 'code_type_point_distribution_simplifie_osm'
                         })
    # Rayon de 80 mètres
    perimetre_recherche_voisins_osm['geometry'] = perimetre_recherche_voisins_osm.buffer(80)

    voisins_osm = gpd.sjoin(perimetre_recherche_voisins_osm, gdf_bpe)
    id_doublons = voisins_osm.loc[
        voisins_osm['code_type_point_distribution_simplifie_osm'] == voisins_osm['code_type_point_distribution_simplifie'], ['id_point_distribution_osm', 'code_type_point_distribution_osm', 'id_point_distribution']]
    id_doublons['doublon_a_supprimer'] = True

    df_points_distribution = df_points_distribution.merge(id_doublons, how='left', left_on='id_point_distribution', right_on='id_point_distribution')
    df_points_distribution = df_points_distribution.drop(columns=['geometry'])

    return df_points_distribution



def calculer_referentiel_points_distribution(chemin_fichier_commerces_osm: Path,
                                             chemin_fichier_zip_commerces_bpe: Path,
                                             nom_fichier_dans_zip_bpe: str,
                                             chemin_dossier_output: Path):
    df_osm = charger_donnees_osm(chemin_fichier_commerces_osm)
    df_bpe = charger_donnees_bpe(chemin_fichier_zip_commerces_bpe, nom_fichier_dans_zip_bpe)
    df_consolide = _construction_base_consolidee_osm_bpe(df_osm, df_bpe)
    df_consolide = _ajouter_colonne_id_point_distribution(df_consolide)
    df_consolide = _identifier_doublons_osm_bpe(df_consolide)
    _exporter_referentiel_commerces(chemin_dossier_output, df_consolide)


def _exporter_referentiel_commerces(chemin_dossier_output, df_consolide):
    colonnes_a_exporter = ['id_point_distribution', 'id_commune', 'id_departement', 'id_region', 'nom_point_distribution',
               'code_type_point_distribution',
               'libelle_type_point_distribution', 'longitude', 'latitude', 'source']
    df_consolide.loc[
        df_consolide.doublon_a_supprimer != True,
        colonnes_a_exporter
        ].to_csv(chemin_dossier_output / "referentiel_points_distribution.csv",
                        sep=";",
                        index=False)
    df_consolide.loc[:,
    colonnes_a_exporter + ['id_point_distribution_osm', 'code_type_point_distribution_osm', 'doublon_a_supprimer']
        ].to_csv(chemin_dossier_output / "referentiel_points_distribution_sans_dedoublonnage.csv",
                        sep=";",
                        index=False)
