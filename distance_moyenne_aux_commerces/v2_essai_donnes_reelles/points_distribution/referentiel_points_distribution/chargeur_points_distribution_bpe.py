import zipfile
from pathlib import Path

import pandas as pd
import geopandas as gpd

from communs.crater_logger import log
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater, \
    traduire_code_insee_vers_id_departement_crater, traduire_code_insee_vers_id_region_crater
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.config_referentiels_points_distribution import \
    CODE_GRANDE_SURFACE_GENERALISTE, CODE_PETITE_SURFACE_GENERALISTE, CODE_BOULANGERIE_PATISSERIE, \
    CODE_COMMERCE_SPECIALISE

CODES_COMMERCES_ALIMENTAIRES_BPE = {
    'B101': {'code': CODE_GRANDE_SURFACE_GENERALISTE, 'libelle': "Hypermarché"},
    'B102': {'code': CODE_GRANDE_SURFACE_GENERALISTE, 'libelle': "Supermarché"},
    'B201': {'code': CODE_PETITE_SURFACE_GENERALISTE, 'libelle': "Supérette"},
    'B202': {'code': CODE_PETITE_SURFACE_GENERALISTE, 'libelle': "Épicerie"},
    'B203': {'code': CODE_BOULANGERIE_PATISSERIE, 'libelle': "Boulangerie Patisserie"},
    'B204': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Boucherie charcuterie"},
    'B205': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Produits surgelés"},
    'B206': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Poissonnerie"}
}


def charger_donnees_bpe(chemin_fichier_zip_commerces_bpe: Path, nom_fichier: str):
    df_bpe = _charger_source_bpe(chemin_fichier_zip_commerces_bpe, nom_fichier)
    df_bpe = _normaliser_donnees_bpe(df_bpe)
    return df_bpe


def _charger_source_bpe(chemin_fichier_zip_commerces_bpe, nom_fichier: str):
    log.info(f"Chargement commerces BPE depuis {chemin_fichier_zip_commerces_bpe}")
    data_bpe = pd.read_csv(
                            zipfile.ZipFile(chemin_fichier_zip_commerces_bpe, 'r').open(nom_fichier),
                            sep=";",
                            dtype={
                               'AAV2020': str,
                               'BV2012': str,
                               'UU2020': str,
                               'REG': str,
                               'DEP': str,
                               'DEPCOM': str},
                           )
    return data_bpe


def _normaliser_donnees_bpe(df_bpe):
    log.info(f"Normalisation données BPE")
    # On filtre les données non geolocalisées et les commerces non alimentaires
    df_bpe = (df_bpe
                  .loc[df_bpe.TYPEQU.isin(CODES_COMMERCES_ALIMENTAIRES_BPE.keys()), :]
                  .loc[df_bpe.QUALITE_XY != 'Non géolocalisé', :]
                  )
    # Calcul long/lat
    gdf_bpe = gpd.GeoDataFrame(df_bpe, geometry=gpd.points_from_xy(df_bpe.LAMBERT_X, df_bpe.LAMBERT_Y, crs="EPSG:2154"))
    gdf_bpe.to_crs("EPSG:4326", inplace=True)

    gdf_bpe['longitude'] = gdf_bpe.geometry.x
    gdf_bpe['latitude'] = gdf_bpe.geometry.y

    #  Normalisation des champs et des noms
    df_bpe['id_commune'] = traduire_code_insee_vers_id_commune_crater(df_bpe['DEPCOM'])
    df_bpe['id_departement'] = traduire_code_insee_vers_id_departement_crater(df_bpe['DEP'])
    df_bpe['id_region'] = traduire_code_insee_vers_id_region_crater(df_bpe['REG'])
    df_bpe = df_bpe.rename(columns=
                           {'AN': 'annee',
                            'LAMBERT_Y': 'lambert_y',
                            'LAMBERT_X': 'lambert_x',
                            'QUALITE_XY': 'qualite_geolocalisation'
                            })
    # traduction type de commerces
    dict_correspondance_codes_commerces = {code: valeur_normalisee['code'] for (code, valeur_normalisee) in
                                           CODES_COMMERCES_ALIMENTAIRES_BPE.items()}
    dict_correspondance_libelle_commerces = {code: valeur_normalisee['libelle'] for (code, valeur_normalisee) in
                                             CODES_COMMERCES_ALIMENTAIRES_BPE.items()}
    df_bpe['code_type_point_distribution'] = df_bpe.TYPEQU.map(dict_correspondance_codes_commerces)
    df_bpe['libelle_type_point_distribution'] = df_bpe.TYPEQU.map(dict_correspondance_libelle_commerces)
    # La BPE n'a pas le nom exact des commerces, on met un nom générique
    df_bpe['nom_point_distribution'] = df_bpe['libelle_type_point_distribution'] + ' BPE'

    df_bpe = df_bpe.loc[:, ['id_region', 'id_departement', 'id_commune',
                            # 'annee',
                            'nom_point_distribution',
                            'code_type_point_distribution', 'libelle_type_point_distribution',
                            # 'lambert_x','lambert_y',
                            'qualite_geolocalisation',
                            'longitude', 'latitude']]

    #  Il y a des doublons dans la BPE => le meme commmerce declaré sur plusieurs lignes
    df_bpe = df_bpe.drop_duplicates(ignore_index=True)
    return df_bpe


















