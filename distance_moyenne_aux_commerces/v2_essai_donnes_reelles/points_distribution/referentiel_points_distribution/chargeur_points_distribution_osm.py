from pathlib import Path

import pandas as pd

#  Pas évident de faire ce mapping => les types ne sont pas normalisés dans OSM, plus de 1500 différents
# mais un value_counts() sur le dataframe, montre quand meme que les principaux types ressortent le plus souvent
# par contre pas de disctinction entre les tailles de supermarchés
# Voir  https://wiki.openstreetmap.org/wiki/FR:Key:shop
from communs.crater_logger import log
from communs.traduction_id_territoires import traduire_code_insee_vers_id_commune_crater, \
    traduire_code_insee_vers_id_departement_crater, traduire_code_insee_vers_id_region_crater
from distance_moyenne_aux_commerces.v2_essai_donnes_reelles.points_distribution.referentiel_points_distribution.config_referentiels_points_distribution import \
    CODE_GRANDE_SURFACE_GENERALISTE, CODE_PETITE_SURFACE_GENERALISTE, CODE_BOULANGERIE_PATISSERIE, \
    CODE_COMMERCE_SPECIALISE

CODES_COMMERCES_ALIMENTAIRES_OSM = {
    'supermarket': {'code': CODE_GRANDE_SURFACE_GENERALISTE, 'libelle': "Supermarché"},
    'department_store': {'code': CODE_GRANDE_SURFACE_GENERALISTE, 'libelle': "Department store"},
    'convenience': {'code': CODE_PETITE_SURFACE_GENERALISTE, 'libelle': "Supérette"},
    'deli': {'code': CODE_PETITE_SURFACE_GENERALISTE, 'libelle': "Épicerie fine"},
    'bakery': {'code': CODE_BOULANGERIE_PATISSERIE, 'libelle': "Boulangerie"},
    'pastry': {'code': CODE_BOULANGERIE_PATISSERIE, 'libelle': "Patisserie"},
    'butcher': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Boucherie charcuterie"},
    'seafood': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Poissonnerie"},
    'frozen_food': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Produits surgelés"},
    'cheese': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Fromagerie"},
    'dairy': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Crèmerie"},
    'greengrocer': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Primeur"},
    'pasta': {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Pâtes"},
    "caterer": {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Traiteur"},
    "cannery": {'code': CODE_COMMERCE_SPECIALISE, 'libelle': "Conserverie"},
    # 'farm': {'code': 'AUTRE', 'libelle': "Vente à la ferme"},
    # 'wholesale': {'code': 'AUTRE', 'libelle': "Vente en entrepôt"},
}

def charger_donnees_osm(chemin_fichier_commerces_osm: Path):
    log.info(f"Chargement commerces OSM depuis {chemin_fichier_commerces_osm}")
    df_osm = _charger_source_osm(chemin_fichier_commerces_osm)
    return _normaliser_donnees_osm(df_osm)


def _charger_source_osm(chemin_fichier_commerces_osm: Path):
    df_osm = pd.read_csv(chemin_fichier_commerces_osm, sep=";",
                         dtype={
                             "code_region": str,
                             "code_departement": str,
                             "code_commune": str,
                             "com_insee": str
                         })
    return df_osm


def _normaliser_donnees_osm(df_osm):
    log.info(f"Normalisation données source OSM")

    df_osm = df_osm.loc[df_osm.type.isin(CODES_COMMERCES_ALIMENTAIRES_OSM.keys())].copy()

    # Extraction longitude/latitude
    df_osm_ll = df_osm['Geo Point'].str.split(",", expand=True)
    df_osm['longitude'] = df_osm_ll.iloc[:, 1]
    df_osm['latitude'] = df_osm_ll.iloc[:, 0]

    #  Normalisation des champs et des noms
    df_osm['id_commune'] = traduire_code_insee_vers_id_commune_crater(df_osm['code_commune'])
    df_osm['id_departement'] = traduire_code_insee_vers_id_departement_crater(df_osm['code_departement'])
    df_osm['id_region'] = traduire_code_insee_vers_id_region_crater(df_osm['code_region'])
    df_osm = df_osm.rename(columns={'name': 'nom_point_distribution'})
    # traduction type de commerces
    dict_correspondance_codes_commerces = {code_bpe: type_pivot['code'] for (code_bpe, type_pivot) in
                                           CODES_COMMERCES_ALIMENTAIRES_OSM.items()}
    dict_correspondance_libelle_commerces = {code_bpe: type_pivot['libelle'] for (code_bpe, type_pivot) in
                                             CODES_COMMERCES_ALIMENTAIRES_OSM.items()}
    df_osm['code_type_point_distribution'] = df_osm.type.map(dict_correspondance_codes_commerces)
    df_osm['libelle_type_point_distribution'] = df_osm.type.map(dict_correspondance_libelle_commerces)

    df_osm = df_osm.loc[:, ['id_region', 'id_departement', 'id_commune',
                            'nom_point_distribution',
                            'code_type_point_distribution', 'libelle_type_point_distribution',
                            'longitude', 'latitude']]
    #  Suppression des doublons pour certains cas de commerces déclarés dans 2 communes
    # Voir par exemple la boucherie F.Gueydon qui est en 2 exemplaires
    # 1 à Talence (com_id=33522, mais com_insee=33522)
    # 1 à Talence selon com_id mais a bordeaux selon com_insee (com_id=33522, mais com_insee=33063)
    df_osm = df_osm.drop_duplicates(ignore_index=True)
    return df_osm

