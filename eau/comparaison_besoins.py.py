#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 30 14:57:42 2023

@author: vincent
"""
import numpy as np
import matplotlib.pyplot as plt


# Source FAO chap. 6


def calculer_courbe_Kc(plante):
    plante['duree_culture'] = plante['L_ini'] + plante['L_dev'] + plante['L_mid'] + plante['L_late']
    jours = np.arange(plante['plantation'],plante['plantation']+plante['duree_culture'])
    Kc = np.zeros(len(jours))
    
    j0 = jours[0]
    j1 = j0 + plante['L_ini']
    j2 = j1 + plante['L_dev']
    j3 = j2 + plante['L_late']
    
    ind = np.argwhere(jours<=j1)
    Kc[ind] = plante['Kc_ini']
    
    
    ind = np.argwhere((jours>j1) & (jours<=j2))
    Kc[ind] = plante['Kc_ini'] + (ind-ind[0])*(plante['Kc_mid']-plante['Kc_ini'])/(j2-j1)
    
    ind = np.argwhere((jours>j2) & (jours<=j3))
    Kc[ind] = plante['Kc_mid']
    
    ind = np.argwhere(jours>j3)
    Kc[ind] = plante['Kc_mid'] + (ind-ind[0])*(plante['Kc_end']-plante['Kc_mid'])/(jours[-1]-j3)
    
    if jours[-1]>365:
        jours=jours-365
    plante['jours'] = jours
    plante['Kc'] = Kc
    
    plante['Besoin_total'] = np.sum(Kc)
    
    return

plt.close('all')

# Mais (grain)
date_plantation = 3.5 * 30 # Plantation mi avril
mais_grain = {'plantation':date_plantation,
        'L_ini':30,
        'L_dev':40,
        'L_mid':50,
        'L_late':30,
        'Kc_ini':0.3,
        'Kc_mid':1.20,
        'Kc_end':0.6,
        'nom':'Mais grain'
        }

# Mais (doux) 
date_plantation = 5 * 30 # Plantation mai/juin --> fin mai
mais_doux = {'plantation':date_plantation,
        'L_ini':20,
        'L_dev':25,
        'L_mid':25,
        'L_late':10,
        'Kc_ini':0.3,
        'Kc_mid':1.15,
        'Kc_end':1.05,
        'nom':'Mais doux'
        }

# Blé hiver
date_plantation = 10.5 * 30 # Plantation mi novembre
Ble_hiver = {'plantation':date_plantation,
        'L_ini':30,
        'L_dev':140,
        'L_mid':40,
        'L_late':30,
        'Kc_ini':0.7,
        'Kc_mid':1.15,
        'Kc_end':0.4,
        'nom':'Blé (Winter wheat, no frost)'
        }

# Ble, 35/45° Lat
date_plantation = 3 * 30 # Plantation fin mars
Ble = {'plantation':date_plantation,
        'L_ini':20,
        'L_dev':25,
        'L_mid':60,
        'L_late':30,
        'Kc_ini':0.3,
        'Kc_mid':1.15,
        'Kc_end':0.4,
        'nom':'Blé'
        }

# Orge, 35/45° Lat
date_plantation = 3 * 30 # Plantation fin mars
Orge = {'plantation':date_plantation,
        'L_ini':20,
        'L_dev':25,
        'L_mid':60,
        'L_late':30,
        'Kc_ini':0.3,
        'Kc_mid':1.15,
        'Kc_end':0.25,
        'nom':'Orge'
        }

# Sorgho
date_plantation = 5 * 30 # Plantation fin mai
Sorgho = {'plantation':date_plantation,
        'L_ini':20,
        'L_dev':35,
        'L_mid':40,
        'L_late':30,
        'Kc_ini':0.3,
        'Kc_mid':1.1,
        'Kc_end':0.55,
        'nom':'Sorgho'
        }

# Patate
date_plantation = 3.5 * 30 # Plantation avril
Patate = {'plantation':date_plantation,
        'L_ini':30,
        'L_dev':35,
        'L_mid':50,
        'L_late':30,
        'Kc_ini':0.5,
        'Kc_mid':1.15,
        'Kc_end':0.75,
        'nom':'Patate'
        }

liste_cultures = [mais_grain, Ble, Orge, Sorgho, Patate]

plt.figure(figsize=[10,8])
for culture in liste_cultures:
    calculer_courbe_Kc(culture)
    plt.plot(culture['jours'],culture['Kc'],label=culture['nom'])
plt.xlabel('DOY')
plt.ylabel('Kc')
plt.legend(loc='upper left')
plt.text(0,0.05,'Janvier',rotation='vertical')
plt.text(31,0.05,'Février',rotation='vertical')
plt.text(59,0.05,'Mars',rotation='vertical')
plt.text(90,0.05,'Avril',rotation='vertical')
plt.text(120,0.05,'Mai',rotation='vertical')
plt.text(151,0.05,'Juin',rotation='vertical')
plt.text(181,0.05,'Juillet',rotation='vertical')
plt.text(212,0.05,'Aout',rotation='vertical')
plt.text(243,0.05,'Septembre',rotation='vertical')
plt.text(273,0.05,'Octobre',rotation='vertical')
plt.text(304,0.05,'Novembre',rotation='vertical')
plt.text(334,0.05,'Décembre',rotation='vertical')
plt.xlim([0,365])
plt.ylim([0,1.3])
plt.grid()
plt.tight_layout()
plt.savefig('Comparaison_courbes_Kc.png')


noms = [culture['nom'] for culture in liste_cultures]
besoin_total = [culture['Besoin_total'] for culture in liste_cultures]

plt.figure()
plt.bar(noms,besoin_total)
plt.ylabel("Kc integré")
plt.title('Besoin en eau sur un cycle de culture')
plt.savefig('Besoin_en_eau_total_par_culture.png')










