#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 14:48:13 2022

@author: vincent
"""
import glob
import os
import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable



#%%
def load_SWI_data(swi_data_path):
    filelist = glob.glob(os.path.join(swi_data_path,'*.csv'))
    if len(filelist)==0:
        print('EMPTY FILE LIST')
    
    swi_data = pd.DataFrame()
    for f in filelist:
        df = pd.read_csv(f,sep=';',decimal=',')
        swi_data = pd.concat([swi_data,df])
    
    swi_data = swi_data.sort_values(by='NUMERO')

    return swi_data


def select_swi_month_and_convert_to_geo(swi_df,month=202012):
    
    swi_data_subset = swi_df[swi_df.DATE==month]
    gdf = gpd.GeoDataFrame(swi_data_subset, geometry=gpd.points_from_xy(swi_data_subset.LAMBX, swi_data_subset.LAMBY))
    
    return gdf
   

def plot_swi_data(swi_gdf,title,filename=''):
    fig, ax = plt.subplots(1, 1, figsize=[10,10])
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    swi_gdf.plot(ax=ax, column='SWI_UNIF_MENS3', cmap='bwr_r', legend=True, vmin=0, vmax=2, cax=cax, legend_kwds={'label':'SWI'})
    ax.set_title('SWI ' + str(title))
    if len(filename)>0:
        plt.savefig(filename)


def select_swi_year_and_convert_to_geo(swi_df,year=2020):
    swi_data_subset = swi_df[np.floor(swi_df.DATE/100)==year]
    swi_year_mean = swi_data_subset.groupby(by='NUMERO').mean()
    swi_year_gdf = gpd.GeoDataFrame(swi_year_mean, geometry=gpd.points_from_xy(swi_year_mean.LAMBX, swi_year_mean.LAMBY))
    swi_year_gdf = swi_year_gdf.set_crs('epsg:2154')
    return swi_year_gdf

def load_bnpe_data(bnpe_data_path):
    bnpe_data = pd.read_csv(os.path.join(bnpe_data_path,'prelevements.csv'))
    bnpe_gdf = gpd.GeoDataFrame(bnpe_data, geometry=gpd.points_from_xy(bnpe_data.Longitude, bnpe_data.Latitude))
    bnpe_gdf = bnpe_gdf.set_crs('epsg:4326')
    bnpe_gdf = bnpe_gdf.to_crs('epsg:2154')
    return bnpe_gdf
    
def plot_bnpe_data(bnpe_gdf,filename=''):
    fig, ax = plt.subplots(1, 1, figsize=[10,10])
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    bnpe_gdf.plot(ax=ax, column='Volume (m3)', cmap='viridis', legend=True, vmin=0, vmax=1e4, cax=cax, legend_kwds={'label':'Volume (m3)'}, markersize=2)
    ax.set_title('Irrigation 2020')
    ax.set_xlim(0, 1.3e6)
    ax.set_ylim(6e6, 7.15e6)
    if len(filename)>0:
        plt.savefig(filename)


def load_departements(filename):
    departements_gdf = gpd.read_file(filename)
    departements_gdf = departements_gdf.to_crs('epsg:2154')
    return departements_gdf

def plot_departements(gdf,filename=''):
    fig, ax = plt.subplots(1, 1, figsize=[10,10])
    gdf.plot(ax=ax)
    if len(filename)>0:
        plt.savefig(filename)

def plot_gdf(gdf,filename='', column='', legend='True', vmin=0,vmax=1, cmap='viridis',title='',label=''):
    fig, ax = plt.subplots(1, 1, figsize=[10,10])
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    
    gdf.plot(ax=ax, column=column, cmap=cmap, legend=legend, vmin=vmin, vmax=vmax, cax=cax, legend_kwds={'label':label}, markersize=2)
    ax.set_title(title)
    ax.set_xlim(0, 1.3e6)
    ax.set_ylim(6e6, 7.15e6)
    if len(filename)>0:
        plt.savefig(filename)

def trier_noms_dataframe(df,col_name):
    # Pas ideal, par exemple trie mal 'Winter Wheat'
    sorted_name = col_name + '_sorted'
    df[sorted_name] = df[col_name].values
    for i in df.index:
        name = df.loc[i,col_name]
        lower_name = name.lower()
        short_name = lower_name.split(' ')[0]
        #print((name,short_name))
        df.loc[i,sorted_name] = short_name
    #return df

    
plt.close('all')

#%%
# Donnees SWI telechargees sur https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=301&id_rubrique=40
swi_data = load_SWI_data(r'../../local/Data_eau/SWI/SWI_Package_1969-2020')

# Selection d'un mois pour les tests
# Voir https://www.cartosport.com/catnat_swi_uniforme/ pour comparaison
month = 202012
swi_month = select_swi_month_and_convert_to_geo(swi_data,month)

#Affichage
# METTRE PLOT GENERIQUE
plot_swi_data(swi_month,month,filename='SWI_'+str(month)+'.png')

# Selection d'une annee
year = 2020
swi_year = select_swi_year_and_convert_to_geo(swi_data,year)
# METTRE PLOT GENERIQUE
plot_swi_data(swi_year,year,filename='SWI_moyen_'+str(year)+'.png')

# Moyenne annuelle France
years = [y for y in range(1980,2021)]
SWI_France = []
for y in years:
    val = select_swi_year_and_convert_to_geo(swi_data,y).SWI_UNIF_MENS3.mean()
    SWI_France.append(val)
plt.figure()
plt.plot(years,SWI_France)
plt.xlabel('Annee')
plt.ylabel('SWI moyen annuel')
plt.savefig('Evolution_SWI_Moyen.png')


#%%
# Chargement données BNPE
# https://bnpe.eaufrance.fr/acces-donnees/france/annee/2020
bnpe_gdf = load_bnpe_data(r'../../local/Data_eau/BNPE/exportBNPE_202211042008157')
# METTRE PLOT GENERIQUE
plot_bnpe_data(bnpe_gdf,filename='Irrigation_BNPE_20202.png')

# https://www.data.gouv.fr/fr/datasets/carte-des-departements-2-1/
# Chargement départements
departements_gdf = load_departements(r'../../local/Data_eau/Departements/contour-des-departements.geojson')
# METTRE PLOT GENERIQUE
plot_departements(departements_gdf,filename='departements.png')


#%%
# Moyenne SWI par dept
pointInPolys = gpd.sjoin(swi_year, departements_gdf, how='left')
SWI_avg = pointInPolys.groupby('index_right').mean()
departements_gdf['SWI'] = SWI_avg.SWI_UNIF_MENS3
plot_gdf(departements_gdf,filename='SWI_2020_par_departement.png', column='SWI', legend='True', vmin=0,vmax=2, cmap='bwr_r',title='SWI 2020',label='SWI')

# Moyenne Irrigation par dept
pointInPolys = gpd.sjoin(bnpe_gdf, departements_gdf, how='left')
Irrig_avg = pointInPolys.groupby('index_right').mean()
departements_gdf['Irrig m3'] = Irrig_avg['Volume (m3)']
plot_gdf(departements_gdf,filename='Irrig_2020_par_departement.png', column='Irrig m3', legend='True', vmin=0,vmax=1e5, cmap='viridis',title='Irrigation 2020',label='Volume m3')

#%%
# Evolution SWI annuel
for year in range(1980,2021):
    swi_year = select_swi_year_and_convert_to_geo(swi_data,year)
    pointInPolys = gpd.sjoin(swi_year, departements_gdf, how='left')
    SWI_avg = pointInPolys.groupby('index_right').mean()
    col_name = 'SWI'+str(year)
    departements_gdf[col_name] = SWI_avg.SWI_UNIF_MENS3

dept_list = ['11','29','31','24','51']

years = [y for y in range(1980,2021)]
plt.figure()
for dpt in dept_list:
    SWI_value = []
    for y in years:
        val = departements_gdf[departements_gdf['code']==dpt]['SWI'+str(y)].iloc[0]
        SWI_value.append(val)
    plt.plot(years,SWI_value,label=dpt)
plt.xlabel('Annee')
plt.ylabel('SWI moyen')
plt.legend()
plt.savefig('SWI_annuel.png')

#%%
# Chargement des données SAU
chemin_donnees_production = r'../../crater-data-resultats/data/crater/productions_besoins/sau_par_culture.csv'
sau_par_culture = pd.read_csv(chemin_donnees_production,sep=';')
sau_par_culture_dpt = sau_par_culture[sau_par_culture['categorie_territoire']=='DEPARTEMENT']


#%%
# Root depth et Depletion Fraction par groupe de culture
# https://www.fao.org/3/X0490E/x0490e0e.htm#chapter%208%20%20%20etc%20under%20soil%20water%20stress%20conditions
# Chapitre 8 du doc FAO
# PROBABLEMENT PAS UTILE !
Zr = {'CER':1.2,
      'DVC':0,
      'FLC':0.5,
      'FOU':1.2,
      'OLP':1,
      'SNC':0}
p = {'CER':0.55,
      'DVC':0,
      'FLC':0.3,
      'FOU':0.55,
      'OLP':0.6,
      'SNC':0}
# Coefficient de culture Zr*p par departement, en ponderant les valeurs FAO par les SAU de chaque département
coefficient_culture = pd.DataFrame(columns=['id_departement','K'])
for id_dept in sau_par_culture_dpt.id_territoire.unique():
    sau_dpt = sau_par_culture_dpt[sau_par_culture_dpt['id_territoire']==id_dept]
    sau_gpe_culture = sau_dpt.groupby(by='code_groupe_culture').sum()
    sau_gpe_culture = sau_gpe_culture.drop(index=['SNC','DVC'])
    sau_gpe_culture = sau_gpe_culture.drop(columns='production_groupe_culture_ha')
    K = sau_gpe_culture.loc['CER','production_culture_ha']*Zr['CER']*p['CER'] + sau_gpe_culture.loc['FLC','production_culture_ha']*Zr['FLC']*p['FLC'] + sau_gpe_culture.loc['FOU','production_culture_ha']*Zr['FOU']*p['FOU'] + sau_gpe_culture.loc['OLP','production_culture_ha']*Zr['OLP']*p['OLP']
    K = np.round(K/sau_gpe_culture.production_culture_ha.sum(),2)
    if np.isnan(K):
        K=-1
    # ATTENTION : erreurs sur quelques dept, regarder le detail...
    new_row = pd.Series({'id_departement':id_dept[-2:],'K':float(K)})
    coefficient_culture = pd.concat([coefficient_culture,new_row.to_frame().T])
    
    
# Merge avec gdf contenant les donnees irrigation et SWI
departements_gdf = departements_gdf.merge(coefficient_culture, left_on='code',right_on='id_departement')
departements_gdf['K'] = pd.to_numeric(departements_gdf['K'])
plot_gdf(departements_gdf,filename='coef_longueur_racine.png', column='K', legend='True', vmin=0.4,vmax=0.7, cmap='viridis',title='Coefficient longueur racine K=Zr*p',label='K=Zr*p')


#%%
# Plant growth and Kc coefficient
# https://www.fao.org/3/X0490E/x0490e0b.htm#chapter%206%20%20%20etc%20%20%20single%20crop%20coefficient%20(kc)
# Codes CRATer ajoutés de manière grossière a la main + tri sur les climats. A reprendre proprement
chemin_duree_developpement = r'./duree_developpement.csv'
duree_developpement = pd.read_csv(chemin_duree_developpement,sep=';')
trier_noms_dataframe(duree_developpement,'Crop')
chemin_Kc = r'./crop_coefficients.csv'
coefs_culture = pd.read_csv(chemin_Kc,sep=';')
trier_noms_dataframe(coefs_culture,'Crop')
Kc = duree_developpement.merge(coefs_culture,on='Crop_sorted',how='inner')
Kc['Kc_avg'] = (Kc['Init. (Lini)']*Kc['Kc ini'] + Kc['Dev. (Ldev)']*(Kc['Kc ini']+Kc['Kc mid'])/2 + Kc['Mid (Lmid)']*Kc['Kc mid'] + Kc['Late (Llate)']*(Kc['Kc mid']+Kc['Kc end'])/2) / Kc['Total']
Kc_groupe_culture = Kc.groupby(by='Cod').mean()
# Coefficient Kc par departement, en pondérant par SAU de chaque groupe culture
Kc_dep = pd.DataFrame(columns=['id_departement','Kc'])
for id_dept in sau_par_culture_dpt.id_territoire.unique():
    sau_dpt = sau_par_culture_dpt[sau_par_culture_dpt['id_territoire']==id_dept]
    sau_gpe_culture = sau_dpt.groupby(by='code_groupe_culture').sum()
    sau_gpe_culture = sau_gpe_culture.drop(index=['SNC','DVC'])
    sau_gpe_culture = sau_gpe_culture.drop(columns='production_groupe_culture_ha')
    coef_Kc = sau_gpe_culture.loc['CER','production_culture_ha']*Kc_groupe_culture.loc['CER','Kc_avg'] + sau_gpe_culture.loc['FLC','production_culture_ha']*Kc_groupe_culture.loc['FLC','Kc_avg'] + sau_gpe_culture.loc['OLP','production_culture_ha']*Kc_groupe_culture.loc['OLP','Kc_avg']
    coef_Kc = np.round(coef_Kc/sau_gpe_culture.production_culture_ha.sum(),2)
    if np.isnan(coef_Kc):
        K=-1
    # ATTENTION : erreurs sur quelques dept, regarder le detail...
    new_row = pd.Series({'id_departement':id_dept[-2:],'Kc':float(coef_Kc)})
    coefficient_culture = pd.concat([coefficient_culture,new_row.to_frame().T])

# Merge avec gdf contenant les donnees irrigation et SWI
departements_gdf = departements_gdf.merge(coefficient_culture, left_on='code',right_on='id_departement')
departements_gdf['Kc'] = pd.to_numeric(departements_gdf['Kc'])
plot_gdf(departements_gdf,filename='coef_culture.png', column='Kc', legend='True', vmin=0,vmax=1, cmap='viridis',title='Coefficient de culture Kc',label='Kc moyen')

# Comparaison SWI / irrigation / besoins de cultures
plt.figure(figsize=[10,10])
plt.subplot(221)
plt.plot(departements_gdf.SWI,departements_gdf.Kc,'.',markersize=5)
plt.xlabel('SWI 2020')
plt.ylabel('Kc')
plt.subplot(222)
plt.plot(departements_gdf.SWI,departements_gdf['Irrig m3'],'.',markersize=5)
plt.xlabel('SWI 2020')
plt.ylabel('Irrig m3')
plt.ylim([0,1e5])
plt.subplot(223)
plt.plot(departements_gdf.Kc,departements_gdf['Irrig m3'],'.',markersize=5)
plt.xlabel('Kc')
plt.ylabel('Irrig m3')
plt.ylim([0,1e5])
plt.tight_layout()
plt.savefig('correlations_SWI_Irrig_Kc.png')

# Est-ce que les cultures ont assez d'eau naturellement ?
# Attention, difficile de dire ce qui est une 'bonne valeur sur cette échelle
# En particulier on a fait tout un tas de normalisation par les SAU. Verifier les grandeurs intensives et extensives pour s'assurer qu'on ne dit pas de conneries...
departements_gdf['satisfaction_culture'] = departements_gdf['SWI'] / departements_gdf['Kc']
plot_gdf(departements_gdf,filename='satisfaction_culture.png', column='satisfaction_culture', legend='True', vmin=0,vmax=5, cmap='viridis',title='Satisfaction du besoin en eau des cultures SWI/Kc',label='SWI/Kc') 
    
    
#%%
# Carte reserves en eau GIS sols
#https://doi.org/10.15454/JPB9RB
# Les valeurs des classes sont : 1 : < 50 mm 2 : 50-100 mm 3 : 100-150 mm 4 : 150 - 200 mm 5 : >= 200 mm 9 : non sols (villes, lacs, etc.)
chemin_donnees_GIS = r'../../local/Data_eau/GIS_sols/bdgsf_classe_ru.zip'
GIS_gdf = gpd.read_file(chemin_donnees_GIS)

#GIS_gdf = GIS_gdf.set_crs('epsg:27582')
GIS_gdf = GIS_gdf.to_crs('epsg:2154')

classe_dict = {1:'[1] < 50 mm',
               2:'[2] 50-100 mm',
               3:'[3] 100-150 mm',
               4:'[4] 150-200 mm',
               5:'[5] >= 200 mm',
               9:'[0] Non sols'}

GIS_gdf['Nom_classe'] = [classe_dict[x] for x in GIS_gdf.CLASSE]

fig, ax = plt.subplots(1, 1, figsize=[10,10])
GIS_gdf.plot(ax=ax, column='Nom_classe', cmap='Blues', legend=True, categorical=True)
ax.set_title('Reserves en eau utile de sols')
ax.set_xlim(0, 1.3e6)
ax.set_ylim(6e6, 7.15e6)
plt.savefig('./Reserves_en_eau_GIS_sols.png')






















