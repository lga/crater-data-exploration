#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 15:44:05 2022

@author: vincent
"""

import pandas as pd

'''
# Chragement referentiel territoires
chemin_referentiel_territoires = r'../../crater-data-resultats/data/crater/territoires/referentiel_territoires.csv'
referentiel_territoires = pd.read_csv(chemin_referentiel_territoires,sep=';')

liste_regions = referentiel_territoires[referentiel_territoires.categorie_territoire=='REGION'].id_territoire.tolist()

# Chargment données SAU région
sau_departements = pd.DataFrame()

for region in liste_regions:
    print(region)
    chemin_fichier_sau = r'../../crater-data-resultats/data/surface_agricole_utile/sau_par_commune_et_culture/sau_rpg_communes_{}.csv'.format(region)
    sau_region = pd.read_csv(chemin_fichier_sau,sep=';')
    #Ajout données territoire
    sau_region = sau_region.merge(referentiel_territoires,left_on='id_commune',right_on='id_territoire',how='inner')
    sau_region = sau_region.drop(columns=[ 'nom_commune',
       'nombre_parcelles', 'sau_moyenne_par_parcelle_ha', 'id_territoire', 'id_commune',
       'nom_territoire', 'categorie_territoire', 'id_pays', 'id_region', 'ids_regroupements_communes',
       'categorie_regroupement_communes', 'id_epci', 'categorie_epci',
       'nb_communes', 'ids_arrondissements_commune', 'codes_postaux_commune',
       'annee_dernier_mouvement_commune', 'type_mouvement_commune'])

    sau_departements = pd.concat([sau_departements,sau_region.groupby(by=['id_departement','code_culture_rpg']).sum()])
'''

# Chargement direct des données SAU
chemin_donnees_production = r'../../crater-data-resultats/data/crater/productions_besoins/sau_par_culture.csv'
sau_par_culture = pd.read_csv(chemin_donnees_production,sep=';')

sau_par_culture_dpt = sau_par_culture[sau_par_culture['categorie_territoire']=='DEPARTEMENT']








