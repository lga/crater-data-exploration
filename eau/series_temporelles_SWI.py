#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  1 21:18:05 2023

@author: vincent
"""

import glob
import os
import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt

#%%

def load_SWI_data(swi_data_path):
    filelist = glob.glob(os.path.join(swi_data_path,'*.csv'))
    if len(filelist)==0:
        print('EMPTY FILE LIST')
    
    swi_data = pd.DataFrame()
    for f in filelist:
        df = pd.read_csv(f,sep=';',decimal=',')
        swi_data = pd.concat([swi_data,df])
    
    swi_data = swi_data.sort_values(by='NUMERO')

    return swi_data

def load_departements(filename):
    departements_gdf = gpd.read_file(filename)
    departements_gdf = departements_gdf.to_crs('epsg:2154')
    return departements_gdf


# Donnees SWI telechargees sur https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=301&id_rubrique=40
swi_data = load_SWI_data(r'../../local/Data_eau/SWI/SWI_Package_1969-2020')
swi_gdf = gpd.GeoDataFrame(swi_data, geometry=gpd.points_from_xy(swi_data.LAMBX, swi_data.LAMBY))
swi_gdf = swi_gdf.set_crs('epsg:2154')

# https://www.data.gouv.fr/fr/datasets/carte-des-departements-2-1/
# Chargement départements
departements_gdf = load_departements(r'../../local/Data_eau/Departements/contour-des-departements.geojson')

pointInPolys = gpd.sjoin(swi_gdf, departements_gdf, how='left')
#SWI_avg = pointInPolys.groupby('index_right').mean()
#departements_gdf['SWI'] = SWI_avg.SWI_UNIF_MENS3

#%%

def calculer_serie_par_departement(select_dept):
    subset = pointInPolys[pointInPolys['code']==select_dept]
    subset_avg_by_date = subset.groupby('DATE').mean()
    
    subset_avg_by_date['year'] = np.floor(subset_avg_by_date.index/100)
    subset_avg_by_date['month'] = subset_avg_by_date.index - subset_avg_by_date.year*100
    subset_avg_by_date['date'] = pd.to_datetime(subset_avg_by_date.index.astype(str),format='%Y%m')
    subset_avg_by_date['SWI_NORM'] = subset_avg_by_date['SWI_UNIF_MENS3']
    
    # Normalisation
    # D'apres https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8051111/ eq. 2
    for i in range(1,13):
        month_avg = subset_avg_by_date[subset_avg_by_date['month']==i].SWI_UNIF_MENS3.mean()
        month_std = subset_avg_by_date[subset_avg_by_date['month']==i].SWI_UNIF_MENS3.std()
        subset_avg_by_date.loc[subset_avg_by_date['month']==i,'SWI_NORM'] = (subset_avg_by_date.loc[subset_avg_by_date['month']==i,'SWI_UNIF_MENS3'] - month_avg)/month_std
    
    return subset_avg_by_date

#%%

dept_list = ['31','29','53','71','11']
N=len(dept_list)

plt.close('all')

plt.figure(num=1,figsize=[12,8])
plt.figure(num=2,figsize=[12,8])
for i in range(N):
    dept = dept_list[i]
    subset_avg_by_date = calculer_serie_par_departement(dept)
    
    plt.figure(num=1)
    plt.subplot(N,1,i+1)
    plt.plot(subset_avg_by_date['date'],subset_avg_by_date['SWI_UNIF_MENS3'],label='dept = '+ dept)
    plt.xlabel('Date')
    plt.ylabel('SWI Unif Mens3')
    plt.grid(which='both')
    plt.legend(loc='upper right')
    
    plt.figure(num=2)
    plt.subplot(N,1,i+1)
    plt.plot(subset_avg_by_date['date'],subset_avg_by_date['SWI_NORM'],label='dept = '+ dept)
    plt.xlabel('Date')
    plt.ylabel('SWI NORM')
    plt.grid(which='both')
    plt.legend(loc='upper right')


plt.figure(num=1)

plt.savefig('serie_SWI_MENS3.png')

plt.figure(num=2)
plt.savefig('serie_SWI_NORM.png')


#%%
# En lisant les papiers de soubeyroux, il vaut probablement mieux travailler par point de mesure du SWI, et faire le moyennage par département seulement après

numero_point_mesure = 3456
# On en choisit un au pif pour les tests



def recuperer_serie_par_point_mesure(numero_point_de_mesure):
    swi_point_unique = swi_data[swi_data['NUMERO']==numero_point_de_mesure].copy()
    
    swi_point_unique['date'] = pd.to_datetime(swi_point_unique.DATE.astype(str),format='%Y%m')
    swi_point_unique['year'] = np.floor(swi_point_unique.DATE/100)
    swi_point_unique['month'] = swi_point_unique.DATE - swi_point_unique.year*100
    swi_point_unique = swi_point_unique.sort_values(by='date')
    
    # Normalisation
    # D'apres https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8051111/ eq. 2
    swi_point_unique['SWI_NORM'] = swi_point_unique['SWI_UNIF_MENS3']
    for i in range(1,13):
        month_avg = swi_point_unique[swi_point_unique['month']==i].SWI_UNIF_MENS3.mean()
        month_std = swi_point_unique[swi_point_unique['month']==i].SWI_UNIF_MENS3.std()
        swi_point_unique.loc[swi_point_unique['month']==i,'SWI_NORM'] = (swi_point_unique.loc[swi_point_unique['month']==i,'SWI_UNIF_MENS3'] - month_avg)/month_std
    
    return swi_point_unique

def detecter_secheresses(swi_1pt,seuil):
    swi_1pt['secheresse'] = swi_1pt['SWI_NORM'] * 0
    
    # On detecte les secheresses
    swi_1pt.loc[swi_1pt['SWI_NORM']<seuil,'secheresse'] = 1
    
    # On etend aux valeurs de SSWI<0 avant et apres le pic de detection
    for i in range(1,len(swi_1pt)):
        if swi_1pt['secheresse'].iloc[i-1] == 1:
            if swi_1pt['SWI_NORM'].iloc[i] < 0:
                swi_1pt.secheresse.iat[i] = 1
    
    for i in range(len(swi_1pt)-2,0,-1):
        if swi_1pt['secheresse'].iloc[i+1] == 1:
            if swi_1pt['SWI_NORM'].iloc[i] < 0:
                swi_1pt.secheresse.iat[i] = 1
    
    # indexation des episodes de secheresse
    recap_secheresses = pd.DataFrame(columns=['indice','date_debut','date_fin','duree','magnitude','severite'])
    i = 0
    indice_secheresse = 0
    swi_1pt['indice_secheresse'] = 0 * swi_1pt['secheresse']
    swi_1pt['duree_secheresse'] = 0 * swi_1pt['secheresse']
    swi_1pt['magnitude_secheresse'] = 0 * swi_1pt['secheresse']
    swi_1pt['severite_secheresse'] = 0 * swi_1pt['secheresse']
    while i<len(swi_1pt):
        if swi_1pt['secheresse'].iloc[i] == 0:
            i +=1
        else:
            indice_secheresse += 1
            duree = 1
            date_debut = swi_1pt['date'].iloc[i]
            severite = 0
            magnitude = 0
            while swi_1pt['secheresse'].iloc[i] == 1:
                swi_1pt.indice_secheresse.iat[i] = indice_secheresse
                swi_1pt.duree_secheresse.iat[i] = duree
                swi_1pt.magnitude_secheresse.iat[i] = magnitude
                swi_1pt.severite_secheresse.iat[i] = severite
                duree += 1
                magnitude += swi_1pt['SWI_NORM'].iloc[i]
                severite = np.min((severite,swi_1pt['SWI_NORM'].iloc[i]))
                date_fin = swi_1pt['date'].iloc[i]
                i +=1
            recap_secheresses.loc[len(recap_secheresses)] = [indice_secheresse,date_debut,date_fin,duree,magnitude,severite]

    
    return swi_1pt, recap_secheresses
    

swi_1pt = recuperer_serie_par_point_mesure(numero_point_mesure)
seuil = -1.6
swi_1pt, recap_secheresses = detecter_secheresses(swi_1pt,seuil)

plt.figure(figsize=[10,8])

plt.subplot(311)
plt.plot(swi_1pt['date'],swi_1pt['SWI_UNIF_MENS3'])
plt.grid()
plt.xlabel('Date')
plt.ylabel('SWI UNIF MENS3')

plt.subplot(312)
plt.plot(swi_1pt['date'],swi_1pt['SWI_NORM'])
plt.plot([swi_1pt['date'].iloc[0],swi_1pt['date'].iloc[-1]],[seuil,seuil],'g')
#plt.plot(swi_1pt['date'],swi_1pt['secheresse'],'r')
plt.fill_between(swi_1pt['date'],0*swi_1pt['SWI_NORM'],swi_1pt['SWI_NORM']*swi_1pt['secheresse'],facecolor='red')
#plt.plot(swi_1pt['date'],swi_1pt['indice_secheresse'])
plt.grid()
plt.xlabel('Date')
plt.ylabel('SWI NORM MENS3')

plt.subplot(313)
plt.plot(swi_1pt['date'],swi_1pt['indice_secheresse'],label='indice')
plt.plot(swi_1pt['date'],swi_1pt['duree_secheresse'], label='duree cumulee')
plt.plot(swi_1pt['date'],swi_1pt['magnitude_secheresse'].abs(), label='magnitude cumulee')
plt.plot(swi_1pt['date'],swi_1pt['severite_secheresse'].abs(), label='severite cumulee')
plt.grid()
plt.xlabel('Date')
plt.legend()


plt.suptitle('SWI au point '+str(numero_point_mesure))
plt.tight_layout()

plt.savefig('Analyse_serie_SWI_1_point.png')








