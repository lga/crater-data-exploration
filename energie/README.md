# Comment lancer le code ?

## Calculs énergie directes
Se placer dans le module synthese_energies_directes.py
Lancer le main. Il génère un fichier csv "synthese_methodes_energies_directes" dans le dossier output, qui résume les résultats de l'extraction des données de consommations d'électricité, de gaz naturel, et les résultats des estimations en consommation de produits pétroliers, le tout en comparaison de données de référence (SDES = service des données et études statistiques - données statistiques en matière de logement, de construction, de transports, d’énergie, d’environnement et de développement durable)

## Modélisation avec le RICA


* la première étape est de générer le dataframe de train et de test. Lancer le module generer_dataframe_et_echantillon_test_train
* la deuxième étape est de lancer la modélisation : lancer le module application_modele_PP



