import pandas as panda


def donner_nom_territoire(code, type_terri):
    chemin_fichier = "./energie/outils/codes_des_territoires.csv"
    fichier = panda.read_csv(chemin_fichier, sep=";")
    if type_terri == "R":
        nouveau_code = "R-" + code
    elif type_terri == "D":
        nouveau_code = "D-" + code
    ligne = fichier[fichier["code"] == nouveau_code]
    nom_terri = ligne["intitule"].item()
    print("nom", nom_terri)
    return nom_terri
