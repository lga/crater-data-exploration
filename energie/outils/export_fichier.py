import io
import shutil
from pathlib import Path

from pandas import DataFrame


def reinitialiser_dossier(nom_dossier: Path):
    if nom_dossier.exists():
        shutil.rmtree(nom_dossier)
    nom_dossier.mkdir(parents=True)


def exporter_csv(df: DataFrame, nom_fichier_output: Path, avec_index=False):
    df.to_csv(
        nom_fichier_output,
        sep=";",
        encoding="utf-8",
        index=avec_index,
        float_format="%.2f",
    )


def exporter_description_dataframe(df: DataFrame, nom_fichier_output: Path):
    buffer = io.StringIO()
    df.info(verbose=True, buf=buffer)
    s = buffer.getvalue()
    with open(nom_fichier_output, "w", encoding="utf-8") as f:
        f.write(s)


def exporter_csv_et_description(
    df: DataFrame, dossier_output: Path, nom_fichier_sans_extension, avec_index=False
):
    exporter_csv(df, dossier_output / (nom_fichier_sans_extension + ".csv"), avec_index)
    exporter_description_dataframe(
        df, dossier_output / (nom_fichier_sans_extension + ".txt")
    )
