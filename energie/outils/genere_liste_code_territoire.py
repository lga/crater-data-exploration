import pandas as panda


def generer_liste_code():
    fichier_contenant_code = panda.read_csv(
        "./energie/outils/codes_des_territoires.csv", sep=";"
    )
    liste_codes = fichier_contenant_code["code"].to_list()
    liste_code_regions = []
    liste_code_departement = []
    for element in liste_codes:
        code = element[2:4]
        if element[0] == "R":
            liste_code_regions.append(code)
        elif element[0] == "D":
            liste_code_departement.append(code)
    return (liste_code_regions, liste_code_departement)
