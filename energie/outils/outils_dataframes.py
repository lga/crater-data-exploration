from typing import Dict

import pandas
from pandas import DataFrame

DictDataFrames: type = Dict[str, pandas.DataFrame]


def produit_cartesien(df_1: DataFrame, df_2: DataFrame) -> DataFrame:
    return df_1.assign(key=1).merge(df_2.assign(key=1), on="key").drop("key", axis=1)
