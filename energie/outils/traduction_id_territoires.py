def traduire_code_insee_vers_id_commune_crater(df_colonne):
    return "C-" + df_colonne


def traduire_code_insee_vers_id_epci_crater(df_colonne):
    return "E-" + df_colonne


def traduire_code_insee_vers_id_departement_crater(df_colonne):
    return "D-" + df_colonne


def traduire_code_insee_vers_id_region_crater(df_colonne):
    return "R-" + df_colonne


def traduire_code_insee_vers_id_pays_crater(df_colonne):
    return "P-" + df_colonne
