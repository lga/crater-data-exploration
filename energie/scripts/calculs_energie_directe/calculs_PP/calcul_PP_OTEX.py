# TODO: review-fichier
from cmath import nan
import pandas as panda
from pathlib import Path
import os
import numpy as np
import matplotlib.pyplot as plt
from energie.outils.chargeur_agreste import charger_agreste


CHEMIN_CRATER_AGRESTE = Path("../crater-data-sources/agreste/1970-2010/FDS_G_2002.zip")
DICTIONNAIRE_CODE_REGION = dictionnaire_code_region = {
    "Guadeloupe": "NR01",
    "Martinique": "NR02",
    "Guyane": "NR03",
    "La Réunion": "NR04",
    "IdF": "NR11",
    "CVL": "NR24",
    "BFC": "NR27",
    "Normandie": "NR28",
    "HdF": "NR32",
    "GE": "NR44",
    "PL": "NR52",
    "Bretagne": "NR53",
    "NA": "NR75",
    "Occitanie": "NR76",
    "ARA": "NR84",
    "PACA": "NR93",
    "Corse": "NR94",
}

# TODO: enlever, fonc. referencee par fonc. non-referencee
def donner_code_associe_au_nom_region(nom_code):
    code = DICTIONNAIRE_CODE_REGION[nom_code]
    return code


# TODO: enlever, fonc. non-referencee
def charger_fichier_OTEX(nom_code):
    intru = "............"
    code = donner_code_associe_au_nom_region(nom_code)
    fichier_init = panda.read_csv(
        "./energie/liste_conso_energetique_OTEX/New_FDS_G_2010_20101.csv", sep=";"
    )
    premiere_ligne = fichier_init.columns
    fichier_OTEX = panda.DataFrame(columns=premiere_ligne)
    i = 1
    fini = False
    vu_les_fichiers_interet = False

    while fini == False and i < 18:
        fichier_considere = panda.read_csv(
            "./energie/liste_conso_energetique_OTEX/New_FDS_G_2010_2010"
            + str(i)
            + ".csv",
            sep=";",
        )
        colonne_region = fichier_considere["REGION"].to_list()
        lignes_avec_code_region = fichier_considere.loc[
            (fichier_considere["REGION"] == code)
            & ~(fichier_considere["COM"] == "............")
        ]
        liste_communes = lignes_avec_code_region["COM"].to_list()
        intru = "............"
        listes_communes_sans_doublon = list(set(liste_communes))

        if len(liste_communes) != 0:
            while intru in liste_communes:
                liste_communes.remove(intru)
        if (code not in colonne_region) or (len(liste_communes) == 0):
            i += 1
        if vu_les_fichiers_interet == True:
            fini = True
        else:
            vu_les_fichiers_interet = True
            fichier_OTEX.reset_index()
            fichier_OTEX = panda.concat([fichier_OTEX, fichier_considere])
            i += 1
    return fichier_OTEX


def reorganiser_pivot_par_OTEX(pivot):
    liste_colonnes = list(pivot.columns)
    print(liste_colonnes)
    n = len(liste_colonnes)
    index_de_la_col_ensemble = pivot.columns.get_loc("Ensemble")
    i = index_de_la_col_ensemble
    pivot_ensemble_exploit_a_la_fin = pivot[
        liste_colonnes[0:i] + liste_colonnes[i + 1 : n + 1] + [liste_colonnes[i]]
    ]
    return pivot_ensemble_exploit_a_la_fin


def calculer_somme_exploitations_par_OTEX(pivot_regorganise):
    liste_cols = pivot_regorganise.columns[1:]
    liste_totaux = []
    n = len(liste_cols)
    for c in liste_cols:
        total = pivot_regorganise[c].sum()
        nom_colonne = c
        liste_totaux.append((c, total))
    return liste_totaux


def remplir_ligne_total_exploit_par_OTEX(pivot_reorganise):
    liste_totaux = calculer_somme_exploitations_par_OTEX(pivot_reorganise)
    ligne_des_totaux = {
        "COM": "TOTAL",
        "Bovins lait (Otex 45)": 0,
        "Bovins mixte (Otex 47)": 0,
        "Bovins viande (Otex 46)": 0,
        "Cultures fruitières et autres cultures permanentes (Otex 36, 37, 38)": 0,
        "Elevages hors sol  (Otex 51, 52, 53, 74)": 0,
        "Grandes cultures (Otex 15, 16)": 0,
        "Maraîchage et horticulture (Otex 21, 22)": 0,
        "Ovins, caprins et autres herbivores (Otex 48)": 0,
        "Polyculture, polyélevage, autres (Otex 61, 73, 83, 84, 90)": 0,
        "Viticulture (Otex 35)": 0,
        "Ensemble": 0,
    }
    for l in liste_totaux:
        if l[1] != ligne_des_totaux[l[0]]:
            ligne_des_totaux[l[0]] = l[1]
    print(ligne_des_totaux)
    return ligne_des_totaux


def generer_table_energie_OTEX_communes(code):

    fichier_OTEX_non_selec = charger_agreste(
        CHEMIN_CRATER_AGRESTE, "FDS_G_2002_2010.txt", "G_2002"
    )
    intru = "............"
    fichier_OTEX = fichier_OTEX_non_selec.loc[
        (fichier_OTEX_non_selec["REGION"] == code)
    ]
    ligne_selection = fichier_OTEX.loc[
        (
            fichier_OTEX["G_2002_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (fichier_OTEX["G_2002_LIB_DIM3"] == "Exploitations")
        & ~(fichier_OTEX["COM"] == intru)
        & (fichier_OTEX["QUALITE"] == "OUI")
    ]
    ligne_selection_final = ligne_selection.reset_index(drop=True)

    liste_OTEX_doublons = fichier_OTEX["G_2002_LIB_DIM2"].to_list()
    liste_OTEX = list(set(liste_OTEX_doublons))

    pivot = ligne_selection.pivot(
        index="COM", columns="G_2002_LIB_DIM2", values="VALEUR"
    ).reset_index()
    pivot_reorganise = reorganiser_pivot_par_OTEX(pivot)
    ligne_des_totaux = remplir_ligne_total_exploit_par_OTEX(pivot_reorganise)
    donnees_total_integre = pivot_reorganise.append(ligne_des_totaux, ignore_index=True)
    donnees_total_integre.to_csv(
        "./energie/outputs/donnees_conso_PP_par_OTEX_pour_" + code + ".csv", sep=";"
    )

    return donnees_total_integre


def calculer_somme_energie_OTEX_territoire(nom_de_code):
    donnees_otex_total = generer_table_energie_OTEX_communes(nom_de_code)
    ligne_somme_exploit_par_OTEX = donnees_otex_total[-1:].transpose()
    c = ligne_somme_exploit_par_OTEX.columns.to_list()[0]
    valeurs_sommes_exploit = ligne_somme_exploit_par_OTEX[c].to_list()
    donnees_conso_par_OTEX = panda.read_csv(
        "./energie/inputs/correspondance_OTEX_conso_energetique.csv", sep=","
    )
    donnee_consommation_energetique_par_otex_tot = donnees_conso_par_OTEX[
        0:
    ].transpose()
    donnee_consommation_energetique_par_otex_tot[
        "Somme exploit"
    ] = valeurs_sommes_exploit
    donnee_consommation_energetique_par_otex_tot = (
        donnee_consommation_energetique_par_otex_tot.drop(labels="COM", axis=0)
    )
    donnee_consommation_energetique_par_otex_tot[
        "valeur multipliée tep"
    ] = donnee_consommation_energetique_par_otex_tot["Somme exploit"].multiply(
        donnee_consommation_energetique_par_otex_tot[0], axis=0
    )
    # donnee_consommation_energetique_par_otex_tot=ligne_somme_exploit_par_OTEX.reset_index().drop("COM", axis=1).mul(ligne_conso_par_OTEX.reset_index().drop("COM", axis=1),axis=0)
    donnee_consommation_energetique_par_otex_tot.to_csv(
        "./energie/outputs/donnee_consommation_energetique_par_otex_tot_pour_"
        + nom_de_code
        + ".csv",
        sep=";",
    )
    donnnees_chaque_OTEX = donnee_consommation_energetique_par_otex_tot.drop(
        "Ensemble", axis=0
    )
    somme_chaque_OTEX = donnnees_chaque_OTEX["valeur multipliée tep"].sum()
    somme_ensemble_q = donnee_consommation_energetique_par_otex_tot.loc[["Ensemble"]]
    somme_ensemble = somme_ensemble_q["valeur multipliée tep"].to_list()[0]
    print("Somme", somme_ensemble)
    # log.info(f"le territoire{nom_de_code},a pour consommation totale en tep {somme}")
    return (somme_chaque_OTEX * 0.001 * (1 / 0.086), somme_ensemble * 0.001)


# restructurer
# pour remplacer méthode nom_de_code, mettre trois lignes qui sous sélectionnent (à possibelement virer ou mettre en commentaire)

# TODO: enlever, fonc. non-referencee
def generer_histogramme_des_conso_PP_OTEX_regions():
    liste_region = [
        "IdF",
        "CVL",
        "BFC",
        "Normandie",
        "HdF",
        "GE",
        "PL",
        "Bretagne",
        "NA",
        "Occitanie",
        "ARA",
        "PACA",
        "Corse",
    ]
    liste_conso_energetique_OTEX_differencie = []
    liste_conso_energetique_OTEX_ensemble = []
    for l in liste_region:
        sommes = calculer_somme_energie_OTEX_territoire(l)
        liste_conso_energetique_OTEX_differencie.append(sommes[0])
        liste_conso_energetique_OTEX_ensemble.append(sommes[1])
    print(liste_conso_energetique_OTEX_differencie)
    n = len(liste_region)
    x_axis = np.arange(n)
    plt.bar(
        x_axis - 0.2,
        liste_conso_energetique_OTEX_differencie,
        label="coefficients différenciés par OTEX",
        color="b",
        width=0.30,
    )
    plt.bar(
        x_axis + 0.2,
        liste_conso_energetique_OTEX_ensemble,
        label="coefficient unique pour toutes les exploits",
        color="g",
        width=0.30,
    )
    plt.xticks(x_axis, liste_region, rotation=60)
    plt.title("Consommation énergétique de produits pétroliers - méthode OTEX")
    plt.ylabel("Consommation en ktep")
    plt.legend()
    plt.show()
    plt.savefig("./energie/outputs/histogramme_PP_par_OTEX.png")


if __name__ == "__main__":
    calculer_somme_energie_OTEX_territoire("NR93")
