# TODO: review-fichier
from logging import log
from pathlib import Path
import pandas as panda

# définition chemins fichiers
CHEMIN_CRATER_DATA_SAU = Path(
    "../crater-data-resultats/data/surface_agricole_utile/sau_par_commune_et_culture"
)
# 11
CHEMIN_DONNEES_CORRESPONDANCE_TERRITOIRES = Path(
    "../crater-data-resultats/data/crater/territoires/referentiel_territoires.csv"
)
# referentiel territoire
CHEMIN_DONNEES_REF_RPG = Path(
    "./energie/inputs/correspondance_nomenclature_rpg_vers_crater.csv"
)
# copie depuis le crater-data ; config
CHEMIN_DONNEES_COUTS_ENERGETIQUE = "./energie/inputs/consommations_carburant.csv"


def init():
    # import fichiers
    fichier_terri = panda.read_csv(CHEMIN_DONNEES_CORRESPONDANCE_TERRITOIRES, sep=";")
    donnees_sau = panda.concat(
        panda.read_csv(f, sep=";") for f in CHEMIN_CRATER_DATA_SAU.glob("*.csv")
    )
    donnees_rpg_energie = panda.read_csv(
        CHEMIN_DONNEES_REF_RPG, sep=";", encoding="utf-8"
    )
    donnees_consommations_carburants = panda.read_csv(
        CHEMIN_DONNEES_COUTS_ENERGETIQUE, sep=";"
    )
    # jointures de fichiers
    jointure_1_code_ener_cout_ener = panda.merge(
        left=donnees_rpg_energie,
        right=donnees_consommations_carburants,
        left_on="code_culture_energie",
        right_on="code_culture_energie",
        how="left",
    )
    return (
        fichier_terri,
        donnees_sau,
        donnees_rpg_energie,
        donnees_consommations_carburants,
        jointure_1_code_ener_cout_ener,
    )


def definir_colonne_interet(code):
    """cette fonction définit dans quelle colonne il faut chercher le nom du territoire désiré (en entrée est donné le code du territoire)"""
    if code[0] == "P":
        nom = "id_pays"
    elif code[0] == "R":
        nom = "id_region"
    elif code[0] == "D":
        nom = "id_departement"
    elif code[0] == "B":
        nom = "ids_regroupement_communes"
    elif code[0] == "A":
        nom = "ids_regroupement_communes"
    elif code[0] == "C":
        nom == "id_territoire"
    else:
        nom = "erreur"
    return nom


def donner_nom(code, fichier_terri):
    """cette fonction donne le nom du territoire d'intérêt dont on connaît le code (donnée en entrée)"""
    rows = fichier_terri.loc[fichier_terri["id_territoire"] == code]
    nom_territoire = rows["nom_territoire"]
    return nom_territoire


def definir_zone_interet(code):
    """cette fonction renvoie une liste de communes appartenant au territoire d'intérêt, dont on a donné le code"""
    nom = definir_colonne_interet(code)
    fichier_terri = panda.read_csv(CHEMIN_DONNEES_CORRESPONDANCE_TERRITOIRES, sep=";")
    if nom == "erreur":
        print("erreur nom")
        return None

    fichier_filtre = fichier_terri[
        (fichier_terri[nom] == code)
        & (fichier_terri["categorie_territoire"] == "COMMUNE")
    ]
    # fichier_filtre renvoie une portion de fichier où ne sont présentes que les lignes correspondant aux communes de la zone d'intér^t spécifiée par le code
    liste_communes_interet = fichier_filtre["id_territoire"].tolist()
    # liste_communes_interet contient les id_territoires de toutes les communes de la zone d'intérêt
    n = len(liste_communes_interet)
    return liste_communes_interet


def donner_liste_entites_interet(fichier_terri):
    """cette fonction renvoie un dictionnaire qui donne la liste"""
    liste_id_territoire = fichier_terri["id_territoire"].to_list()
    liste_pays = []
    liste_regions = []
    liste_departements = []
    liste_regroup_comm = []
    liste_epci = []
    liste_communes = []
    for element in liste_id_territoire:
        if element[0] == "P":
            liste_pays.append(element)
        if element[0] == "R":
            liste_regions.append(element)
        if element[0] == "D":
            liste_departements.append(element)
        if element[0] == "E":
            liste_epci.append(element)
        if element[0] == "C":
            liste_communes.append(element)
        else:
            liste_regroup_comm.append(element)
        dictionnaire = {
            "pays": liste_pays,
            "régions": liste_regions,
            "départements": liste_departements,
            "regroupements": liste_regroup_comm,
            "epci": liste_epci,
            "communes": liste_communes,
        }
    return dictionnaire


# TODO: enlever, fonction non-referencee
def sommer_sau_commune(liste_comm, donnees_sau):
    """pour une liste de communes (donnée d'entrée, liste des codes communes), donne la sau totale sur la totalité des communes"""
    donnees_sau_communes = donnees_sau.loc[donnees_sau["id_commune"].isin(liste_comm)]
    donnees_sau_communes = donnees_sau_communes.reset_index()
    surface_totale = donnees_sau_communes["sau_ha"].sum()

    return surface_totale


def renvoyer_resultat_conso_energie_PP_ha(code):
    """cette fonction renvoie, pour un code de territoire, la consommation en produit pétrolier totale sur le territoire en GWh"""
    fichier_terri = panda.read_csv(CHEMIN_DONNEES_CORRESPONDANCE_TERRITOIRES, sep=";")
    donnees_sau = panda.concat(
        panda.read_csv(f, sep=";") for f in CHEMIN_CRATER_DATA_SAU.glob("*.csv")
    )
    donnees_rpg_energie = panda.read_csv(
        CHEMIN_DONNEES_REF_RPG, sep=";", encoding="utf-8"
    )
    donnees_consommations_carburants = panda.read_csv(
        CHEMIN_DONNEES_COUTS_ENERGETIQUE, sep=";"
    )
    jointure_1_code_ener_cout_ener = panda.merge(
        left=donnees_rpg_energie,
        right=donnees_consommations_carburants,
        left_on="code_culture_energie",
        right_on="code_culture_energie",
        how="left",
    )
    nom = donner_nom(code, fichier_terri)
    liste_comm = definir_zone_interet(code)
    donnees_sau_communes = donnees_sau.loc[donnees_sau["id_commune"].isin(liste_comm)]
    # donnees_sa_communes est un dataframe qui contient pour chaque commune la surface en ha de chaque culture présente dans la commune. Ici, on a circonscrit les lignes à celles correspondant uniqueent aux communes de la zone d'intérêt (isin liste communes)
    cout_ener_surface_communes = panda.merge(
        left=donnees_sau_communes,
        right=jointure_1_code_ener_cout_ener,
        how="left",
        on="code_culture_rpg",
    )
    # cout_ener_surface_communes est un dataframe contenant toutes les données de surfaces et des coûts énéergétiques à l'ha des cultures présentes dans les communes de la zone d'intérêt
    cout_ener_surface_communes["cout_energetique_multiplie"] = (
        cout_ener_surface_communes["sau_ha"] * cout_ener_surface_communes["conso_l_ha"]
    )
    # creation d'une nouvelle colonne de consommation énergétique = sau*cout à l'hectare
    cout_energetique_groupe_commune = cout_ener_surface_communes.groupby(
        by=["id_commune"]
    )
    somme_cout_energetique = cout_ener_surface_communes[
        "cout_energetique_multiplie"
    ].sum()
    conversion_total = somme_cout_energetique * 0.001 * 0.8 * 1.05 * 0.001 * (1 / 0.086)
    print(conversion_total)

    return conversion_total


def generer_csv_energie_communes(code="P-FR") -> None:
    fichier_terri = panda.read_csv(CHEMIN_DONNEES_CORRESPONDANCE_TERRITOIRES, sep=";")
    donnees_sau = panda.concat(
        panda.read_csv(f, sep=";") for f in CHEMIN_CRATER_DATA_SAU.glob("*.csv")
    )
    donnees_rpg_energie = panda.read_csv(
        CHEMIN_DONNEES_REF_RPG, sep=";", encoding="utf-8"
    )
    donnees_consommations_carburants = panda.read_csv(
        CHEMIN_DONNEES_COUTS_ENERGETIQUE, sep=";"
    )
    jointure_1_code_ener_cout_ener = panda.merge(
        left=donnees_rpg_energie,
        right=donnees_consommations_carburants,
        left_on="code_culture_energie",
        right_on="code_culture_energie",
        how="left",
    )
    nom = donner_nom(code, fichier_terri)
    liste_comm = definir_zone_interet(code, fichier_terri)
    donnees_sau_communes = donnees_sau.loc[donnees_sau["id_commune"].isin(liste_comm)]
    # donnees_sa_communes est un dataframe qui contient pour chaque commune la surface en ha de chaque culture présente dans la commune. Ici, on a circonscrit les lignes à celles correspondant uniqueent aux communes de la zone d'intérêt (isin liste communes)
    cout_ener_surface_communes = panda.merge(
        left=donnees_sau_communes,
        right=jointure_1_code_ener_cout_ener,
        how="left",
        on="code_culture_rpg",
    )
    # cout_ener_surface_communes est un dataframe contenant toutes les données de surfaces et des coûts énéergétiques à l'ha des cultures présentes dans les communes de la zone d'intérêt
    cout_ener_surface_communes["conso_energetique"] = (
        cout_ener_surface_communes.sau_ha
        * cout_ener_surface_communes.conso_l_ha
        * 1.05
        * 0.8
        * 0.001
    )
    cout_energetique_groupe_commune = cout_ener_surface_communes.groupby(
        by=["id_commune"]
    ).sum()
    fichier_csv_cout_energetique = cout_energetique_groupe_commune.to_csv(
        "./energie/outputs/consommation_energetique_PP_ha_pour" + nom + ".csv", sep=";"
    )
    return fichier_csv_cout_energetique


# TODO: enlever ces lignes en commentaire ?
#
# '''les tests'''
# dictionnaire_listes_territoires=donne_liste_entites_interet()
# liste_des_codes_regions=dictionnaire_listes_territoires["régions"]
# liste_des_codes_departements=dictionnaire_listes_territoires["départements"]
# liste_des_codes_regroupements=dictionnaire_listes_territoires["regroupements"]
# liste_des_codes_epci=dictionnaire_listes_territoires["epci"]
# liste_des_codes_communes=dictionnaire_listes_territoires["communes"]
#
#
# for region in liste_des_codes_regions:
#     resultat_conso_energie_territoire(region)
#
# for departement in liste_des_codes_departements:
#     resultat_conso_energie_territoire(departement)
#
# for regroupement in liste_des_codes_regroupements:
#     resultat_conso_energie_territoire(regroupement)
#
# for commune in liste_des_codes_communes:
#     resultat_conso_energie_territoire(commune)

"""

if __name__ == '__main__':
    csv_energie_communes()
    log.info("FIN MAIN")
"""
