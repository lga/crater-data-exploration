import os
from pathlib import Path
from unittest import result
import pandas as panda

from communs.crater_logger import chronometre

chemin_dossier_SDES = "../crater-data-sources/energie/data_SDES_2014-2020"
liste_fichier_region = os.listdir("../crater-data-sources/energie/data_SDES_2014-2020")


GWH_EN_KTEP = 0.086

donnees_synthese = panda.DataFrame(
    columns=[
        "Régions",
        "Données SDES (ktep)",
        "Méthode /ha (ktep)",
        "Méthode /OTEX (ktep) ",
    ]
)


@chronometre
def lire_excel(f):
    print(f)
    fichier_en_cours = panda.read_excel(chemin_dossier_SDES + "/" + f)
    return fichier_en_cours


def lire_dossier_donnes_SDES():
    liste_fichier_region = os.listdir(
        "../crater-data-sources/energie/data_SDES_2014-2020"
    )
    liste_fichier_region.remove("références et métadonnées")
    for element in liste_fichier_region:
        print(element)
        if element[0][0] == ".":
            print(element)
            liste_fichier_region.remove(element)
    print(liste_fichier_region)
    return liste_fichier_region


def sommer_valeurs_2014_2020(lignes):
    somme = lignes.iloc[0, 3:10].sum()
    return somme


def extraire_lignes_interet(fichier_en_cours):
    ligne_conso_PP_avec_peche = fichier_en_cours[
        fichier_en_cours["code_entite"] == "CA2"
    ]
    ligne_conso_PP_de_la_peche = fichier_en_cours[
        fichier_en_cours["code_entite"] == "CA21"
    ]
    ligne_conso_GAZ = fichier_en_cours[fichier_en_cours["code_entite"] == "CA4"]
    ligne_conso_ELEC = fichier_en_cours[fichier_en_cours["code_entite"] == "CA5"]
    return (
        ligne_conso_PP_avec_peche,
        ligne_conso_PP_de_la_peche,
        ligne_conso_GAZ,
        ligne_conso_ELEC,
    )


def moyenner(somme):
    resultat = somme / 7
    return resultat


def renvoyer_valeurs_SDES_regions(liste_fichier_region):
    liste_numeros_regions = []
    liste_valeurs_PP = []
    liste_valeurs_GAZ = []
    liste_valeurs_ELEC = []
    for f in [x for x in liste_fichier_region if "xlsx#" not in x]:
        # modifier cette partie là pour utiliser la colonne code pour récupérer la valeur
        fichier_en_cours = lire_excel(f)
        nom_entier = fichier_en_cours.columns[1]
        numero = nom_entier[0:2]
        fichier_en_cours.columns = [
            "code_entite",
            "entite",
            "unite_mesure",
            "valeur_2014",
            "valeur_2015",
            "valeur_2016",
            "valeur_2017",
            "valeur_2018",
            "valeur_2019",
            "valeur_2020",
        ]
        (
            ligne_conso_PP_avec_peche,
            ligne_conso_PP_de_la_peche,
            ligne_conso_GAZ,
            ligne_conso_ELEC,
        ) = extraire_lignes_interet(fichier_en_cours)
        somme_PP_dont_peche = sommer_valeurs_2014_2020(ligne_conso_PP_avec_peche)
        somme_PP_peche = sommer_valeurs_2014_2020(ligne_conso_PP_de_la_peche)
        somme_sans_peche = somme_PP_dont_peche - somme_PP_peche
        somme_GAZ = sommer_valeurs_2014_2020(ligne_conso_GAZ)
        somme_ELEC = sommer_valeurs_2014_2020(ligne_conso_ELEC)
        resultats_PP = moyenner(somme_sans_peche)
        resultats_GAZ = moyenner(somme_GAZ)
        resultats_ELEC = moyenner(somme_ELEC)
        liste_numeros_regions.append(numero)
        liste_valeurs_PP.append(resultats_PP)
        liste_valeurs_GAZ.append(resultats_GAZ)
        liste_valeurs_ELEC.append(resultats_ELEC)
    return (
        liste_numeros_regions,
        liste_valeurs_PP,
        liste_valeurs_ELEC,
        liste_valeurs_GAZ,
    )
