import pandas as panda
from energie.scripts.calculs_energie_directe.calculs_PP.calcul_PP_OTEX import (
    calculer_somme_energie_OTEX_territoire,
)

from energie.scripts.calculs_energie_directe.calculs_PP.calcul_PP_energie_ha import (
    renvoyer_resultat_conso_energie_PP_ha,
)
from energie.scripts.calculs_energie_directe.calculs_PP.donnees_SDES import (
    lire_dossier_donnes_SDES,
    renvoyer_valeurs_SDES_regions,
)
from energie.outils.equivalence_nom_territoire import donner_nom_territoire


def generer_donnee_PP_ha(code):
    nouveau_code = "R-" + code
    resultat = renvoyer_resultat_conso_energie_PP_ha(nouveau_code)
    return resultat


def generer_donnee_PP_OTEX(code):
    nouveau_code = "NR" + code
    resultat = calculer_somme_energie_OTEX_territoire(nouveau_code)
    return resultat[0]


def generer_donnees_synthese():

    liste_fichier_region = lire_dossier_donnes_SDES()
    liste_resultats = renvoyer_valeurs_SDES_regions(liste_fichier_region)
    print(liste_resultats)
    print(liste_resultats[0])
    print(liste_resultats[1])
    liste_numeros_regions = liste_resultats[0]
    liste_valeurs_SDES = liste_resultats[1]
    synthese_methode = panda.DataFrame(
        columns=[
            "Région",
            "Méthode /ha (GWh)",
            "Méthode /OTEX (GWh)",
            "Chiffres SDES (GWh)",
        ]
    )
    for numero in liste_numeros_regions:
        ligne = panda.DataFrame(
            columns=[
                "Région",
                "Méthode /ha (GWh)",
                "Méthode /OTEX (GWh)",
                "Chiffres SDES (GWh)",
            ]
        )
        nom_terri = donner_nom_territoire(numero, "R")
        donnees = [nom_terri]
        donnees.append(generer_donnee_PP_ha(numero))
        donnees.append(generer_donnee_PP_OTEX(numero))
        index = liste_numeros_regions.index(numero)
        donnees.append(liste_valeurs_SDES[index])
        ligne = panda.DataFrame(
            [donnees],
            columns=[
                "Région",
                "Méthode /ha (GWh)",
                "Méthode /OTEX (GWh)",
                "Chiffres SDES (GWh)",
            ],
        )
        synthese_methode = synthese_methode.append(ligne)
    synthese_methode.to_csv("./energie/outputs/synthese_methode_calcul_PP.csv", sep=";")


if __name__ == "__main__":
    generer_donnees_synthese()

# histogrammes
