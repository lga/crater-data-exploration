import unittest
from pathlib import Path


from energie.outils.chargeur_agreste import charger_agreste
from energie.scripts.calculs_energie_directe.calculs_PP.calcul_PP_OTEX import (
    generer_table_energie_OTEX_communes,
)
from energie.scripts.calculs_energie_directe.calculs_PP.calcul_PP_energie_ha import (
    generer_csv_energie_communes,
)

CHEMIN_CRATER_AGRESTE = Path("../crater-data-sources/agreste/1970-2010/FDS_G_2002.zip")
# variable = constante, en début de fichier ou dans un fichier de config

# faire tests avec petits jeux de données
# découper test pivot par exe

# verif est ce que le fichier csv énergie contient ce à quoi je m'attends


class TestEnergiePP(unittest.TestCase):

    # @classmethod
    # def setUpClass(cls):
    #     None
    #     # reinitialiser_dossier(DOSSIER_TEST_DATA_OUTPUT)

    def test_calcul_PP_ha(self):
        generer_csv_energie_communes()

    def test_agreste_chargeur(self):
        charger_agreste(CHEMIN_CRATER_AGRESTE, "FDS_G_2002_2010.txt", "G_2002")

    def test_genere_calcul_PP_OTEX(self):
        generer_table_energie_OTEX_communes("ARA")


if __name__ == "__main__":
    unittest.main()
