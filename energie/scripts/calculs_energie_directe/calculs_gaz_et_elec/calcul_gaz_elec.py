from pathlib import Path
import pandas as panda

from communs.crater_logger import chronometre


CHEMIN_FICHIER_DONNEES_ENEDIS_ELEC = "../crater-data-sources/energie/donnees_conso_elec_gaz/ENEDIS-consommation-electrique-par-iris-et-code-naf.csv"
CHEMIN_FICHIER_DONNEES_ENEDIS_GAZ = "../crater-data-sources/energie/donnees_conso_elec_gaz/ENEDIS-consommation-gaz-par-iris-et-code-naf.csv"
CHEMIN_FICHIER_DONNEES_ORE = "../crater-data-sources/energie/donnees_conso_elec_gaz/ORE-conso-elec-gaz-annuelle-par-secteur-dactivite-agregee-epci.csv"

MWH_TO_KTEP = 0.000086


@chronometre
def charger_fichier(chemin):
    fichier_des_conso = panda.read_csv(chemin, sep=";")
    return fichier_des_conso


def selectionner_lignes_region_et_agricole_gaz_ENEDIS(fichier, code):
    print(fichier.columns)
    fichier_selectionne_region_agri_gaz = fichier[
        (fichier["Année"] == 2020)
        & (fichier["Code Région"] == int(code))
        & (fichier["Libellé Grand Secteur"] == "Agriculture")
    ]
    return fichier_selectionne_region_agri_gaz


def selectionner_lignes_region_et_agricole_elec_ENEDIS(fichier, code):
    fichier_selectionne_region_agri_elec = fichier[
        (fichier["Année"] == 2020)
        & (fichier["Code Région"] == int(code))
        & (fichier["CODE GRAND SECTEUR"] == "AGRICULTURE")
    ]
    return fichier_selectionne_region_agri_elec


def sommer_et_convertir_consommation_gaz_ENEDIS(lignes_selectionnees):
    somme = lignes_selectionnees["Consommation (MWh)"].sum()
    conso_energie_convertie = somme * MWH_TO_KTEP
    return conso_energie_convertie


def sommer_et_convertir_consommation_elec_ENEDIS(lignes_selectionnees):
    somme = lignes_selectionnees["Conso totale (MWh)"].sum()
    conso_energie_convertie = somme * MWH_TO_KTEP
    return conso_energie_convertie


def renvoyer_consommations_en_gaz_ou_elec_ENEDIS(code, type_gaz_ou_elec):
    if type_gaz_ou_elec == "GAZ":
        chemin = CHEMIN_FICHIER_DONNEES_ENEDIS_GAZ
        print(chemin)
        fichier_donnees_gaz = charger_fichier(chemin)
        lignes_donnees_conso_gaz = selectionner_lignes_region_et_agricole_gaz_ENEDIS(
            fichier_donnees_gaz, code
        )
        consommation = sommer_et_convertir_consommation_gaz_ENEDIS(
            lignes_donnees_conso_gaz
        )
    elif type_gaz_ou_elec == "ELEC":
        chemin = CHEMIN_FICHIER_DONNEES_ENEDIS_ELEC
        print(chemin)
        fichier_donnees_elec = charger_fichier(chemin)
        lignes_donnees_conso_elec = selectionner_lignes_region_et_agricole_elec_ENEDIS(
            fichier_donnees_elec, code
        )
        consommation = sommer_et_convertir_consommation_elec_ENEDIS(
            lignes_donnees_conso_elec
        )
    return consommation


def selectionner_ligne_region_gaz_ORE(fichier, code):
    fichier_selectionne_region_agri_gaz_ORE = fichier[
        (fichier["Année"] == 2020)
        & (fichier["Code Région"] == int(code))
        & (fichier["Filière"] == "Gaz")
    ]
    return fichier_selectionne_region_agri_gaz_ORE


def selectionner_ligne_region_elec_ORE(fichier, code):
    fichier_selectionne_region_agri_elec_ORE = fichier[
        (fichier["Année"] == 2020)
        & (fichier["Code Région"] == int(code))
        & (fichier["Filière"] == "Electricité")
    ]
    return fichier_selectionne_region_agri_elec_ORE


def sommer_et_convertir_consommation_ORE(lignes_selectionnees):
    somme = lignes_selectionnees["Consommation Agriculture (MWh)"].sum()
    conso_energie_convertie = somme * MWH_TO_KTEP
    return conso_energie_convertie


def renvoyer_consommations_gaz_et_elec_ORE(code):
    chemin = CHEMIN_FICHIER_DONNEES_ORE
    fichier_donnees = charger_fichier(chemin)
    lignes_donnees_conso_gaz = selectionner_ligne_region_gaz_ORE(fichier_donnees, code)
    lignes_donnees_conso_elec = selectionner_ligne_region_elec_ORE(
        fichier_donnees, code
    )
    consommation_gaz = sommer_et_convertir_consommation_ORE(lignes_donnees_conso_gaz)
    consommation_elec = sommer_et_convertir_consommation_ORE(lignes_donnees_conso_elec)
    return (consommation_gaz, consommation_elec)
