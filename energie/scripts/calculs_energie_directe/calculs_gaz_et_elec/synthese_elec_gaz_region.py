from energie.outils.genere_liste_code_territoire import generer_liste_code
from energie.scripts.calculs_energie_directe.calculs_gaz_et_elec.calcul_gaz_elec import (
    renvoyer_consommations_en_gaz_ou_elec_ENEDIS,
    renvoyer_consommations_gaz_et_elec_ORE,
)
import pandas as panda

from energie.outils.equivalence_nom_territoire import donner_nom_territoire


def genere_donnees_gaz_et_elec_region(code):
    gaz_elec = renvoyer_consommations_gaz_et_elec_ORE(code)
    return gaz_elec


def genere_csv_gaz_elec():
    colonnes = [
        "Région",
        "Electricité (ktep) -Données ENEDIS",
        "Electricité (ktep) -Données ORE",
        "Gaz (ktep) -Données ENEDIS",
        "Gaz (ktep) -Données ORE",
    ]
    liste_code_regions = generer_liste_code()[0]
    donnees_conso_elec_gaz = panda.DataFrame(columns=colonnes)
    for code in liste_code_regions:
        nom_region = donner_nom_territoire(code, "R")
        donnees = [nom_region]
        donnees.append(renvoyer_consommations_en_gaz_ou_elec_ENEDIS(code, "ELEC"))
        donnees.append(renvoyer_consommations_gaz_et_elec_ORE(code)[1])
        donnees.append(renvoyer_consommations_en_gaz_ou_elec_ENEDIS(code, "GAZ"))
        donnees.append(renvoyer_consommations_gaz_et_elec_ORE(code)[0])
        ligne_a_ajouter = panda.DataFrame([donnees], columns=colonnes)
        donnees_conso_elec_gaz = donnees_conso_elec_gaz.append(ligne_a_ajouter)
    donnees_conso_elec_gaz.to_csv(
        "./energie/outputs/donnees_conso_elec_gaz_region.csv", sep=";"
    )
    return None
