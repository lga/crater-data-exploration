import pandas as panda

from energie.scripts.calculs_energie_directe.calculs_PP.donnees_SDES import (
    lire_dossier_donnes_SDES,
    renvoyer_valeurs_SDES_regions,
)
from energie.scripts.calculs_energie_directe.calculs_PP.synthese_methodes_PP import (
    generer_donnee_PP_OTEX,
    generer_donnee_PP_ha,
)
from energie.outils.equivalence_nom_territoire import donner_nom_territoire
from energie.scripts.calculs_energie_directe.calculs_gaz_et_elec.synthese_elec_gaz_region import (
    genere_donnees_gaz_et_elec_region,
)


def generer_df_et_csv_synthese_energies_directes():
    colonnes_PP = ["Région", "Données SDES", "Méthode par ha", "Méthode par OTEX"]
    colonnes_gaz_elec = ["Région", "Données SDES", "Extraction ORE"]
    donnees_synthese_energies_directes_PP = panda.DataFrame(columns=colonnes_PP)
    donnees_synthese_energies_directes_gaz = panda.DataFrame(columns=colonnes_gaz_elec)
    donnees_synthese_energies_directes_elec = panda.DataFrame(columns=colonnes_gaz_elec)
    liste_fichier_region = lire_dossier_donnes_SDES()
    (
        liste_numeros_regions,
        liste_valeurs_PP,
        liste_valeurs_ELEC,
        liste_valeurs_GAZ,
    ) = renvoyer_valeurs_SDES_regions(liste_fichier_region)
    print(liste_numeros_regions)
    for numero in liste_numeros_regions:
        index = liste_numeros_regions.index(numero)
        nom_terri = donner_nom_territoire(numero, "R")
        donnees_PP = [nom_terri]
        donnees_PP.append(liste_valeurs_PP[index])
        valeur_PP_ha = generer_donnee_PP_ha(numero)
        valeur_PP_OTEX = generer_donnee_PP_OTEX(numero)
        donnees_PP.append(valeur_PP_ha)
        donnees_PP.append(valeur_PP_OTEX)
        ligne_PP = panda.DataFrame([donnees_PP], columns=colonnes_PP, index=[index])
        donnees_synthese_energies_directes_PP = (
            donnees_synthese_energies_directes_PP.append(ligne_PP)
        )
        donnees_ELEC = [nom_terri]
        donnees_ELEC.append(liste_valeurs_ELEC[index])
        donnees_ELEC.append(genere_donnees_gaz_et_elec_region(numero)[1])
        ligne_ELEC = panda.DataFrame([donnees_ELEC], columns=colonnes_gaz_elec)
        donnees_synthese_energies_directes_elec = (
            donnees_synthese_energies_directes_elec.append(ligne_ELEC)
        )
        donnees_GAZ = [nom_terri]
        donnees_GAZ.append(liste_valeurs_GAZ[index])
        donnees_GAZ.append(genere_donnees_gaz_et_elec_region(numero)[0])
        ligne_GAZ = panda.DataFrame([donnees_GAZ], columns=colonnes_gaz_elec)
        donnees_synthese_energies_directes_gaz = (
            donnees_synthese_energies_directes_gaz.append(ligne_GAZ)
        )

    donnees_synthese_energies_directes_PP.to_csv(
        "./energie/outputs/synthese_methodes_energies_directes_PP.csv", sep=";"
    )
    donnees_synthese_energies_directes_gaz.to_csv(
        "./energie/outputs/synthese_methodes_energies_directes_gaz.csv", sep=";"
    )
    donnees_synthese_energies_directes_elec.to_csv(
        "./energie/outputs/synthese_methodes_energies_directes_elec.csv", sep=";"
    )
    return None


generer_df_et_csv_synthese_energies_directes()
