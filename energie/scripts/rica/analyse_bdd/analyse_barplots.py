from pathlib import Path

import matplotlib
import matplotlib.pylab as plt
import numpy as np
from collections import OrderedDict
from energie.scripts.rica.transformations_bdd.transformations_ajout_colonne_premiere_production_et_part_premiere_prod_dans_PBRTO import (
    ajouter_colonnes_premiere_prod_et_part_premiere_prod,
)


def count_to_dict(lst):
    unique_liste = list(OrderedDict.fromkeys(lst))
    dico = {}
    for element in unique_liste:
        nb = lst.count(element)
        dico[element] = nb
    return dico


def count_to_dict_freq(lst):
    unique_liste = list(OrderedDict.fromkeys(lst))
    dico = {}
    N = len(lst)
    for element in unique_liste:
        nb = lst.count(element)
        dico[element] = nb / N
    return dico


def transformer_listes_premieres_productions_en_dico(liste_production):
    dico_production = count_to_dict(liste_production)
    return dico_production


def produire_barplot_dictionnaire_unique(dictionnaire, nom):
    noms = list(dictionnaire.keys())
    valeurs = list(dictionnaire.values())

    for valeur in valeurs:
        if valeur < 50:
            i = valeurs.index(valeur)
            noms.pop(i)
            valeurs.remove(valeur)

    y_pos = np.arange(len(noms))

    plt.barh(y_pos, valeurs)

    # Create names on the x-axis
    plt.yticks(y_pos, noms)
    plt.yticks(fontsize=5)
    plt.savefig("./energie/outputs/bar_" + nom + ".png")
    plt.show()


def produire_barplot_dictionnaire_par_paire(
    dictionnaire1, dictionnaire2, nom, dossier_output: Path
):
    noms_1 = list(dictionnaire1.keys())
    valeurs_1 = list(dictionnaire1.values())
    y_pos1 = np.arange(len(noms_1))
    N1 = len(dictionnaire1)
    width = 0.35
    ind1 = range(N1)
    plt.figure("figure_1")
    plt.subplot(2, 1, 1)
    plt.barh(ind1, valeurs_1, width, color="r")  # création du diagramme
    plt.yticks(ind1, noms_1)
    plt.yticks(fontsize=8)

    noms_2 = list(dictionnaire2.keys())
    valeurs_2 = list(dictionnaire2.values())
    N2 = len(dictionnaire2)
    width = 0.35
    ind2 = range(N2)
    plt.subplot(2, 1, 2)
    plt.barh(ind2, valeurs_2, width, color="b")
    plt.yticks(ind2, noms_2)
    plt.yticks(fontsize=8)
    plt.savefig(dossier_output / f"bar_{nom}.png")


def creer_boxplot_des_premieres_prod():
    df_avec_premiere_prod = ajouter_colonnes_premiere_prod_et_part_premiere_prod()
    liste_production = df_avec_premiere_prod["premiere_prod"].to_list()
    dictionnaire_premiere_prod = transformer_listes_premieres_productions_en_dico(
        liste_production
    )
    nom = "premieres_productions"
    produire_barplot_dictionnaire_unique(dictionnaire_premiere_prod, nom)
    return None
