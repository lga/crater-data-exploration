import pandas as panda
import matplotlib.pyplot as plt


def explorer_variables_caracteristiques_majeures_exploitations(df, liste_colonnes):
    df_rica_selection_colonne = df[liste_colonnes]
    i = 0
    while i < len(liste_colonnes):
        boxplot = df_rica_selection_colonne.boxplot(liste_colonnes[i : i + 10])
        plt.savefig("./energie/outputs/boxplot" + str(i) + ".pdf")
        i += 10
    return None
