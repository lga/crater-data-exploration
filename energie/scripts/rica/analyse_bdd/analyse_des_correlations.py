def matrice_de_correlation_kendall(df_num):
    corr_matrix = df_num.corr(method="kendall")
    corr_matrix.to_csv(
        "matrice_correlation_pour_donnees_numeriques_only_kendall.csv", sep=";"
    )
    return corr_matrix


def matrice_de_correlation_spearman(df_num):
    corr_matrix = df_num.corr(method="spearman")
    corr_matrix.to_csv(
        "matrice_correlation_pour_donnees_numeriques_only_spearman.csv", sep=";"
    )
    return corr_matrix
