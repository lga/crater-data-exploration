import matplotlib.pyplot as plt


def generer_histogrammes_de_toutes_les_varibles_dun_df(df_rica):
    colonnes = df_rica.columns
    nb_figures_max = 10
    i = 0
    while i < len(colonnes):
        df_rica_selectionne = df_rica.iloc[:, i : i + 10]
        histo = df_rica_selectionne.hist(figsize=(20, 20), bins=100)
        i += 10
        plt.savefig("./energie/outputs/histogrammes" + str(i) + ".pdf")
    return None
