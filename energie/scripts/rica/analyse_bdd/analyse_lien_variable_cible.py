import matplotlib.pyplot as plt
from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne,
)
import pandas
import seaborn as sn
from communs.export_fichier import exporter_csv


def rica_partiel_application(df_rica, df_variables_applicables):
    va = df_variables_applicables.loc[(df_variables_applicables["Application"] == 1)]
    liste_va = va["Variable RICA"].to_list()
    liste_va.remove("OTEXE")
    liste_va.remove("OTEFDA")
    liste_va.remove("NREG")
    liste_va.remove("FJURI")
    df_rica_va = df_rica[liste_va]
    return df_rica_va


def representer_relation_variable_cible_variable_explicative(
    df_rica_complet, variable_explicative
):
    df_rica_transfo = (
        transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne(
            df_rica_complet
        )[0]
    )
    df_variables_applicables = pandas.read_csv(
        "./energie/inputs/variables_rica_utilisables_modele.csv", sep=","
    )
    df_rica_partiel = rica_partiel_application(
        df_rica_transfo, df_variables_applicables
    )
    y = df_rica_transfo["CHRCAQG"]
    x = df_rica_transfo[variable_explicative]
    plt.figure()
    plt.scatter(x, y)
    plt.title("Charges réelles en carburants en fonction de " + variable_explicative)
    plt.savefig("./energie/outputs/relation_lineaire" + variable_explicative + ".png")


def generer_correlations_et_heatmap_pr_variable_cible_carburants(df_rica):
    df_corr_tot = df_rica.corr()
    df_corr_tot_s = df_corr_tot["CHRCAQG"].sort_values(axis=0, ascending=False)
    df_corr_sup_01_tot = df_corr_tot_s[df_corr_tot_s >= 0.1]
    liste_va_tot = []
    for i in range(17):
        va = df_corr_sup_01_tot.index.values[i]
        liste_va_tot.append(va)
    df_hm_tot = df_corr_tot[liste_va_tot]
    df_hm_tot = df_hm_tot.loc[liste_va_tot]
    print(liste_va_tot)
    heatmap_variables_tot = sn.heatmap(data=df_hm_tot, cmap="YlGnBu")
    heatmap_variables_tot.set_xticklabels(
        heatmap_variables_tot.get_xmajorticklabels(), fontsize=5
    )

    plt.savefig("./energie/outputs/heatmap_tot.png")
    exporter_csv(
        df_corr_sup_01_tot,
        "./energie/outputs/correlations_total_rica_partiel.csv",
        True,
    )


def afficher_pourcentage_carburant_dans_region_diagnostic_climagri(df_rica):
    df_rica_transfo = (
        transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne(
            df_rica
        )[0]
    )
    df_select = df_rica_transfo[
        [
            "NREG",
            "EXTR2",
            "CHRCOQF",
            "CHRCOVF",
            "CHRCAQG",
            "CHRCAVG",
            "CHRCOQG",
            "ACHCAQG",
            "CHRCA",
        ]
    ]
    df_region_occitanie = df_rica_transfo.loc[df_rica_transfo["NREG"] == "NREG76"]
    df_region_hdf = df_rica_transfo.loc[df_rica_transfo["NREG"] == "NREG32"]
    df_region_grand_est = df_rica_transfo.loc[df_rica_transfo["NREG"] == "NREG44"]
    df_region_occitanie = df_region_occitanie[
        [
            "NREG",
            "EXTR2",
            "CHRCOQF",
            "CHRCOVF",
            "CHRCAQG",
            "CHRCAVG",
            "CHRCOQG",
            "ACHCAQG",
            "CHRCA",
        ]
    ]
    df_region_occitanie = df_region_occitanie.reset_index()
    df_region_hdf = df_region_hdf[
        [
            "NREG",
            "EXTR2",
            "CHRCOQF",
            "CHRCOVF",
            "CHRCAQG",
            "CHRCAVG",
            "CHRCOQG",
            "ACHCAQG",
            "CHRCA",
        ]
    ]
    df_region_hdf = df_region_hdf.reset_index()
    df_region_grand_est = df_region_grand_est[
        [
            "NREG",
            "EXTR2",
            "CHRCOQF",
            "CHRCOVF",
            "CHRCAQG",
            "CHRCAVG",
            "CHRCOQG",
            "ACHCAQG",
            "CHRCA",
        ]
    ]
    df_region_grand_est = df_region_grand_est.reset_index()
    df_region_occitanie["gaz extrapol"] = (
        df_region_occitanie["CHRCOQG"] * df_region_occitanie["EXTR2"]
    )
    df_region_hdf["gaz extrapol"] = df_region_hdf["CHRCOQG"] * df_region_hdf["EXTR2"]
    df_region_grand_est["gaz extrapol"] = (
        df_region_grand_est["CHRCOQG"] * df_region_grand_est["EXTR2"]
    )
    s_gaz_occi = df_region_occitanie["gaz extrapol"].sum()
    s_gaz_hdf = df_region_hdf["gaz extrapol"].sum()
    s_gaz_grand_est = df_region_grand_est["gaz extrapol"].sum()
    g_occi = (s_gaz_occi / 568) * 0.00083 * 0.001
    g_hdf = (s_gaz_hdf / 568) * 0.00083 * 0.001
    g_ge = (s_gaz_grand_est / 568) * 0.00083 * 0.001

    df_region_occitanie["fioul extrapol"] = (
        df_region_occitanie["CHRCOQF"] * df_region_occitanie["EXTR2"]
    )
    df_region_hdf["fioul extrapol"] = df_region_hdf["CHRCOQF"] * df_region_hdf["EXTR2"]
    df_region_grand_est["fioul extrapol"] = (
        df_region_grand_est["CHRCOQF"] * df_region_grand_est["EXTR2"]
    )
    s_fioul_occi = df_region_occitanie["fioul extrapol"].sum()
    s_fioul_hdf = df_region_hdf["fioul extrapol"].sum()
    s_fioul_grand_est = df_region_grand_est["fioul extrapol"].sum()
    f_o = s_fioul_occi * 0.840 * 0.001 * 0.001
    f_hdf = s_fioul_hdf * 0.840 * 0.001 * 0.001
    f_ge = s_fioul_grand_est * 0.840 * 0.001 * 0.001
    df_region_occitanie["carbu extrapol"] = (
        df_region_occitanie["CHRCAQG"] * df_region_occitanie["EXTR2"]
    )
    df_region_hdf["carbu extrapol"] = df_region_hdf["CHRCAQG"] * df_region_hdf["EXTR2"]
    df_region_grand_est["carbu extrapol"] = (
        df_region_grand_est["CHRCAQG"] * df_region_grand_est["EXTR2"]
    )
    s_carbu_occi = df_region_occitanie["carbu extrapol"].sum()
    s_carbu_hdf = df_region_hdf["carbu extrapol"].sum()
    s_carbu_grand_est = df_region_grand_est["carbu extrapol"].sum()
    c_o = s_carbu_occi * 0.840 * 0.001 * 0.001
    c_hdf = s_carbu_hdf * 0.840 * 0.001 * 0.001
    c_ge = s_carbu_grand_est * 0.840 * 0.001 * 0.001
    print(
        "occitanie, pourcentage rpz par carburant sur tous les PP ",
        c_o / (c_o + f_o + g_occi),
    )
    print(
        "hauts de france, pourcentage rpz par carburant sur tous les PP ",
        c_hdf / (c_hdf + f_hdf + g_hdf),
    )
    print(
        "grand est , pourcentage rpz par carburant sur tous les PP ",
        c_ge / (c_ge + f_ge + g_ge),
    )
    return None
