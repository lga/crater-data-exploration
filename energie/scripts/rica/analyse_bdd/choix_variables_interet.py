import matplotlib.pyplot as plt


def comparer_consommation_achat(df_rica_complet):
    liste_charges_reelles_engrais = df_rica_complet["CHREN"].to_list()
    liste_achat_engrais = df_rica_complet["ACHEN"].to_list()
    liste_charges_reelles_carburant = df_rica_complet["CHRCA"].to_list()
    liste_achat_engrais = df_rica_complet["ACHCA"].to_list()
    liste_charges_reelles_combustibles = df_rica_complet["CHRCO"].to_list()
    liste_achat_combustibles = df_rica_complet["ACHCO"].to_list()
    data = [
        liste_charges_reelles_engrais,
        liste_achat_engrais,
        liste_charges_reelles_carburant,
        liste_achat_engrais,
        liste_charges_reelles_combustibles,
        liste_achat_combustibles,
    ]
    fig, ax1 = plt.subplots(figsize=(10, 6))
    fig.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)
    bp = ax1.boxplot(data, notch=0, sym="+", vert=1, whis=1.5)
    ax1.set_xticklabels(
        ["CHREN", "ACHEN", "CHRCA", "ACHCA", "CHRCO", "ACHCO"], rotation=45, fontsize=8
    )
    plt.savefig("./energie/outputs/boxplot_achat_conso.png")
    plt.show()


def comparer_consommation_achat_2(df_rica_complet):
    liste_charges_reelles_engrais = df_rica_complet["CHREN"].to_list()
    liste_achat_engrais = df_rica_complet["ACHEN"].to_list()
    liste_charges_reelles_carburant = df_rica_complet["CHRCA"].to_list()
    liste_achat_engrais = df_rica_complet["ACHCA"].to_list()
    data = [
        liste_charges_reelles_engrais,
        liste_achat_engrais,
        liste_charges_reelles_carburant,
        liste_achat_engrais,
    ]
    fig, ax1 = plt.subplots(figsize=(10, 6))
    fig.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)
    bp = ax1.boxplot(data, notch=0, sym="+", vert=1, whis=1.5)
    ax1.set_xticklabels(["CHREN", "ACHEN", "CHRCA", "ACHCA"], rotation=45, fontsize=8)
    plt.savefig("./energie/outputs/boxplot_achat_conso_sans_combustible.png")
    plt.show()


def comparer_consommation_achat_3(df_rica_complet):

    liste_charges_reelles_combustibles = df_rica_complet["CHRCO"].to_list()
    liste_achat_combustibles = df_rica_complet["ACHCO"].to_list()
    data = [liste_charges_reelles_combustibles, liste_achat_combustibles]
    fig, ax1 = plt.subplots(figsize=(10, 6))
    fig.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)
    bp = ax1.boxplot(data, notch=0, sym="+", vert=1, whis=1.5)
    ax1.set_xticklabels(["CHRCO", "ACHCO"], rotation=45, fontsize=8)
    plt.savefig("./energie/outputs/boxplot_achat_conso_que_combustible.png")
    plt.show()
