import pandas as panda
from collections import OrderedDict
from cmath import nan
import matplotlib.pyplot as plt
from pathlib import Path
from energie.outils.logger import log

from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_FICHIER_RICA_DONNEES,
    CHEMIN_FICHIER_VARIABLES_APPLICABLES,
)
from energie.scripts.rica.pipeline.generer_dataframe_et_echantillon_train_et_test import (
    normaliser_sans_renommer_dataset_rica,
)
from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne,
)


def afficher_info_rica_complet(df_rica_complet):
    print(df_rica_complet.head(5))
    print(df_rica_complet.shape)
    print(df_rica_complet.columns.to_list())
    df_rica_complet.info()


def compter_et_extraire_variables_categorielle_nb_modes(df_rica_complet, dictionnaire):
    modalites = panda.read_csv("modalites_disc_et_cat_explicites.csv", sep=";")
    lignes_variables_numeriques = dictionnaire[dictionnaire["Type"] == "num"]
    variables_numeriques = lignes_variables_numeriques["Variable"].to_list()
    lignes_variables_cate = dictionnaire[dictionnaire["Type"] == "char"]
    variables_cate = lignes_variables_cate["Variable"].to_list()
    liste_variables_modalites = list(
        OrderedDict.fromkeys(modalites["VARIABLE"].to_list())
    )
    liste_variables_modalites_reelles = [
        x for x in liste_variables_modalites if x in variables_cate
    ]
    dictionnaire_mod = {}
    for i in range(len(modalites)):
        variable = modalites.VARIABLE[i]
        if variable in liste_variables_modalites_reelles:
            n = len(modalites[modalites["VARIABLE"] == variable])
            dictionnaire_mod[variable] = n
    taille_dico = len(dictionnaire_mod)
    liste_variables_cat_binaires = []
    for cle, valeur in dictionnaire_mod.keys, dictionnaire_mod.values():
        if valeur == 2:
            if modalites.moyenne[cle] != nan:
                liste_variables_cat_binaires.append(cle)
    binaire = len(liste_variables_cat_binaires)
    for i in liste_variables_cat_binaires:

        histo = df_rica_complet.hist()
        plt.savefig("./energie/outputs/histo" + str(i) + "categorielle_binaire.jpeg")
    log.info(
        f"il y a {binaire} variables catégorielles binaires, qui sont : {liste_variables_cat_binaires}"
    )
    log.info(f"Il y a {taille_dico} variables categorielles")
    # variables catégorielles de moins de 7 modalites, on fait des boxplots
    print(liste_variables_modalites_reelles)


def generer_dataframe_rica_complet_avec_variables_applicables(
    chemin_rica_complet, chemin_va_applicables
):
    fichier_va_appli = panda.read_csv(chemin_va_applicables, sep=",")
    lignes_variables = fichier_va_appli.loc[(fichier_va_appli["Application"] == 1)]
    liste_va_applicables = lignes_variables["Variable RICA"].to_list()
    liste_va_applicables.remove("OTEFDA")
    liste_va_applicables.remove("OTEXE")
    liste_va_applicables.remove("OTEFDD")
    liste_va_applicables.remove("FJURI")
    liste_va_applicables.remove("NREG")
    fichier_rica_complet = panda.read_csv(chemin_rica_complet, sep=";")

    lignes_va_char = lignes_variables.loc[lignes_variables["Type"] == "char"]
    liste_va_a_transfo = lignes_va_char["Variable RICA"].to_list()
    liste_va_a_transfo.remove("OTEFDA")
    liste_va_a_transfo.remove("OTEXE")
    liste_va_a_transfo.remove("OTEFDD")
    liste_va_a_transfo.remove("FJURI")
    liste_va_a_transfo.remove("NREG")
    fichier_rica_complet_transfo = (
        transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne(
            fichier_rica_complet
        )[0]
    )
    fichier_rica_va_appli = fichier_rica_complet_transfo[liste_va_applicables]
    fichier_rica_va_appli.to_csv("./energie/outputs/rica_va_appli.csv", sep=";")
    return fichier_rica_va_appli


if __name__ == "__main__":

    generer_dataframe_rica_complet_avec_variables_applicables(
        CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_FICHIER_VARIABLES_APPLICABLES
    )
