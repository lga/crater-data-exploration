from pathlib import Path
import zipfile
from matplotlib import pyplot as plt

import numpy as np

import pandas as pandas
from pandas import DataFrame


from scipy import stats
import statsmodels.api as sm
from energie.outils.logger import log

from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from energie.scripts.rica.communs.configs_energie_rica_agreste import SELECTION_VARIABLES_INTERET





def visualiser_y_y_predict(y, y_array_predi, w):
    y_predi_df = pandas.DataFrame(y_array_predi, columns=["y_predi"])
    y_df = pandas.DataFrame(np.array(y), columns=["y"])
    w_df = pandas.DataFrame(np.array(w), columns=["w"])
    y_predi = y_predi_df["y_predi"]
    x = [i for i in range(len(y))]
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    nw_y = y_predi - y.sort_values()
    ax1.scatter(x, nw_y, s=0.4, c="r", label="x=unité agricole, y=y_predi-y")
    plt.legend(loc="upper left")
    plt.show()
    plt.savefig("./energie/outputs/comp_y_y_predi.pdf")

    ecarts = abs(y - y_predi).to_list()
    dt = {"ecart_y_y_predi": ecarts}
    df_ecart = pandas.DataFrame(dt)
    moyenne_des_ecarts = abs(y - y_predi).sum() / len(y)
    print("moyenne des écarts", moyenne_des_ecarts)
    somme_y = y_df.multiply(w_df["w"], axis=0).sum()[0]
    somme_y_predi = y_predi_df.multiply(w_df["w"], axis=0).sum()[0]
    ecart_somme_en_gwh = (
        abs(somme_y - somme_y_predi) * 0.001 * 840 * 0.001 * 1 * 11628 * 0.000001
    )
    print("écart somme en gwh", ecart_somme_en_gwh)
    return (somme_y, somme_y_predi, ecart_somme_en_gwh)


def visualiser_2(y, y_predi):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    ax1.scatter(y, y_predi, s=0.4, c="g", label="y_en_x,y_pred_en_y")
    plt.legend(loc="upper left")
    plt.show()
    plt.savefig("./energie/outputs/scatter_comp_y_y_predi.pdf")


def appliquer_coefficient_extrapolation(df_rica):
    serie_extr2 = df_rica["coef_extrapol"]
    df_rica_ss_extr2 = df_rica.loc[:, df_rica.columns != "coef_extrapol"]
    df_final = serie_extr2 * df_rica_ss_extr2
    return df_final


def sommer_region(df_predit):

    df_predit = df_predit[
        [
            "otex_16_postes",
            "id_region",
            "surface_totale_ha",
            "production_brute_euros",
            "prediction_pp",
        ]
    ]
    df_communes_predi = df_predit.groupby("id_region").sum()
    somme_France = df_communes_predi["prediction_pp"].sum()
    df_communes_predi["prediction_PP_en_GWh"] = (
        df_communes_predi["prediction_pp"] * 0.001 * 840 * 0.001 * 1 * 11628 * 0.000001
    )
    df_communes_predi = df_communes_predi[["prediction_pp", "prediction_PP_en_GWh"]]
    df_communes_predi.to_csv("./energie/outputs/verifications_regions.csv", sep=";")
    return (df_communes_predi, somme_France)


def test_de_student(coefs, nb_obs):
    # on veut tester h0 : le coef est nul contre h1 : le coef est non nul
    # stat de test : t = (coef estimé - coef de h0)/ variance
    # quid variance ?
    # degres_de_liberte=nb observation - nombre de coefs
    dof = nb_obs - len(coefs)
    stat_de_test = stats.t.isf(0.0025, dof)
    stat = coefs[0]
    return None


def visualiser_y_y_predict(y, y_predi):
    x = [i for i in range(len(y))]
    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    ax1.scatter(x, y, c="b", label="y")
    ax1.scatter(x, y_predi, c="r", label="y_predi")
    plt.legend(loc="upper left")
    plt.show()
    plt.savefig("./energie/outputs/comp_y_y_predi.pdf")


def tracer_distribution_residus(y, y_predi):
    residuals = y_predi - y
    plt.figure()
    residuals.hist()
    plt.savefig("./energie/outputs/histo_residus.png")


def tracer_qq_plot(y, y_predi):
    residuals = y_predi - y

    residuals = residuals.to_numpy()
    residuals = (residuals - residuals.mean()) / residuals.std()
    plt.figure()
    sm.qqplot(residuals, line="45")
    plt.savefig("./energie/outputs/qqplots_residuals.png")


def tracer_homoscédasticité(y, y_predictions):
    residuals = y_predictions - y
    residuals = residuals.to_numpy()
    plt.figure()
    plt.scatter(y_predictions, residuals)
    plt.savefig("./energie/outputs/analyse_homoscédasticité.png")


def loguer_evaluation_modele(y, y_predictions, scores_cross_val):
    # visualiser_y_y_predict(y, y_predictions)
    log.info(f"Evaluation de la régression :")
    # log.info(f'  - variables explicatives {variables_explicatives}')
    log.info(f"  - r2 = {r2_score(y, y_predictions)}")
    log.info(f"  - REQM = {np.sqrt(mean_squared_error(y, y_predictions))}")
    log.info(f"  - EAM = {mean_absolute_error(y, y_predictions)}")
    log.info(
        f"  - ecart moyen % (sum(y) - sum(y_prediction)) / sum(y) = {100 * (y.sum() - y_predictions.sum()) / y.sum()}"
    )
    log.info(f"  - cross val, liste scores [REQM] = {-scores_cross_val}")
    log.info(f"  - cross val, score moyen [REQM moyen] = {-scores_cross_val.mean()}")
    log.info(f"  - cross val, ecart type  = {scores_cross_val.std()}")
    mrse = np.sqrt(mean_squared_error(y, y_predictions))
    r2 = r2_score(y, y_predictions)
    ream = mean_absolute_error(y, y_predictions)
    df = pandas.DataFrame(
        {
            "Racine de l'erreur quadratique moyenne": mrse,
            "Moyenne score cross-validation": scores_cross_val.mean(),
            "Ecart type score cross-validation": scores_cross_val.std(),
        },
        index=[0],
    )
    df.to_csv("./energie/outputs/metriques_model.csv", sep=";")
    tracer_qq_plot(y, y_predictions)
    tracer_homoscédasticité(y, y_predictions)
    tracer_distribution_residus(y, y_predictions)
