import statsmodels.api as sm
import pandas as panda
from pingouin import ancova

from energie.scripts.rica.communs.configs_energie_rica_agreste import NOMS_COLONNES_VARIABLES_EXPLICATIVES


df_train = panda.read_csv(
    "./energie/outputs/dataset/rica_train_set_normalise.csv", sep=";"
)


x = df_train["surface_totale_ha"].tolist()
y = df_train["charges_carburants_stockes_litre"].tolist()

x = sm.add_constant(x)


result = sm.OLS.from_formula(
    formula="charges_carburants_stockes_litre ~ surface_totale_ha + production_brute_euros + ugb_total_nb + unité_travail_agricole",
    data=df_train,
).fit()


print(result.summary())

# df_ancova=ancova(data=df_train,dv="charges_carburants_stockes_litre", covar=['surface_totale_ha','production_brute_euros'], between='otex_16_postes')
df_ancova = ancova(
    data=df_train,
    dv="charges_carburants_stockes_litre",
    covar=["surface_totale_ha", "production_brute_euros"],
    between="id_region",
)
df_ancova.to_csv("./energie/outputs/ancova.csv", sep=";")
