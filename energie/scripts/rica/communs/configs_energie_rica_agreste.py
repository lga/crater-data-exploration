from pathlib import Path

##chemins fichiers Agreste
FICHIER_RA_2020 = Path(
    "../crater-data-sources/agreste/2020/cartostat/cartostats_SAU_PBS_OTEX_REGION_extrait_le_30_06_2022.csv"
)
FICHIER_RA_2010 = Path("./energie/inputs/RA_2010_SAU_PBS_UTA.csv")
CHEMIN_DATA_AGRESTE_UTA = Path(
    "../crater-data-sources/agreste/1970-2010/FDS_G_2002.zip"
)
CHEMIN_DATA_AGRESTE_FJURI = Path(
    "../crater-data-sources/agreste/1970-2010/FDS_G_2001.zip"
)
CHEMIN_DATA_AGRESTE_UGBTOT = Path(
    "../crater-data-sources/agreste/1970-2010/FDS_G_2141.zip"
)

#chemins fichiers rica

CHEMIN_FICHIER_RICA_DONNEES = Path(
    "../crater-data-sources/agreste/RicaMicrodonnées2020/Rica_France_micro_Donnees_ex2020.csv"
)
CHEMIN_FICHIER_RICA_DICTIONNAIRE = Path(
    "../crater-data-sources/agreste/RicaMicrodonnées2020/RICA2020_dictionnaire_modalite_FMD.xlsx"
)


FICHIER_VARIABLES_EXPLOITABLES_RICA = Path(
    "./energie/inputs/variables_rica_utilisables_modele.csv"
)


CHEMIN_FICHIER_VARIABLES_APPLICABLES = Path(
    "./energie/inputs/variables_rica_utilisables_modele.csv"
)

CHEMIN_DOSSIER_OUTPUT = Path("./energie/outputs")
CHEMIN_DOSSIER_OUTPUT_DATASET = CHEMIN_DOSSIER_OUTPUT / "dataset"
CHEMIN_DOSSIER_OUTPUT_STATISTIQUES = CHEMIN_DOSSIER_OUTPUT / "statistiques"
CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP = CHEMIN_DOSSIER_OUTPUT / "evaluation_pp"
CHEMIN_DATAFRAME_PARTIEL = Path("./energie/df_rica_partiel_test.csv")
CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET = (
    CHEMIN_DOSSIER_OUTPUT_DATASET / "rica_train_set_normalise.csv"
)
CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET_AG_EXPLOIT = (
    CHEMIN_DOSSIER_OUTPUT_DATASET / "rica_train_set_normalise_ag_exploit.csv"
)
CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET = (
    CHEMIN_DOSSIER_OUTPUT_DATASET / "rica_validation_set_normalise.csv"
)

CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET_AG_EXPLOIT = (
    CHEMIN_DOSSIER_OUTPUT_DATASET / "rica_validation_set_normalise_ag_exploit.csv"
)
CHEMIN_FICHIER_RICA_NORMALISE_METROPOLE = (
    CHEMIN_DOSSIER_OUTPUT_DATASET / "rica_metropole_normalise.csv"
)
CHEMIN_DOSSIER_SIMULATION = Path("./energie/outputs/simulation")

CHEMIN_SDES_REGION = Path(
    "../crater-data-sources/energie/data_SDES_2014-2020/références et métadonnées/donnees_SDES_carburants.csv"
)
CHEMIN_FICHIER_RICA_SOMME = CHEMIN_DOSSIER_OUTPUT_DATASET / "df_general_sommes_exploit.csv"
#chemin ref num region

CHEMIN_REF_NUM_REGION = Path(
    "../crater-data-sources/insee/geographie/departements_regions/2021/region2021.csv"
)
#selection variables

SELECTION_VARIABLES_INTERET = [
    "OTEFDD",
    "OTEFDA",
    "NREG",
    "SAUTI",
    "PBUCE",
    "ZALTI",
    "UGBTO",
    "CHRCAQG",
    "CHRCOQF",
    "CHRCOQG",
    "EXTR2",
    "UTATO",
]

SELECTION_VARIABLES_INTERET_RENOMMEES = [
    "otex_16_postes",
    "id_region",
    "surface_totale_ha",
    "production_brute_euros",
    "charges_carburants_stockes_litre",
    "ponderation",
]

NOMS_COLONNES_VARIABLES_EXPLICATIVES = [
    "surface_totale_ha"
]

DICT_RENOMMAGE_COLONNES_RICA = {
    "Code": "code_communes",
    "OTEFDD": "otex_16_postes",
    "OTEFDA": "otex_11_postes",
    "NREG": "id_region",
    "SAUTI": "surface_totale_ha",
    "ZALTI": "zone_altimetrique_metre",
    "UGBTO": "ugb_total_nb",
    "PBUCE": "production_brute_euros",
    "CHRCAQG": "charges_carburants_stockes_litre",
    "CHRCOQF": "charges_fioul_domestique_litre",
    "CHRCOQG": "charges_gaz_kg",
    "EXTR2": "ponderation",
    "UTATO": "unité_travail_agricole",
}