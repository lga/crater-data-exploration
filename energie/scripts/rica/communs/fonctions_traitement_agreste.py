
import zipfile

import pandas

from energie.scripts.rica.communs.configs_energie_rica_agreste import FICHIER_RA_2020, FICHIER_RA_2010, CHEMIN_DATA_AGRESTE_UTA, CHEMIN_DATA_AGRESTE_FJURI,CHEMIN_DATA_AGRESTE_UGBTOT


def generer_fichier_initial_communes():
    fichier_variables_RA = pandas.read_csv(
        FICHIER_RA_2020, sep=";", skiprows=2, dtype={"Code": str}
    )
    fichier_variables_RA["code_region"] = (
        fichier_variables_RA["Région 2020"].str.split(" ").str[0]
    )
    fichier_variables_RA["code_region"] = fichier_variables_RA["code_region"].astype(
        "int64"
    )
    dictionnaire_colonnes = {
        "Spécialisation de la production agricole en 2020 (17 postes)": "OTEFDD",
        "code_region": "NREG",
        "SAU en 2020": "SAUTI",
        "PBS en 2020": "PBUCE",
    }
    fichier_variables_RA = fichier_variables_RA.rename(columns=dictionnaire_colonnes)
    fichier_variables_RA = fichier_variables_RA.drop(
        [
            "Libellé",
            "Nombre d'exploitations en 2020",
            "Spécialisation de la production agricole en 2020 (17 postes) - estimation (*=oui)",
            "SAU en 2020 - estimation (*=oui)",
            "PBS en 2020 - estimation (*=oui)",
        ],
        axis=1,
    )
    fichier_variables_RA = fichier_variables_RA.loc[
        (fichier_variables_RA["NREG"] != "01")
        & (fichier_variables_RA["NREG"] != "02")
        & (fichier_variables_RA["NREG"] != "03")
        & (fichier_variables_RA["NREG"] != "04")
        & (fichier_variables_RA["NREG"] != "06")
        & (fichier_variables_RA["OTEFDD"] != 0)
        & (fichier_variables_RA["OTEFDD"] != 9000)
    ]  # lignes_aberrantes=fichier_variables_RA[fichier_variables_RA['SAUTI'].str.startswith('0',' ')]
    fichier_variables_RA["PBUCE"] = fichier_variables_RA["PBUCE"] * 1000
    # fichier_variables_RA["OTEFDD"]="OTEFDD"+fichier_variables_RA["OTEFDD"].astype(str)
    # fichier_variables_RA["NREG"]="NREG"+fichier_variables_RA["NREG"].astype(str)
    return fichier_variables_RA
    # ["OTEXE","NREG","SAUTI","UTATO","UGBTO"]


def generer_fichier_initial_communes_2010():
    fichier_variables_RA = pandas.read_csv(
        FICHIER_RA_2010, sep=";", skiprows=2, dtype={"Code": str}
    )
    dictionnaire_colonnes = {
        "Superficie agricole utilisée, 2010": "SAUTI",
        "PBS totale, 2010": "PBUCE",
        "Nombre d'UTA en 2010": "UTATO",
    }
    fichier_variables_RA = fichier_variables_RA.rename(columns=dictionnaire_colonnes)
    fichier_variables_RA = fichier_variables_RA[["Code", "SAUTI", "PBUCE", "UTATO"]]
    return fichier_variables_RA


def generer_fichier_SAU_par_OTEX():
    df_SAU_PBS_seg = pandas.read_csv(
        zipfile.ZipFile(CHEMIN_DATA_AGRESTE_UTA, "r").open("FDS_G_2002_2010.txt"),
        sep=";",
    )
    df_SAU_seg_com = df_SAU_PBS_seg[
        (df_SAU_PBS_seg["COM"] != "............")
        & (df_SAU_PBS_seg["REGION"] != "NR01")
        & (df_SAU_PBS_seg["REGION"] != "NR02")
        & (df_SAU_PBS_seg["REGION"] != "NR03")
        & (df_SAU_PBS_seg["REGION"] != "NR04")
        & (
            df_SAU_PBS_seg["G_2002_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (df_SAU_PBS_seg["G_2002_LIB_DIM2"] != "Ensemble")
        & (df_SAU_PBS_seg["G_2002_LIB_DIM3"] == "Superficie agricole utilisée (ha)")
    ]
    df_SAU_seg_com_piv = df_SAU_seg_com.pivot(
        index="COM", columns="G_2002_LIB_DIM2", values="VALEUR"
    )
    df_SAU_seg_com_piv_reord = df_SAU_seg_com_piv[
        [
            "Bovins lait (Otex 45)",
            "Grandes cultures (Otex 15, 16)",
            "Elevages hors sol  (Otex 51, 52, 53, 74)",
            "Bovins mixte (Otex 47)",
            "Bovins viande (Otex 46)",
            "Ovins, caprins et autres herbivores (Otex 48)",
            "Viticulture (Otex 35)",
            "Polyculture, polyélevage, autres (Otex 61, 73, 83, 84, 90)",
            "Cultures fruitières et autres cultures permanentes (Otex 36, 37, 38)",
            "Maraîchage et horticulture (Otex 21, 22)",
        ]
    ]
    df_PBS_com = df_SAU_PBS_seg[
        (df_SAU_PBS_seg["COM"] != "............")
        & (df_SAU_PBS_seg["REGION"] != "NR01")
        & (df_SAU_PBS_seg["REGION"] != "NR02")
        & (df_SAU_PBS_seg["REGION"] != "NR03")
        & (df_SAU_PBS_seg["REGION"] != "NR04")
        & (
            df_SAU_PBS_seg["G_2002_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (df_SAU_PBS_seg["G_2002_LIB_DIM2"] == "Ensemble")
        & (
            df_SAU_PBS_seg["G_2002_LIB_DIM3"]
            == "Production brute standard (millier d'euros)"
        )
    ]
    df_PBS_com_piv = df_PBS_com.pivot(
        index="COM", columns="G_2002_LIB_DIM2", values="VALEUR"
    )
    df_PBS_com_piv["PBS en euros"] = df_PBS_com_piv["Ensemble"] * 1000
    df_PBS_com_piv = df_PBS_com_piv[["PBS en euros"]]
    df_merge_com = pandas.merge(
        df_PBS_com_piv, df_SAU_seg_com_piv_reord, left_index=True, right_index=True
    )
    df_merge_com = df_merge_com.fillna(0)
    df_region_COM = df_PBS_com[["COM", "REGION"]]
    df_region_COM = df_region_COM.set_index("COM")
    df_merge_com_region = pandas.merge(
        df_merge_com, df_region_COM, left_on="COM", right_on="COM"
    )
    df_merge_com_region = df_merge_com_region.rename(columns={"REGION": "id_region"})
    df_merge_com_region = df_merge_com_region.replace(
        to_replace={
            "NR11": "11",
            "NR24": "24",
            "NR27": "27",
            "NR28": "28",
            "NR32": "32",
            "NR44": "44",
            "NR52": "52",
            "NR53": "53",
            "NR75": "75",
            "NR76": "76",
            "NR84": "84",
            "NR93": "93",
            "NR94": "94",
        }
    )

    return (df_merge_com, df_merge_com_region)


def generer_fichier_UGB():
    df_UGB = pandas.read_csv(
        zipfile.ZipFile(CHEMIN_DATA_AGRESTE_UGBTOT, "r").open("FDS_G_2141_2010.txt"),
        sep=";",
    )
    df_UGB_communes = df_UGB[
        (df_UGB["COM"] != "............")
        & (df_UGB["REGION"] != "NR01")
        & (df_UGB["REGION"] != "NR02")
        & (df_UGB["REGION"] != "NR03")
        & (df_UGB["REGION"] != "NR04")
        & (
            df_UGB["G_2141_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (df_UGB["G_2141_LIB_DIM2"] == "Elevages (total hors apiculture)(1)")
        & (
            df_UGB["G_2141_LIB_DIM3"]
            == "Unité Gros Bétail Alimentation grossière (UGBAG)"
        )
    ]
    df_UTA_valeurs_UGB = df_UGB_communes[["COM", "VALEUR"]]
    df_UTA_valeurs_UGB = df_UTA_valeurs_UGB.rename(columns={"VALEUR": "UGBTO"})
    df_UTA_valeurs_UGB["UGBTO"] = df_UTA_valeurs_UGB["UGBTO"].fillna(0)
    return df_UTA_valeurs_UGB


def retourner_code_communes_ou_UTA_nul(fichier_uta):
    fichier_UTA_choi = fichier_uta.loc[fichier_uta["UTATO"] == 0]
    liste_codes_com_UTA_nul = fichier_UTA_choi["COM"].to_list()
    return liste_codes_com_UTA_nul


def generer_fichier_FJURI(CHEMIN_DATA_AGRESTE_FJURI):
    df_FJURI = pandas.read_csv(
        zipfile.ZipFile(CHEMIN_DATA_AGRESTE_FJURI, "r").open("FDS_G_2001_2010.txt"),
        sep=";",
    )
    df_FJURI_communes = df_FJURI[
        (df_FJURI["COM"] != "............")
        & (df_FJURI["REGION"] != "NR01")
        & (df_FJURI["REGION"] != "NR02")
        & (df_FJURI["REGION"] != "NR03")
        & (df_FJURI["REGION"] != "NR04")
        & (df_FJURI["G_2001_MOD_DIM2"] != "001")
        & (
            df_FJURI["G_2001_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (df_FJURI["G_2001_LIB_DIM3"] == "Exploitations")
    ]
    return df_FJURI_communes


def generer_fichier_UTA():
    df_UTA = pandas.read_csv(
        zipfile.ZipFile(CHEMIN_DATA_AGRESTE_UTA, "r").open("FDS_G_2002_2010.txt"),
        sep=";",
    )
    df_UTA_communes = df_UTA[
        (df_UTA["COM"] != "............")
        & (df_UTA["REGION"] != "NR01")
        & (df_UTA["REGION"] != "NR02")
        & (df_UTA["REGION"] != "NR03")
        & (df_UTA["REGION"] != "NR04")
        & (
            df_UTA["G_2002_LIB_DIM1"]
            == "Ensemble des exploitations (hors pacages collectifs)"
        )
        & (df_UTA["G_2002_LIB_DIM2"] == "Ensemble")
        & (df_UTA["G_2002_LIB_DIM3"] == "Unités de travail annuel (UTA)")
    ]
    df_UTA_valeurs_UTA = df_UTA_communes[["COM", "VALEUR"]]
    df_UTA_valeurs_UTA = df_UTA_valeurs_UTA.rename(columns={"VALEUR": "UTATO"})
    df_UTA_valeurs_UTA["UTATO"] = df_UTA_valeurs_UTA["UTATO"].fillna(0)
    return df_UTA_valeurs_UTA


def df_merger_communes_init_et_df_agreste(dataframe_communes, dataframe_agreste):
    df_merge = pandas.merge(
        left=dataframe_communes,
        right=dataframe_agreste,
        left_on="Code",
        right_on="COM",
        how="inner",
    )
    return df_merge


def sommer_region(df_predit):
    df_communes_predi = df_predit.groupby("id_region").sum()
    df_communes_predi = df_communes_predi.reset_index()
    somme_France = df_communes_predi["charges_carburants_stockes_litre"].sum()
    df_communes_predi["prediction_PP_en_GWh"] = (
        df_communes_predi["charges_carburants_stockes_litre"]
        * 0.001
        * 840
        * 0.001
        * 1
        * 11628
        * 0.000001
    )
    df_communes_predi = df_communes_predi[["id_region", "prediction_PP_en_GWh"]]
    df_communes_predi.to_csv("./energie/outputs/verifications_regions.csv", sep=";")
    return df_communes_predi
