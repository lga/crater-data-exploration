from pathlib import Path
import zipfile

import pandas
from matplotlib import pyplot as plt
from pandas import DataFrame

from sklearn.base import BaseEstimator, TransformerMixin
from energie.outils.logger import log
import numpy as np
import statsmodels.api as sm
from sklearn.compose import make_column_transformer, ColumnTransformer

from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    valeur_tranche,
    creer_dictionnaire_modalites_explicitement_categorielle_pour_R,
)
from energie.scripts.rica.communs.configs_energie_rica_agreste import \
CHEMIN_FICHIER_RICA_DICTIONNAIRE,\
FICHIER_VARIABLES_EXPLOITABLES_RICA,\
DICT_RENOMMAGE_COLONNES_RICA


class TranchesVersValeursMoyennes(BaseEstimator, TransformerMixin):
    def __init__(self, df_modalites_rica, nom_colonnes_tranches):
        self.df_modalites_rica = df_modalites_rica
        self.dict_nom_colonnes_tranches_vers_valeurs = nom_colonnes_tranches

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return remplacer_tranches_par_valeurs_moyennes(
            X, self.df_modalites_rica, self.dict_nom_colonnes_tranches_vers_valeurs
        )


class RenommerColonnes(BaseEstimator, TransformerMixin):
    def __init__(self, dict_nouveaux_noms):
        self.dict_nouveaux_noms = dict_nouveaux_noms

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X.rename(columns=self.dict_nouveaux_noms)


def charger_fichier_rica(chemin_fichier_rica: Path) -> DataFrame:
    return pandas.read_csv(chemin_fichier_rica, sep=";")


def charger_modalites_rica_partielles():
    df_modalites = pandas.read_excel(
        CHEMIN_FICHIER_RICA_DICTIONNAIRE, sheet_name="MODALITE FMD 2020"
    )
    return df_modalites.loc[
        df_modalites.VARIABLE.isin(DICT_RENOMMAGE_COLONNES_RICA.keys()), :
    ].reset_index(drop=True)

    return None


def remplacer_tranches_par_valeurs_moyennes(
    df_rica, df_modalites_rica, nom_colonnes_tranches
):
    modalites_en_discretisees = valeur_tranche(df_modalites_rica)
    modalites_discr_et_categories_explites = (
        creer_dictionnaire_modalites_explicitement_categorielle_pour_R(
            modalites_en_discretisees
        )
    )
    for colonne in nom_colonnes_tranches:

        donnees_categories_explicites = modalites_discr_et_categories_explites[
            modalites_discr_et_categories_explites["VARIABLE"] == colonne
        ]
        liste_categories_explicites = donnees_categories_explicites["MOD"].to_list()
        liste_valeurs_moyennes_categories = donnees_categories_explicites[
            "moyenne"
        ].to_list()
        df_rica[colonne] = (
            df_rica[colonne]
            .astype("str")
            .replace(
                to_replace=liste_categories_explicites,
                value=liste_valeurs_moyennes_categories,
            )
        )
    return df_rica


def appliquer_coefficient_ponderation(df_rica):
    serie_extr2 = df_rica["ponderation"]
    df_rica_ss_extr2 = df_rica.loc[:, df_rica.columns != "ponderation"]
    df_final = serie_extr2 * df_rica_ss_extr2
    return df_final


def extraire_variables_exploitables_modelisation():
    fichier_variables_exploitables = pandas.read_csv(
        FICHIER_VARIABLES_EXPLOITABLES_RICA, sep=","
    )
    select_variables = fichier_variables_exploitables.loc[
        fichier_variables_exploitables["Application"] == 1
    ]
    liste_variables = select_variables["Variable RICA"].to_list()
    return liste_variables
