from pathlib import Path
import joblib
import numpy as np
import pandas as panda
from energie.outils.export_fichier import reinitialiser_dossier
from energie.outils.logger import log
from energie.scripts.rica.communs.fonctions_traitement_agreste import (
    df_merger_communes_init_et_df_agreste,
    generer_fichier_UTA,
)
from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_SDES_REGION,
    DICT_RENOMMAGE_COLONNES_RICA,
    NOMS_COLONNES_VARIABLES_EXPLICATIVES,
    CHEMIN_REF_NUM_REGION,
    CHEMIN_DOSSIER_SIMULATION
)
from energie.scripts.rica.communs.fonctions_traitement_agreste import (
    generer_fichier_SAU_par_OTEX,
    generer_fichier_UGB,
    retourner_code_communes_ou_UTA_nul,
    sommer_region,
)

from energie.scripts.rica.communs.fonctions_traitement_agreste import (
    generer_fichier_initial_communes,
    generer_fichier_initial_communes_2010,
)

CHEMIN_DOSSIER_OUTPUT = Path("./energie/outputs").resolve()
CHEMIN_MODELE_GENERE = Path(
    "./energie/outputs/evaluation_pp/modele_eval_conso_pp.dump"
).resolve()



def evaluer_conso_pp_communes(chemin_modele_genere,modele_type,variables):
    reinitialiser_dossier(CHEMIN_DOSSIER_SIMULATION)

    modele = joblib.load(chemin_modele_genere)
    if modele_type=="reg_lin_segmentation_otex":
        df_communes_prepare, df_communes_pour_verif_region = generer_fichier_SAU_par_OTEX()
        
    else:
        df_communes_prepare,df_communes_pour_verif_region = preparerDataFrameCommunes(variables)
    y_predict_communes = modele.predict(df_communes_prepare)
    df_communes_prepare["charges_carburants_stockes_litre"] = y_predict_communes
    df_communes_pour_verif_region[
        "charges_carburants_stockes_litre"
    ] = y_predict_communes
    df_communes_prepare.to_csv(
        CHEMIN_DOSSIER_SIMULATION / "prediction_pp_communes.csv", sep=";"
    )
    somme_France_predite = comparaison_regions_sdes(df_communes_pour_verif_region)


def preparerDataFrameCommunes(liste_variables_explicatives):
    dataframe_communes = generer_fichier_initial_communes()
    dataframe_com_UTA = generer_fichier_UTA()
    dataframe_com_UGB = generer_fichier_UGB()
    liste_codes = retourner_code_communes_ou_UTA_nul(dataframe_com_UTA)
    # df_communes_where_UTA_nul=dataframe_communes.loc[dataframe_communes['Code'].isin(liste_codes)]
    df_merge_UTA = df_merger_communes_init_et_df_agreste(
        dataframe_communes, dataframe_com_UTA
    )
    df_merge_UTA_a_UGB = df_merger_communes_init_et_df_agreste(
        df_merge_UTA, dataframe_com_UGB
    )
    dataframe_final_com = df_merge_UTA_a_UGB.rename(
        columns=DICT_RENOMMAGE_COLONNES_RICA
    )

    if "id_region" not in liste_variables_explicatives:
        liste_colonnes_et_region = liste_variables_explicatives + ["id_region"]
        dataframe_finale_com_region = dataframe_final_com[liste_colonnes_et_region]
        dataframe_final_com = dataframe_final_com[liste_variables_explicatives]
        return dataframe_final_com, dataframe_finale_com_region
    else:
        dataframe_final_com = dataframe_final_com[liste_variables_explicatives]
        dataframe_finale_com_region = dataframe_final_com.copy()
        return dataframe_final_com, dataframe_finale_com_region


def preparerDataFrameCommunes_2010():
    dataframe_communes = generer_fichier_initial_communes_2010()
    df_2020 = generer_fichier_initial_communes()
    df_merge_2010_2020 = panda.merge(
        left=dataframe_communes,
        right=df_2020[["Code", "OTEFDD"]],
        left_on="Code",
        right_on="Code",
        how="left",
    )
    # dataframe_communes=dataframe_communes.rename(columns=DICT_RENOMMAGE_COLONNES_RICA)
    # dataframe_final_com=dataframe_final_com[["id_region","surface_totale_ha", "production_brute_euros","unité_travail_agricole"]]
    return dataframe_communes


def comparaison_regions_sdes(dataframe_communes_prediction):
    colonnes = dataframe_communes_prediction.columns.to_list()
    colonnes = colonnes + ["NCC"]
    df_sommes_communes = sommer_region(dataframe_communes_prediction)
    df_ref_num_region = panda.read_csv(CHEMIN_REF_NUM_REGION, sep=",")
    df_sommes_communes = df_sommes_communes.astype({"id_region": "int64"})
    df_mergee_sommes_region = panda.merge(
        left=df_sommes_communes,
        right=df_ref_num_region,
        how="left",
        left_on="id_region",
        right_on="REG",
    )
    df_mergee_sommes_region = df_mergee_sommes_region[["prediction_PP_en_GWh", "NCC"]]
    df_mergee_sommes_region = df_mergee_sommes_region.rename({"NCC": "Région"}, axis=1)
    df_ref_region_val = panda.read_csv(
        CHEMIN_SDES_REGION, sep=";", dtype={"SDES": np.float64}
    )
    df_merge_avec_ref = panda.merge(
        left=df_mergee_sommes_region,
        right=df_ref_region_val,
        how="left",
        left_on="Région",
        right_on="Région",
    )
    df_merge_avec_ref = df_merge_avec_ref[
        ["Région", "prediction_PP_en_GWh", "SDES carburants en GWH"]
    ]
    df_merge_avec_ref["Ecart"] = (
        df_merge_avec_ref["prediction_PP_en_GWh"]
        - df_merge_avec_ref["SDES carburants en GWH"]
    ) / df_merge_avec_ref["SDES carburants en GWH"]

    df_merge_avec_ref["Ecart abs"] = abs(df_merge_avec_ref["Ecart"])
    df_sans_Corse = df_merge_avec_ref.loc[df_merge_avec_ref["Région"] != "CORSE"]
    moyenne_ecart_region = df_sans_Corse["Ecart abs"].mean()
    log.info(f"Moyenne des écarts région {moyenne_ecart_region}")
    df_merge_avec_ref.to_csv(
        CHEMIN_DOSSIER_OUTPUT /"donnees_comparaison_sdes_region.csv", sep=";"
    )
    return df_merge_avec_ref


if __name__ == "__main__":

    evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE)
