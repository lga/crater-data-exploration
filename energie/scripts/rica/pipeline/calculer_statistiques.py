from pathlib import Path

import pandas

from energie.outils.export_fichier import (
    reinitialiser_dossier,
    exporter_csv,
    exporter_description_dataframe,
)
from energie.outils.logger import log

from energie.scripts.rica.communs.fonctions_traitement_rica import (
    charger_fichier_rica)
from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_FICHIER_RICA_DONNEES,
    CHEMIN_DOSSIER_OUTPUT_STATISTIQUES,
    CHEMIN_FICHIER_RICA_NORMALISE_METROPOLE,
    CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
    CHEMIN_DOSSIER_SIMULATION
)
from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    CHEMIN_DONNEES_RICA_DICO,
)


def calculer_statistiques(dossier_output: Path):
    reinitialiser_dossier(dossier_output)
    calculer_correlations_colonnes_PP(dossier_output)
    calculer_statistiques_dataset(
        CHEMIN_FICHIER_RICA_NORMALISE_METROPOLE, dossier_output, "rica_metropole"
    )
    calculer_statistiques_dataset(
        CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP / "rica_train_set_avec_prediction.csv",
        dossier_output,
        "rica_trained",
    )
    calculer_statistiques_dataset(
        CHEMIN_DOSSIER_SIMULATION / "prediction_pp_communes.csv",
        dossier_output,
        "communes_simulation",
    )
    calculer_totaux_niveau_france(dossier_output)


def calculer_correlations_colonnes_PP(dossier_output):
    df_rica = charger_fichier_rica(CHEMIN_FICHIER_RICA_DONNEES)
    df_correlations = df_rica.corr()
    df_correlations_pp = df_correlations.loc[:, ["CHRCAQG"]]
    dictionnaire = (
        pandas.read_excel(CHEMIN_DONNEES_RICA_DICO, sheet_name="DICTIONNAIRE FMD 2020")
        .set_index("Variable")
        .loc[:, ["Libellé"]]
    )
    exporter_csv(
        df_correlations_pp.join(dictionnaire),
        dossier_output / f"rica_variables_correlees_avec_conso_pp.csv",
        True,
    )


def calculer_statistiques_dataset(
    chemin_fichier_rica, dossier_output: Path, prefixe_nom_fichier: str
):
    df_rica = charger_fichier_rica(chemin_fichier_rica)
    exporter_statistiques_rica(df_rica, dossier_output, prefixe_nom_fichier)
    generer_scatter_matrix(
        df_rica, dossier_output / f"{prefixe_nom_fichier}_scatter_matrix.png"
    )


def exporter_statistiques_rica(df_rica, dossier_output, prefixe_nom_fichier: str):
    exporter_description_dataframe(
        df_rica, dossier_output / f"{prefixe_nom_fichier}_info.txt"
    )
    df_describe = df_rica.describe()
    df_describe.loc["total", :] = df_rica.sum().transpose()
    exporter_csv(
        df_describe, dossier_output / f"{prefixe_nom_fichier}_describe.csv", True
    )
    exporter_csv(
        df_rica.corr(), dossier_output / f"{prefixe_nom_fichier}_corr.csv", True
    )


def calculer_totaux_niveau_france(dossier_output):
    df_rica_metropole = charger_fichier_rica(CHEMIN_FICHIER_RICA_NORMALISE_METROPOLE)
    df_rica_metropole = df_rica_metropole[
        df_rica_metropole.columns[~df_rica_metropole.columns.isin(["ponderation"])]
    ].multiply(df_rica_metropole["ponderation"], axis=0)
    df_rica_metropole = (
        df_rica_metropole.agg(["sum"])
        .transpose()
        .rename(columns={"sum": "total_france_depuis_rica_metropole"})
    )

    df_rica_prediction = charger_fichier_rica(
        CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP / "rica_train_set_avec_prediction.csv"
    )
    df_rica_prediction = df_rica_prediction[
        df_rica_prediction.columns[~df_rica_prediction.columns.isin(["ponderation"])]
    ].multiply(df_rica_prediction["ponderation"], axis=0)
    df_rica_prediction = (
        df_rica_prediction.agg(["sum"])
        .transpose()
        .rename(columns={"sum": "total_france_depuis_rica_trainset_et_predictions"})
    )

    df_communes_simulation = charger_fichier_rica(
        CHEMIN_DOSSIER_SIMULATION / "prediction_pp_communes.csv"
    )
    df_communes_simulation = (
        df_communes_simulation.agg(["sum"])
        .transpose()
        .rename(columns={"sum": "total_france_depuis_simulation_communes"})
    )

    df_totaux_france = df_rica_metropole.join(df_rica_prediction).join(
        df_communes_simulation
    )
    total_france_depuis_rica = df_totaux_france.loc[
        "charges_carburants_stockes_litre", "total_france_depuis_rica_metropole"
    ]
    total_france_depuis_communes = df_totaux_france.loc[
        "charges_carburants_stockes_litre", "total_france_depuis_simulation_communes"
    ]
    log.info(
        f"Bilan : total France depuis Rica(M litres)={total_france_depuis_rica/1000000:.0f}, total France estimé via communes(M Litres)={total_france_depuis_communes/1000000:.0f}"
    )
    log.info(
        f"Bilan : ecart en pourcents ={(100 * (total_france_depuis_rica - total_france_depuis_communes) / total_france_depuis_rica):.2f}"
    )

    exporter_csv(df_totaux_france, dossier_output / f"rica_stat_total_france.csv", True)


def generer_scatter_matrix(df_rica_partiel, fichier_sortie: Path):

    df = df_rica_partiel.loc[
        :,
        ["surface_totale_ha", "charges_carburants_stockes_litre"]
        # ['surface_totale_ha', 'production_brute_euros', 'ugb_total_nb', 'charges_carburants_stockes_litre']
    ]
    hist = pandas.plotting.scatter_matrix(df, figsize=(20, 20), hist_kwds={"bins": 50})

    fig = hist[0][0].get_figure()
    fig.savefig(fichier_sortie)
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


if __name__ == "__main__":
    df_rica = charger_fichier_rica(CHEMIN_FICHIER_RICA_DONNEES)
    calculer_totaux_niveau_france(CHEMIN_DOSSIER_OUTPUT_STATISTIQUES)
    # calculer_statistiques(CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_DOSSIER_OUTPUT_STATISTIQUES)
