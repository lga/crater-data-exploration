
from cmath import nan
from collections import OrderedDict
from pathlib import Path


import joblib
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.svm import SVR
from sklearn.preprocessing import PolynomialFeatures
import numpy as np


import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from energie.outils.export_fichier import reinitialiser_dossier, exporter_csv

from energie.scripts.rica.communs.fonctions_traitement_rica import charger_fichier_rica
from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_DOSSIER_OUTPUT_DATASET,
    CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
    CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
    CHEMIN_FICHIER_RICA_SOMME,
    CHEMIN_FICHIER_RICA_DONNEES,
    CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
    NOMS_COLONNES_VARIABLES_EXPLICATIVES,
)
from energie.scripts.rica.pipeline.generer_dataframe_et_echantillon_train_et_test import (
    generer_datasets,
)
from energie.scripts.rica.analyse_pertinence_et_resultats_regression.analyse_predictions_et_graphes_justif_reg_lin import (
    loguer_evaluation_modele,
)


def entrainerModeleConsommationPP(
    chemin_fichier_rica: Path, chemin_fichier_rica_test: Path, dossier_output: Path,
variables:list,modele:str):
    reinitialiser_dossier(dossier_output)

    df_rica = charger_fichier_rica(chemin_fichier_rica)

    df_rica_test = charger_fichier_rica(chemin_fichier_rica_test)

    colonnes_X = variables
    colonne_y = "charges_carburants_stockes_litre"
    # colonne_w = "ponderation"

    X = df_rica.loc[:, colonnes_X]
    X_test = df_rica_test.loc[:, colonnes_X]
    y = df_rica.loc[:, colonne_y]
    y_test = df_rica_test.loc[:, colonne_y]
    # w = df_rica.loc[:, colonne_w]

    if modele == "reglin_classique":
        pl_rica=pipeline_normal(X,y,variables)
    if modele =="reglin_exploit_agregees":
        pl_rica=pipeline_normal(X,y,variables)
    if modele == "reg_lin_segmentation_otex":
        #pl_rica,X=pipelines_seg(X,y)
        pl_rica, X = pipelines_seg_sans_scale(X, y)
  

    joblib.dump(pl_rica, dossier_output / "modele_eval_conso_pp.dump")
    # degreeChoice (X,y,50)
    # degre=9
    # y_predictions,cross_val_score=polynomial_regression(degre,X_test,y)
    y_predictions = pl_rica.predict(X)
    if modele == "reglin_classique":
        y_predictions_test=pl_rica.predict(X_test)
    if modele =="reglin_exploit_agregees":
        y_predictions_test=pl_rica.predict(X_test)
    scores_cross_val = cross_val_score(
        pl_rica, X, y, cv=10, scoring="neg_root_mean_squared_error"
    )


    print(modele)
    loguer_evaluation_modele(y, y_predictions, scores_cross_val)
    if modele !="reglin_exploit_agregees":
        exporter_train_set_avec_predictions(df_rica, dossier_output, y_predictions)

    return pl_rica


def pipeline_normal(X, y,variables):
    pl_rica = Pipeline(
        [
            (
                "preparation",
                ColumnTransformer(
                    [
                        ("num", StandardScaler(), variables),
                        # ('polynomial', PolynomialFeatures(3),colonnes_X)
                        # ("cat", OneHotEncoder(sparse=False), ["id_region", "otex_16_postes"])
                    ]
                ),
            ),
            ("regression", LinearRegression()),
        ]
    )
    pl_rica.fit(X, y)
    # **{'regression__sample_weight': w}
    return pl_rica


def pipelines_seg(X, y):
    # tjrs production brute avant surface !
    X_a_completer = X[["production_brute_euros", "surface_totale_ha"]]

    l_otex = X["otex_11_postes"].to_list()
    otex_uniques = list(OrderedDict.fromkeys(l_otex))
    for otex in otex_uniques:
        nom_col = "surface_totale_" + str(otex)
        X_a_completer[nom_col] = 0
        X_a_completer["surface_totale_" + str(otex)] = np.where(
            X["otex_11_postes"] == otex, X["surface_totale_ha"], nan
        )
    X_reduit = X_a_completer.drop(["surface_totale_ha"], axis=1)
    colonnes = X_reduit.columns.to_list()
    # pipeline où il standard scale
    pl_rica = Pipeline(
        [
            (
                "preparation",
                ColumnTransformer(
                    [
                        ("num", StandardScaler(), colonnes),
                    ]
                ),
            ),
            ("regression", LinearRegression()),
        ]
    )
    pl_rica.fit(X_reduit, y)
    return pl_rica


def pipelines_seg_sans_scale(X, y):
    # tjrs production brute avant surface !
    X_a_completer = X[["production_brute_euros", "surface_totale_ha"]]

    l_otex = X["otex_11_postes"].to_list()
    otex_uniques = list(OrderedDict.fromkeys(l_otex))
    for otex in otex_uniques:
        nom_col = "surface_totale_" + str(otex)
        X_a_completer[nom_col] = 0
        X_a_completer["surface_totale_" + str(otex)] = np.where(
            X["otex_11_postes"] == otex, X["surface_totale_ha"], 0
        )
    X_reduit = X_a_completer.drop(["surface_totale_ha"], axis=1)
    modele = LinearRegression().fit(X_reduit, y)
    return (modele, X_reduit)


def exporter_train_set_avec_predictions(df_rica, dossier_output, y_predictions):
    df_rica["charges_carburants_stockes_litre_prediction"] = y_predictions
    df_rica["ecart_prediction_valeur"] = (
        df_rica["charges_carburants_stockes_litre_prediction"]
        - df_rica["charges_carburants_stockes_litre"]
    )
    df_rica["ecart_prediction_valeur_ponderee"] = (
        df_rica["ecart_prediction_valeur"] * df_rica["ponderation"]
    )
    df_rica["ecart_prediction_pourcent"] = (
        100
        * (
            df_rica["charges_carburants_stockes_litre_prediction"]
            - df_rica["charges_carburants_stockes_litre"]
        )
        / df_rica["charges_carburants_stockes_litre"]
    )
    exporter_csv(df_rica, dossier_output / "rica_train_set_avec_prediction.csv")


def degreeChoice(x, y, degree):
    liste_rmse = []
    liste_i = []
    for i in range(degree):
        i += 1
        polynomial_features = PolynomialFeatures(degree=i)
        scaler = StandardScaler()
        x_scaled = scaler.fit_transform(x)
        x_poly = polynomial_features.fit_transform(x_scaled)
        model = LinearRegression()
        model.fit(x_poly, y)
        y_poly_pred = model.predict(x_poly)
        rmse = np.sqrt(mean_squared_error(y, y_poly_pred))
        liste_rmse.append(rmse)
        liste_i.append(i)
    plt.plot(liste_i, liste_rmse, color="r")
    plt.savefig("./energie/outputs/choix_degre.png")
    plt.show()


def polynomial_regression(degree, X_test, y):
    polynomial_features = PolynomialFeatures(degree)
    scaler = StandardScaler()
    x_scaled = scaler.fit_transform(X_test)
    x_poly = polynomial_features.fit_transform(x_scaled)
    x_test_poly = polynomial_features.fit_transform(X_test)
    model = LinearRegression()
    model.fit(x_poly, y)
    y_poly_pred = model.predict(x_test_poly)
    return (y_poly_pred, polynomial_features)


if __name__ == "__main__":
    generer_datasets(CHEMIN_FICHIER_RICA_SOMME, CHEMIN_DOSSIER_OUTPUT_DATASET)
    entrainerModeleConsommationPP(
        CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
        CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
        CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
    )
