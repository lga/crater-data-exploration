# import des packages

from collections import OrderedDict
from pathlib import Path

from pandas import DataFrame

from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

from energie.outils.export_fichier import reinitialiser_dossier, exporter_csv
from energie.scripts.rica.analyse_bdd.analyse_barplots import (
    count_to_dict_freq,
    produire_barplot_dictionnaire_par_paire,
)

# import des fonctions
from energie.scripts.rica.communs.fonctions_traitement_rica import (
    RenommerColonnes,
    TranchesVersValeursMoyennes,
    charger_fichier_rica,
    charger_modalites_rica_partielles,
    remplacer_tranches_par_valeurs_moyennes)

from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_FICHIER_RICA_SOMME,
    CHEMIN_FICHIER_RICA_DONNEES,
    CHEMIN_DOSSIER_OUTPUT_DATASET,
    SELECTION_VARIABLES_INTERET,
    DICT_RENOMMAGE_COLONNES_RICA,
    CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
)

from energie.scripts.rica.transformations_bdd.filtrer_bugs_bdd_init import (
    selectionner_rica_complet_metropolitain,
)


# repartition exploitation par region et par OTEX dans l'échantillon d'entrainenement vs dans l'échantillon de test


def generer_barplot_comparaison_test_train_region(
    df_rica, train_set, test_set, dossier_output: Path
):
    colonne_region = df_rica["NREG"].to_list()
    regions_uniques = list(OrderedDict.fromkeys(colonne_region))
    dictionnaire_exploit_region_train = {}
    dictionnaire_exploit_region_test = {}
    nb_exploit_train = train_set.shape[0]
    nb_exploit_test = test_set.shape[0]
    for region in regions_uniques:
        train_region = train_set.loc[train_set["NREG"] == region]
        test_region = test_set.loc[test_set["NREG"] == region]
        nb_exploit_region_train = train_region.shape[0]
        nb_exploit_region_test = test_region.shape[0]
        dictionnaire_exploit_region_train[str(region)] = (
            nb_exploit_region_train / nb_exploit_train
        )
        dictionnaire_exploit_region_test[str(region)] = (
            nb_exploit_region_test / nb_exploit_test
        )
    nom = "comparaison_echantillonnage_carburant_repartition_par_region"
    produire_barplot_dictionnaire_par_paire(
        dictionnaire_exploit_region_train,
        dictionnaire_exploit_region_test,
        nom,
        dossier_output,
    )
    return None


def generer_barplot_comparaison_test_train_otex(
    df_rica, train_set, test_set, dossier_output: Path
):
    colonne_otex = df_rica["OTEFDA"].to_list()
    otex_uniques = list(OrderedDict.fromkeys(colonne_otex))

    dictionnaire_exploit_otex_train = {}
    dictionnaire_exploit_otex_test = {}
    nb_exploit_train = train_set.shape[0]
    nb_exploit_test = test_set.shape[0]
    for otex in otex_uniques:
        train_region = train_set.loc[train_set["OTEFDA"] == otex]
        test_region = test_set.loc[test_set["OTEFDA"] == otex]
        nb_exploit_region_train = train_region.shape[0]
        nb_exploit_region_test = test_region.shape[0]
        dictionnaire_exploit_otex_train[str(otex)] = (
            nb_exploit_region_train / nb_exploit_train
        )
        dictionnaire_exploit_otex_test[str(otex)] = (
            nb_exploit_region_test / nb_exploit_test
        )
    nom = "comparaison_echantillonnage_carburant_repartition_par_otex"
    produire_barplot_dictionnaire_par_paire(
        dictionnaire_exploit_otex_train,
        dictionnaire_exploit_otex_test,
        nom,
        dossier_output,
    )
    return (dictionnaire_exploit_otex_train, dictionnaire_exploit_otex_test)


def generer_barplot_comparaison_test_train_region_x_otex(
    df_rica, train_set, test_set, dossier_output: Path
):
    colonne_region = df_rica["NREG"].to_list()
    regions_uniques = list(OrderedDict.fromkeys(colonne_region))
    colonne_otex = df_rica["OTEFDA"].to_list()
    otex_uniques = list(OrderedDict.fromkeys(colonne_otex))
    df_rica["region"] = colonne_region
    for region in regions_uniques:
        train_region = train_set.loc[train_set["NREG"] == region]
        liste_des_otex_X_train = train_region["OTEFDA"].to_list()

        dictionnaire_region_train = count_to_dict_freq(liste_des_otex_X_train)
        test_region = test_set.loc[test_set["NREG"] == region]
        liste_des_otex_X_test = test_region["OTEFDA"].to_list()

        dictionnaire_region_test = count_to_dict_freq(liste_des_otex_X_test)
        nom = "comparaison_echantillonnage_carburant" + str(region)
        produire_barplot_dictionnaire_par_paire(
            dictionnaire_region_train, dictionnaire_region_test, nom, dossier_output
        )
    return None


def normaliser_dataset_rica(df_rica: DataFrame):
    df_rica_noramlise = df_rica.loc[:, SELECTION_VARIABLES_INTERET]
    df_modalites_rica = charger_modalites_rica_partielles()

    pl_formater_donnees_rica = Pipeline(
        [
            (
                "tranches vers valeurs",
                TranchesVersValeursMoyennes(
                    df_modalites_rica, ["SAUTI", "ZALTI", "UGBTO"]
                ),
            ),
            ("renommer colonnes", RenommerColonnes(DICT_RENOMMAGE_COLONNES_RICA)),
        ]
    )
    df_rica_noramlise = pl_formater_donnees_rica.fit_transform(df_rica_noramlise)
    return df_rica_noramlise


def normaliser_sans_renommer_dataset_rica(df_rica: DataFrame, liste_va_a_transfo):

    df_modalites_rica = charger_modalites_rica_partielles()

    pl_formater_donnees_rica = Pipeline(
        [
            (
                "tranches vers valeurs",
                TranchesVersValeursMoyennes(df_modalites_rica, liste_va_a_transfo),
            )
        ]
    )
    df_rica_noramlise = pl_formater_donnees_rica.fit_transform(df_rica)
    return df_rica_noramlise


def normaliser_dataset_rica_somme(df_rica: DataFrame):
    df_rica_noramlise = df_rica[["SAUTI", "CHRCAQG"]]
    df_rica_noramlise = df_rica_noramlise.rename(
        columns={
            "SAUTI": "surface_totale_ha",
            "CHRCAQG": "charges_carburants_stockes_litre",
        }
    )
    return df_rica_noramlise


def generer_datasets(chemin_fichier_rica: Path, dossier_output: Path,modele="classique"):
    if modele=="classique":
        reinitialiser_dossier(dossier_output)
    df_rica_complet = charger_fichier_rica(chemin_fichier_rica)
    # suppression des regions non indentifiees et codees en 95 et 98
    df_rica_metropole = df_rica_complet.loc[
        ~(df_rica_complet["NREG"].isin([95, 98])), :
    ]

    rica_train_set, rica_validation_set = generer_test_et_validation_sets(
        df_rica_metropole, dossier_output
    )
    if modele=="reglin_exploit_agregees":
        df_rica_metropole_normalise = normaliser_dataset_rica_somme(df_rica_metropole)
        exporter_csv(
        df_rica_metropole_normalise, dossier_output / "rica_metropole_normalise_ag_exploit.csv"
    )
        df_rica_train_set_normalise = normaliser_dataset_rica_somme(rica_train_set)
        exporter_csv(
        df_rica_train_set_normalise, dossier_output / "rica_train_set_normalise_ag_exploit.csv"
    )
        df_rica_validation_set_normalise = normaliser_dataset_rica_somme(rica_validation_set)
        exporter_csv(
        df_rica_validation_set_normalise,
        dossier_output / "rica_validation_set_normalise_ag_exploit.csv",
    )
    if modele=="classique":
        df_rica_metropole_normalise = normaliser_dataset_rica(df_rica_metropole)
        exporter_csv(df_rica_metropole_normalise, dossier_output / "rica_metropole_normalise.csv")
        df_rica_train_set_normalise = normaliser_dataset_rica(rica_train_set)
        exporter_csv(df_rica_train_set_normalise, dossier_output / "rica_train_set_normalise.csv")
        df_rica_validation_set_normalise = normaliser_dataset_rica(rica_validation_set)
        exporter_csv(df_rica_validation_set_normalise,dossier_output / "rica_validation_set_normalise.csv")


def generer_test_et_validation_sets(df_rica, dossier_output):
    ##explicitation variables categorielles et valeur moyenne appliquée aux variables catégorielles transformées en numérique
    # chargement des variables explicatives et variables à expliquer
    # on prépare des ensembles d’apprentissage et de tests
    train_set, validation_set = train_test_split(
        df_rica, test_size=0.2, random_state=10
    )
    train_set.to_csv(dossier_output / "train_set.csv", sep=";", index=False)
    validation_set.to_csv(dossier_output / "validation_set.csv", sep=";", index=False)
    # vérification de la répartition
    # generer_barplot_comparaison_test_train_region(df_rica,train_set,test_set)
    # tout le traitement : filtrage colonnes en premier lieu , binarisation etc entrainement modele
    # dico_train, dico_test = generer_barplot_comparaison_test_train_otex(df_rica_sb, train_set, validation_set,
    #                                                                     dossier_output)
    # generer_barplot_comparaison_test_train_region_x_otex(df_rica,train_set, test_set)
    return train_set, validation_set


if __name__ == "__main__":
    generer_datasets(CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_DOSSIER_OUTPUT_DATASET)
