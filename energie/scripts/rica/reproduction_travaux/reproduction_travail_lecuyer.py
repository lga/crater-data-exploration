import pandas as panda
from energie.outils.genere_liste_code_territoire import generer_liste_code
from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    valeur_tranche,
)
from pathlib import Path
from energie.scripts.rica.communs.configs_energie_rica_agreste import CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_FICHIER_RICA_DICTIONNAIRE


def generer_df_calculs_ENG_et_PP_PAR_SAU_PAR_CI_PAR_PB(df_rica, modalites):
    df_SAU = df_rica[
        ["NREG", "CDEXE", "SAUTI", "CHREN", "CHRCA", "CHRCO", "CINTR", "PBRTO"]
    ].copy()
    mod_SAU = modalites[modalites["VARIABLE"] == "SAUTI"]
    mod_select = mod_SAU[["VARIABLE", "MOD", "moyenne"]]
    df_mergee_SAU = panda.merge(
        left=df_SAU, right=mod_select, how="left", left_on="SAUTI", right_on="MOD"
    )
    df_mergee_SAU["ENG_PAR_SAU"] = df_mergee_SAU["CHREN"] / df_mergee_SAU["moyenne"]
    df_mergee_SAU["ENG_PAR_CI"] = df_mergee_SAU["CHREN"] / df_mergee_SAU["CINTR"]
    df_mergee_SAU["ENG_PAR_PB"] = df_mergee_SAU["CHREN"] / df_mergee_SAU["PBRTO"]
    df_mergee_SAU["PP_PAR_SAU"] = (
        df_mergee_SAU["CHRCA"] + df_mergee_SAU["CHRCO"]
    ) / df_mergee_SAU["moyenne"]
    df_mergee_SAU["PP_PAR_CI"] = (
        df_mergee_SAU["CHRCA"] + df_mergee_SAU["CHRCO"]
    ) / df_mergee_SAU["CINTR"]
    df_mergee_SAU["PP_PAR_PB"] = (
        df_mergee_SAU["CHRCA"] + df_mergee_SAU["CHRCO"]
    ) / df_mergee_SAU["PBRTO"]
    return df_mergee_SAU


def regrouper_charges_par_region(df):
    df_par_region = df[
        [
            "SAUTI",
            "CHREN",
            "CHRCA",
            "CHRCO",
            "CINTR",
            "PBRTO",
            "ENG_PAR_SAU",
            "ENG_PAR_CI",
            "ENG_PAR_PB",
            "PP_PAR_SAU",
            "PP_PAR_CI",
            "PP_PAR_PB",
        ]
    ].copy()
    df_par_region_groupe = df_par_region.groupby(by=["NREG"]).mean()
    return df_par_region_groupe


def regrouper_charges_par_otex(df):
    df_par_otex = df[
        [
            "SAUTI",
            "CHREN",
            "CHRCA",
            "CHRCO",
            "CINTR",
            "PBRTO",
            "ENG_PAR_SAU",
            "ENG_PAR_CI",
            "ENG_PAR_PB",
            "PP_PAR_SAU",
            "PP_PAR_CI",
            "PP_PAR_PB",
        ]
    ].copy()
    df_par_otex_groupe = df.groupby(by=["OTEFDA"]).mean()
    return df_par_otex_groupe


def reproduction_travail_lecuyer():
    dictionnaire_vrai = panda.read_excel(
        CHEMIN_FICHIER_RICA_DICTIONNAIRE, sheet_name="DICTIONNAIRE FMD 2020"
    )
    modalites = panda.read_excel(
        CHEMIN_FICHIER_RICA_DICTIONNAIRE, sheet_name="MODALITE FMD 2020"
    )
    valeur_tranche(modalites)
    liste_codes_region = generer_liste_code()[0]
    df_rica_complet = panda.read_csv(CHEMIN_FICHIER_RICA_DONNEES, sep=";")
    df = generer_df_calculs_ENG_et_PP_PAR_SAU_PAR_CI_PAR_PB(df_rica_complet, modalites)
    df_region = regrouper_charges_par_region(df)
    df_region.to_csv("./energie/outputs/lecuyer/lecuyer_region.csv", sep=";")
    df_otex = regrouper_charges_par_otex(df)
    df_otex.to_csv("./energie/outputs/lecuyer/lecuyer_otex.csv", sep=";")
    return None
