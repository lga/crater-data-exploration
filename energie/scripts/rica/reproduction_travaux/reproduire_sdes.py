from collections import OrderedDict
from pathlib import Path
import pandas as panda
import numpy as np
from energie.scripts.rica.communs.configs_energie_rica_agreste import CHEMIN_FICHIER_RICA_DONNEES
from energie.scripts.rica.pipeline.appliquer_modele_dataframe_communes import (
    CHEMIN_REF_NUM_REGION,
)

from energie.scripts.rica.transformations_bdd.filtrer_bugs_bdd_init import (
    selectionner_rica_complet_metropolitain,
)


def moyenne_consommation_PP_par_region_par_exploit(df_rica_complet):
    df_nb_exploit_region = panda.read_csv(
        "./energie/inputs/RA_2020_nb_exploit_par_region.csv", sep=";"
    )
    df_moyenne_PP = df_rica_complet[
        ["NREG", "CHRCOQF", "CHRCAQG", "CHRCOQG", "EXTR2"]
    ].copy()
    df_moyenne_PP["SOMME_PP"] = (
        df_moyenne_PP["CHRCOQF"] + df_moyenne_PP["CHRCAQG"]
    ) * df_moyenne_PP["EXTR2"]

    df_moyenne_PP["Somme_Gaz"] = df_moyenne_PP["CHRCOQG"] * df_moyenne_PP["EXTR2"]
    df_moyenne_PP["SOMME_gaz_GWh"] = (
        df_moyenne_PP["Somme_Gaz"] * 0.001 * (46 / 42) * 11628 * 0.000001
    )
    # k en t en tep en kwh en Gwh
    df_moyenne_PP["SOMME_PP_ss_gaz_GWh"] = (
        df_moyenne_PP["SOMME_PP"] * 0.001 * 840 * 0.001 * 1 * 11628 * 0.000001
    )
    # L en m3 en kg en t en tep en kwh en gwh
    df_moyenne_PP["SOMME_PP_GWh"] = (
        df_moyenne_PP["SOMME_gaz_GWh"] + df_moyenne_PP["SOMME_PP_ss_gaz_GWh"]
    )
    df_moyenne_PP_region = df_moyenne_PP.groupby(by=["NREG"]).sum().reset_index()

    # df_moyenne_PP_region["SOMME_PP_GWH_par_exploit"]=np.nan
    # df_moyenne_PP_region["SOMME_PP_GWH_tot"]=np.nan
    # for i in range(len(df_moyenne_PP_region)):
    #     code=df_moyenne_PP_region["NREG"][i]
    #     lignes_region=df_moyenne_PP[(df_moyenne_PP["NREG"]==int(code))]
    #     nb_exploit=extraire_nombre_exploitation_par_region_agreste(df_nb_exploit_region,code)
    #     df_moyenne_PP_region["SOMME_PP_GWH_par_exploit"][i]=df_moyenne_PP_region["SOMME_PP_GWh"][i]/nb_exploit
    df_moyenne_PP_region.to_csv(
        "./energie/outputs/verifications_chiffres_pp_sdes.csv", sep=";"
    )
    return df_moyenne_PP_region


def generer_SDES_carburants_par_region(df_rica_complet):
    df_moyenne_PP = df_rica_complet[
        ["NREG", "CHRCOQF", "CHRCAQG", "CHRCOQG", "EXTR2"]
    ].copy()
    colonne_region = df_rica_complet["NREG"].to_list()
    regions_uniques = list(OrderedDict.fromkeys(colonne_region))
    liste_valeurs = []
    for region in regions_uniques:
        df_region = df_moyenne_PP.loc[df_moyenne_PP["NREG"] == region]
        df_region["CARBURANT_EXTR"] = df_moyenne_PP["CHRCAQG"] * df_moyenne_PP["EXTR2"]
        somme_region = df_region["CARBURANT_EXTR"].sum()
        somme_region_en_GWH = somme_region * 0.001 * 840 * 0.001 * 1 * 11628 * 0.000001
        liste_valeurs.append([region, somme_region_en_GWH])
    df_sdes_carbu = panda.DataFrame(
        data=liste_valeurs, columns=["Région_num", "SDES carburants en GWH"]
    )
    df_ref_num_region = panda.read_csv(CHEMIN_REF_NUM_REGION, sep=",")
    df_sdes_carbu = df_sdes_carbu.astype({"Région_num": "int64"})
    df_mergee_sommes_region = panda.merge(
        left=df_sdes_carbu,
        right=df_ref_num_region,
        how="left",
        left_on="Région_num",
        right_on="REG",
    )
    df_mergee_sommes_region = df_mergee_sommes_region.rename({"NCC": "Région"}, axis=1)
    df_mergee_sommes_region = df_mergee_sommes_region[
        ["Région", "SDES carburants en GWH"]
    ]
    df_mergee_sommes_region.to_csv(
        "./energie/inputs/donnees_SDES_carburants.csv", sep=";"
    )


def extraire_nombre_exploitation_par_region_agreste(fichier_exploit_regio, code):
    fichier_exploit_regio = panda.read_csv(
        "./energie/inputs/RA_communes_SAU_PBS_NREG_OTEX.csv", sep=";"
    )
    fichier_exploit_regio["code_region"] = (
        fichier_exploit_regio["Région 2020"].str.split(" ").str[0]
    )
    fichier_exploit_regio = fichier_exploit_regio[
        ~fichier_exploit_regio["code_region"].isin(["01", "02", "03", "04", "06"])
    ]
    lignes_nb_exploit = fichier_exploit_regio[
        fichier_exploit_regio["code_region"] == str(code)
    ]

    nb_exploit = lignes_nb_exploit["Nombre d'exploitations en 2020"].values[0]
    return nb_exploit


if __name__ == "__main__":

    df_rica_complet = panda.read_csv(CHEMIN_FICHIER_RICA_DONNEES, sep=";")
    df_rica = selectionner_rica_complet_metropolitain(df_rica_complet)
    # moyenne_consommation_PP_par_region_par_exploit(df_rica)
    generer_SDES_carburants_par_region(df_rica)
