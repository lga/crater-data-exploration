import argparse
from pathlib import Path

from energie.outils.export_fichier import reinitialiser_dossier
from energie.outils.logger import log
from energie.scripts.rica.generateur_communes_artificielles.agreger_exploitations_en_communes import generer_donnnes_exploit_agregees
from energie.scripts.rica.pipeline.appliquer_modele_dataframe_communes import (
    evaluer_conso_pp_communes,
    CHEMIN_MODELE_GENERE,
)


from energie.scripts.rica.pipeline.calculer_statistiques import calculer_statistiques
from energie.scripts.rica.communs.configs_energie_rica_agreste import (
    CHEMIN_FICHIER_RICA_DONNEES,
    CHEMIN_DOSSIER_OUTPUT_STATISTIQUES,
    CHEMIN_DOSSIER_OUTPUT_DATASET,
    CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
    CHEMIN_DOSSIER_OUTPUT,
    CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
    CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET_AG_EXPLOIT,
    CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
    CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET_AG_EXPLOIT,
    CHEMIN_FICHIER_RICA_SOMME,
)
from energie.scripts.rica.pipeline.entrainer_modele_consommation_pp import (
    entrainerModeleConsommationPP,
)
from energie.scripts.rica.pipeline.generer_dataframe_et_echantillon_train_et_test import (
    generer_datasets,
)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        usage="%(prog)s NOM_DU_TRAITEMENT\n"
        + "Lancer un traitement sur les données RICA\n"
        + "Valeurs possible pour NOM_DU_TRAITEMENT :\n"
        + "\tcalcul-stats\tcalculer les statistiques sur la base RICA\n"
        + "\tgeneration-donnees-RegLin-agreg-exploit\tgénérer les données RICA pour le modèle RegLin_Ag_Exploit_SA (agrégation d'exploitations)"
        + "\textraction-dataset-RegLin-classique\textraire les datasets train et validation pour les modèles sans agrégation d'exploitations"
        + "\textraction-dataset-RegLin-agreg-exploit\textraire les datasets train et validation pour le modèle avec agrégation d'exploitations"
        + "\tevaluation-pp-RegLinSAU\tgénerer le modèle RegLinSAU (régression linéaire classique, variable = surface agricole utile) et l'évaluer sur les communes de l'Agreste (validation échelle régionale)"
        + "\tevaluation-pp-RegLinSPU\tgénerer le modèle RegLinSPU (régression linéaire classique, variables = surface agricole utile, production standard brute, et nb d'ugb total) et l'évaluer sur les communes de l'Agreste (validation échelle régionale)"
        +  "\tevaluation-pp-RegLinSP\tgénerer le modèle RegLinSPU (régression linéaire classique, variables = surface agricole utile, production standard brute) et l'évaluer sur les communes de l'Agreste (validation échelle régionale)"
        + "\tevaluation-pp-RegLin_Seg_SAU_OTEX\tgénérer le modèle RegLin_Seg_SAU_OTEX (régression linéaire classique, répartition des surfaces en fonction de l'OTEX de l'exploitation, variables = surface agricole utile,production standard brute, et l'évaluer sur les communes de l'Agreste (validation échelle régionale)"
        +  "\tevaluation-pp-RegLin_Ag_Exploit_SA\tgénérer le modèle RegLin_Ag_Exploit_SA (régression linéaire classique, instances = communes artificielles (agrégations d'exploitations) variable = surface agricole utile, et l'évaluer sur les communes de l'Agreste (validation échelle régionale)"
        + "\tcomplet\texécution de tous les traitements dans l'ordre pour la régression linéaire classique avec surface et pbs\n"
    )
    parser.add_argument(
        "NOM_DU_TRAITEMENT",
        help="Nom du traitement",
        choices=[
            "calcul-stats",
            "generation-donnees-RegLin-agreg-exploit",
            "extraction-dataset-RegLin-classique",
            "extraction-dataset-RegLin-agreg-exploit",
            "evaluation-pp-RegLinSAU",
            "evaluation-pp-RegLinSPU",
            "evaluation-pp-RegLinSP",
            "evaluation-pp-RegLin_Seg_SAU_OTEX",
            "evaluation-pp-RegLin_Ag_Exploit_SA",
            "complet",
        ],
    )

    args = parser.parse_args()

    if args.NOM_DU_TRAITEMENT == "calcul-stats":
        log.info("- Calcul de statistiques sur les résultats")
        calculer_statistiques(CHEMIN_DOSSIER_OUTPUT_STATISTIQUES)

    if args.NOM_DU_TRAITEMENT == "extraction-dataset-RegLin-classique":
        log.info(
            f"- Génération du dataset (train set et test set) dans {CHEMIN_DOSSIER_OUTPUT_DATASET}"
        )
        generer_datasets(CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_DOSSIER_OUTPUT_DATASET,modele="classique")
    
    if args.NOM_DU_TRAITEMENT == "generation-donnees-RegLin-agreg-exploit":
        log.info(
            f"- Génération des données dans {CHEMIN_DOSSIER_OUTPUT_DATASET}"
        )
        generer_donnnes_exploit_agregees(CHEMIN_DOSSIER_OUTPUT_DATASET)

    if args.NOM_DU_TRAITEMENT == "extraction-dataset-RegLin-agreg-exploit":

        log.info(
            f"- Génération du dataset (train set et test set) dans {CHEMIN_DOSSIER_OUTPUT_DATASET}"
        )
        generer_datasets(CHEMIN_FICHIER_RICA_SOMME, CHEMIN_DOSSIER_OUTPUT_DATASET,modele="reglin_exploit_agregees")


    if args.NOM_DU_TRAITEMENT == "evaluation-pp-RegLinSAU":
        log.info("- Evaluation de la consommation de produits pétroliers par commune - RegLinSAU")

        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
            variables=["surface_totale_ha"],
            modele="reglin_classique"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reglin_classique",variables=["surface_totale_ha"])


    if args.NOM_DU_TRAITEMENT == "evaluation-pp-RegLinSPU":
        log.info("- Evaluation de la consommation de produits pétroliers par commune - RegLinSPU")

        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
            ["surface_totale_ha","production_brute_euros","ugb_total_nb"],
            "reglin_classique"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reglin_classique",variables=["surface_totale_ha","production_brute_euros","ugb_total_nb"])


    if args.NOM_DU_TRAITEMENT == "evaluation-pp-RegLinSP":
        log.info("- Evaluation de la consommation de produits pétroliers par commune - RegLinSP")

        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
            ["surface_totale_ha","production_brute_euros"],
            "reglin_classique"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reglin_classique",variables=["surface_totale_ha","production_brute_euros"])


    if args.NOM_DU_TRAITEMENT == "evaluation-pp-RegLin_Seg_SAU_OTEX":
        log.info("- Evaluation de la consommation de produits pétroliers par commune -  RegLin_Seg_SAU_OTEX")
    
        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
            ["otex_11_postes","production_brute_euros","surface_totale_ha"],
            "reg_lin_segmentation_otex"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reg_lin_segmentation_otex",variables=["otex_11_postes","production_brute_euros","surface_totale_ha"])


    if args.NOM_DU_TRAITEMENT == "evaluation-pp-RegLin_Ag_Exploit_SA":

        log.info("- Evaluation de la consommation de produits pétroliers par commune - RegLin_Ag_Exploit_SA")

        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET_AG_EXPLOIT,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET_AG_EXPLOIT,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,["surface_totale_ha"],modele="reglin_exploit_agregees"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reglin_exploit_agregees",variables=["surface_totale_ha"])


    if args.NOM_DU_TRAITEMENT == "complet":
        log.info("Exécution de tous les traitements")
        reinitialiser_dossier(CHEMIN_DOSSIER_OUTPUT)
        log.info(
            f"- Génération du dataset (train set et test set) dans {CHEMIN_DOSSIER_OUTPUT_STATISTIQUES}"
        )
        generer_datasets(CHEMIN_FICHIER_RICA_DONNEES, CHEMIN_DOSSIER_OUTPUT_DATASET,modele="classique")
        log.info("- Evaluation de la consommation de produits pétroliers par commune")
        entrainerModeleConsommationPP(
            CHEMIN_FICHIER_RICA_NORMALISE_TRAIN_SET,
            CHEMIN_FICHIER_RICA_NORMALISE_VALIDATION_SET,
            CHEMIN_DOSSIER_OUTPUT_EVALUATION_PP,
            variables=["surface_totale_ha","production_brute_euros"],
            modele="reglin_classique"
        )
        evaluer_conso_pp_communes(CHEMIN_MODELE_GENERE,modele_type="reglin_classique",variables=["surface_totale_ha","production_brute_euros"])
        log.info("- Calcul de statistiques")
        calculer_statistiques(CHEMIN_DOSSIER_OUTPUT_STATISTIQUES)
    log.info("Fin du traitement")
