from collections import OrderedDict
import pandas as panda


def construire_df_regions_disc_num_only_sans_f_avec_prod(df_rica_complet):
    df = panda.read_csv(
        "./energie/outputs/df_modalites_discretisees_num_only_restreinte_sans_d_fi_avec_prod.csv",
        sep=";",
    )
    colonne_region = df_rica_complet["NREG"].to_list()
    regions_uniques = list(OrderedDict.fromkeys(colonne_region))
    df["region"] = colonne_region
    for region in regions_uniques:
        df_region = df[df["region"] == region]
        df_region.to_csv(
            "./energie/outputs/df_regions/"
            + str(region)
            + "df_discr_num_only_sans_f_avec_production.csv",
            sep=";",
        )
    return None
