from energie.scripts.rica.transformations_bdd.transformations_categorie_en_moyenne_categorie import (
    transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne,
)


def donner_liste_colonnes_a_exclure_mano(liste_colonnes):
    liste_noms_colonnes_a_exclure = [
        "EXTR2",
        "ANDJA",
        "RESEX",
        "RESFI",
        "RESCO",
        "RESCP",
        "AUTOF",
        "TF001",
        "BFROU",
        "BFROV",
        "TRESO",
        "TRESV",
        "FROUN_NR",
        "XTRES",
        "TAUDET_NR",
        "ANLMT",
        "TAIM1",
        "TF011",
        "TAIM5",
        "TCIR5",
        "TACT2",
        "TACF1",
        "TACF5",
        "SUBI2",
        "TCAP3",
        "TDTE3",
        "TPSF1",
        "TPSF2",
        "TPSF3",
        "TAIM4",
        "TACT4",
        "CHEXG",
        "VCMPC",
        "ACHEX",
        "PIMMO",
        "PAGR2",
        "INDAS",
        "SUBEX",
        "PRFIN",
        "PAGR3",
        "QUOTE",
        "PCEAC",
        "FRET5",
        "AIMI5",
        "TERR5",
        "BOIS5",
        "AMET5",
        "AMEB5",
        "FONC5",
        "CONS5",
        "ISPE5",
        "MATE5",
        "AUIM5",
        "AMEF5",
        "PLAN5",
        "PLFO5",
        "ANIR5",
        "PART5",
        "POPA5",
        "AIMF5",
        "REALI",
        "STOCK",
        "DISPO",
        "REGA5",
        "CINI3",
        "APPL3",
        "VCII3",
        "SINE3",
        "SUBI3",
        "PCAV3",
        "PERM2",
        "PERM3",
        "COURT",
    ]
    selection_colonnes_pbrut_valeur = [x for x in liste_colonnes if x.startswith("PBV")]
    selection_colonnes_pbrut_quantites = [
        x for x in liste_colonnes if x.startswith("PBQ")
    ]
    selection_colonnes_production_quantites = [
        x for x in liste_colonnes if x.startswith("PRQ")
    ]
    selection_colonnes_production_surf_irr = [
        x for x in liste_colonnes if x.startswith("SUI")
    ]
    selection_colonnes_production_surf_tot = [
        x for x in liste_colonnes if x.startswith("SUT")
    ]
    selection_colonnes_production_surf_tot = [
        x for x in liste_colonnes if x.startswith("SU")
    ]
    selection_colonnes_vente_auto_conso_quantites = [
        x for x in liste_colonnes if x.startswith("VAQ")
    ]
    selection_colonnes_vente_auto_conso_valeurs = [
        x for x in liste_colonnes if x.startswith("VAV")
    ]
    liste_totale_colonnes_a_exclure = (
        liste_noms_colonnes_a_exclure
        + selection_colonnes_pbrut_valeur
        + selection_colonnes_pbrut_quantites
        + selection_colonnes_production_quantites
        + selection_colonnes_production_surf_irr
        + selection_colonnes_production_surf_tot
        + selection_colonnes_vente_auto_conso_quantites
        + selection_colonnes_vente_auto_conso_valeurs
    )
    return liste_totale_colonnes_a_exclure


def donner_liste_colonnes_a_exclure_gardant_pbrut_par_prod(liste_colonnes):
    liste_noms_colonnes_a_exclure = [
        "EXTR2",
        "ANDJA",
        "RESEX",
        "RESFI",
        "RESCO",
        "RESCP",
        "AUTOF",
        "TF001",
        "BFROU",
        "BFROV",
        "TRESO",
        "TRESV",
        "FROUN_NR",
        "XTRES",
        "TAUDET_NR",
        "ANLMT",
        "TAIM1",
        "TF011",
        "TAIM5",
        "TCIR5",
        "TACT2",
        "TACF1",
        "TACF5",
        "SUBI2",
        "TCAP3",
        "TDTE3",
        "TPSF1",
        "TPSF2",
        "TPSF3",
        "TAIM4",
        "TACT4",
        "CHEXG",
        "VCMPC",
        "ACHEX",
        "PIMMO",
        "PAGR2",
        "INDAS",
        "SUBEX",
        "PRFIN",
        "PAGR3",
        "QUOTE",
        "PCEAC",
        "FRET5",
        "AIMI5",
        "TERR5",
        "BOIS5",
        "AMET5",
        "AMEB5",
        "FONC5",
        "CONS5",
        "ISPE5",
        "MATE5",
        "AUIM5",
        "AMEF5",
        "PLAN5",
        "PLFO5",
        "ANIR5",
        "PART5",
        "POPA5",
        "AIMF5",
        "REALI",
        "STOCK",
        "DISPO",
        "REGA5",
        "CINI3",
        "APPL3",
        "VCII3",
        "SINE3",
        "SUBI3",
        "PCAV3",
        "PERM2",
        "PERM3",
        "COURT",
    ]
    selection_colonnes_pbrut_quantites = [
        x for x in liste_colonnes if x.startswith("PBQ")
    ]
    selection_colonnes_production_quantites = [
        x for x in liste_colonnes if x.startswith("PRQ")
    ]
    selection_colonnes_production_surf_irr = [
        x for x in liste_colonnes if x.startswith("SUI")
    ]
    selection_colonnes_production_surf_tot = [
        x for x in liste_colonnes if x.startswith("SUT")
    ]
    selection_colonnes_vente_auto_conso_quantites = [
        x for x in liste_colonnes if x.startswith("VAQ")
    ]
    selection_colonnes_vente_auto_conso_valeurs = [
        x for x in liste_colonnes if x.startswith("VAV")
    ]
    liste_totale_colonnes_a_exclure = (
        liste_noms_colonnes_a_exclure
        + selection_colonnes_pbrut_quantites
        + selection_colonnes_production_quantites
        + selection_colonnes_production_surf_irr
        + selection_colonnes_production_surf_tot
        + selection_colonnes_vente_auto_conso_quantites
        + selection_colonnes_vente_auto_conso_valeurs
    )
    return liste_totale_colonnes_a_exclure


def donner_liste_colonnes_a_exclure_gardant_pbrut_par_prod_sans_f_sans_achat(
    liste_colonnes,
):
    liste = donner_liste_colonnes_a_exclure_gardant_pbrut_par_prod(liste_colonnes)
    selection_colonnes_achats = [x for x in liste_colonnes if x.startswith("ACH")]
    liste_nouvelles_colonnes_a_exclure = liste + selection_colonnes_achats
    return liste_nouvelles_colonnes_a_exclure


def donner_liste_colonnes_numeriques_et_categorielles_discretisees(
    df_rica_complet, dictionnaire
):
    liste_variables_categorisables = (
        transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne(
            df_rica_complet, dictionnaire
        )
    )
    colonnes = df_rica_complet.columns.to_list()
    dictionnaire_selectionnee = dictionnaire[dictionnaire["Type"] == "num"]
    liste_variables_numeriques = dictionnaire_selectionnee["Variable"].to_list()
    liste_variables_numeriques.remove("IDNUM")  # num mais pas signifiant
    liste_variables_numeriques.remove("MILEX")  # num mais pas signifiant

    colonnes_num = [x for x in colonnes if x in liste_variables_numeriques]

    colonnes_num_et_discretisees = colonnes_num + liste_variables_categorisables
    colonnes_num_et_discretisees.remove("QUOLA")
    colonnes_num_et_discretisees.remove("SBVJACS")
    return colonnes_num_et_discretisees


def transformer_bdd_avec_exclusion_colonnes(df, liste_colonnes):
    df = df.drop(liste_colonnes, axis=1)
    return df


def enlever_colonnes_plus_de_50_pourcents_de_zeros(dataframe):
    liste_colonnes = dataframe.columns.to_list()
    liste_colonnes_finales = liste_colonnes.copy()
    nb_lignes = len(dataframe.index)
    for colonne in liste_colonnes:
        values = dataframe[colonne].to_list()
        nb_zeros = values.count(0)
        if nb_zeros / nb_lignes > 0.5:
            liste_colonnes_finales.remove(str(colonne))
    data_frame_sans_variables_trop_zeros = dataframe[liste_colonnes_finales]
    print(liste_colonnes_finales)
    return data_frame_sans_variables_trop_zeros


def ajouter_une_colonne_au_df(df_rica_complet, df, colonne):
    df[colonne] = df_rica_complet[colonne]
    return df


def generer_df_avec_liste_colonnes(df, liste_colonnes, nom_csv):
    df_final = df[liste_colonnes]
    df_final.to_csv("./energie/outputs/df_" + nom_csv + ".csv", sep=";")
    return df_final
