def selectionner_rica_complet_metropolitain(df_rica_complet):
    df_rica = df_rica_complet[
        ~(df_rica_complet["NREG"] == 95) & ~(df_rica_complet["NREG"] == 98)
    ]
    return df_rica
