from pathlib import Path
import pandas as panda

CHEMIN_DONNEES_RICA_DATA = Path(
    "../crater-data-sources/agreste/RicaMicrodonnées2020/Rica_France_micro_Donnees_ex2020.csv"
)


def selectionner_premiere_production_et_calculer_importance_pp_produit_brut(
    df_rica_complet, colonnes_rica
):
    selection_plage_des_colonnes = colonnes_rica[280:985]
    selection_colonnes_production_valeur = [
        x for x in selection_plage_des_colonnes if x.startswith("PBV")
    ]
    # idxmax
    selection_colonnes_production_valeur = [
        x for x in selection_plage_des_colonnes if x.startswith("PBV")
    ]
    liste_premieres_productions = (
        df_rica_complet.loc[:, selection_colonnes_production_valeur]
        .idxmax(axis=1)
        .to_list()
    )

    liste_valeurs_des_premieres_productions = []
    liste_valeurs_premiere_production_sur_production_total = []
    i = 0
    for index, row in df_rica_complet.iterrows():
        premiere_production = liste_premieres_productions[i]
        liste_valeurs_des_premieres_productions.append(row[premiere_production])
        liste_valeurs_premiere_production_sur_production_total.append(
            row[premiere_production] / row["PBRTO"]
        )
        i += 1
    return (
        liste_premieres_productions,
        liste_valeurs_premiere_production_sur_production_total,
    )


def ajouter_colonnes_premiere_prod_et_part_premiere_prod(df_rica):

    colonnes_rica = df_rica.columns.to_list()
    (
        liste_premieres_productions,
        liste_valeurs_premiere_production_sur_production_total,
    ) = selectionner_premiere_production_et_calculer_importance_pp_produit_brut(
        df_rica, colonnes_rica
    )
    df_plus_premiere_prod = df_rica.assign(premiere_prod=liste_premieres_productions)
    df_final = df_plus_premiere_prod.assign(
        part_premiere_prod=liste_valeurs_premiere_production_sur_production_total
    )
    return df_final
