import numpy as np
import pandas as panda
from collections import OrderedDict
from pathlib import Path

CHEMIN_DONNEES_RICA_DICO = Path(
    "../crater-data-sources/agreste/RicaMicrodonnées2020/RICA2020_dictionnaire_modalite_FMD.xlsx"
)
CHEMIN_DONNEES_RICA_DATA = Path(
    "../crater-data-sources/agreste/RicaMicrodonnées2020/Rica_France_micro_Donnees_ex2020.csv"
)


def verifier_si_valeur_numerique(chaine):
    nums = "1234567890"
    isnum = [x in nums for x in chaine]
    return not (False in isnum)


def rechercher_valeurs_numeriques(s):
    liste = s.split()
    chiffres = []
    for x in liste:
        if verifier_si_valeur_numerique(x):
            chiffres.append(float(x))
    if len(chiffres) > 2:
        print("Plus de 2 valeurs numériques trouvées dans : " + s)
    return chiffres


def valeur_tranche(modalites):
    modalites["moyenne"] = np.nan

    for i in range(len(modalites)):
        variable = modalites.VARIABLE[i]
        n = len(modalites[modalites["VARIABLE"] == variable])
        chiffres = rechercher_valeurs_numeriques(modalites.LIBMOD[i])

        if n > 2:
            if modalites.MOD[i] == 0:
                if len(chiffres) == 0:
                    if modalites.LIBMOD[i].find("nul") != -1:

                        modalites.loc[i, "moyenne"] = 0
                    elif modalites.LIBMOD[i].find("Pas de") != -1:

                        if modalites.VARIABLE[i] != "VDETAIL":
                            modalites.loc[i, "moyenne"] = 0
                    elif modalites.LIBMOD[i].find("Sans objet") != -1:

                        modalites.loc[i, "moyenne"] = 0
            if modalites.MOD[i] == 1:

                if len(chiffres) == 1:
                    modalites.loc[i, "moyenne"] = chiffres[0] / 2
                else:
                    print("")
            elif modalites.MOD[i] > 1:
                if len(chiffres) == 2:
                    modalites.loc[i, "moyenne"] = (chiffres[0] + chiffres[1]) / 2
                elif len(chiffres) == 1 and "supérieur" in modalites.LIBMOD[i]:
                    modalites.loc[i, "moyenne"] = chiffres[0]
                elif modalites.LIBMOD[i] == "Plus de 80 ans":
                    modalites.loc[i, "moyenne"] = 80
                elif modalites.LIBMOD[i] == "Plus de 80 ans":
                    modalites.LIBMOD[
                        i
                    ] == "Majeure partie de l'exploitation au-dessus de 600 mètres"
                    modalites.loc[i, "moyenne"] = 1000

    modalites.to_csv("./energie/outputs/df_modalites_moyennes.csv", sep=";")
    return modalites


def remplacer_tranche_par_valeur(code_colonne, dictionnaire, df_rica, modalites):
    df = df_rica[["ACHEN", code_colonne]].copy()
    if (
        "tranche"
        in dictionnaire[dictionnaire["Variable"] == code_colonne]["Libellé"].iloc[0]
    ):
        for i in range(len(df)):
            valeur_tranche = df[code_colonne][i]
            start = modalites[
                (modalites.VARIABLE == code_colonne) & (modalites.MOD == valeur_tranche)
            ].loc[0]
            df.loc[i, code_colonne] = start
    return df


def creer_dictionnaire_modalites_explicitement_categorielle_pour_R(modalites_rica):
    modalites_rica["MOD"] = modalites_rica["MOD"].astype(str)
    modalites_rica["modalite_cat"] = modalites_rica["VARIABLE"] + modalites_rica["MOD"]
    return modalites_rica


def transformer_categories_en_categories_explicites_pour_r_et_ajout_moyenne(
    df_rica_complet,
):
    dictionnaire = panda.read_excel(
        CHEMIN_DONNEES_RICA_DICO, sheet_name="DICTIONNAIRE FMD 2020"
    )
    modalites_rica = panda.read_excel(
        CHEMIN_DONNEES_RICA_DICO, sheet_name="MODALITE FMD 2020"
    )
    modalites_en_discretisees = valeur_tranche(modalites_rica)
    modalites_discr_et_categories_explites = (
        creer_dictionnaire_modalites_explicitement_categorielle_pour_R(
            modalites_en_discretisees
        )
    )
    modalites_discr_et_categories_explites.to_csv(
        "./energie/outputs/modalites_disc_et_cat_explicites.csv", sep=";"
    )
    lignes_modalites_discretisables = modalites_discr_et_categories_explites.dropna()
    liste_variables_categorielles_discretisables = list(
        OrderedDict.fromkeys(lignes_modalites_discretisables["VARIABLE"].to_list())
    )
    lignes_var_categorielles = dictionnaire[dictionnaire["Type"] == "char"]
    liste_variables_categorielles = list(
        OrderedDict.fromkeys(lignes_var_categorielles["Variable"].to_list())
    )
    listes_vraies_colonnes_rica = df_rica_complet.columns
    df_rica_discretise = df_rica_complet.copy()
    for colonne in liste_variables_categorielles:

        if colonne in listes_vraies_colonnes_rica:
            df_rica_complet[colonne] = colonne + df_rica_complet[colonne].astype(str)
            df_rica_discretise[colonne] = colonne + df_rica_discretise[colonne].astype(
                str
            )
            if colonne in liste_variables_categorielles_discretisables:
                donnees_categories_explicites = modalites_discr_et_categories_explites[
                    modalites_discr_et_categories_explites["VARIABLE"] == colonne
                ]
                liste_categories_explicites = donnees_categories_explicites[
                    "modalite_cat"
                ].to_list()
                liste_valeurs_moyennes_categories = donnees_categories_explicites[
                    "moyenne"
                ].to_list()
                df_rica_discretise[colonne] = df_rica_discretise[colonne].replace(
                    to_replace=liste_categories_explicites,
                    value=liste_valeurs_moyennes_categories,
                )
    liste_donnees_dictionnaire = list(
        OrderedDict.fromkeys(dictionnaire["Variable"].to_list())
    )

    for element in liste_variables_categorielles:
        if element not in liste_donnees_dictionnaire:
            liste_variables_categorielles_discretisables.remove(element)

    colonnes_finales = df_rica_discretise.columns.to_list()

    df_rica_complet.to_csv(
        "./energie/outputs/df_modalites_categorielles_claires_pour_R.csv", sep=";"
    )
    df_rica_discretise.to_csv(
        "./energie/outputs/df_modalites_discretisees.csv", sep=";"
    )

    return (df_rica_discretise, liste_variables_categorielles_discretisables)
