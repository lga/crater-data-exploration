# Calcul des NODU pour quantifier les intrants

[Methodologie](https://agriculture.gouv.fr/telecharger/106547?token=90fe2c9e64650e3f9164076be113b1307327a103934e774ea1d818adacaf12ac)

## Sources de données

Données ANSES E-Phy disponibles [ici](https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/) (et téléchargées dans le dossier ./donnees/anses_e-phy). On peut explorer la base de données directement sur le site [E-Phy](https://ephy.anses.fr/).

Données AGRESTE SAA [presque tout](https://agreste.agriculture.gouv.fr/agreste-web/disaron/SAANR_DEVELOPPE_2/detail/) et [vigne](https://agreste.agriculture.gouv.fr/agreste-web/disaron/SAA_VIGNE/detail/). Un fichier par an (assez lourd). Données partiellement téléchargées dans le dossier ./donnees/agreste_saa


