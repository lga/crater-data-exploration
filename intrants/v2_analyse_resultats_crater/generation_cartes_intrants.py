from pathlib import Path

import pandas

from communs.chargeur_geometries import _charger_geometries_territoire_cible, CHEMIN_FICHIER_GEOMETRIES_COMMUNES, \
    CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS, CHEMIN_FICHIER_GEOMETRIES_EPCIS, CHEMIN_FICHIER_GEOMETRIES_REGIONS
from communs.config import CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES
from communs.crater_logger import log
from communs.exporter_carte import exporter_carte
from intrants.v2_analyse_resultats_crater.generation_diagrammes_intrants_crater import charger_indicateurs_intrants


def generer_cartes_intrants(dossier_intrants_crater: Path, dossier_sortie: Path, categorie_territoire, annee):

    log.info(f"Generation des cartes pour {categorie_territoire}")
    log.info(f"  - chargement fichier indicateurs")
    df_intrants = charger_indicateurs_intrants(dossier_intrants_crater)
    df_intrants_annee = df_intrants.loc[df_intrants.annee_achat == annee, :]

    log.info(f"  - chargement geometrie des {categorie_territoire}")
    gdf = _charger_geometries_territoire_cible("P-FR", categorie_territoire,
                                                        CHEMIN_FICHIER_GEOMETRIES_COMMUNES,
                                                        CHEMIN_FICHIER_GEOMETRIES_DEPARTEMENTS,
                                                        CHEMIN_FICHIER_GEOMETRIES_EPCIS,
                                                        CHEMIN_FICHIER_GEOMETRIES_REGIONS,
                                                        CHEMIN_FICHIER_REFERENTIEL_TERRITOIRES)

    # generer_carte_intrant(df_intrants_annee, gdf, categorie_territoire, annee, 'quantite_substance_avec_du_kg', dossier_sortie)
    # generer_carte_intrant(df_intrants_annee, gdf, categorie_territoire, annee, 'quantite_substance_sans_du_kg', dossier_sortie)
    # generer_carte_intrant(df_intrants_annee, gdf, categorie_territoire, annee, 'NODU_ha', dossier_sortie)
    generer_carte_intrant(df_intrants_annee, gdf, categorie_territoire, annee, 'NODU_normalise', dossier_sortie, bins=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100000])


def generer_carte_intrant(df, gdf, categorie_territoire, annee, nom_colonne_indicateur, dossier_sortie, bins=None):
    log.info(f"  - export carte {categorie_territoire}_{annee}_carte_{nom_colonne_indicateur}.png ")
    fichier_sortie = dossier_sortie / f'{categorie_territoire}_{annee}_carte_{nom_colonne_indicateur}.png'
    exporter_carte(gdf, df, nom_colonne_indicateur, fichier_sortie, bins)
