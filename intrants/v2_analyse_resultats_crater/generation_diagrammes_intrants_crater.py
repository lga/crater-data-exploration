from pathlib import Path

import numpy as np
import pandas
from matplotlib import pyplot as plt

from communs.chargeur_grille_densite_communes import charger_grille_densite_communes
from communs.crater_logger import log
from communs.export_fichier import exporter_csv, exporter_csv_et_description


def charger_indicateurs_intrants(dossier_intrants_crater: Path):
    return pandas.read_csv(
        dossier_intrants_crater / 'intrants_agricoles_moyennes_triennales.csv',
        sep=";"
    )


def charger_nodus_codes_postaux(dossier_intrants_crater: Path):

    return pandas.read_csv(
        dossier_intrants_crater / 'statistiques' / 'nodu_par_codes_postaux.csv',
        sep=";",
        dtype={'code_postal_acheteur': 'str'}
    )




def analyser_substances_manquantes(dossier_intrants_crater: Path, dossier_sortie: Path):

    df_substances = pandas.read_csv(
        dossier_intrants_crater / 'statistiques' / 'substances_sans_du.csv',
        sep=";"
    )
    df_substances = df_substances.loc[:, ['substance']].drop_duplicates().sort_values('substance')
    exporter_csv(df_substances, dossier_sortie / 'liste_substances_sans_du.csv')


def analyser_nodus_codes_postaux(dossier_intrants_crater: Path, dossier_sortie: Path, annee):

    df_nodus_cp = charger_nodus_codes_postaux(dossier_intrants_crater)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'CMR', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'T, T+, CMR', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'Env A', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'Env B', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'Santé', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'N Organique', dossier_sortie)
    # _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'N minéral', dossier_sortie)    _generer_analyse_codes_postaux(df_nodus_cp, 2019, 'CMR', dossier_sortie)
    _generer_analyse_codes_postaux(df_nodus_cp, annee, 'CMR|Env|Santé|Organique|minéral', dossier_sortie)


def _generer_analyse_codes_postaux(df_codes_postaux,
                                   annee, classification_substance,
                                   dossier_sortie: Path):
    base_nom_fichier = f'codes_postaux_{classification_substance}_{annee}'
    df_cp_filtre = df_codes_postaux.loc[
                   (df_codes_postaux['annee_achat'] == annee)
                   & df_codes_postaux['id_region'].str.startswith('R-')
                   & (df_codes_postaux['code_postal_acheteur'] != '00000')
                   & (df_codes_postaux['classification'].str.contains(classification_substance, case=False, regex=True))
    , :]
    exporter_csv_et_description(df_cp_filtre, dossier_sortie, base_nom_fichier)
    # _generer_histogramme(df_cp_filtre, dossier_sortie, base_nom_fichier, 'quantite_substance_kg', 100)
    _generer_histogramme(df_cp_filtre, dossier_sortie, base_nom_fichier, 'NODU_ha', 100)
    _generer_histogramme(df_cp_filtre, dossier_sortie, base_nom_fichier, 'quantite_substance_sans_du_kg', 100)

def _generer_histogrammes(dossier_intrants_crater: Path, dossier_sortie: Path, annee, fichier_source_grille_densite_communes):

    df =  charger_indicateurs_intrants(dossier_intrants_crater)
    df_filtre = _filtrer_par_annee_et_categorie_territoire(df, dossier_sortie, annee, 'EPCI')
    _generer_histogramme(df_filtre, dossier_sortie, f'EPCI_{annee}', 'NODU_normalise', 30, (0,25))
    _generer_scatter_plot(df_filtre, dossier_sortie, f'EPCI_{annee}', 'NODU_ha', 'sau_ra_2020_ha')
    # _generer_histogramme(df_filtre, dossier_sortie, f'EPCI_{annee}', 'quantite_substance_kg', 30, (0, 200000))
    # _generer_histogramme(df_filtre, dossier_sortie, f'EPCI_{annee}', 'NODU_ha', 30, (0, 30000))

    gdc = charger_grille_densite_communes(fichier_source_grille_densite_communes)
    df = df.merge(gdc, left_on='id_territoire', right_on='id_commune', how='outer')
    df_filtre = _filtrer_par_annee_et_categorie_territoire(df, dossier_sortie, annee, 'COMMUNE')
    # _generer_histogramme(df_filtre, dossier_sortie, f'COMMUNE_{annee}', 'quantite_substance_kg', 30, (0, 2000))
    # _generer_histogramme(df_filtre, dossier_sortie, f'COMMUNE_{annee}', 'NODU_ha', 30, (0, 5000))
    _generer_histogramme(df_filtre, dossier_sortie, f'COMMUNE_{annee}', 'NODU_normalise', 30, (0,10))
    _generer_histogramme_stacked(df_filtre, dossier_sortie, f'COMMUNE_{annee}', 'NODU_normalise', 30, (0,10))
    _generer_scatter_plot(df_filtre, dossier_sortie, f'COMMUNE_{annee}', 'NODU_ha', 'sau_ra_2020_ha')




def _filtrer_par_annee_et_categorie_territoire(df, dossier_sortie, annee, categorie_territoire):
    base_nom_fichier = f'{categorie_territoire}_{annee}'
    df_territoires_filtre = df.loc[
                            df['categorie_territoire'].isin([categorie_territoire]) & (
                                    df['annee_achat'] == annee), :]
    exporter_csv_et_description(df_territoires_filtre, dossier_sortie, base_nom_fichier)

    return df_territoires_filtre



def _generer_histogramme(df, dossier_sortie, base_nom_fichier, nom_colonne, bins = 100, clip=None):
    nom_analyse = f'{base_nom_fichier}_histogramme_{nom_colonne}'
    if (clip is not None):
        nom_analyse += '_clippe'

    log.info(f"Generation de {nom_analyse}")

    df_indicateur = df.loc[:, [nom_colonne]].replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    if (clip is not None):
        df_indicateur = df_indicateur.clip(clip[0], clip[1])
    hist = df_indicateur.hist(bins=bins)

    fig = hist[0][0].get_figure()
    fig.savefig(f'{dossier_sortie}/{nom_analyse}.png')
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


def _generer_histogramme_stacked(df, dossier_sortie, base_nom_fichier, nom_colonne, bins = 100, clip=None):
    nom_analyse = f'{base_nom_fichier}_histogramme_stacked_{nom_colonne}'
    if (clip is not None):
        nom_analyse += '_clippe'

    log.info(f"Generation de {nom_analyse}")

    df_indicateur = df.loc[:, [nom_colonne, 'categorie_densite']].replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    if (clip is not None):
        df_indicateur[nom_colonne] = df_indicateur[nom_colonne].clip(clip[0], clip[1])
    df_pivot = df_indicateur.pivot(values='NODU_normalise', columns='categorie_densite')
    hist = df_pivot.hist(bins=bins)

    fig = hist[0][0].get_figure()
    fig.savefig(f'{dossier_sortie}/{nom_analyse}.png')
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


def _generer_diagramme_densite(df, dossier_sortie, base_nom_fichier, nom_colonne):
    nom_analyse = f'{base_nom_fichier}_diag_densite_{nom_colonne}'
    log.info(f"Generation de  {nom_analyse}")

    df_indicateur = df.loc[:, [nom_colonne]].replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    hist = df_indicateur.plot.density()

    fig = hist.get_figure()
    fig.savefig(f'{dossier_sortie}/{nom_analyse}.png')
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


def _generer_scatter_matrix(df, dossier_sortie, base_nom_fichier, nom_colonne1, nom_colonne2):
    nom_analyse = f'{base_nom_fichier}_matrice_correlation_{nom_colonne1}_{nom_colonne2}'
    log.info(f"Generation de {nom_analyse}")

    df_indicateur = df.loc[:, [nom_colonne1, nom_colonne2]].replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    hist = pandas.plotting.scatter_matrix(df_indicateur)

    fig = hist[0][0].get_figure()
    fig.savefig(f'{dossier_sortie}/{nom_analyse}.png')
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


def _generer_scatter_plot(df, dossier_sortie, base_nom_fichier, nom_colonne1, nom_colonne2):
    nom_analyse = f'{base_nom_fichier}_scatter_plot_{nom_colonne1}_{nom_colonne2}'
    log.info(f"Generation de {nom_analyse}")

    if ('categorie_densite' in df.columns):
        df_indicateur = df.loc[:, [nom_colonne1, nom_colonne2, 'categorie_densite', 'indice_densite']].replace([np.inf, -np.inf], np.nan,
                                                                        inplace=False).dropna()
        hist = df_indicateur.plot.scatter(x=nom_colonne1, y=nom_colonne2, c='indice_densite', colormap='Accent_r', alpha=0.7)
    else:
        df_indicateur = df.loc[:, [nom_colonne1, nom_colonne2]].replace([np.inf, -np.inf], np.nan,
                                                                        inplace=False).dropna()
        hist = df_indicateur.plot.scatter(x=nom_colonne1, y=nom_colonne2)

    fig = hist.get_figure()
    fig.savefig(f'{dossier_sortie}/{nom_analyse}.png')
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()
