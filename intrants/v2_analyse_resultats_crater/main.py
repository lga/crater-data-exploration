import sys
from pathlib import Path

from communs.crater_logger import log
from communs.export_fichier import reinitialiser_dossier
from intrants.v2_analyse_resultats_crater.generation_cartes_intrants import generer_cartes_intrants
from intrants.v2_analyse_resultats_crater.generation_diagrammes_intrants_crater import analyser_substances_manquantes, \
    analyser_nodus_codes_postaux, _generer_histogrammes

CHEMIN_CRATER_DATA_SOURCES = Path('../../../crater-data-sources')
CHEMIN_FICHIER_GEOMETRIES_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / 'open_street_map/geometries_communes/2021/communes-20210101/communes-20210101.shp'
CHEMIN_FICHIER_GEOMETRIES_EPCIS = CHEMIN_CRATER_DATA_SOURCES / 'banatic/geometries_epcis/2021/Métropole/EPCI 2021_region.shp'

DOSSIER_DONNEES_CRATER = Path("./donnees_crater")
DOSSIER_DONNEES_CRATER_CMR = DOSSIER_DONNEES_CRATER / "intrants_agricoles_cmr"
DOSSIER_DONNEES_CRATER_TOUS_SAUF_AUTRE = DOSSIER_DONNEES_CRATER / "intrants_agricoles_tous_sauf_autre"

DOSSIER_SORTIE = Path("./resultats_analyses")
DOSSIER_SORTIE_CMR = DOSSIER_SORTIE / "cmr"
DOSSIER_SORTIE_TOUS_SAUF_AUTRE = DOSSIER_SORTIE / "tous_sauf_autre"



def generer_analyse(dossier_donnees_crater: Path, dossier_sortie: Path):
    log.info(f"Générer analyse pour {dossier_donnees_crater}")
    reinitialiser_dossier(dossier_sortie)
    analyser_substances_manquantes(dossier_donnees_crater, dossier_sortie)
    generer_analyse_annee(2017, dossier_donnees_crater, dossier_sortie)
    generer_analyse_annee(2018, dossier_donnees_crater, dossier_sortie)
    generer_analyse_annee(2019, dossier_donnees_crater, dossier_sortie)
    generer_analyse_annee(2020, dossier_donnees_crater, dossier_sortie)


def generer_analyse_annee(annee, dossier_donnees_crater, dossier_sortie):
    # analyser_nodus_codes_postaux(dossier_donnees_crater, dossier_sortie, annee)
    FICHIER_SOURCE_GRILLLE_DENSITE_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / 'insee/grille_communale_densite/2021/grille_densite_2021_agrege.xlsx'
    _generer_histogrammes(dossier_donnees_crater, dossier_sortie, annee, FICHIER_SOURCE_GRILLLE_DENSITE_COMMUNES)
    generer_cartes_intrants(dossier_donnees_crater, dossier_sortie, "COMMUNE", annee)
    generer_cartes_intrants(dossier_donnees_crater, dossier_sortie, "EPCI", annee)


if __name__ == '__main__':
    # generer_analyse(DOSSIER_DONNEES_CRATER_CMR, DOSSIER_SORTIE_CMR)
    generer_analyse(DOSSIER_DONNEES_CRATER_TOUS_SAUF_AUTRE, DOSSIER_SORTIE_TOUS_SAUF_AUTRE)
    log.info("FIN MAIN")