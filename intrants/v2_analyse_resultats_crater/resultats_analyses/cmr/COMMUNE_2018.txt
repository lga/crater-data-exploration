<class 'pandas.core.frame.DataFrame'>
Int64Index: 34836 entries, 23105 to 266950
Data columns (total 12 columns):
 #   Column                 Non-Null Count  Dtype  
---  ------                 --------------  -----  
 0   id_territoire          34836 non-null  object 
 1   annee_achat            34836 non-null  float64
 2   nom_territoire         34836 non-null  object 
 3   categorie_territoire   34836 non-null  object 
 4   sau_ra_2020_ha         34833 non-null  float64
 5   codes_postaux_commune  34835 non-null  object 
 6   quantite_substance_kg  34029 non-null  float64
 7   NODU_ha                34029 non-null  float64
 8   NODU_normalise         33068 non-null  float64
 9   id_commune             34836 non-null  object 
 10  categorie_densite      34836 non-null  object 
 11  indice_densite         34836 non-null  float64
dtypes: float64(6), object(6)
memory usage: 3.5+ MB
