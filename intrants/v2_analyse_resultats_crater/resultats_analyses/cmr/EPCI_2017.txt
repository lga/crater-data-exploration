<class 'pandas.core.frame.DataFrame'>
Int64Index: 1231 entries, 14487 to 23097
Data columns (total 9 columns):
 #   Column                 Non-Null Count  Dtype  
---  ------                 --------------  -----  
 0   id_territoire          1231 non-null   object 
 1   annee_achat            1231 non-null   int64  
 2   nom_territoire         1231 non-null   object 
 3   categorie_territoire   1231 non-null   object 
 4   sau_ra_2020_ha         1231 non-null   float64
 5   codes_postaux_commune  0 non-null      object 
 6   quantite_substance_kg  1228 non-null   float64
 7   NODU_ha                1228 non-null   float64
 8   NODU_normalise         1228 non-null   float64
dtypes: float64(4), int64(1), object(4)
memory usage: 96.2+ KB
