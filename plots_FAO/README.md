# Affichage données FAO

Les données ont été téléchargées [ici](https://www.fao.org/faostat/fr/#data/FBSH) et [là](https://www.fao.org/faostat/fr/#data/FBS) (avant et après 2013). A chaque fois, le fichier complet pour l'Europe a été téléchargé (menu à droite de la page).

Un fichier csv contenant les données des graphes est disponible dans chaque cas.


## Europe

### Céréales

![Céréales, Europe](Europe_Cereales.png)

### Oléagineux

![Oleagineux, Europe](Europe_Oleagineux.png)

### Protéagineux

![Proteagineux, Europe](Europe_Proteagineux.png)


## France

### Céréales

![Céréales, France](France_Cereales.png)

### Oléagineux

![Oleagineux, France](France_Oleagineux.png)

### Protéagineux

![Proteagineux, France](France_Proteagineux.png)
