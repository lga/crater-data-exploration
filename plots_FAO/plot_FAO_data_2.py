#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 14:01:58 2021

@author: vincent
"""

import pandas as pd
import matplotlib.pyplot as plt

plt.close('all')

def selectionner_donnees(categorie_culture):
    produits_avant_2013 = categorie_culture['produits_avant_2013']
    produits_apres_2013 = categorie_culture['produits_apres_2013']
    
    df_avant_2013_EUR = df_avant_2013[[prod in produits_avant_2013 for prod in df_avant_2013.Produit.tolist()]]
    df_avant_2013_FR = df_avant_2013_EUR[df_avant_2013_EUR['Zone']=='France']
    df_apres_2013_EUR = df_apres_2013[[prod in produits_apres_2013 for prod in df_apres_2013.Produit.tolist()]]
    df_apres_2013_FR = df_apres_2013_EUR[df_apres_2013_EUR['Zone']=='France']
    
    return df_avant_2013_EUR, df_avant_2013_FR, df_apres_2013_EUR, df_apres_2013_FR

def recuperer_infos_sommees(donnees, annee, liste_nourriture, liste_alim_animaux, liste_autres):
    '''
    Pour une année donnée, on somme les données dans chacune des 3 catégories
    '''
    annee_str = 'Y'+str(annee)
    alim_animaux = donnees[[elt in  liste_alim_animaux for elt in donnees.Élément.tolist()]].sum()[annee_str]
    nourriture = donnees[[elt in  liste_nourriture for elt in donnees.Élément.tolist()]].sum()[annee_str]
    autres = donnees[[elt in  liste_autres for elt in donnees.Élément.tolist()]].sum()[annee_str]
    d = {'Annee':int(annee),'Nourriture (kT)':nourriture,'Alim_animaux (kT)':alim_animaux,'Autres (kT)':autres}
    return d

def remettre_en_forme(df_avant_2013_EUR, df_avant_2013_FR, df_apres_2013_EUR, df_apres_2013_FR):
    df_resultat_EUR = pd.DataFrame(columns=liste_colonnes)
    df_resultat_FR = pd.DataFrame(columns=liste_colonnes)
    for annee in range(1961,2014):
        # Donnees EUR
        d = recuperer_infos_sommees(df_avant_2013_EUR, annee, liste_nourriture, liste_alim_animaux, liste_autres)
        df_resultat_EUR = df_resultat_EUR.append(d,ignore_index=True) 
        # Donnees FR
        d = recuperer_infos_sommees(df_avant_2013_FR, annee, liste_nourriture, liste_alim_animaux, liste_autres)
        df_resultat_FR = df_resultat_FR.append(d,ignore_index=True)
        
    for annee in range(2014,2019):
        # Donnees EUR
        d = recuperer_infos_sommees(df_apres_2013_EUR, annee, liste_nourriture, liste_alim_animaux, liste_autres)
        df_resultat_EUR = df_resultat_EUR.append(d,ignore_index=True) 
        # Donnees FR
        d = recuperer_infos_sommees(df_apres_2013_FR, annee, liste_nourriture, liste_alim_animaux, liste_autres)
        df_resultat_FR = df_resultat_FR.append(d,ignore_index=True)
        
    # Passage de l'année en index de la dataframe
    df_resultat_EUR = df_resultat_EUR.set_index('Annee')
    df_resultat_FR = df_resultat_FR.set_index('Annee')
    
    return df_resultat_EUR, df_resultat_FR



# Listes d'éléments à utiliser (colonne 'Elements')
liste_nourriture = ['Nourriture']
liste_alim_animaux, = ['Aliments pour animaux']
liste_autres = ['Semences','Pertes','Autres Utilisations', 'Traitement', 'Autres utilisations (non alimentaire)', 'Résidus']

liste_colonnes = ['Annee','Nourriture (kT)','Alim_animaux (kT)','Autres (kT)']

# Chargement des donnees avant 2013
filename = r'./BilansAlimentairesHistorique_F_Europe/BilansAlimentairesHistorique_F_Europe.csv'
df_avant_2013 = pd.read_csv(filename,encoding='latin1',low_memory=False)

# Chargement des données apres 2013
filename = r'./BilansAlimentaires_F_Europe/BilansAlimentaires_F_Europe.csv'
df_apres_2013 = pd.read_csv(filename,encoding='latin1',low_memory=False)

# Selection des produits a utiliser
cereales = {'produits_avant_2013': ['Cèrèales - Excl Bière'],
            'produits_apres_2013': ['Cèrèales - Excl Bière'],
            'Nom':'Cereales'}

oleagineux = {'produits_avant_2013': ['Cultures Oleágineuses'],
            'produits_apres_2013': ['Cultures Oleágineuses'],
            'Nom':'Oleagineux'}

proteagineux = {'produits_avant_2013': ['Haricots',
'Pois', 'Légumineuses Autres et produits'],
            'produits_apres_2013': ['Haricots',
            'Pois', 'Légumineuses Autres et produits'],
            'Nom':'Proteagineux'}

categories_cultures = [cereales, oleagineux, proteagineux]


for categorie_culture in categories_cultures:
    df_avant_2013_EUR, df_avant_2013_FR, df_apres_2013_EUR, df_apres_2013_FR = selectionner_donnees(categorie_culture)

    df_resultat_EUR, df_resultat_FR = remettre_en_forme(df_avant_2013_EUR, df_avant_2013_FR, df_apres_2013_EUR, df_apres_2013_FR)
    
    df_resultat_EUR.plot.area()
    nom_figure = 'Europe, {}'.format(categorie_culture['Nom'])
    plt.title(nom_figure)
    plt.savefig(nom_figure.replace(', ','_')+'.png')
    df_resultat_EUR.to_csv(nom_figure.replace(', ','_')+'.csv')
    
    df_resultat_FR.plot.area()
    nom_figure = 'France, {}'.format(categorie_culture['Nom'])
    plt.title(nom_figure)
    plt.savefig(nom_figure.replace(', ','_')+'.png')
    df_resultat_FR.to_csv(nom_figure.replace(', ','_')+'.csv')




'''
Liste des produits avant 2013 :
'Population', 'Blé', 'Riz (Eq Blanchi)', 'Orge', 'Maïs', 'Seigle',
       'Avoine', 'Millet', 'Sorgho', 'Céréales, Autres', 'Manioc',
       'Pommes de Terre', 'Patates douces', 'Racines, Nda',
       'Sucre, betterave', 'Sucre Eq Brut', 'Edulcorants Autres', 'Miel',
       'Haricots', 'Pois', 'Légumineuses Autres', 'Noix', 'Soja',
       'Arachides Decortiquees', 'Graines de tournesol',
       'Graines Colza/Moutarde', 'Graines de coton', 'Coco (Incl Coprah)',
       'Sésame', 'Palmistes', 'Olives', 'Plantes Oleiferes, Autre',
       'Huile de Soja', "Huile d'Arachide", 'Huile de Tournesol',
       'Huile de Colza&Moutarde', 'Huile Graines de Coton',
       'Huile de Palmistes', 'Huile de Palme', 'Huile de Coco',
       'Huile de Sésame', "Huile d'Olive", 'Huile de Germe de Maïs',
       'Huil Plantes Oleif Autr', 'Tomates', 'Oignons', 'Légumes, Autres',
       'Oranges, Mandarines', 'Citrons & Limes', 'Pamplemousse',
       'Agrumes, Autres', 'Bananes', 'Bananes plantains', 'Pommes',
       'Ananas', 'Dattes', 'Raisin', 'Fruits, Autres', 'Café',
       'Feve de Cacao', 'Thé', 'Poivre', 'Piments', 'Épices, Autres',
       'Vin', 'Bière', 'Boissons Fermentés', 'Boissons Alcooliques',
       'Alcool, non Comestible', 'Viande de Bovins',
       "Viande d'Ovins/Caprins", 'Viande de Suides',
       'Viande de Volailles', 'Viande, Autre', 'Abats Comestible',
       'Beurre, Ghee', 'Crème', 'Graisses Animales Crue',
       'Huiles de Poissons', 'Huiles de Foie de Poisso', 'Oeufs',
       'Lait - Excl Beurre', 'Poissons Eau Douce', 'Perciform',
       'Poissons Pelagiques', 'Poissons Marins, Autres', 'Crustacés',
       'Cephalopodes', 'Mollusques, Autres', 'Animaux Aquatiques Autre',
       'Plantes Aquatiques', 'Aliments pour enfants', 'Miscellanees',
       'Total General', 'Produits Vegetaux', 'Produits Animaux',
       'Cèrèales - Excl Bière', 'Racines Amyl', 'Cultures Sucrieres',
       'Sucre & Edulcorants', 'Légumineuses Sèches', 'Fruit Coque',
       'Cultures Oleágineuses', 'Huiles végétales', 'Légumes',
       'Fruits - Excl Vin', 'Stimulants', 'Épices', 'Viande', 'Abats',
       'Graisses animales', 'Poisson & Fruits de Mer',
       'Produits Aquatiques, Aut', 'Ignames', 'Sucre, canne', 'Girofles',
       'Huile de Son de Riz'

Liste des produits apres 2013 :
'Population', 'Total General', 'Produits Vegetaux',
       'Produits Animaux', 'Cèrèales - Excl Bière', 'Blé et produits',
       'Riz et produits', 'Orge et produits', 'Maïs et produits',
       'Seigle et produits', 'Avoine', 'Millet et produits',
       'Sorgho et produits', 'Céréales, Autres', 'Racines Amyl',
       'Manioc et produits', 'Pommes de Terre et produits',
       'Patates douces', 'Racines nda', 'Cultures Sucrieres',
       'Sucre, betterave', 'Sucre & Edulcorants', 'Sucre Eq Brut',
       'Edulcorants Autres', 'Miel', 'Légumineuses Sèches', 'Haricots',
       'Pois', 'Légumineuses Autres et produits', 'Fruit Coque',
       'Noix et produits', 'Cultures Oleágineuses', 'Soja', 'Arachides',
       'Graines de tournesol', 'Graines Colza/Moutarde',
       'Graines de coton', 'Coco (Incl Coprah)', 'Sésame', 'Palmistes',
       'Olives', 'Plantes Oleiferes, Autre', 'Huiles végétales',
       'Huile de Soja', "Huile d'Arachide", 'Huile de Tournesol',
       'Huile de Colza&Moutarde', 'Huile Graines de Coton',
       'Huile de Palmistes', 'Huile de Palme', 'Huile de Coco',
       "Huile d'Olive", 'Huile de Germe de Maïs',
       'Huil Plantes Oleif Autr', 'Légumes', 'Tomates et produits',
       'Oignons', 'Légumes, Autres', 'Fruits - Excl Vin',
       'Oranges, Mandarines', 'Citrons & Limes et produits',
       'Pamplemousse et produits', 'Agrumes, Autres', 'Bananes',
       'Bananes plantains', 'Pommes et produits', 'Ananas et produits',
       'Dattes', 'Raisin', 'Fruits, Autres', 'Stimulants',
       'Café et produits', 'Feve de Cacao et produits', 'Thé', 'Épices',
       'Poivre', 'Piments', 'Girofles', 'Épices, Autres',
       'Boissons Alcooliques', 'Vin', 'Bière', 'Boissons Fermentés',
       'Alcool, non Comestible', 'Viande', 'Viande de Bovins',
       "Viande d'Ovins/Caprins", 'Viande de porcins',
       'Viande de Volailles', 'Viande, Autre', 'Abats',
       'Abats Comestible', 'Graisses animales', 'Beurre, Ghee', 'Crème',
       'Graisses Animales Crue', 'Huiles de Poissons',
       'Huiles de Foie de Poisso', 'Oeufs', 'Lait - Excl Beurre',
       'Poisson & Fruits de Mer', 'Poissons Eau Douce', 'Perciform',
       'Poissons Pelagiques', 'Poissons Marins, Autres', 'Crustacés',
       'Cephalopodes', 'Mollusques, Autres', 'Produits Aquatiques, Aut',
       'Animaux Aquatiques Autre', 'Plantes Aquatiques', 'Miscellanees',
       'Aliments pour enfants', 'Ignames', 'Sucre, canne',
       'Huile de Sésame', 'Sucre non centrifugé', 'Huile de Son de Riz']
'''