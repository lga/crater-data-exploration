import os

import geopandas
import pandas
import pygeos

from sau_ocsge.logger import log
from sau_ocsge.recoupement_classifications import OUTPUT_FOLDER, OCS_GE_SHP


def generer_shp_contour(fichier_shp_source, fichier_shp_resultat):
    log.info("ETAPE generer_shp_contour")
    gdf_source = geopandas.read_file(fichier_shp_source)
    polygon_contour = gdf_source.unary_union
    gdf_contour = geopandas.GeoSeries(polygon_contour, crs='EPSG:2154')
    gdf_contour = gdf_contour.buffer(0) #pour corriger mini trous dus au fait que les parcelles OCS ne sont pas forcéments parfaitement contiguës
    gdf_contour.to_file(fichier_shp_resultat)


def generer_shp_intersection_avec_contour(fichier_shp_contour, fichier_shp_initial, fichier_shp_resultat):
    log.info("generer_shp_intersection_avec_contour")
    gdf_contour = geopandas.read_file(fichier_shp_contour)
    log.info(f"Lire fichier shp {fichier_shp_initial}")
    gdf_hors_contour = geopandas.read_file(fichier_shp_initial)
    log.info("Supprimer les zones hors du contour")
    gdf_resultat = geopandas.overlay(gdf_contour, gdf_hors_contour, how='intersection')
    gdf_resultat.to_file(fichier_shp_resultat)


# def charger_shp(liste_noms_fichiers_shp):
#
#     liste_gdf = []
#
#     log.info(f'Chargement couche témoin {nom_couche_temoin}')
#     gdf_ocs = geopandas.read_file(fichier_couche_temoin)
#
#     log.info('Chargement RPG')
#     gdf_rpg = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/rpg_territoire/rpg_territoire.shp')
#
#     return liste_gdf

def calculer_intersection(liste_couches, fichier_resultat):
    log.info(f'Calculer intersection multiple')

    gdf_intersection = None
    suffixe_fichier_resultat = ''
    distance_buffer = 0
    for couche in liste_couches:
        if gdf_intersection is None:
            gdf_intersection = charger_shp(couche['prefix'], couche['fichier_shp'], couche['liste_colonnes'], distance_buffer)
            suffixe_fichier_resultat += '_' + couche['prefix']
        else:
            gdf = charger_shp(couche['prefix'], couche['fichier_shp'], couche['liste_colonnes'], distance_buffer)
            log.info(f"Jointure spatiale en cours avec le fichier {couche['fichier_shp']}")
            gdf_intersection = geopandas.overlay(gdf_intersection, gdf, how=couche['mode_overlay'])

            gdf_intersection['geometry'] = gdf_intersection['geometry'].buffer(distance_buffer)
            gdf_intersection['geometry'] = pygeos.set_precision(gdf_intersection.geometry.values.data, 1e-6)

            suffixe_fichier_resultat += '_' + couche['prefix']
            nom_fichier_intermediaire = os.path.splitext(str(fichier_resultat))[0] + suffixe_fichier_resultat + os.path.splitext(str(fichier_resultat))[1]
            log.info(f'Jointure spatiale terminée, sauvegard fichier intermediaire : {nom_fichier_intermediaire}')
            gdf_intersection.to_file(nom_fichier_intermediaire)


    gdf_intersection.to_file(fichier_resultat)


def charger_shp(prefix, nom_fichier, liste_colonnes, distance_buffer):
    log.info(f"Chargement fichier shp : {nom_fichier}")
    gdf = geopandas.read_file(nom_fichier).loc[:, liste_colonnes + ['geometry']]
    gdf = gdf.rename(columns={col: prefix + '_' + col for col in liste_colonnes})

    gdf['geometry'] = gdf['geometry'].buffer(distance_buffer)
    gdf['geometry'] = pygeos.set_precision(gdf.geometry.values.data, 1e-6)

    log.info(f"Verification validite du shp {nom_fichier} : is_valid = {gdf.is_valid.all()}")

    gdf.to_file(os.path.splitext(str(nom_fichier))[0] + "-precision.shp")

    return gdf


def exporter_df(fichier_shp, fichier_resultat):
    gdf = geopandas.read_file(fichier_shp)
    gdf['surface_ha'] = gdf.geometry.area * 10 ** -4
    df = pandas.DataFrame(gdf)
    # On enleve les micro polygones (surface < 1m2)
    df = df.loc[df['surface_ha'] > 0.0001 , df.columns != 'geometry']
    df.index.name = 'id_parcelle'
    df.to_csv(fichier_resultat, sep=';', index=True)