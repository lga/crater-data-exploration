from pathlib import Path

import pandas, geopandas

from sau_ocsge.analyses_spatiales import generer_shp_contour, generer_shp_intersection_avec_contour, \
    calculer_intersection, exporter_df
from sau_ocsge.outils import reinitialiser_dossier
from sau_ocsge.logger import log


# def lancer_traitement():
#     pandas.set_option('display.max_columns', None)
#     # A lancer une fois pour générer les différents shp intermédiaires
#     # generer_contour_territoire()
#     # generer_surfaces_iso_altitude()
#     # generer_rpg_territoire()
#     # generer_shp_intersection_et_difference_rpg('ocs', OCS_GE_SHP)
#
#     # generer_analyses_rpg_ocs()
#     # calculer_stats_recoupement_alti_rpg()
#
#     #     RPG / MOS
#     generer_shp_intersection_et_difference_rpg_avec_temoin('mos', MOS_SHP)
#     generer_analyses_rpg_versus_temoin('mos',
#                                        MOS_SHP,
#                                        f'{OUTPUT_FOLDER}/geodata/rpg_intersection_mos/rpg_intersection_mos.shp',
#                                        f'{OUTPUT_FOLDER}/geodata/mos_hors_rpg/mos_hors_rpg.shp',
#                                        ['niv4_14', 'intit4_14'], 'niv4_14')


def generer_shp_reduit_au_contour(fichier_contour, dossier_input, dossier_output):

    generer_shp_intersection_avec_contour(fichier_contour,
                                          dossier_input / 'rpg/RPG_2-0_SHP_LAMB93_R93-2017/PARCELLES_GRAPHIQUES.shp',
                                          dossier_output / 'rpg_territoire.shp')
    generer_shp_intersection_avec_contour(fichier_contour,
                                          dossier_input / 'ocs/0.2_OCS2D_2017/OCSGE_NCA_2017_MAN.shp',
                                          dossier_output / 'ocs-ge_territoire.shp')
    generer_shp_intersection_avec_contour(fichier_contour,
                                          dossier_input / 'mos/mos2014/ocsol_2014_nca.shp',
                                          dossier_output / 'mos_territoire.shp')
    generer_shp_intersection_avec_contour(fichier_contour,
                                          '../../crater-data-sources/data_gouv_fr/corine_land_cover/2018/CLC_PNE_RG/CORINE_LAND_COVER_FRANCE_METROPOLITAINE_EPSG2154.gpkg',
                                          dossier_output / 'clc_territoire.shp')


def generer_shp_intersection(dossier_shp_input, dossier_output):
    reinitialiser_dossier(dossier_output)

    calculer_intersection([
                           {'prefix': 'ocs',
                            'fichier_shp': dossier_shp_input / 'ocs-ge_territoire.shp',
                            'liste_colonnes': ['couverture', 'usage'],
                            'mode_overlay': 'intersection'},
                           {'prefix': 'mos',
                            'fichier_shp': dossier_shp_input / 'mos_territoire.shp',
                            'liste_colonnes': ['niv4_14', 'intit4_14'],
                            'mode_overlay': 'intersection'},
                           {'prefix': 'clc',
                            'fichier_shp': dossier_shp_input / 'clc_territoire.shp',
                            'liste_colonnes': ['code_18'],
                            'mode_overlay': 'intersection'},
                           {'prefix': 'rpg',
                            'fichier_shp': dossier_shp_input / 'rpg_territoire.shp',
                            'liste_colonnes': ['CODE_CULTU', 'CODE_GROUP'],
                            'mode_overlay': 'union'}
                           ],
                          dossier_output / 'intersection_territoire.shp')

def generer_csv_intersection(dossier_shp_intersection, dossier_output):
    reinitialiser_dossier(dossier_output)

    exporter_df(dossier_shp_intersection / 'intersection_territoire.shp',
                dossier_output / 'intersection_territoire.csv')


def ajouter_regroupements(df, type):
    if type == 'rpg':
        df = ajouter_regroupements_rpg(df)
    elif type == 'ocs':
        df = ajouter_regroupements_ocs(df)
    elif type == 'mos':
        df = ajouter_regroupements_mos(df)
    elif type == 'clc':
        df = ajouter_regroupements_clc(df)
    else:
        log.info("ERREUR TYPE")
    return df


def ajouter_regroupements_rpg(df):
    regroupements_rpg = pandas.read_excel(
        "data/input/data/regroupements.xlsx", sheet_name='rpg', usecols='A:D', dtype='str'
    ).loc[:, ['code_culture_rpg', 'categorie_rpg', 'categorie2_rpg']]
    df.rename(columns={"CODE_CULTU": "rpg_CODE_C"}, inplace=True)
    df = df.merge(regroupements_rpg, how='left', left_on="rpg_CODE_C", right_on="code_culture_rpg")
    df['categorie_rpg'] = df['categorie_rpg'].fillna("inconnu")
    df.loc[df['rpg_CODE_C'].isnull(), 'categorie_rpg'] = 'hors RPG'
    df['categorie2_rpg'] = df['categorie2_rpg'].fillna("inconnu")
    df.loc[df['rpg_CODE_C'].isnull(), 'categorie2_rpg'] = 'hors RPG'
    return df

def ajouter_regroupements_ocs(df):
    regroupements_ocs = pandas.read_excel(
        "data/input/data/regroupements.xlsx", sheet_name='ocs', usecols='A:F', dtype='str'
    ).loc[:, ['code_usage_ocs', 'code_couverture_ocs', 'categorie_ocs', 'categorie2_ocs']]
    df = df.merge(regroupements_ocs, how='left', left_on=["ocs_usage", "ocs_couver"], right_on=["code_usage_ocs", "code_couverture_ocs"])
    df['categorie_ocs'] = df['categorie_ocs'].fillna("inconnu")
    df.loc[df['ocs_usage'].isnull(), 'categorie_ocs'] = 'hors OCS'
    df['categorie2_ocs'] = df['categorie2_ocs'].fillna("inconnu")
    df.loc[df['ocs_usage'].isnull(), 'categorie2_ocs'] = 'hors OCS'
    return df

def ajouter_regroupements_mos(df):
    regroupements_mos = pandas.read_excel(
        "data/input/data/regroupements.xlsx", sheet_name='mos', usecols='A:D', dtype='str'
    ).loc[:, ['mos_niv4_1', 'categorie_mos', 'categorie2_mos']]
    df = df.merge(regroupements_mos, how='left', left_on="mos_niv4_1", right_on="mos_niv4_1")
    df['categorie_mos'] = df['categorie_mos'].fillna("inconnu")
    df.loc[df['mos_niv4_1'].isnull(), 'categorie_mos'] = 'hors MOS'
    df['categorie2_mos'] = df['categorie2_mos'].fillna("inconnu")
    df.loc[df['mos_niv4_1'].isnull(), 'categorie2_mos'] = 'hors MOS'
    return df

def ajouter_regroupements_clc(df):
    regroupements_clc = pandas.read_excel(
        "data/input/data/regroupements.xlsx", sheet_name='clc', usecols='A:D', dtype='str'
    ).loc[:, ['clc_code_1', 'categorie_clc', 'categorie2_clc']]
    df.rename(columns={"code_18": "clc_code_1"}, inplace=True)
    df = df.merge(regroupements_clc, how='left', left_on="clc_code_1", right_on="clc_code_1")
    df['categorie_clc'] = df['categorie_clc'].fillna("inconnu")
    df.loc[df['clc_code_1'].isnull(), 'categorie_clc'] = 'hors CLC'
    df['categorie2_clc'] = df['categorie2_clc'].fillna("inconnu")
    df.loc[df['clc_code_1'].isnull(), 'categorie2_clc'] = 'hors CLC'
    return df


def exporter_typologies_surfaces_depuis_csv_intersection(dossier_output):
    log.info('Export des typologies de surface')
    df = pandas.read_csv(f'{dossier_output}/intersection.csv', sep=";", dtype='str')
    df['mos_niv4_1'] = df['mos_niv4_1'].str.replace('.0', '', regex=False)
    df.loc[:,['rpg_CODE_C', 'rpg_CODE_G']]\
        .drop_duplicates()\
        .to_csv(f'{dossier_output}/typologies_surfaces_rpg.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')
    df.loc[:,['ocs_usage', 'ocs_couver']]\
        .drop_duplicates() \
        .sort_values(by=['ocs_usage', 'ocs_couver']) \
        .to_csv(f'{dossier_output}/typologies_surfaces_ocs.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')
    df.loc[:,['mos_niv4_1', 'mos_intit4']]\
        .drop_duplicates()\
        .to_csv(f'{dossier_output}/typologies_surfaces_mos.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')
    df.loc[:,['clc_code_1']]\
        .drop_duplicates()\
        .to_csv(f'{dossier_output}/typologies_surfaces_clc.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')


def analyser_csv_intersection(dossier_output):
    log.info('Analyse résultats intersection')
    df = pandas.read_csv(f'{dossier_output}/intersection.csv', sep=";", dtype='str')
    df['mos_niv4_1'] = df['mos_niv4_1'].str.replace('.0', '', regex=False)
    df['surface_ha'] = df['surface_ha'].astype(float)
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df = ajouter_regroupements(df, 'rpg')
    df = ajouter_regroupements(df, 'ocs')
    df = ajouter_regroupements(df, 'mos')
    df = ajouter_regroupements(df, 'clc')
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df = df.loc[:,
        ['categorie_rpg', 'categorie_ocs', 'categorie_mos', 'categorie_clc',
         'categorie2_rpg', 'categorie2_ocs', 'categorie2_mos', 'categorie2_clc',
         'surface_ha']
         ].groupby(
        ['categorie_rpg', 'categorie2_rpg',
         'categorie_ocs', 'categorie2_ocs',
         'categorie_mos', 'categorie2_mos',
         'categorie_clc','categorie2_clc'],
        as_index=False
        ).sum(
        ).sort_values(by=['surface_ha'], ascending=False)
    df2 = df.loc[:,
         ['categorie_rpg', 'categorie_ocs', 'categorie_mos', 'categorie_clc', 'surface_ha']
         ].groupby(
        ['categorie_rpg', 'categorie_ocs', 'categorie_mos', 'categorie_clc'],
        as_index=False
    ).sum(
    ).sort_values(by=['surface_ha'], ascending=False)
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df.to_csv(f'{dossier_output}/analyse_intersection.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')
    df2.to_csv(f'{dossier_output}/analyse_intersection2.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')


def analyser_surfaces(dossier_output, type):
    log.info('Analyse ' + type)
    gdf = geopandas.read_file(f'{dossier_output}/{type}_territoire.shp')
    gdf['surface_ha'] = gdf.geometry.area * 10 ** -4
    df = pandas.DataFrame(gdf)
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df = ajouter_regroupements(df, type)
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df = df.loc[:,
         ['categorie_' + type, 'categorie2_' + type, 'surface_ha']
         ].groupby(
        ['categorie_' + type, 'categorie2_' + type],
        as_index=False
    ).sum(
    ).sort_values(by=['categorie_' + type, 'categorie2_' + type], ascending=False)
    log.info('surface totale = ' + str(df['surface_ha'].sum()) + " ha")
    df.to_csv(f'{dossier_output}/analyse_' + type + '.csv', sep=";", encoding="utf-8", index=False, float_format='%.3f')


def traitement_nice():
    pandas.set_option('display.max_columns', None)
    log.info('TRAITEMENT NICE')
    DOSSIER_INPUT = Path('data/input/metropole_nca')
    DOSSIER_OUTPUT = Path('data/output/metropole_nca')
    DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR = DOSSIER_OUTPUT / 'shp_reduit_contour'
    DOSSIER_OUTPUT_SHP_INTERSECTION = DOSSIER_OUTPUT / 'shp_intersection'
    DOSSIER_OUTPUT_CSV_INTERSECTION = DOSSIER_OUTPUT / 'csv_intersection'

    # reinitialiser_dossier(DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR)
    #
    # generer_shp_contour(DOSSIER_INPUT / 'ocs/0.2_OCS2D_2017/OCSGE_NCA_2017_MAN.shp',
    #                     DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR / 'contour_territoire.shp')
    #
    # generer_shp_reduit_au_contour(DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR / 'contour_territoire.shp',
    #                               DOSSIER_INPUT,
    #                               DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR)

    #generer_shp_intersection(DOSSIER_OUTPUT_SHP_REDUIT_AU_CONTOUR, DOSSIER_OUTPUT_SHP_INTERSECTION)

    #generer_csv_intersection(DOSSIER_OUTPUT_SHP_INTERSECTION, DOSSIER_OUTPUT_CSV_INTERSECTION)

    exporter_typologies_surfaces_depuis_csv_intersection(DOSSIER_OUTPUT)
    analyser_csv_intersection(DOSSIER_OUTPUT)


def traitement_alpes_maritimes():
    log.info('TRAITEMENT ALPES MARITIMES')
    pandas.set_option('display.max_columns', None)
    DOSSIER_INPUT = Path('data/input/geodata')
    DOSSIER_OUTPUT = Path('data/output/alpes_maritimes')

    # reinitialiser_dossier(DOSSIER_OUTPUT)
    # generer_shp_intersection_avec_contour(DOSSIER_INPUT / 'contour_alpes_maritimes/contour.shp',
    #                                       DOSSIER_INPUT / 'rpg/RPG_2-0_SHP_LAMB93_R93-2017/PARCELLES_GRAPHIQUES.shp',
    #                                       DOSSIER_OUTPUT / 'rpg_territoire.shp')
    # generer_shp_intersection_avec_contour(DOSSIER_INPUT / 'contour_alpes_maritimes/contour.shp',
    #                                       '../../crater-data-sources/data_gouv_fr/corine_land_cover/2018/CLC_PNE_RG/CORINE_LAND_COVER_FRANCE_METROPOLITAINE_EPSG2154.gpkg',
    #                                       DOSSIER_OUTPUT / 'clc_territoire.shp')
    analyser_surfaces(DOSSIER_OUTPUT, 'rpg')
    analyser_surfaces(DOSSIER_OUTPUT, 'clc')


def calculs_sau_test_nice():
    pandas.set_option('display.max_columns', None)
    # calculer_sau('test_data/intersection_territoire.csv','test_data/synthese_sau.csv')

if __name__ == '__main__':
    # calculs_sau_test_nice()
    traitement_nice()
    traitement_alpes_maritimes()