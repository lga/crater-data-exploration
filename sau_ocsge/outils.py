import shutil
from pathlib import Path


def reinitialiser_dossier(nom_dossier: Path):
    if nom_dossier.exists():
        shutil.rmtree(nom_dossier)
    nom_dossier.mkdir(parents=True)