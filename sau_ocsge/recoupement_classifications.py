import geopandas
import pandas
from matplotlib import pyplot as plt

from sau_ocsge.logger import log

OCS_GE_SHP = 'data/input/metropole_nca/ocs/0.2_OCS2D_2017/OCSGE_NCA_2017_MAN.shp'
MOS_SHP = 'data/input/metropole_nca/mos/mos2014/ocsol_2014_nca.shp'
RPG_SHP = '../../crater-data-sources/ign/RPG_2-0_SHP_LAMB93_R93-2017/PARCELLES_GRAPHIQUES.shp'
OUTPUT_FOLDER = 'data/output/metropole_nca'

# OCS_GE_SHP = 'data/input/loire-atlantique/ocs/OCS_GE_1-1_2013_SHP_LAMB93_D044_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D44-2013/OCCUPATION_SOL.shp'
# RPG_SHP = '../../crater-data-sources/ign/RPG_2-0_SHP_LAMB93_R52-2017/PARCELLES_GRAPHIQUES.shp'
# OUTPUT_FOLDER = 'data/output/loire-atlantique'


def generer_contour_territoire():
    log.info("ETAPE generer_contour_territoire")
    gdf_ocs = geopandas.read_file(OCS_GE_SHP)
    polygon_metro = gdf_ocs.unary_union
    gdf_metro = geopandas.GeoSeries(polygon_metro, crs='EPSG:2154')
    gdf_metro = gdf_metro.buffer(0.1) #pour corriger mini trous dus au fait que les parcelles OCS ne sont pas forcéments parfaitement contiguës
    gdf_metro.to_file(f'{OUTPUT_FOLDER}/geodata/contour_territoire/contour_territoire.shp')
    gdf_metro.plot(cmap=None, figsize=(20, 20))
    plt.savefig(f'{OUTPUT_FOLDER}/images/ocs_contour_metropole.png')


def generer_rpg_territoire():
    log.info("ETAPE generer_rpg_territoire")
    gdf_metro = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/contour_territoire/contour_territoire.shp')
    log.info("Extraire le RPG limité au périmètre de la métropole de Nice")
    gdf_rpg = geopandas.read_file(RPG_SHP)
    log.info("Intersection RPG avec contour métropole")
    gdf_rpg_metro = geopandas.overlay(gdf_metro, gdf_rpg, how='intersection')
    gdf_rpg_metro.to_file(f'{OUTPUT_FOLDER}/geodata/rpg_territoire/rpg_territoire.shp')
    gdf_rpg_metro.plot(column='CODE_CULTU', cmap=None, legend=True, figsize=(20, 20))
    plt.savefig(f'{OUTPUT_FOLDER}/images/rpg_carte_par_code_culture.png')

# #Pas été testé, pb de librairies
# def generer_surfaces_iso_altitude():
#     log.info("Générer les surfaces de même altitude")
#     raster = gdal.Open(r'/Users/Benjamin/Desktop/crater/crater-data-exploration/sau_ocsge/input/geodata/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69_D006_2020-09-15/BDALTIV2/1_DONNEES_LIVRAISON_2021-02-00157/BDALTIV2_MNT_25M_ASC_LAMB93_IGN69_D006')
#     print(raster.RasterCount)
#     print(raster.RasterXSize)
#     print(raster.RasterYSize)
#
#     #open band and modify
#     band = raster.GetRasterBand(1)
#     data = band.ReadAsArray()
#     print(data)
#     data = 10 * data
#     print(data)
#     data[data > 2000] = -1
#     print(data)
#     band.WriteArray(data)
#
#     #polygonize
#     drv = ogr.GetDriverByName('ESRI Shapefile')
#     outfile = drv.CreateDataSource(r'output/geodata/polygonizedRaster.shp')
#     outlayer = outfile.CreateLayer('polygonized raster', srs=None)
#     newField = ogr.FieldDefn('DN', ogr.OFTReal)
#     outlayer.CreateField(newField)
#     gdal.Polygonize(band, None, outlayer, 0, [])
#     outfile = None




def calculer_sau_totale(gdf):
    gdf['surface'] = gdf.geometry.area * 10 ** -4
    somme_ha = gdf['surface'].sum()
    return somme_ha


def generer_analyses_rpg_ocs():
    log.info('ETAPE generer_analyses_rpg_ocs')
    gdf_ocs = initialiser_gdf_ocs()
    gdf_rpg = initialiser_gdf_rpg()
    gdf_intersection_rpg_ocs = initialiser_gdf_intersection()
    gdf_ocs_hors_rpg = initialiser_gdf_ocs_hors_rpg()

    surface_totale_rpg = gdf_rpg['surface_ha'].sum()
    log.info(f'Surface totale RPG : {surface_totale_rpg}')
    surface_totale_ocs = gdf_ocs['surface_ha'].sum()
    log.info(f'surface totale OCS : {surface_totale_ocs}')
    surface_totale_intersection = gdf_intersection_rpg_ocs['surface_ha_intersection'].sum()
    log.info(f'Surface totale RPG intersection avec OCS : {surface_totale_intersection}')
    log.info(f'OCS couvre le RPG à {surface_totale_intersection / surface_totale_rpg * 100} %')

    calculer_stats_rpg(gdf_rpg, gdf_intersection_rpg_ocs)
    calculer_stats_ocs(gdf_ocs, gdf_intersection_rpg_ocs, gdf_ocs_hors_rpg)


def calculer_stats_ocs(gdf_ocs, gdf_intersection_ocs_rpg, gdf_ocs_hors_rpg):
    df_ocs = pandas.DataFrame(
        gdf_ocs
            .loc[:,
        ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture',
         'surface_ha']]
            .groupby(
            ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture'],
            as_index=False)
            .sum()
    )
    df_intersection_ocs_rpg = pandas.DataFrame(
        gdf_intersection_ocs_rpg
            .loc[:,
        ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture',
         'surface_ha_intersection']]
            .groupby(
            ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture'],
            as_index=False)
            .sum()
    )
    df_ocs_hors_rpg = pandas.DataFrame(
        gdf_ocs_hors_rpg
            .loc[:,
        ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture',
         'surface_ha']]
            .groupby(
            ['code_usage_couverture', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture'],
            as_index=False)
            .sum()
    )

    df_ocs = df_ocs.rename(columns={'surface_ha': 'surface_totale'})
    df_intersection_ocs_rpg = df_intersection_ocs_rpg.rename(columns={'surface_ha_intersection': 'surface_intersection_rpg'})
    df_ocs_hors_rpg = df_ocs_hors_rpg.rename(columns={'surface_ha': 'surface_hors_rpg'})

    df_ocs = (df_ocs
              .merge(df_intersection_ocs_rpg, on='code_usage_couverture')
              .merge(df_ocs_hors_rpg, on='code_usage_couverture')
              )


    log.info(f'Sauvegarde fichier analyse_ocs.csv')
    df_ocs = (df_ocs
                        .loc[:, ['code_usage_couverture',
                                 'code_usage', 'description_usage',
                                 'code_couverture', 'description_couverture',
                                 'surface_intersection_rpg', 'surface_hors_rpg',
                                 'surface_totale']])
    df_ocs = df_ocs.sort_values(by=['surface_intersection_rpg'], ascending=False)
    df_ocs.to_csv(f'{OUTPUT_FOLDER}/analyse_ocs.csv', sep=";", encoding="utf-8", index=False,
                        float_format='%.2f')


def calculer_stats_rpg(gdf_rpg, gdf_intersection):
    df_stats_rpg = pandas.DataFrame(
        gdf_rpg
            .loc[:, ['code_culture_rpg', 'nom_culture_rpg', 'code_groupe_culture', 'nom_groupe_culture', 'surface_ha']]
            .rename(columns={'surface_ha': 'surface_ha_rpg_avant_intersection'})
            .groupby(['code_culture_rpg', 'nom_culture_rpg', 'code_groupe_culture', 'nom_groupe_culture'], as_index=False)
            .sum()
    )
    df_intersection = pandas.DataFrame(
        gdf_intersection
            .loc[:,
        ['code_culture_rpg', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture',
         'surface_ha_intersection']]
            .groupby(
            ['code_culture_rpg', 'code_usage', 'description_usage', 'code_couverture', 'description_couverture'],
            as_index=False)
            .sum()
    )
    df_stats_rpg = df_stats_rpg.merge(df_intersection, on='code_culture_rpg')
    df_stats_rpg['pourcent_surface_intersection_sur_rpg'] = 100 * df_stats_rpg['surface_ha_intersection'] / \
                                                            df_stats_rpg['surface_ha_rpg_avant_intersection']
    log.info(f'Sauvegarde fichier stats_rpg_sur_intersection')
    df_stats_rpg = (df_stats_rpg
                        .loc[:, ['code_groupe_culture', 'nom_groupe_culture',
                                 'code_culture_rpg', 'nom_culture_rpg',
                                 'code_usage', 'description_usage', 'code_couverture', 'description_couverture',
                                 'surface_ha_intersection',
                                 'surface_ha_rpg_avant_intersection',
                                 'pourcent_surface_intersection_sur_rpg'
                                 ]]
                    .rename(columns={
        'code_groupe_culture': 'code_groupe_culture_crater', 'nom_groupe_culture':'nom_groupe_culture_crater',
        'surface_ha_intersection': 'surface_intersection_ocs',
        'code_usage': 'code_usage_ocs', 'description_usage':'description_usage_ocs',
        'code_couverture':'code_couverture_ocs', 'description_couverture':'description_couverture_ocs'
    })
                    )
    df_stats_rpg = df_stats_rpg.sort_values(by=['surface_intersection_ocs'], ascending=False)
    df_stats_rpg.to_csv(f'{OUTPUT_FOLDER}/analyse_rpg.csv', sep=";", encoding="utf-8", index=False,
                        float_format='%.2f')


def initialiser_gdf_rpg():
    log.info('Chargement RPG')
    gdf_rpg = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/rpg_territoire/rpg_territoire.shp')
    gdf_rpg['surface_ha'] = gdf_rpg.geometry.area * 10 ** -4
    gdf_rpg = ajouter_colonnes_classif_rpg_crater(gdf_rpg)
    return gdf_rpg


def initialiser_gdf_ocs():
    log.info("Chargement couche OCS")
    gdf_ocs = (geopandas
               .read_file(OCS_GE_SHP)
               .rename(columns={'usage': 'code_usage',
                                'couverture': 'code_couverture',
                                # Utile pour certains fichiers OCS qui n'utilisent pas le meme nommage de colonnes
                                'CODE_US': 'code_usage',
                                'CODE_CS': 'code_couverture'}))
    gdf_ocs['surface_ha'] = gdf_ocs.geometry.area * 10 ** -4
    gdf_ocs = ajouter_colonnes_classif_ocs(gdf_ocs)
    return gdf_ocs


def initialiser_gdf_intersection():
    log.info('Chargement intersection')
    gdf_intersection = (geopandas
                        .read_file(f'{OUTPUT_FOLDER}/geodata/rpg_intersection_ocs/rpg_intersection_ocs.shp')
                        .rename(columns={'usage': 'code_usage',
                                         'couverture': 'code_couverture',
                                         # Utile pour certains fichiers OCS qui n'utilisent pas le meme nommage de colonnes
                                            'CODE_US': 'code_usage',
                                            'CODE_CS': 'code_couverture'
                                         })
                        )
    gdf_intersection = ajouter_colonnes_classif_ocs(gdf_intersection)
    gdf_intersection = ajouter_colonnes_classif_rpg_crater(gdf_intersection)
    gdf_intersection['surface_ha_intersection'] = gdf_intersection.geometry.area * 10 ** -4
    return gdf_intersection


def initialiser_gdf_ocs_hors_rpg():
    log.info('Chargement shp ocs hors rpg')
    gdf_ocs_hors_rpg = (geopandas
                        .read_file(f'{OUTPUT_FOLDER}/geodata/ocs_hors_rpg/ocs_hors_rpg.shp')
                        .rename(columns={'usage': 'code_usage',
                                         'couverture': 'code_couverture',
                                         # Utile pour certains fichiers OCS qui n'utilisent pas le meme nommage de colonnes
                                            'CODE_US': 'code_usage',
                                            'CODE_CS': 'code_couverture'
                                         })
                        )
    gdf_ocs_hors_rpg = ajouter_colonnes_classif_ocs(gdf_ocs_hors_rpg)
    gdf_ocs_hors_rpg['surface_ha'] = gdf_ocs_hors_rpg.geometry.area * 10 ** -4
    return gdf_ocs_hors_rpg


def ajouter_colonnes_classif_ocs(gdf):
    gdf['code_usage_couverture'] = gdf['code_usage'] + "-" + gdf['code_couverture']
    df_description_couverture = pandas.read_csv("data/input/classifications/classification_ocs_couverture_sol.csv", sep=";")
    gdf = gdf.merge(df_description_couverture,
                    on="code_couverture")
    df_description_usage = pandas.read_csv("data/input/classifications/classification_ocs_usage_sol.csv", sep=";")
    gdf = gdf.merge(df_description_usage, on="code_usage")
    return gdf


def ajouter_colonnes_classif_rpg_crater(gdf_rpg):
    correspondance_codes_rpg_vers_codes_cultures = pandas.read_csv(
        "../../crater-data-sources/crater/codes_rpg_vers_codes_cultures.csv", sep=";", dtype='str')
    groupes_cultures_crater = pandas.read_csv("../../crater-data-sources/crater/cultures_et_groupes_cultures.csv",
                                              sep=";", dtype='str')
    correspondance_codes_rpg_vers_codes_cultures = correspondance_codes_rpg_vers_codes_cultures.merge(
        groupes_cultures_crater, left_on="code_culture_crater",
        right_on="code_culture")
    gdf_rpg = gdf_rpg.merge(correspondance_codes_rpg_vers_codes_cultures, left_on="CODE_CULTU",
                            right_on="code_culture_rpg")
    return gdf_rpg



#
# Calcul diff générique entre RPG et une couche témoin
#


def generer_shp_intersection_et_difference_rpg_avec_temoin(nom_couche_temoin, fichier_couche_temoin):
    log.info(f'ETAPE generer_shp_intersection_et_difference_rpg_avec_temoin : {nom_couche_temoin}')
    log.info(f'Chargement couche témoin {nom_couche_temoin}')
    gdf_ocs = geopandas.read_file(fichier_couche_temoin)

    log.info('Chargement RPG')
    gdf_rpg = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/rpg_territoire/rpg_territoire.shp')

    log.info(f'Calcul RPG intersecté avec {nom_couche_temoin}')
    gdf_intersection = geopandas.overlay(gdf_ocs, gdf_rpg, how='intersection')
    gdf_intersection.to_file(f'{OUTPUT_FOLDER}/geodata/rpg_intersection_{nom_couche_temoin}/rpg_intersection_{nom_couche_temoin}.shp')
    log.info(f'Calcul {nom_couche_temoin} hors rpg')
    gdf_intersection = geopandas.overlay(gdf_ocs, gdf_rpg, how='difference')
    gdf_intersection.to_file(f'{OUTPUT_FOLDER}/geodata/{nom_couche_temoin}_hors_rpg/{nom_couche_temoin}_hors_rpg.shp')



def generer_analyses_rpg_versus_temoin(nom_couche_temoin, shp_couche_temoin, shp_couche_intersection, shp_couche_temoin_hors_rpg, colonnes_gdf_temoin, colonne_jointure):
    log.info('ETAPE generer_analyses_rpg_versus_temoin')
    log.info(f'Chargement couche temoin {nom_couche_temoin}')
    gdf_temoin = (geopandas.read_file(shp_couche_temoin) )
    gdf_temoin['surface_ha'] = gdf_temoin.geometry.area * 10 ** -4

    gdf_rpg = initialiser_gdf_rpg()

    log.info('Chargement intersection')
    gdf_intersection = (geopandas
                        .read_file(shp_couche_intersection)
                        )
    gdf_intersection = ajouter_colonnes_classif_rpg_crater(gdf_intersection)
    gdf_intersection['surface_ha_intersection'] = gdf_intersection.geometry.area * 10 ** -4

    log.info('Chargement shp temoin hors rpg')
    gdf_temoin_hors_rpg = (geopandas
           .read_file(shp_couche_temoin_hors_rpg)
           )
    gdf_temoin_hors_rpg['surface_ha'] = gdf_temoin_hors_rpg.geometry.area * 10 ** -4

    surface_totale_rpg = gdf_rpg['surface_ha'].sum()
    log.info(f'Surface totale RPG : {surface_totale_rpg}')
    surface_totale_temoin = gdf_temoin['surface_ha'].sum()
    log.info(f'surface totale {nom_couche_temoin} : {surface_totale_temoin}')
    surface_totale_intersection = gdf_intersection['surface_ha_intersection'].sum()
    log.info(f'Surface totale RPG intersection avec {nom_couche_temoin} : {surface_totale_intersection}')
    log.info(f'{nom_couche_temoin} couvre le RPG à {surface_totale_intersection / surface_totale_rpg * 100} %')

    calculer_stats_rpg_temoin(nom_couche_temoin, gdf_rpg, gdf_intersection, colonnes_gdf_temoin)
    calculer_stats_temoin(nom_couche_temoin, gdf_temoin, gdf_intersection, gdf_temoin_hors_rpg, colonnes_gdf_temoin, colonne_jointure)


def calculer_stats_rpg_temoin(nom_gdf_temoin, gdf_rpg, gdf_intersection, colonnes_gdf_temoin):
    df_stats_rpg = pandas.DataFrame(
        gdf_rpg
            .loc[:, ['code_culture_rpg', 'nom_culture_rpg', 'code_groupe_culture', 'nom_groupe_culture', 'surface_ha']]
            .rename(columns={'surface_ha': 'surface_ha_rpg_avant_intersection'})
            .groupby(['code_culture_rpg', 'nom_culture_rpg', 'code_groupe_culture', 'nom_groupe_culture'], as_index=False)
            .sum()
    )
    df_intersection = pandas.DataFrame(
        gdf_intersection
            .loc[:,
        ['code_culture_rpg'] + colonnes_gdf_temoin + ['surface_ha_intersection']]
            .groupby(
            ['code_culture_rpg'] + colonnes_gdf_temoin,
            as_index=False)
            .sum()
    )
    df_stats_rpg = df_stats_rpg.merge(df_intersection, on='code_culture_rpg')
    df_stats_rpg['pourcent_surface_intersection_sur_rpg'] = 100 * df_stats_rpg['surface_ha_intersection'] / \
                                                            df_stats_rpg['surface_ha_rpg_avant_intersection']
    log.info(f'Sauvegarde fichier analyse_rpg_{nom_gdf_temoin}.csv')
    df_stats_rpg = (df_stats_rpg
                        .loc[:, ['code_groupe_culture', 'nom_groupe_culture',
                                 'code_culture_rpg', 'nom_culture_rpg']
                                 + colonnes_gdf_temoin +
                                 ['surface_ha_intersection',
                                 'surface_ha_rpg_avant_intersection',
                                 'pourcent_surface_intersection_sur_rpg'
                                 ]]
                    .rename(columns={
        'code_groupe_culture': 'code_groupe_culture_crater', 'nom_groupe_culture':'nom_groupe_culture_crater'
    })
                    )
    df_stats_rpg = df_stats_rpg.sort_values(by=['surface_ha_intersection'], ascending=False)
    df_stats_rpg.to_csv(f'{OUTPUT_FOLDER}/analyse_rpg_{nom_gdf_temoin}.csv', sep=";", encoding="utf-8", index=False,
                        float_format='%.2f')


def calculer_stats_temoin(nom_gdf_temoin, gdf_temoin, gdf_intersection, gdf_temoin_hors_rpg, colonnes_gdf_temoin, colonne_jointure):
    df = pandas.DataFrame(
        gdf_temoin
            .loc[:, colonnes_gdf_temoin + ['surface_ha']]
            .groupby(colonnes_gdf_temoin,
            as_index=False)
            .sum()
    )
    df_intersection_ocs_rpg = pandas.DataFrame(
        gdf_intersection
            .loc[:, colonnes_gdf_temoin + ['surface_ha_intersection']]
            .groupby(colonnes_gdf_temoin, as_index=False)
            .sum()
    )
    df_ocs_hors_rpg = pandas.DataFrame(
        gdf_temoin_hors_rpg
            .loc[:,  colonnes_gdf_temoin + ['surface_ha']]
            .groupby(colonnes_gdf_temoin, as_index=False)
            .sum()
    )

    df = df.rename(columns={'surface_ha': 'surface_totale'})
    df_intersection_ocs_rpg = df_intersection_ocs_rpg.rename(columns={'surface_ha_intersection': 'surface_intersection_rpg'})
    df_ocs_hors_rpg = df_ocs_hors_rpg.rename(columns={'surface_ha': 'surface_hors_rpg'})

    df = (df
              .merge(df_intersection_ocs_rpg, on=colonne_jointure)
              .merge(df_ocs_hors_rpg, on=colonne_jointure)
              )


    log.info(f'Sauvegarde fichier analyse_{nom_gdf_temoin}.csv')
    df = (df.loc[:, colonnes_gdf_temoin + ['surface_intersection_rpg', 'surface_hors_rpg', 'surface_totale']])
    df = df.sort_values(by=['surface_intersection_rpg'], ascending=False)
    df.to_csv(f'{OUTPUT_FOLDER}/analyse_{nom_gdf_temoin}.csv', sep=";", encoding="utf-8", index=False,
                        float_format='%.2f')

def calculer_stats_recoupement_alti_rpg():
    log.info("ETAPE calculer_stats_recoupement_alti_rpg")
    #  ne marche pas pour l'instant => l'intersection avec des lignes de niveau n'est pas la bonne solution
    log.info("Chargement couche alti")
    gdf_alti = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/courbes_niveau_region_paca/region_paca_courbes_niveau_200m.shp')
    # gdf_alti_sup_1500 = gdf_alti.loc[gdf_alti['altitude'] >= 1500]

    gdf_rpg = geopandas.read_file(f'{OUTPUT_FOLDER}/geodata/rpg_territoire/rpg_territoire.shp')
    log.info(f'SAU totale RPG : {calculer_sau_totale(gdf_rpg)}')

    # TODO ajouter jointure avec groupes cultures crater

    gdf_rpg_par_alti = geopandas.overlay(gdf_rpg, gdf_alti, how='intersection')
    gdf_rpg_par_alti['surface'] = gdf_rpg_par_alti.geometry.area * 10 ** -4
    log.info(f'SAU totale RPG sous 1500m altitude : {calculer_sau_totale(gdf_rpg_par_alti.query("altitude < 1500"))}')
    df_rpg_alti = pandas.DataFrame(gdf_rpg_par_alti)
    df_rpg_alti = (df_rpg_alti.loc[:, ['CODE_CULTU', 'altitude', 'surface']]
                    .rename(columns={'CODE_CULTU':'code_culture_rpg', 'surface': 'surface_ha'})
                    .groupby(['code_culture_rpg', 'altitude'], as_index=False)
                    .sum())

    df_rpg_alti = df_rpg_alti.sort_values(by=['surface_ha'], ascending=False)
    df_rpg_alti.to_csv(f'{OUTPUT_FOLDER}/analyse_rpg_altitude.csv', sep=";", encoding="utf-8", index=False,
                        float_format='%.2f')



def lancer_traitement():
    pandas.set_option('display.max_columns', None)
    # A lancer une fois pour générer les différents shp intermédiaires
    # generer_contour_territoire()
    # generer_surfaces_iso_altitude()
    # generer_rpg_territoire()
    # generer_shp_intersection_et_difference_rpg('ocs', OCS_GE_SHP)

    # generer_analyses_rpg_ocs()
    # calculer_stats_recoupement_alti_rpg()

    #     RPG / MOS
    generer_shp_intersection_et_difference_rpg_avec_temoin('mos', MOS_SHP)
    generer_analyses_rpg_versus_temoin('mos',
                                       MOS_SHP,
                                       f'{OUTPUT_FOLDER}/geodata/rpg_intersection_mos/rpg_intersection_mos.shp',
                                       f'{OUTPUT_FOLDER}/geodata/mos_hors_rpg/mos_hors_rpg.shp',
                                       ['niv4_14', 'intit4_14'], 'niv4_14')


if __name__ == '__main__':
    pandas.set_option('display.max_columns', None)
    # lancer_traitement()
