import unittest
from pathlib import Path

import pandas

from sau_ocsge.analyses_spatiales import generer_shp_contour, generer_shp_intersection_avec_contour, \
    calculer_intersection, exporter_df
from sau_ocsge.main import generer_shp_reduit_au_contour, generer_shp_intersection, generer_csv_intersection
from sau_ocsge.outils import reinitialiser_dossier
from sau_ocsge.recoupement_classifications import OCS_GE_SHP

DOSSIER_DATA_INPUT = Path('./data/input/metropole_nca')
DOSSIER_TEST_DATA_INPUT = Path('./test_data/input')
DOSSIER_TEST_DATA_OUTPUT = Path('./test_data/output')
DOSSIER_TEST_DATA_OUTPUT_SHP_REDUIT = DOSSIER_TEST_DATA_OUTPUT / 'shp_reduit_contour'
DOSSIER_TEST_DATA_OUTPUT_SHP_INTERSECTION = DOSSIER_TEST_DATA_OUTPUT / 'shp_intersection'
DOSSIER_TEST_DATA_OUTPUT_CSV_INTERSECTION = DOSSIER_TEST_DATA_OUTPUT / 'csv_intersection'


class TestAnalyseSAU(unittest.TestCase):

    # @classmethod
    # def setUpClass(cls):
    #     None
    #     # reinitialiser_dossier(DOSSIER_TEST_DATA_OUTPUT)


    def test_generer_shp_reduit_au_contour(self):
        pandas.set_option('display.max_columns', None)
        generer_shp_reduit_au_contour(DOSSIER_TEST_DATA_INPUT / 'contour_rectangle.shp',
                                      DOSSIER_DATA_INPUT,
                                      DOSSIER_TEST_DATA_OUTPUT_SHP_REDUIT)

    def test_calculer_intersection(self):
        pandas.set_option('display.max_columns', None)
        reinitialiser_dossier(DOSSIER_TEST_DATA_OUTPUT_SHP_INTERSECTION)

        generer_shp_intersection(DOSSIER_TEST_DATA_OUTPUT_SHP_REDUIT, DOSSIER_TEST_DATA_OUTPUT_SHP_INTERSECTION)


    def test_generer_csv_intersection(self):
        pandas.set_option('display.max_columns', None)
        generer_csv_intersection(DOSSIER_TEST_DATA_OUTPUT_SHP_INTERSECTION, DOSSIER_TEST_DATA_OUTPUT_CSV_INTERSECTION)

if __name__ == '__main__':
    unittest.main()